﻿<%@ Page Title="Add Member" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyDownlineRegistration.aspx.cs" Inherits="mlm_web.MyDownlineRegistration" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Add Memmber
       
                    <small>Register</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">My Business</a></li>
                    <li class="active">Add Member</li>
                </ol>
            </section>
            <section class="content">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Package And Sponsor Details</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Select Package</label>
                                <asp:DropDownList ID="ddlPackage" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlPackage_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-md-4">
                                <label>EPIN Number</label>
                                <asp:TextBox ID="txtEPINNumber" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Sponsor User Id</label>
                                <asp:TextBox ID="txtSponsorUser" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Personal Information</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Contact Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>

                            </div>

                            <div class="form-group col-md-6">
                                <label>Mobile Number</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-mobile"></i>
                                    </div>
                                    <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control" data-inputmask='"mask": "9999999999"' data-mask="" placeholder="10-digit mobile number without country code"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Email Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-at"></i>
                                    </div>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-default">Cancel</button>
                <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="btn btn-info pull-right" OnClick="btnRegister_Click" />

            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
