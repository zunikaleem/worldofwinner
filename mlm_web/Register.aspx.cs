﻿using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class Register : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindPackage();
                lblCompanyName.Text = CurrentDomain.Session.CompanyName;
            }
        }

        private void BindPackage()
        {
            try
            {
                Utility.Binder.BindDropDownList(ddlPackage, BusinessLogic.Package.getPackages(), "PackageID", "PackageName", "", "--Select--");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Signin.aspx");
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtPassword.Text.Equals(txtConfirmPassword.Text))
                {
                    throw new Exception("Password and confirm password does not matched.");
                }

                BusinessLogic.Customer customer = new BusinessLogic.Customer();
                customer.SystemUser = new BusinessLogic.SystemUser();

                customer.SystemUser.ContactName = txtCustomerName.Text;

                customer.EmailID = txtEmail.Text;

                customer.SystemUser.MobileNumber = txtMobile.Text;

                customer.SystemUser.UserID = customer.Register(Convert.ToInt32(ddlPackage.SelectedValue), txtSponsorID.Text, txtEPINNumber.Text, txtPassword.Text);

                if (!customer.SystemUser.UserID.Equals(string.Empty))
                {
                    CurrentDomain.Session.RegisteredCustomer = customer;
                    CurrentDomain.Session.AcknowledgementType = "Registration";
                    Response.Redirect("Acknowledgement.aspx");
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show( ex.Message);
            }
        }
    }
}