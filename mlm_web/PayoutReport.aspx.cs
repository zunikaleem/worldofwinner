﻿using System;
using System.Data;
using System.Web.UI;
namespace mlm_web
{
    public partial class PayoutReport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                //DateTime payoutFromDate= DateTime.ParseExact(txtFromDate.Text, "yyyy-mm-dd", CultureInfo.CurrentCulture);
                //DateTime payoutToDate = DateTime.ParseExact(txtToDate.Text, "yyyy-mm-dd", CultureInfo.CurrentCulture);

                DateTime payoutFromDate = Convert.ToDateTime(txtFromDate.Text);
                DateTime payoutToDate = Convert.ToDateTime(txtToDate.Text);

                DataTable payoutList = BusinessLogic.Payout.GetPayouts(payoutFromDate, payoutToDate);

                GridView1.DataSource = payoutList;
                GridView1.DataBind();
                divPayouts.Visible = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
    }
}