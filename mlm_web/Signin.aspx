﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Signin.aspx.cs" Inherits="mlm_web.Signin" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>
        <asp:Literal runat="server" ID="ltCompanyName" ></asp:Literal>| Log in
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="fonts/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="fonts/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="Signin.aspx">
                <b>
                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                </b>
            </a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form id="form1" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server"></asp:UpdateProgress>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                            <ProgressTemplate>
                                <al:Loader ID="Loader1" runat="server" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <div class="form-group" id="form_userid">
                            <input type="text" class="form-control" id="txtUserId" runat="server" placeholder="Enter user id" />
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group" id="form_password">
                            <input type="password" class="form-control" placeholder="Password" id="txtPassword" runat="server" />
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="checkbox icheck">
                                    <label>
                                        <input type="checkbox" />
                                        Remember Me
                                    </label>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <asp:Button ID="btnLogin" ClientIDMode="Static" class="btn btn-primary btn-block btn-flat" runat="server" Text="Sign In"  OnClick="btnLogin_Click" />
                            </div>
                            <!-- /.col -->
                        </div>

                        <!-- Modal Show Message -->
                        <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header" id="messageheader">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;</button>
                                        <h4 class="modal-title" id="modaltitle">Message</h4>
                                    </div>
                                    <div class="modal-body" id="messagebody">
                                        <div class="col-lg-12 form-horizontal" style="overflow: auto;">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                            Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /End Modal Show Message-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </form>
            <a href="#">I forgot my password</a><br />
            <br />
            <a href="register.aspx" class="text-center">Register a new membership</a>
        </div>
    </div>




    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });

                $("#btnLogin").click(function () {
                    return Validate();
                });
            });
        });
    </script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });

            $("#btnLogin").click(function () {
                return Validate();
            });
        });

        function Validate() {
            var retVal = true;
            if ($('#txtUserId').val().length == 0) {
                $('#form_userid').addClass("form-group has-error");
                retVal = false;
            }
            else {
                $('#form_userid').addClass("form-group has-success");
            }

            if ($('#txtPassword').val().length == 0) {
                $('#form_password').addClass("form-group has-error");
                retVal = false;
            }
            else {
                $('#form_password').addClass("form-group has-success");
            }

            return retVal;


        }
    </script>
</body>
</html>
