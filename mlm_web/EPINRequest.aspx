﻿<%@ Page Title="EPIN Request" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EPINRequest.aspx.cs" Inherits="mlm_web.EPINRequest" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>EPIN 
       
                    <small>Pending List</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">My Business</a></li>
                    <li><a href="#">EPIN Wallet</a></li>
                    <li class="active">EPIN Request</li>
                </ol>
            </section>
            <section class="content">


                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">EPIN Request List</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" 
                                runat="server" AutoGenerateColumns="False" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating"
                                GridLines="None" OnRowDataBound="GridView1_RowDataBound">
                                <Columns>
                                    <asp:CommandField ButtonType="Image" CancelImageUrl="icons/cancel.png" EditImageUrl="icons/edit.png"
                                        ShowEditButton="True" UpdateImageUrl="icons/update.png">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>

                                    <asp:TemplateField HeaderText="EPINRequestID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgEPINRequestID" runat="server" Text='<%# Bind("EPINRequestID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddl1" runat="server" Enabled="false"></asp:DropDownList>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlgStatus" runat="server" CssClass="form-control" required>
                                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Transfer" Value="Transfer"></asp:ListItem>
                                                <asp:ListItem Text="Reject" Value="Reject"></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Admin Comment">
                                        <ItemTemplate >
                                            <asp:TextBox ID="txtAdminComment" runat="server" Enabled="false" CssClass="form-control" ></asp:TextBox>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgAdminComment" runat="server" CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transfer Quantity">
                                         <ItemTemplate >
                                            <asp:TextBox ID="txtQuantity" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgTransferredQuantity" runat="server" Text='<%# Bind("Quantity") %>' CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request By">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CustomerUserID") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CustomerUserID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Name">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("ContactName") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("ContactName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Number">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ContactNumber") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ContactNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Package">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("PackageName") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("PackageName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Date">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("RequestDate", "{0:dd/MM/yy hh:mm:ss}") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("RequestDate", "{0:dd/MM/yy hh:mm:ss}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="User Comment">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5c" runat="server" Text='<%# Bind("UserComment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Transaction Receipt">

                                        <ItemTemplate>
                                            <asp:LinkButton ID="hlbTransactionReceiipt" runat="server" Text='<%# Bind("TransactionPath") %>' OnClick="hlbTransactionReceiipt_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>!! No Pending Request !!</EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>


                </div>

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Processed List</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView2" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" 
                                runat="server" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="GridView2_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ProcessDate" HeaderText="Action Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}" />
                                    <asp:BoundField DataField="TransferredQuantity" HeaderText="Transferred Quantity" />
                                    <asp:BoundField DataField="AdminComment" HeaderText="Admin Comment" />
                                    <asp:BoundField DataField="CustomerUserID" HeaderText="Request By" />
                                    <asp:BoundField DataField="ContactName" HeaderText="Contact Name" />
                                    <asp:BoundField DataField="ContactNumber" HeaderText="Contact Number" />
                                    <asp:BoundField DataField="PackageName" HeaderText="Package" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                                    
                                    <asp:BoundField DataField="RequestDate" HeaderText="Request Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}" />
                                    <asp:BoundField DataField="UserComment" HeaderText="User Comment" />
                                     <asp:TemplateField HeaderText="Transaction Receipt">

                                        <ItemTemplate>
                                            <asp:LinkButton ID="hlbTransactionReceiiptf" runat="server" Text='<%# Bind("TransactionPath") %>' OnClick="hlbTransactionReceiipt_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    !! No request available at this moment. !!
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Modal Show Message -->
            <div class="modal fade" id="modalMessageReceipt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="modal-title">Receipt</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-lg-12 form-horizontal" style="overflow: auto;">
                                <asp:Image ID="imgReceipt" runat="server" Width="400" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /End Modal Show Message-->
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
