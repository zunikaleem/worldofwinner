﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class LevelwisePayoutReport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            DataTable payoutList = BusinessLogic.Payout.GetLevelwisePayouts();

            GridView1.DataSource = payoutList;
            GridView1.DataBind();
            divPayouts.Visible = true;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    if (cell.Text == "&nbsp;")
                    {
                        cell.Text = "0";
                    }
                }

                float totalEarn = 0;
                int totalCells = e.Row.Cells.Count;
                for (int i = 1; i < e.Row.Cells.Count - 3; i++)
                {
                    totalEarn += Convert.ToSingle(e.Row.Cells[i].Text);
                }

                e.Row.Cells[totalCells - 3].Text = totalEarn.ToString();

                string userID = e.Row.Cells[0].Text;

                BusinessLogic.Customer customer = BusinessLogic.Customer.GetCustomer(BusinessLogic.SystemUser.GetSystemUser(userID));
                float? totalPaid = customer.SystemUser.GetAccounts().Sum(h => h.DebitAmount);

                e.Row.Cells[totalCells - 2].Text = totalPaid.ToString();

                e.Row.Cells[totalCells - 1].Text = Math.Round((totalEarn - totalPaid ?? default(float)), 2).ToString();
            }
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            // Intitialize TableCell list
            List<TableCell> columns = new List<TableCell>();
            foreach (DataControlField column in GridView1.Columns)
            {
                //Get the first Cell /Column
                TableCell cell = row.Cells[0];
                // Then Remove it after
                row.Cells.Remove(cell);
                //And Add it to the List Collections
                columns.Add(cell);
            }
            // Add cells
            row.Cells.AddRange(columns.ToArray());
        }
    }
}