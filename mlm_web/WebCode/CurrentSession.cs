﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using BusinessLogic;

namespace CurrentDomain
{
    public class Session
    {
        public static SystemUser CurrentSystemUser
        {
            get
            {
                if (HttpContext.Current.Session["CurrentSystemUser"] == null) return new SystemUser();

                else return HttpContext.Current.Session["CurrentSystemUser"] as SystemUser;
            }
            set
            {
                HttpContext.Current.Session["CurrentSystemUser"] = value;
            }
        }
        public static Customer CurrentCustomer
        {
            get
            {
                if (HttpContext.Current.Session["CurrentCustomer"] == null) return new Customer();

                else return HttpContext.Current.Session["CurrentCustomer"] as Customer;
            }
            set
            {
                HttpContext.Current.Session["CurrentCustomer"] = value;
            }
        }
        public static Customer RegisteredCustomer
        {
            get
            {
                if (HttpContext.Current.Session["RegisteredCustomer"] == null) return new Customer();

                else return HttpContext.Current.Session["RegisteredCustomer"] as Customer;
            }
            set
            {
                HttpContext.Current.Session["RegisteredCustomer"] = value;
            }
        }
        public static string AcknowledgementType
        {
            get
            {
                if (HttpContext.Current.Session["AcknowledgementType"] == null) return string.Empty;

                else return HttpContext.Current.Session["AcknowledgementType"] as string;
            }
            set
            {
                HttpContext.Current.Session["AcknowledgementType"] = value;
            }
        }
        public static bool TermAndCondition
        {
            get
            {
                if (HttpContext.Current.Session["TermAndCondition"] == null) return false;

                else return Convert.ToBoolean(HttpContext.Current.Session["TermAndCondition"]);
            }
            set
            {
                HttpContext.Current.Session["TermAndCondition"] = value;
            }
        }

        public static Customer CurrentAccountCustomer
        {
            get
            {
                if (HttpContext.Current.Session["CurrentAccountCustomer"] == null) return new Customer();

                else return HttpContext.Current.Session["CurrentAccountCustomer"] as Customer;
            }
            set
            {
                HttpContext.Current.Session["CurrentAccountCustomer"] = value;
            }
        }

        public static SystemUser CurrentAccountSystemUser
        {
            get
            {
                if (HttpContext.Current.Session["CurrentAccountSystemUser"] == null) return new SystemUser();

                else return HttpContext.Current.Session["CurrentAccountSystemUser"] as SystemUser;
            }
            set
            {
                HttpContext.Current.Session["CurrentAccountSystemUser"] = value;
            }
        }

        public static SystemUser CurrentAccountSytsemUser
        {
            get
            {
                if (HttpContext.Current.Session["CurrentAccountSytsemUser"] == null) return new SystemUser();

                else return HttpContext.Current.Session["CurrentAccountSytsemUser"] as SystemUser;
            }
            set
            {
                HttpContext.Current.Session["CurrentAccountSytsemUser"] = value;
            }
        }

        public static List<Account> MyAccount 
        {
            get
            {
                if (HttpContext.Current.Session["MyAccount"] == null) return new List<Account>();

                else return HttpContext.Current.Session["MyAccount"] as List<Account>;
            }
            set
            {
                HttpContext.Current.Session["MyAccount"] = value;
            }
        }

        public static Customer CurrentEditCustomer
        {
            get
            {
                if (HttpContext.Current.Session["CurrentEditCustomer"] == null) return new Customer();

                else return HttpContext.Current.Session["CurrentEditCustomer"] as Customer;
            }
            set
            {
                HttpContext.Current.Session["CurrentEditCustomer"] = value;
            }
        }

        public static string CompanyName
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session["CompanyName"]);
            }
            set
            {
                HttpContext.Current.Session["CompanyName"] = value;
            }
        }

        public static float? NetBalance
        {
            get
            {
                return Convert.ToSingle(HttpContext.Current.Session["NetBalance"]);
            }
            set
            {
                HttpContext.Current.Session["NetBalance"] = value;
            }
        }

        public static DataTable GSTReport
        {
            get
            {
                if (HttpContext.Current.Session["GSTReport"] == null) return new DataTable();

                else return HttpContext.Current.Session["GSTReport"] as DataTable;
            }
            set
            {
                HttpContext.Current.Session["GSTReport"] = value;
            }
        }

        public static DataTable TDSReport
        {
            get
            {
                if (HttpContext.Current.Session["TDSReport"] == null) return new DataTable();

                else return HttpContext.Current.Session["TDSReport"] as DataTable;
            }
            set
            {
                HttpContext.Current.Session["TDSReport"] = value;
            }
        }
    }
}