﻿using System.Web.UI;
using System;

public class MessageBox
{

    private MessageBox() { }

    public static void Show(UpdatePanel updatePanel, string message, MessageType messageType)
    {
        try
        {
            message = message.Replace("\n", "\\n");
            message = message.Replace("\r", "\\r");
            message = message.Replace("\"", "'");
            message = message.Replace("'", "");

            string script = string.Empty;



            switch (messageType)
            {
                case MessageType.Success:
                    script = "javascript:$('#modaltitle').text('Success');$('#messagebody').text('" + message + "');$('#messageheader').css({'background-color':'#46BE8A','color':'white','border':'1px','border-style':'solid'});$('#modalMessage').modal('show');";
                    break;
                case MessageType.Error:
                    script = "javascript:$('#modaltitle').text('Error');$('#messagebody').text('" + message + "');$('#messageheader').css({'background-color':'#FB434A','color':'white','border':'1px','border-style':'solid'});$('#modalMessage').modal('show');";
                    break;
                case MessageType.Info:
                    script = "javascript:$('#modaltitle').text('Information');$('#messagebody').text('" + message + "');$('#messageheader').css({'background-color':'#F39834','color':'white','border':'1px','border-style':'solid'});$('#modalMessage').modal('show');";
                    break;
                default:
                    script = "javascript:$('#modaltitle').text('Message');$('#messagebody').text('" + message + "');$('#messagebody').modal('show');";
                    break;
            }
            ScriptManager.RegisterClientScriptBlock(updatePanel, updatePanel.GetType(), "message", script, true);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            LogMessageToDB(message, messageType);
        }
    }

    private static void LogMessageToDB(string message, MessageType messageType)
    {
        try
        {
            Utility.Logger.Priority priority;
            switch (messageType)
            {
                case MessageType.Error:
                    priority = Utility.Logger.Priority.ERROR;
                    break;
                case MessageType.Info:
                    priority = Utility.Logger.Priority.INFO;
                    break;
                case MessageType.Success:
                    priority = Utility.Logger.Priority.TRACE;
                    break;
            }
            //Utility.Logger.putMessage(priority, message);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Show(Page page, string message, MessageType messageType)
    {
        try
        {
            message = message.Replace("\n", "\\n");
            message = message.Replace("\r", "\\r");
            message = message.Replace("\"", "'");
            message = message.Replace("'", "");

            string script = string.Empty;

            switch (messageType)
            {
                case MessageType.Success:
                    script = "javascript:$('#modaltitle').text('Success');$('#messagebody').text('" + message + "');$('#messageheader').css({'background-color':'#46BE8A','color':'white','border':'1px','border-style':'solid'});$('#modalMessage').modal('show');";
                    break;
                case MessageType.Error:
                    script = "javascript:$('#modaltitle').text('Error');$('#messagebody').text('" + message + "');$('#messageheader').css({'background-color':'#FB434A','color':'white','border':'1px','border-style':'solid'});$('#modalMessage').modal('show');";
                    break;
                case MessageType.Info:
                    script = "javascript:$('#modaltitle').text('Information');$('#messagebody').text('" + message + "');$('#messageheader').css({'background-color':'#F39834','color':'white','border':'1px','border-style':'solid'});$('#modalMessage').modal('show');";
                    break;
                default:
                    script = "javascript:$('#modaltitle').text('Message');$('#messagebody').text('" + message + "');$('#modalMessage').modal('show');";
                    break;
            }
            // ScriptManager.RegisterClientScriptBlock(page.GetType(), "message", "$('#modalMessage').modal('show');", true);
            page.ClientScript.RegisterStartupScript(page.GetType(), "hwa", "alert('hi')", true);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}
public enum MessageType
{
    Error,
    Success,
    Info
}

