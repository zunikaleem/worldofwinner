﻿using System;


/// <summary>
/// Summary description for Binder
/// </summary>
namespace Utility
{

    public class Binder
    {
        public static void BindDropDownList(System.Web.UI.WebControls.DropDownList dropDownList, Object list, string valueField, string textField, string initialValue, string initialText)
        {
            try
            {
                dropDownList.DataSource = list;
                dropDownList.DataTextField = textField;
                dropDownList.DataValueField = valueField;
                dropDownList.DataBind();

                //if (initialText.Trim() != string.Empty && initialValue.Trim() != string.Empty)
                if (initialText.Trim() != string.Empty)
                {
                    dropDownList.Items.Insert(0, initialText);
                    dropDownList.Items[0].Value = initialValue;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    //public class Converter
    //{
    //    private static PDFConverter.RemoteConverter converter;
    //    internal static int WordToPDF(string wordFile)
    //    {
    //        try
    //        {
    //            //try to get the remoting-object

    //            converter = (PDFConverter.RemoteConverter)Activator.GetObject(typeof(PDFConverter.RemoteConverter), "http://localhost:8989/RemoteConverter");

    //            if (!converter.WordIsAvailable())
    //            {
    //                throw new Exception("The Word application is not available");
    //            }

    //            bool isConvert=converter.convert(wordFile, wordFile.Replace(".docx", ".pdf").Replace(".doc",".pdf"));

    //            if (isConvert)
    //                return 1;
    //            else
    //                return 0;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //    }
    //}

}
