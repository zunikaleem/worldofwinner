﻿<%@ Page Title="Request EPIN" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequestEPIN.aspx.cs" Inherits="mlm_web.RequestEPIN" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>EPIN 
       
                    <small>Request</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">My Business</a></li>
                    <li><a href="#">EPIN Wallet</a></li>
                    <li class="active">EPIN Request</li>
                </ol>
            </section>
            <section class="content">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Request EPIN</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Select Package</label>
                                <asp:DropDownList ID="ddlPackage" runat="server" CssClass="form-control" required>
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Quantity</label>
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" required TextMode="Number"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Upload Transaction</label>
                                <asp:FileUpload ID="fuTransaction" runat="server" />
                            </div>
                        </div>
                        <div class="row">


                            <div class="form-group col-md-12">
                                <label>Comment</label>
                                <asp:TextBox ID="txtComment" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div id="divStatus" runat="server" visible="false">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <asp:Button ID="btnRequest" runat="server" Text="Request EPIN" CssClass="btn btn-info pull-right" OnClick="btnRequest_Click1" />
                    </div>

                </div>
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">EPIN Request List</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  GridLines="None" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="PackageName" HeaderText="Package"></asp:BoundField>
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity"></asp:BoundField>
                                    <asp:BoundField DataField="UserComment" HeaderText="User Comment"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Transaction Receipt">

                                        <ItemTemplate>
                                            <asp:LinkButton ID="hlbTransactionReceiipt" runat="server" Text='<%# Bind("TransactionPath") %>' OnClick="hlbTransactionReceiipt_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="RequestDate" HeaderText="Request Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}"></asp:BoundField>
                                    <asp:BoundField DataField="ProcessDate" HeaderText="Process Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}"></asp:BoundField>
                                    <asp:BoundField DataField="TransferredQuantity" HeaderText="Received Quantity"></asp:BoundField>
                                    <asp:BoundField DataField="AdminComment" HeaderText="Admin Comment"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Right" />
                                <EmptyDataTemplate>
                                    !! No request found !!
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Modal Receipt -->
            <div class="modal fade" id="modalReceipt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" id="receiptheader">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="modal-title">Receipt</h4>
                        </div>
                        <div class="modal-body" id="receiptbody">
                            <div class="col-lg-12 form-horizontal" style="overflow: auto;">
                                <asp:Image ID="imgReceipt" runat="server" Width="400" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /End Modal Show Message-->

            <!-- Modal Show Message -->
            <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" id="messageheader">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="modal-title" id="modaltitle">Message</h4>
                        </div>
                        <div class="modal-body" id="messagebody">
                            <div class="col-lg-12 form-horizontal" style="overflow: auto;">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /End Modal Show Message-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnRequest" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
