﻿<%@ Page Title="EPIN Wallet" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EPINWallet.aspx.cs" Inherits="mlm_web.EPINWallet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>EPIN Wallet
       
            <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">My Business</a></li>
            <li><a href="#">EPIN Wallet</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Wallet</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                </div>
            </div>

            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">EPIN Inbox</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Sent EPIN</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>EPIN Type</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <asp:DropDownList ID="ddlEPINType" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlEPINType_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True">Used EPIN</asp:ListItem>
                                                    <asp:ListItem>Unused EPIN</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <div class="table-responsive">

                                        <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                                            <Columns>
                                                <asp:BoundField DataField="PackageName" HeaderText="Package"></asp:BoundField>
                                                <asp:BoundField DataField="EPINNumber" HeaderText="EPIN Number"></asp:BoundField>
                                                <asp:BoundField DataField="TransactionDate" HeaderText="Received Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}"></asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys" HorizontalAlign="Right" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    <div class="row">
                                        <div class="table-responsive">
                                            <asp:GridView ID="GridView2" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField DataField="PackageName" HeaderText="Package"></asp:BoundField>
                                                    <asp:BoundField DataField="EPINNumber" HeaderText="EPIN Number"></asp:BoundField>
                                                    <asp:BoundField DataField="TranferTo" HeaderText="Transfer To"></asp:BoundField>
                                                    <asp:BoundField DataField="UpdateDate" HeaderText="Sent Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}"></asp:BoundField>
                                                </Columns>
                                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Right" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
</asp:Content>
