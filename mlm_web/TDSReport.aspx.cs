﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;

namespace mlm_web
{
    public partial class TDSReport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                    BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindGrid()
        {
            CurrentDomain.Session.TDSReport = BusinessLogic.Customer.GetTDSReport();
            GridView1.DataSource = CurrentDomain.Session.TDSReport;
            GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if(e.Row.RowType== System.Web.UI.WebControls.DataControlRowType.Footer)
            {
                var table = CurrentDomain.Session.TDSReport.AsEnumerable();

                e.Row.Cells[7].Text = table.Sum(x => Convert.ToSingle(x[7])).ToString("n2");
                e.Row.Cells[8].Text = table.Average(x => Convert.ToSingle(x[8])).ToString("n2");
                e.Row.Cells[9].Text = table.Sum(x => Convert.ToSingle(x[9])).ToString("n2");
            }
        }
    }
}