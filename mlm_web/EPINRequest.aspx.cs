﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class EPINRequest : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    BindPendingRequest();
                    BindProcessedRequest();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindProcessedRequest()
        {
            try
            {
                List<BusinessLogic.EPINRequest> processedRequest = BusinessLogic.EPINRequest.GetProcessedRequest().OrderByDescending(o => o.ProcessDate).ToList();
                GridView2.DataSource = processedRequest;
                GridView2.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindPendingRequest()
        {
            try
            {
                GridView1.DataSource = BusinessLogic.EPINRequest.GetAllPendingRequest().OrderByDescending(o => o.RequestDate);
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                GridView1.EditIndex = -1;
                BindPendingRequest();

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                GridView1.EditIndex = e.NewEditIndex;
                BindPendingRequest();

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = GridView1.Rows[e.RowIndex];

                int epinRequestID = Convert.ToInt32((row.FindControl("lblgEPINRequestID") as Label).Text.Replace("'", ""));

                BusinessLogic.EPINRequest epinRequest = BusinessLogic.EPINRequest.GetEPINRequest(epinRequestID);


                DropDownList ddlStatus_grid = row.FindControl("ddlgStatus") as DropDownList;

                if (epinRequest != null)
                {
                    if (epinRequest.EPINRequestID != 0)
                    {

                        epinRequest.AdminComment = (row.FindControl("txtgAdminComment") as TextBox).Text;
                        if (ddlStatus_grid.SelectedItem.Text.Equals("Transfer"))
                        {
                            epinRequest.TransferredQuantity = Convert.ToInt32((row.FindControl("txtgTransferredQuantity") as TextBox).Text);
                            int x = epinRequest.TransferEPIN(CurrentDomain.Session.CurrentSystemUser);
                            MessageBox.Show(UpdatePanel1, x.ToString() + " EPINs transferred successfully.", MessageType.Success);
                        }

                        else if (ddlStatus_grid.SelectedItem.Text.Equals("Reject"))
                        {
                            int x = epinRequest.RejectRequest();
                            MessageBox.Show(UpdatePanel1, "EPIN request rejected successfully.", MessageType.Success);
                        }
                        else
                        {
                            throw new Exception("Invalid choice.");
                        }
                    }
                    else
                    {
                        throw new Exception("Not a valid request.");
                    }
                }
                else
                {
                    throw new Exception("Invalid epin request.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
            finally
            {
                GridView1.EditIndex = -1;
                BindPendingRequest();
                BindProcessedRequest();
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatus_grid = e.Row.FindControl("lblgStatus") as Label;
                if (lblStatus_grid != null)
                {
                    switch (lblStatus_grid.Text)
                    {
                        case "Transferred":
                            lblStatus_grid.Attributes.Add("class", "label label-success");
                            break;
                        case "Rejected":
                            lblStatus_grid.Attributes.Add("class", "label label-danger");
                            break;
                        default:
                            lblStatus_grid.Attributes.Add("class", "label label-default");
                            break;
                    }
                }
            }
        }

        protected void hlbTransactionReceiipt_Click(object sender, EventArgs e)
        {
            string path = "UploadedTransactions" + "\\" + (sender as LinkButton).Text;
            imgReceipt.ImageUrl = path;
            string script = "javascript:$('#modalMessageReceipt').modal('show');";//$('#imageFrame').hide();";
            //.attr ( 'disabled', true )

            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "comment", script, true);
        }
    }
}