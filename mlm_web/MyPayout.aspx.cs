﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class MyPayout : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    var payouts = CurrentDomain.Session.CurrentCustomer.GetPayouts();

                    GridView1.DataSource = payouts;
                    GridView1.DataBind();
                    ltPayoutCount.Text = payouts.Count.ToString();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
                }
            }
        }
    }
}