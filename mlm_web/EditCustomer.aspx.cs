﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class EditCustomer : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void BindForm()
        {
            try
            {
                var customer = CurrentDomain.Session.CurrentEditCustomer;
                txtAccountHolderName.Text = customer.SystemUser.AccountHolderName;
                txtAccountNumber.Text = customer.SystemUser.AccountNumber;
                txtBankName.Text = customer.SystemUser.BankName;
                txtIFSCCode.Text = customer.SystemUser.IFSCCode;
                txtAddress.Text = customer.Address;
                ddlAccountType.SelectedIndex = ddlAccountType.Items.IndexOf(((ListItem)ddlAccountType.Items.FindByText(customer.SystemUser.AccountType)));
                txtCity.Text = customer.City;
                txtContactName.Text = customer.SystemUser.ContactName;
                txtDateofBirth.Text = customer.DateOfBirth != null ? customer.DateOfBirth.Value.ToString("dd/MM/yyyy") : "";
                txtEPINNumber.Text = customer.EPIN.EPINNumber;
                txtIFSCCode.Text = customer.SystemUser.IFSCCode;
                txtMobileNumber.Text = customer.SystemUser.MobileNumber;
                txtNomineeName.Text = customer.NomineeName;
                txtPackageCost.Text = customer.EPIN.Package.Cost.ToString();
                txtPackageName.Text = customer.EPIN.Package.PackageName;
                txtPANNumber.Text = customer.SystemUser.PAN;
                txtRelation.Text = customer.Relationship;

                BusinessLogic.Customer sponsor = BusinessLogic.Customer.GetCustomer(customer.SponsorID);

                if (sponsor.CustomerID > 0)
                {
                    txtSponsorMobileNumber.Text = sponsor.SystemUser.MobileNumber;
                    txtSponsorName.Text = sponsor.SystemUser.ContactName;
                    txtSponsorUserID.Text = sponsor.SystemUser.UserID;
                    ddlState.SelectedIndex = ddlState.Items.IndexOf((ListItem)ddlState.Items.FindByText(customer.State));
                    sponsor_details.Visible = true;
                }
                else
                {
                    sponsor_details.Visible = false;
                }

                if (customer.IsApproved())
                {
                    txtPANNumber.Enabled = false;
                    txtBankName.Enabled = false;
                    txtIFSCCode.Enabled = false;
                    txtAccountHolderName.Enabled = false;
                    txtAccountNumber.Enabled = false;
                    txtBankName.Enabled = false;
                    txtIFSCCode.Enabled = false;
                    txtPANNumber.Enabled = false;
                    ddlAccountType.Enabled = false;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var customer = CurrentDomain.Session.CurrentEditCustomer;
                customer.SystemUser.AccountHolderName = txtAccountHolderName.Text;
                customer.SystemUser.AccountNumber = txtAccountNumber.Text;
                customer.SystemUser.AccountType = ddlAccountType.SelectedIndex > 0 ? ddlAccountType.SelectedItem.Text : null;
                customer.Address = txtAddress.Text;
                customer.SystemUser.BankName = txtBankName.Text;
                customer.SystemUser.BranchName = txtBankName.Text;
                customer.City = txtCity.Text;
                customer.SystemUser.ContactName = txtContactName.Text;

                if (txtDateofBirth.Text != string.Empty)
                {
                    customer.DateOfBirth = Convert.ToDateTime(txtDateofBirth.Text); // DateTime.ParseExact(txtDateofBirth.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                }
                else
                {
                    customer.DateOfBirth = null;
                }

                customer.EmailID = txtEmail.Text;
                customer.SystemUser.IFSCCode = txtIFSCCode.Text;
                customer.SystemUser.MobileNumber = txtMobileNumber.Text;
                customer.NomineeName = txtNomineeName.Text;
                customer.SystemUser.PAN = txtPANNumber.Text;
                customer.Relationship = txtRelation.Text;
                customer.State = ddlState.SelectedIndex > 0 ? ddlState.SelectedItem.Text : null;

                int x = customer.UpdateCustomer();

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Profile details have been saved successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Some thing went wrong while saving please contact system adminstrator.", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string userID = txtInputUserID.Text;
            BusinessLogic.Customer customer = BusinessLogic.Customer.GetCustomer(BusinessLogic.SystemUser.GetSystemUser(userID));

            if (customer != null)
            {
                CurrentDomain.Session.CurrentEditCustomer = customer;
                BindForm();
                secPersonalDetails.Visible = true;
            }
            else
            {
                CurrentDomain.Session.CurrentEditCustomer = null;
                secPersonalDetails.Visible = false;
                MessageBox.Show(UpdatePanel1, "No such customer found.", MessageType.Info);
            }

        }
    }
}