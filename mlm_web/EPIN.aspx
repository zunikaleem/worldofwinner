﻿<%@ Page Title="Generate EPIN" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EPIN.aspx.cs" Inherits="mlm_web.EPIN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>EPIN
       
            <small>Generate</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Admin</a></li>
            <li class="active">EPIN Generate</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input the Following</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="box-body form-horizontal">
                        <div class="form-group">
                            <label for="inputPackage" class="col-sm-2 control-label">Package</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlPackage" runat="server" CssClass="form-control" required>
                                </asp:DropDownList>
                            </div>
                            
                           <%-- <label for="inputSystemUserID" class="col-sm-1 control-label">Admin</label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddlSystemUser" runat="server" CssClass="form-control" required>
                                </asp:DropDownList>
                            </div>--%>
                            
                            <label for="inputQuantity" class="col-sm-2 control-label">Quantity</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" placeholder="Enter the quantity" required></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>

                        <asp:Button ID="btnGenerate" runat="server" CssClass="btn btn-info pull-right" Text="Generate" OnClick="btnGenerate_Click"  />
                    </div>
                    <!-- /.box-footer -->

                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-12" id="divEPINs" runat="server" visible="false" >
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Generated Pins</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="box-body form-horizontal">
                        <div class="table-responsive">

                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None">
                                
                                <Columns>
                                  
                                    <asp:BoundField DataField="PackageName" HeaderText="Package Name" />
                                    <asp:BoundField DataField="EPINNumber" HeaderText="EPIN Number" />
                                    <asp:BoundField DataField ="UpdateDate" HeaderText ="Creation Date" />
                                    
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</asp:Content>
