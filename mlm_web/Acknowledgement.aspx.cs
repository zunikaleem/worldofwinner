﻿using mlm_lib.Shared;
using System;
using System.Web.UI;
using Common.ExtensionMethods;

namespace mlm_web
{
    public partial class Acknowledgement : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string acknowledgementType = "Registration"; //CurrentDomain.Session.AcknowledgementType;

            lblCompanyName.Text = CurrentDomain.Session.CompanyName;
            switch (acknowledgementType)
            {
                case "Registration":
                    AcknowledgeRegister();
                    break;
                default:
                    break;
            }

        }

        private void AcknowledgeRegister()
        {
            if (CurrentDomain.Session.RegisteredCustomer != null)
            {
                string message = "Congratulation!!! You have been registered successfully.Kindly note a User ID :" + CurrentDomain.Session.RegisteredCustomer.SystemUser.UserID + " and Password has been sent through SMS on mobile number " + CurrentDomain.Session.RegisteredCustomer.SystemUser.MobileNumber + ". To log into your account <a href='signin.aspx'>Click here</a> ";



                var value = BusinessLogic.Configuration.GetConfiguration(ProjectConstant.SendSmsOnJoining).TryGet<bool>();
                if (value == true)
                {
                    message = $"Congratulation!!! You have been registered successfully." +
                        $"Kindly note a User ID :{CurrentDomain.Session.RegisteredCustomer.SystemUser.UserID} and " +
                        $"Password has been sent through SMS on mobile number {CurrentDomain.Session.RegisteredCustomer.SystemUser.MobileNumber}. " +
                        $"To log into your account <a href='signin.aspx'>Click here</a> ";
                    SMSManager.SendSMS(CurrentDomain.Session.RegisteredCustomer.SystemUser.MobileNumber, Resources.LocalizedText.CompanyName + " welcomes you,kindly note your login id is " + CurrentDomain.Session.RegisteredCustomer.SystemUser.UserID + " and account password is " + CurrentDomain.Session.RegisteredCustomer.SystemUser.UserPassword + " Please visit " + Resources.LocalizedText.CompanyURL);
                }
                else
                {
                    message = $"Congratulation!!! You have been registered successfully." +
                        $"Kindly note a User ID :{CurrentDomain.Session.RegisteredCustomer.SystemUser.UserID} and " +
                        $"Password is {CurrentDomain.Session.RegisteredCustomer.SystemUser.UserPassword}. " +
                        $"To log into your account <a href='signin.aspx'>Click here</a> ";
                }
                ltMsg.Text = message;
                CurrentDomain.Session.RegisteredCustomer = null;
            }
            else
            {
                ltMsg.Text = "Some thing went wrong with registration.";
            }
        }
    }
}