﻿using System;
using System.Data;
using System.Web.UI;

namespace mlm_web
{
    public partial class AdminDashbaord : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {

                    lbEPINGenerated.Text = BusinessLogic.EPIN.GetGenerateEPINCount().ToString();
                    lbUsedEPINs.Text = BusinessLogic.EPIN.UsedEPINsCount().ToString();
                    lbUnusedEPINs.Text = BusinessLogic.EPIN.UnUsedEPINsCount().ToString();
                    ltTotalJoining.Text = BusinessLogic.Customer.GetCustomerCount().ToString();

                    float totalEarning = BusinessLogic.Customer.TotalEarning();
                    lbTotalEarning.Text = totalEarning.ToString("n2");

                    float totalTDS = BusinessLogic.Payout.GetTotalTDS();
                    lbTotalTDS.Text = totalTDS.ToString("n2");

                    float totalServiceCharge = BusinessLogic.Payout.GetTotalServiceCharge();
                    lbServiceCharge.Text = totalServiceCharge.ToString("n2");

                    float totalDistribution = BusinessLogic.Account.TotalDistribution();
                    lbTotalDistribution.Text = totalDistribution.ToString("n2");

                    float netProfit = totalEarning - totalDistribution - totalTDS - totalServiceCharge;
                    lbNetProfit.Text = netProfit.ToString("n2");


                    BindJoiningSummary();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, MessageType.Error);
            }
        }

        private void BindJoiningSummary()
        {
            try
            {
                DataTable table = BusinessLogic.Customer.GetCustomerDashBoard(DateTime.MinValue, DateTime.MaxValue);
                GridView2.DataSource = table;
                GridView2.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lbEPINGenerated_Click(object sender, EventArgs e)
        {
            divEPINs.Visible = true;
            GridView1.DataSource = BusinessLogic.EPIN.GetAllEPINs();
            GridView1.DataBind();
            boxTitle.InnerHtml = "Generated EPINs";
        }

        protected void lbUsedEPINs_Click(object sender, EventArgs e)
        {
            divEPINs.Visible = true;
            GridView1.DataSource = BusinessLogic.EPIN.GetUsedEPINs();
            GridView1.DataBind();
            boxTitle.InnerHtml = "Used EPINs";
        }

        protected void lbUnusedEPINs_Click(object sender, EventArgs e)
        {
            divEPINs.Visible = true;
            GridView1.DataSource = BusinessLogic.EPIN.GetUnusedEPINs();
            GridView1.DataBind();
            boxTitle.InnerHtml = "Unused EPINs";
        }

        protected void ltTotalJoining_Click(object sender, EventArgs e)
        {
            Response.Redirect("JoinedCustomers.aspx");
        }
    }
}