﻿<%@ Page Title="Register" Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="mlm_web.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Dream Achiver | Register</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="fonts/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="fonts/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><b>
                <asp:Label ID="lblCompanyName" runat="server" ></asp:Label></b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Fillup the below form to register</p>
            <form id="form1" runat="server">
                <div class="form-group has-feedback">
                    <asp:DropDownList ID="ddlPackage" runat="server" CssClass="form-control" required>
                    </asp:DropDownList>
                </div>
                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtSponsorID" runat="server" class="form-control" placeholder="Sponsor's User ID" required>

                    </asp:TextBox>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtCustomerName" runat="server" class="form-control" placeholder="Enter your name"
                        required ValidationGroup="PersonalDetails"></asp:TextBox>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtMobile" runat="server" class="form-control" placeholder="Enter your mobile number"
                        required ValidationGroup="PersonalDetails"></asp:TextBox>
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtEmail" pattern="[^ @]*@[^ @]*" runat="server" class="form-control"
                        placeholder="Enter your email address" required ValidationGroup="PersonalDetails"></asp:TextBox>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    
                    <asp:TextBox ID="txtEPINNumber" runat="server" class="form-control" placeholder="Enter EPIN number"
                        required ValidationGroup="EPINDetails" title="This is your EPIN format A1AA1111-A1A1-1A11-1A11-11A1A1AA1A1A" ></asp:TextBox>
                    
                    <span class="glyphicon glyphicon-credit-card form-control-feedback"></span>
                    
                </div>

                <div class="form-group has-feedback">
                     <a href="javascript:validateEPIN()">Validate EPIN</a>
                </div>

                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" class="form-control" placeholder="Login password"
                        required ValidationGroup="LoginDetails"></asp:TextBox>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" class="form-control" placeholder="Confirm Password"
                        required ValidationGroup="LoginDetails"></asp:TextBox>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <span id="message"></span>
                </div>


                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"/>
                                I have read term and condition
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <asp:Button ID="btnLogin" class="btn btn-primary btn-block btn-flat" runat="server" Text="Register" OnClick="btnRegister_Click" />
                    </div>
                    <!-- /.col -->
                </div>

            </form>
            <br />
            <br />
            Already registered ? <a href="signin.aspx" class="text-center">Please login here</a>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });

        $('#txtPassword, #txtConfirmPassword').on('keyup', function () {
            if ($('#txtPassword').val() == $('#txtConfirmPassword').val()) {
                $('#message').html('Password matched').css('color', 'green');
                $(':input[type="submit"]').prop('disabled', false);
            } else {
                $('#message').html('Password not matching').css('color', 'red');
                $(':input[type="submit"]').prop('disabled', true);
            }
        });

         function validateEPIN() {

             var epinNumber = $("#txtEPINNumber").val();
             var selectedPacakge=$("#ddlPackage :selected").text();
             var url = "ValidateEpin.aspx?eno=" + epinNumber + "&selectedpackage=" + selectedPacakge;
            var myWindow = window.open(url, 'Welcome', 'toolbar=0,statusbar=0,location=1,scrollbars=1,resizable=0,width=360,height=120');
            myWindow.moveTo(250, 300);
        }
    </script>
</body>
</html>
