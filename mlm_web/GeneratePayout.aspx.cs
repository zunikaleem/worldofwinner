﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace mlm_web
{
    public partial class GeneratePayout : Page
    {
        List<BusinessLogic.Payout> payouts = new List<BusinessLogic.Payout>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                float serviceCharge = Convert.ToSingle(txtServiceCharge.Text);
                float tds = Convert.ToSingle(txtTDS.Text);
                DateTime registrationDate = DateTime.ParseExact(txtRegistrationDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);

                float stage1Commission = Convert.ToSingle(txtDirectPromotor.Text);
                float stage2Commission = Convert.ToSingle(txtLevel1.Text);
                float stage3Commission = Convert.ToSingle(txtLevel2.Text);
                float stage4Commission = Convert.ToSingle(txtLevel3.Text);
                float stage5Commission = Convert.ToSingle(txtLevel4.Text);
                float stage6Commission = Convert.ToSingle(txtLevel5.Text);

                payouts = BusinessLogic.Payout.generatePayout(stage1Commission, stage2Commission, stage3Commission, stage4Commission, stage5Commission, stage6Commission, registrationDate, serviceCharge, tds, CurrentDomain.Session.CurrentCustomer);

                GridView1.DataSource = payouts;
                GridView1.DataBind();
                divPayouts.Visible = true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.Footer)
            {
                e.Row.Cells[7].Text = "<b><i>" + payouts.Sum(o => o.GrossAmount).ToString() + "</i></b>";
                e.Row.Cells[8].Text = "<b><i>" + payouts.Sum(o => o.NetAmount).ToString() + "</i></b>";

            }
        }
    }
}