﻿<%@ Page Title="Taxes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Taxes.aspx.cs" Inherits="mlm_web.Taxes" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Manage Taxes
       
            <small>Configuration</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Manage Taxes</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Available Taxes</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound" OnRowDeleting="GridView1_RowDeleting">
                                <Columns>
                                    <asp:CommandField ButtonType="Link"
                                        ShowEditButton="True">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="TaxID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgTaxID" runat="server" Text='<%# Bind("TaxID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tax">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgTaxType" runat="server" Text='<%# Bind("TaxType") %>'
                                                CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("TaxType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Calculate In">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("CalculateIn") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgCalculateIn" runat="server" CssClass="form-control" Text='<%# Bind("CalculateIn") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgValue" runat="server" CssClass="form-control" Text='<%# Bind("Value") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Value") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Link" />
                                </Columns>
                            </asp:GridView>

                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <input type="button" value="Add New" id="btnNew" class="btn pull-right btn-primary" onclick="javascript: $('#modalAddPackage').modal('show')" data-toggle="modal" />
                    </div>

                </div>
            </section>

            
                <div class="modal fade" id="modalAddPackage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header primary">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Add New Tax</h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-lg-12 form-horizontal">
                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            Tax</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtTaxType" runat="server" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            Calculate In</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtCalculateIn" runat="server" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            Value</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtValue" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" data-dismiss="modal"
                                    OnClick="btnSave_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                    </div>
                </div>
         
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
