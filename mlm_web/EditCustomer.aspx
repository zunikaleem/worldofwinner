﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditCustomer.aspx.cs" Inherits="mlm_web.EditCustomer" %>
<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Customer
       
            <small>Edit</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
                    <li><a href="#">Customer Mangement</a></li>
                    <li class="active">Edit Customer</li>
                </ol>
            </section>
            <section class="content">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search User</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Enter User ID</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <asp:TextBox ID="txtInputUserID" runat="server" CssClass="form-control" required></asp:TextBox>
                                </div>
                            </div>



                        </div>
                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-success pull-right" formnovalidate="formnovalidate" OnClick="btnSearch_Click" />
                    </div>
                </div>
                <section id="secPersonalDetails" runat="server" visible="false">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Personal Information</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Contact Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control" required></asp:TextBox>
                                    </div>

                                </div>

                                <div class="form-group col-md-6">
                                    <label>Mobile Number</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-mobile"></i>
                                        </div>
                                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control" data-inputmask='"mask": "9999999999"' data-mask="" placeholder="10-digit mobile number without country code" required TextMode="Number"></asp:TextBox>
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Date Of Birth</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <asp:TextBox ID="txtDateofBirth" ClientIDMode="Static" runat="server" class="form-control" TextMode="Date"></asp:TextBox>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Address</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-road"></i>
                                        </div>
                                        <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>City</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-building"></i>
                                        </div>
                                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" required></asp:TextBox>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group col-md-6">
                                    <label>State</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-building"></i>
                                        </div>
                                        <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
                                            <asp:ListItem>--Select--</asp:ListItem>
                                            <asp:ListItem>Andoman and Nicobar</asp:ListItem>
                                            <asp:ListItem>Andhra Pradesh</asp:ListItem>
                                            <asp:ListItem>Arunachal Pradesh</asp:ListItem>
                                            <asp:ListItem>Assam</asp:ListItem>
                                            <asp:ListItem>Bihar</asp:ListItem>
                                            <asp:ListItem>Chandigarh </asp:ListItem>
                                            <asp:ListItem>Chhattisgarh</asp:ListItem>
                                            <asp:ListItem>Dadra and Nagar Haveli </asp:ListItem>
                                            <asp:ListItem>Daman and Diu </asp:ListItem>
                                            <asp:ListItem>National Capital Territory of Delhi </asp:ListItem>
                                            <asp:ListItem>Goa</asp:ListItem>
                                            <asp:ListItem>Gujarat</asp:ListItem>
                                            <asp:ListItem>Haryana</asp:ListItem>
                                            <asp:ListItem>Himachal Pradesh</asp:ListItem>
                                            <asp:ListItem>Jammu and Kashmir</asp:ListItem>
                                            <asp:ListItem>Jharkhand</asp:ListItem>
                                            <asp:ListItem>Karnataka</asp:ListItem>
                                            <asp:ListItem>Kerala</asp:ListItem>
                                            <asp:ListItem>Lakshadweep </asp:ListItem>
                                            <asp:ListItem>Madhya Pradesh</asp:ListItem>
                                            <asp:ListItem>Maharashtra</asp:ListItem>
                                            <asp:ListItem>Manipur</asp:ListItem>
                                            <asp:ListItem>Meghalaya</asp:ListItem>
                                            <asp:ListItem>Mizoram</asp:ListItem>
                                            <asp:ListItem>Nagaland</asp:ListItem>
                                            <asp:ListItem>Odisha</asp:ListItem>
                                            <asp:ListItem>Puducherry </asp:ListItem>
                                            <asp:ListItem>Punjab</asp:ListItem>
                                            <asp:ListItem>Rajasthan</asp:ListItem>
                                            <asp:ListItem>Sikkim</asp:ListItem>
                                            <asp:ListItem>Tamil Nadu</asp:ListItem>
                                            <asp:ListItem>Telangana</asp:ListItem>
                                            <asp:ListItem>Tripura</asp:ListItem>
                                            <asp:ListItem>Uttar Pradesh</asp:ListItem>
                                            <asp:ListItem>Uttarakhand</asp:ListItem>
                                            <asp:ListItem>West Bengal</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Email Address</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-at"></i>
                                        </div>
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Bank Details</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Account Type</label>
                                    <asp:DropDownList ID="ddlAccountType" runat="server" CssClass="form-control">
                                        <asp:ListItem>--Select--</asp:ListItem>
                                        <asp:ListItem>Saving</asp:ListItem>
                                        <asp:ListItem>Current</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group col-md-6">
                                    <label>Bank Name</label>
                                    <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.row -->

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>IFSC Code</label>
                                    <asp:TextBox ID="txtIFSCCode" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Account Number</label>
                                    <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Account Holder Name</label>
                                    <asp:TextBox ID="txtAccountHolderName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>PAN Number</label>
                                    <asp:TextBox ID="txtPANNumber" runat="server" CssClass="form-control" data-inputmask='"mask": "AAAAA9999A"' data-mask=""></asp:TextBox>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Nominee Details</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Nominee Name</label>
                                    <asp:TextBox ID="txtNomineeName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group col-md-6">
                                    <label>Nominee Relation</label>
                                    <asp:TextBox ID="txtRelation" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>

                    <div class="box box-info" id="sponsor_details" runat="server">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sponsor Details</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Sponsor User ID</label>
                                    <asp:TextBox ID="txtSponsorUserID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group col-md-6">
                                    <label>Sponsor Mobile Number</label>
                                    <asp:TextBox ID="txtSponsorMobileNumber" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.row -->

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Sponsor Name</label>
                                    <asp:TextBox ID="txtSponsorName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>

                            </div>
                            <!-- /.row -->


                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Package Details</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Current Package</label>
                                    <asp:TextBox ID="txtPackageName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group col-md-6">
                                    <label>Package Cost</label>
                                    <asp:TextBox ID="txtPackageCost" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.row -->

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>EPIN Number</label>
                                    <asp:TextBox ID="txtEPINNumber" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <asp:Button ID="btnUpdate" runat="server" Text="Save Changes" CssClass="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                    </div>
                </section>
            </section>

            <!-- Modal Show Message -->
            <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" id="messageheader">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="modal-title" id="modaltitle">Message</h4>
                        </div>
                        <div class="modal-body" id="messagebody">
                            <div class="col-lg-12 form-horizontal" style="overflow: auto;">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /End Modal Show Message-->

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="ContentFooter" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">

    <script>

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            SetDateOfBirht();
        });

        $(document).ready(function () {
            SetDateOfBirht();
        });

        function SetDateOfBirht() {
            // max would be 18 years older
            var dtToday = new Date();
            var month = dtToday.getMonth();
            var day = dtToday.getDate();
            var year = dtToday.getFullYear() - 18;

            if (month < 10)
                month = '0' + month.toString();
            if (day < 10)
                day = '0' + day.toString();

            var maxDate = year + '-' + month + '-' + day;

            $('#txtDateofBirth').attr('max', maxDate);

        }
    </script>
</asp:Content>