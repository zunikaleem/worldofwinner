﻿using mlm_lib.Shared;
using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class TermAndCondition : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            CurrentDomain.Session.TermAndCondition = true;

            switch(CurrentDomain.Session.CurrentSystemUser.AccessGroup.AccessGroupName)
            {
                case AccessGroupConstant.SuperAdim:
                    Response.Redirect("AdminDashboard.aspx");
                    break;
                case AccessGroupConstant.Customer:
                    Response.Redirect("CustomerDashboard.aspx");
                    break;
                case AccessGroupConstant.CEOCustomer:
                case AccessGroupConstant.CEOOwner:
                case AccessGroupConstant.B2BCustomer:
                case AccessGroupConstant.B2BOwner:
                    Response.Redirect("MyLedger.aspx");
                    break;
                default:
                    Response.Redirect("signin.aspx");
                    break;
            }
        }
    }
}