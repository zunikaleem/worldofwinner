﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class MyLedger : Page
    {
        List<BusinessLogic.Account> accounts = new List<BusinessLogic.Account>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                accounts = CurrentDomain.Session.CurrentSystemUser.GetAccounts().OrderByDescending(x => x.UpdateDate).ToList();
                gvAccount.DataSource = accounts;
                gvAccount.DataBind();
            }
        }

        protected void gvAccount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[4].Text == mlm_lib.Shared.TransactionType.ReservedCommissionEarned)
                {
                    e.Row.BackColor = System.Drawing.Color.Pink;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                float credit = Convert.ToSingle(accounts.Sum(o => o.CreditAmount));
                float debit = Convert.ToSingle(accounts.Sum(o => o.DebitAmount));
                float balance = (credit - debit);

                e.Row.Cells[1].Text = credit.ToString("n2");
                e.Row.Cells[2].Text = debit.ToString("n2");
                e.Row.Cells[3].Text = "Balance : " + balance.ToString("n2");
            }
        }
    }
}