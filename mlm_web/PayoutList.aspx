﻿<%@ Page Title="Payout Balance Sheet" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayoutList.aspx.cs" Inherits="mlm_web.PayoutList" %>
<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css" />
    <script>
        $(document).ready(function () {
            $('[id*=btnExport]').on('click', function () {
                ExportToExcel('GridView1');
            });

            $(function () {
                $('#GridView1').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            });
            //alert('hi');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Payout Balance Sheet
       
            <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Admin</a></li>
                    <li><a href="#">Payouts</a></li>
                    <li class="active">Payout Balance Sheet</li>
                </ol>
            </section>
            <section class="content">

                <div class="box box-info" id="divPayouts" runat="server" visible="false">
                    <div class="box-header with-border">
                        <h3 class="box-title">Payout Balance Sheet</h3>
                        <div class="box-tools pull-right">
                            <button id="btnExport" value="Export" class="pull-right">Export</button>
                        </div>
                    </div>
                    <div class="box-body form-horizontal">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" ClientIDMode="Static" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AutoGenerateColumns="False" ShowFooter="true" OnRowDataBound="GridView1_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="UserID" HeaderText="User ID" />
                                    <asp:BoundField DataField="ContactName" HeaderText="Name" />
                                    <asp:BoundField DataField="MobileNumber" HeaderText="Mobile Number" />
                                    <asp:BoundField DataField="PANNumber" HeaderText="PAN" />
                                    <asp:BoundField DataField="BankName" HeaderText="Bank" />
                                    <asp:BoundField DataField="BranchName" HeaderText="Branch" />
                                    <asp:BoundField DataField="IFCCode" HeaderText="IFSC Code" />
                                    <asp:BoundField DataField="AccountType" HeaderText="Account Type" />
                                    <asp:BoundField DataField="AccountHolderName" HeaderText="AC. Name" />
                                    <asp:BoundField DataField="AccountNumber" HeaderText="AC. Number" />
                                    <asp:BoundField DataField="TotalCredit" HeaderText="Total Credeit" DataFormatString="{0:C2}" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TotalDebit" HeaderText="Total Debit" DataFormatString="{0:C2}" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Balance" HeaderText="Balance" DataFormatString="{0:C2}" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right"/>
                                </Columns>
                                <FooterStyle Font-Bold="true" Font-Underline="true" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">



    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>


    <!-- page script -->
    <script>

</script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            $('[id*=btnExport]').on('click', function () {
                ExportToExcel('GridView1');
            });

            $(function () {
                $('#GridView1').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });

            //alert('hi bottom');
        });

        function ExportToExcel(Id) {
            var tab_text = "<table border='2px'><tr>";
            var textRange;
            var j = 0;
            tab = document.getElementById(Id);
            var headerRow = $('[id*=GridView1] tr:first');
            tab_text += headerRow.html() + '</tr><tr>';
            var rows = $('[id*=GridView1] tr:not(:has(th))');
            for (j = 0; j < rows.length; j++) {
                if ($(rows[j]).css('display') != 'none') {
                    tab_text = tab_text + rows[j].innerHTML + "</tr>";
                }
            }
            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, Id + ".xls");
            }
            else {                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            }
            return (sa);
        }
    </script>
</asp:Content>
