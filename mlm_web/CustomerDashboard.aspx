﻿<%@ Page Title="Customer Dashboard" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerDashboard.aspx.cs" Inherits="mlm_web.CustomerDashboard" %>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <script>
        $(document).ready(function () {
            $('[id*=btnExport]').on('click', function () {
                ExportToExcel('GridView1');
            });
        });
    </script>

    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Dashboard
        <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">My Business</li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">My Direct Sponsor</span>
                        <span class="info-box-number">
                            <asp:Literal ID="ltDirectSponsor" runat="server"></asp:Literal>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-inr"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Ledger Balance</span>
                        <span class="info-box-number">

                            <asp:Literal ID="ltLedgerBalance" runat="server"></asp:Literal>



                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">EPIN Balance</span>
                        <span class="info-box-number">
                            <asp:Literal ID="ltEPINBalance" runat="server"></asp:Literal></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">My Downlines</span>
                        <span class="info-box-number">
                            <asp:Literal ID="ltCompleteDownline" runat="server"></asp:Literal></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->



        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">My Direct Sponsor (<asp:Literal runat="server" ID="ltSponsorCount" Text="SponsorCount"></asp:Literal>)</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" id="btnExport">
                                <i class="fa fa-file-excel-o"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView1_PageIndexChanging">

                                <Columns>
                                    <asp:BoundField DataField="UserID" HeaderText="User ID"></asp:BoundField>
                                    <asp:BoundField DataField="ContactName" HeaderText="Contact Name"></asp:BoundField>
                                    <asp:BoundField DataField="MobileNumber" HeaderText="Mobile Number"></asp:BoundField>
                                    <asp:BoundField DataField="RegistrationDate" HeaderText="Joining Date" DataFormatString="{0:dd/MM/yy hh:mm}"></asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Right" />
                            </asp:GridView>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <%--   <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>--%>
                    <!-- /.box-footer -->
                </div>

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">My Payouts</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="gvPayout" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None" ShowFooter="true" OnRowDataBound="gvPayout_RowDataBound" PageIndex="10">

                                <Columns>
                                    <asp:BoundField DataField="PayoutDate" HeaderText="Payout Date" DataFormatString="{0:dd/MM/yy hh:mm}"></asp:BoundField>
                                    <asp:BoundField DataField="PayoutType" HeaderText="Payout Type"></asp:BoundField>
                                    <asp:BoundField DataField="SponsorToUserID" HeaderText="Sponsor To"></asp:BoundField>
                                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                                    <asp:BoundField DataField="GrossAmount" HeaderText="Commission" DataFormatString="{0:n2}"></asp:BoundField>
                                    <asp:BoundField DataField="TDS" HeaderText="TDS" DataFormatString="{0:n2}"></asp:BoundField>
                                    <asp:BoundField DataField="ServiceCharge" HeaderText="ServiceCharge" DataFormatString="{0:n2}"></asp:BoundField>
                                    <asp:BoundField DataField="NetAmount" HeaderText="Net Amount" DataFormatString="{0:n2}"></asp:BoundField>
                                </Columns>

                                <HeaderStyle />
                                <FooterStyle Font-Bold="true" Font-Underline="true" />
                            </asp:GridView>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <%--   <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>--%>
                    <!-- /.box-footer -->
                </div>

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">My Accounts</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="gvAccount" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None" ShowFooter="true" OnRowDataBound="gvAccount_RowDataBound">

                                <Columns>
                                    <asp:BoundField DataField="UpdateDate" HeaderText="Transaction Date" DataFormatString="{0:dd/MM/yy hh:mm}"></asp:BoundField>
                                    <asp:BoundField DataField="TransactionType" HeaderText="Transaction Type"></asp:BoundField>
                                    <asp:BoundField DataField="CreditAmount" HeaderText="e-Store Sell Amount" DataFormatString="{0:n2}"></asp:BoundField>
                                    <asp:BoundField DataField="DebitAmount" HeaderText="Debit Amount" DataFormatString="{0:n2}"></asp:BoundField>
                                    <asp:BoundField DataField="Narration" HeaderText="Description"></asp:BoundField>
                                </Columns>
                                <HeaderStyle />
                                <FooterStyle Font-Bold="true" Font-Underline="true" />
                            </asp:GridView>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <%--   <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>--%>
                    <!-- /.box-footer -->
                </div>
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</asp:Content>
<asp:Content ID="ContentFooter" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            $('[id*=btnExport]').on('click', function () {
                ExportToExcel('GridView1');
            });
        });

        function ExportToExcel(Id) {
            var tab_text = "<table border='2px'><tr>";
            var textRange;
            var j = 0;
            tab = document.getElementById(Id);
            var headerRow = $('[id*=GridView1] tr:first');
            tab_text += headerRow.html() + '</tr><tr>';
            var rows = $('[id*=GridView1] tr:not(:has(th))');
            for (j = 0; j < rows.length; j++) {
                if ($(rows[j]).css('display') != 'none') {
                    tab_text = tab_text + rows[j].innerHTML + "</tr>";
                }
            }
            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, Id + ".xls");
            }
            else {                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            }
            return (sa);
        }
    </script>
</asp:Content>
