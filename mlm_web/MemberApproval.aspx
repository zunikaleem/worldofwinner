﻿<%@ Page Title="Members Approval" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MemberApproval.aspx.cs" Inherits="mlm_web.MemberApproval" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Member Approval
       
            <small>Process</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Member Approval</li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-12" runat="server">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Pending for Aprroval</h3>
                            </div>
                            <div class="box-body form-horizontal">
                                <div class="table-responsive">

                                    <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Custome ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgCustomerID" runat="server" Text='<%# Bind("CustomerID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="UserID" HeaderText="User ID" />
                                            <asp:BoundField DataField="ContactName" HeaderText="Name" />
                                            <asp:BoundField DataField="MobileNumber" HeaderText="Mobile" />
                                            <asp:BoundField DataField="RegistrationDate" HeaderText="RegistrationDate" DataFormatString="{0:dd/MM/yy hh:mm:ss}" />
                                            <asp:BoundField DataField="PANNumber" HeaderText="PAN" />
                                            <asp:BoundField DataField="AccountNumber" HeaderText="A/C Number" />
                                            <asp:BoundField DataField="BranchName" HeaderText="Bank Name" />
                                            <asp:BoundField DataField="IFCCode" HeaderText="IFSC Code" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnApprove" runat="server"
                                                        Text="Approve" OnClick="btnApprove_Click" CssClass="btn btn-success" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Modal Show Message -->
            <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" id="messageheader">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="modal-title" id="modaltitle">Message</h4>
                        </div>
                        <div class="modal-body" id="messagebody">
                            <div class="col-lg-12 form-horizontal" style="overflow: auto;">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /End Modal Show Message-->
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
