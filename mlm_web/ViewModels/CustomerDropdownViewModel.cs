﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModels
{
    public class SystemUserDropdownViewModel
    {
        public int SystemUserID { get; set; }

        public string ContactName { get; set; }
    }
}