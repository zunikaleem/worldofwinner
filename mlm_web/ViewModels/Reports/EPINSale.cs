﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mlm_web.ViewModels.Reports
{
    public class EPINSale
    {
        public string PackageName{get;set;}
        public float Cost { get; set; }
        public int TotalSale { get; set; }
        public float Amount { get; set; }
    }
}