﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mlm_web.ViewModels.Reports
{
    public class RegularAndFixedPayoutViewModel
    {
        public string UserID { get; set; }
        public float GrossAmount { get; set; }
        public float TDS { get; set; }
        public float TDSAmount { get; set; }
        public float ServiceCharge { get; set; }
        public float ServiceChargeAmount { get; set; }
        public float NetAmount { get; set; }
        public float TotalPaid { get; set; }
        public float Balance { get; set; }
    }
}