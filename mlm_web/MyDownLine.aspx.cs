﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModels;
using System.Web.UI;

namespace mlm_web
{
    public partial class MyDownLine : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    var list = BusinessLogic.Payout.GetAchievedLevel(CurrentDomain.Session.CurrentCustomer);
                    var stages = new List<LevelViewModel>();
                    foreach (var item in list)
                    {
                        var stage = new LevelViewModel();
                        stage.Id = item;
                        stage.Name = "Level " + item.ToString();

                        stages.Add(stage);

                    }

                    Utility.Binder.BindDropDownList(ddlLevel, stages, "Id", "Name", "0", "--Select Level--");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
                }
            }

        }

        protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlLevel.SelectedIndex > 0)
                {
                    int level = Convert.ToInt32(ddlLevel.SelectedValue);

                    List<BusinessLogic.Customer> downLines = CurrentDomain.Session.CurrentCustomer.MyDownLine(level);

                    GridView1.DataSource = downLines;
                    GridView1.DataBind();
                    ltDownlineCount.Text = " (Total " + downLines.Count().ToString() + ")";
                    divDownline.Visible = true;
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Please select valid level.", MessageType.Info);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
    }
}