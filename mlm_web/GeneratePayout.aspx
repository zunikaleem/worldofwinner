﻿<%@ Page Title="Generate Payout" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeneratePayout.aspx.cs" Inherits="mlm_web.GeneratePayout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css" />
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="plugins/iCheck/all.css" />
    <link rel="stylesheet" href="plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>gPayout Generator
       
            <small>Execute</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Admin</a></li>
            <li><a href="#">Payout</a></li>
            <li class="active">Generate Payout</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Payout Generator</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Registration Date</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <asp:TextBox ID="txtRegistrationDate" runat="server" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></asp:TextBox>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>


                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Direct Promotor</label>
                        <div class="input-group">

                            <asp:TextBox ID="txtDirectPromotor" Text="10" runat="server" CssClass="form-control" placeholder="Direct Promoter Amount"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Level 1 </label>
                        <div class="input-group">
                            <asp:TextBox ID="txtLevel1" Text="5" runat="server" CssClass="form-control" placeholder="Level 1 Amount"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Level 2 </label>
                        <div class="input-group">
                            <asp:TextBox ID="txtLevel2" Text="4" runat="server" CssClass="form-control" placeholder="Level 2 Amount"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>

                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Level 3</label>
                        <div class="input-group">
                            <asp:TextBox ID="txtLevel3" Text="3" runat="server" CssClass="form-control" placeholder="Level 3 Amount"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Level 4</label>
                        <div class="input-group">
                            <asp:TextBox ID="txtLevel4" Text="2" runat="server" CssClass="form-control" placeholder="Level 4 Amount"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>

                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Level 5</label>
                        <div class="input-group">
                            <asp:TextBox ID="txtLevel5" Text="1" runat="server" CssClass="form-control" placeholder="Level 5 Amount"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label>TDS %</label>
                        <div class="input-group">

                            <asp:TextBox ID="txtTDS" Text="5" runat="server" CssClass="form-control" placeholder="TDS percentage"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Service Charge</label>
                        <div class="input-group">

                            <asp:TextBox ID="txtServiceCharge" Text="5" runat="server" CssClass="form-control" placeholder="Service charge percentage"></asp:TextBox>
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <asp:Button ID="btnUpdate" runat="server" Text="Generate" CssClass="btn btn-info pull-right" OnClick="btnUpdate_Click" />
            </div>
        </div>


        <div class="box box-info" id="divPayouts" runat="server" visible="false">
            <div class="box-header with-border">
                <h3 class="box-title">Generated Payouts</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body form-horizontal">
                <div class="table-responsive">
                    <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  GridLines="None" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True">
                        <Columns>
                            <asp:BoundField DataField="PayoutDate" HeaderText="Payout Date" DataFormatString="{0:dd/MM/yy}"></asp:BoundField>
                            <asp:BoundField DataField="CustomerUserID" HeaderText="User ID"></asp:BoundField>
                            <asp:BoundField DataField="SponsorToUserID" HeaderText="Sponsor To"></asp:BoundField>
                            <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                            <asp:BoundField DataField="GrossAmount" HeaderText="Commision"></asp:BoundField>
                            <asp:BoundField DataField="TDS" HeaderText="TDS"></asp:BoundField>
                            <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge"></asp:BoundField>
                            <asp:BoundField DataField="GrossAmount" HeaderText="Gross Amount"></asp:BoundField>
                            <asp:BoundField DataField="NetAmount" HeaderText="Net Amount"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!-- /.box-body -->
        </div>


    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- Page script -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", { "placeholder": "mm/dd/yyyy" });
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });

        });
    </script>
</asp:Content>
