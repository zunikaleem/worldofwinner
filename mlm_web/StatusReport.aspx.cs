﻿using BusinessLogic;
using mlm_web.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class StatusReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            var startDate = Convert.ToDateTime(txtFromDate.Text).Date.ToString("yyyy-MM-dd HH:mm:ss");
            var endDate = Convert.ToDateTime(txtToDate.Text).Date.AddDays(1).AddMilliseconds(-1).ToString("yyyy-MM-dd HH:mm:ss");

            var customerInDateRange = Customer.GetCustomers(startDate, endDate);

            var reservedPayout = Payout.GetPayouts(startDate, endDate, mlm_lib.Shared.PayoutType.Reserved);

            var regularPayout = Payout.GetPayouts(startDate, endDate, mlm_lib.Shared.PayoutType.Regular).OrderBy(x => x.CustomerUserID).ToList();
            var fixedPayout = FixedPayout.GetPayouts(startDate, endDate);

            SetEpinSales(customerInDateRange.Select(x => x.EPIN));
            SetReservedPayout(reservedPayout);
            SetRegularPayout(regularPayout, startDate, endDate);
            SetFixedPayout(fixedPayout, startDate, endDate);
        }

        private void SetRegularPayout(List<Payout> regularPayout, string startDate, string endDate)
        {
            var distinctUserID = regularPayout.Select(x => x.CustomerUserID).Distinct();

            var regularPayoutViewModel = new List<RegularAndFixedPayoutViewModel>();

            foreach (var userID in distinctUserID)
            {
                var systemUser = SystemUser.GetSystemUser(userID);

                var accounts = systemUser.GetAccounts(startDate, endDate, mlm_lib.Shared.TransactionType.CommissionEarned);

                var totalCredit = accounts.Sum(x => x.CreditAmount);
                var totalDebit = accounts.Sum(x => x.DebitAmount);
                var balance = totalCredit - totalDebit;

                regularPayoutViewModel.Add(new RegularAndFixedPayoutViewModel()
                {
                    GrossAmount = regularPayout.Where(x => x.CustomerUserID == userID).Sum(y => y.GrossAmount),
                    NetAmount = regularPayout.Where(x => x.CustomerUserID == userID).Sum(y => y.NetAmount),
                    ServiceCharge = regularPayout.Where(x => x.CustomerUserID == userID).Average(y => y.ServiceCharge),
                    ServiceChargeAmount = regularPayout.Where(x => x.CustomerUserID == userID).Sum(y => y.ServiceChargeAmount),
                    TDS = regularPayout.Where(x => x.CustomerUserID == userID).Average(y => y.TDS),
                    TDSAmount = regularPayout.Where(x => x.CustomerUserID == userID).Sum(y => y.TDSAmount),
                    UserID = userID,
                    TotalPaid = totalDebit.GetValueOrDefault(),
                    Balance = balance.GetValueOrDefault()
                });
            }

            this.gvRegularPayout.DataSource = regularPayoutViewModel.OrderBy(x => x.UserID);
            this.gvRegularPayout.DataBind();
            this.divRegularPayout.Visible = true;
        }

        private void SetFixedPayout(List<FixedPayout> fixedPayouts, string startDate, string endDate)
        {
            var distinctUserID = fixedPayouts.Select(x => x.SystemUser.SystemUserID).Distinct();

            var fixedPayoutViewModel = new List<RegularAndFixedPayoutViewModel>();

            foreach (var userID in distinctUserID)
            {
                var systemUser = SystemUser.GetSystemUser(userID);

                var accounts = systemUser.GetAccounts(startDate, endDate, mlm_lib.Shared.TransactionType.FixedCommissionEarned);

                var totalCredit = accounts.Sum(x => x.CreditAmount);
                var totalDebit = accounts.Sum(x => x.DebitAmount);
                var balance = totalCredit - totalDebit;

                fixedPayoutViewModel.Add(new RegularAndFixedPayoutViewModel()
                {
                    GrossAmount = fixedPayouts.Where(x => x.SystemUser.SystemUserID == userID).Sum(y => y.GrossAmount),
                    NetAmount = fixedPayouts.Where(x => x.SystemUser.SystemUserID == userID).Sum(y => y.NetAmount),
                    ServiceCharge = fixedPayouts.Where(x => x.SystemUser.SystemUserID == userID).Average(y => y.ServiceCharge),
                    ServiceChargeAmount = fixedPayouts.Where(x => x.SystemUser.SystemUserID == userID).Sum(y => y.ServiceChargeAmount),
                    TDS = fixedPayouts.Where(x => x.SystemUser.SystemUserID == userID).Average(y => y.TDS),
                    TDSAmount = fixedPayouts.Where(x => x.SystemUser.SystemUserID == userID).Sum(y => y.TDSAmount),
                    UserID = systemUser.UserID,
                    TotalPaid = totalDebit.GetValueOrDefault(),
                    Balance = balance.GetValueOrDefault()
                });
            }

            this.gvFixedPayout.DataSource = fixedPayoutViewModel.OrderBy(x => x.UserID);
            this.gvFixedPayout.DataBind();
            this.divFixedPayout.Visible = true;
        }

        private void SetReservedPayout(List<Payout> reservedPayout)
        {
            this.gvReservedPayout.DataSource = reservedPayout;
            this.gvReservedPayout.DataBind();
            this.divReservedPayout.Visible = true;
        }

        private void SetEpinSales(IEnumerable<BusinessLogic.EPIN> epins)
        {
            var epinSales = new List<ViewModels.Reports.EPINSale>();

            foreach (var epin in epins)
            {
                if (epinSales.Any(x => x.PackageName == epin.PackageName))
                {
                    var epinSale = epinSales.Where(x => x.PackageName == epin.PackageName).First();

                    epinSale.TotalSale = epinSale.TotalSale + 1;
                    epinSale.Amount = epinSale.Amount + epin.Package.Cost;
                }
                else
                {
                    var epinSale = new ViewModels.Reports.EPINSale
                    {
                        PackageName = epin.PackageName,
                        Cost = epin.Package.Cost,
                        Amount = epin.Package.Cost,
                        TotalSale = 1
                    };
                    epinSales.Add(epinSale);
                }
            }

            gvEPINSales.DataSource = epinSales;
            gvEPINSales.DataBind();
            divEPINSales.Visible = true;
        }
    }
}