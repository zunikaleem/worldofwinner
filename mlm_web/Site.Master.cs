﻿using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class Site1 : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentDomain.Session.CurrentSystemUser.SystemUserID == 0)
            {
                Response.Redirect("Signin.aspx");
            }
            string pageName = ContentPlaceHolder1.Page.GetType().FullName;
            if (CurrentDomain.Session.TermAndCondition == false && pageName != "ASP.termandcondition_aspx")
            {
                Response.Redirect("TermAndCondition.aspx");
            }

            Page.Title = CurrentDomain.Session.CompanyName + " | " + Page.Title;
            lblCompnayName.Text = lblCompanyLogo.Text = CurrentDomain.Session.CompanyName;
        }
    }
}