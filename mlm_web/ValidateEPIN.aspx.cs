﻿using System;
using System.Web.UI;

namespace dreamachiver_web
{
    public partial class ValidateEPIN : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BusinessLogic.EPIN epin = new BusinessLogic.EPIN();

            epin.EPINNumber = Convert.ToString(Request.QueryString["eno"]);
            string packageName = Convert.ToString(Request.QueryString["selectedpackage"]);

            epin.Package = BusinessLogic.Package.GetPackage(packageName);

            if (!(epin.EPINNumber == ""))
            {
                if (!epin.IsValid())
                {
                    Image1.ImageUrl = "icons/icon_cross.jpg";
                    lbMessage.Text = "Invalid Epin entered";
                }
                else
                {
                    Image1.ImageUrl = "icons/icon_check.jpg";
                    lbMessage.Text = "Your Epin is valid! please continue your registration process";
                }
            }
            else
            {
                Image1.ImageUrl = "icons/icon_cross.jpg";
                lbMessage.Text = "Some of the epin Information is not entered!! please try again";
            }
        }
    }
}