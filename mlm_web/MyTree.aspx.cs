﻿using System;
using System.Data;
using System.Web.UI;

namespace mlm_web
{
    public partial class MyTress : Page
    {
        BusinessLogic.Customer customer;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString.Count == 0)
                {
                    customer = CurrentDomain.Session.CurrentCustomer;
                }
                else
                {
                    string uid = Request.QueryString["node"].ToString();
                    var systemuser = BusinessLogic.SystemUser.GetSystemUser(uid);
                    customer = BusinessLogic.Customer.GetCustomer(systemuser);
                }
                if (customer.CustomerID > 0)
                {
                    string node = customer.SystemUser.UserID;//Request.QueryString["node"];
                    DataRow root = customer.GetTreeDetails();
                    display.InnerHtml = display.InnerHtml + "<table style='text-align:center' border='0' width='100%'><tr><td>";
                    display.InnerHtml = display.InnerHtml + String.Format("<center><div class='box box-sucess' style='border:solid green 1px;text-decoration:none; width:100px; height:130px; text-align:left;padding-left :10px;padding-top:10px;><a href='MyTree.aspx?node={0}&parent={1}' style='text-decoration:none'><span style='color: red;'>{2}</span><BR/><span style='color: green'>{3}</span><BR/><span style='color: blue;'>{4}</span><br/><span style='color: crimson'>({5})</span><br/><span style='color:black'>[{6}]</span></a></div></center>", root["UserID"].ToString(), Convert.ToString(root["SponsorUserID"]), root["UserID"].ToString(), root["ContactName"].ToString(), Convert.ToDateTime(root["RegistrationDate"]).ToString("dd/MM/yy"), root["MobileNumber"].ToString(), root["TotalChild"].ToString());

                    DataTable childs = customer.GetChilds();

                    display.InnerHtml = display.InnerHtml + "<br/>|<hr style='border: 0;height: 1px;background: #333;'/><table style='text-align:center' width='100%'><tr>";
                    for (int x = 0; x < childs.Rows.Count; x++)
                    {

                        display.InnerHtml = display.InnerHtml + "<td valign='top'>";
                        display.InnerHtml = display.InnerHtml + String.Format("<center><div style='border:solid green 1px;text-decoration:none; width:100px; height:130px; text-align:left;padding-left :10px;padding-top:10px;'><a href='MyTree.aspx?node={0}&parent={1}' style='text-decoration:none'><span style='color: red;'>{2}</span><BR/><span style='color: green'>{3}</span><BR/><span style='color: blue;'>{4}</span><br/><span style='color: crimson'>{5}</span><br/><span style='color:black'>[{6}]</span></a></div></center>", childs.Rows[x]["UserID"].ToString(), Convert.ToString(childs.Rows[x]["SponsorUserID"]), childs.Rows[x]["UserID"].ToString(), childs.Rows[x]["ContactName"].ToString(), Convert.ToDateTime(childs.Rows[x]["RegistrationDate"]).ToString("dd/MM/yy"), childs.Rows[x]["MobileNumber"].ToString(), childs.Rows[x]["TotalChild"].ToString());


                        display.InnerHtml = display.InnerHtml + "</td>";
                    }

                    display.InnerHtml = display.InnerHtml + "</tr></table></td></tr></table>";
                }

                else
                {
                    ClearPath();
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show( ex.Message);
            }
        }

        private void CreatePath(string node)
        {
            try
            {
                string par;
                string path = node.ToString();
                if (Session["Root"].ToString() != path)
                {
                    do
                    {
                        par = BusinessLogic.Customer.GetParent(node);
                        if (par != "")
                        {
                            path = path + "," + par.ToString();
                            node = par;
                        }
                    }
                    while (par != Session["Root"].ToString() && par != "");
                }
                Session["Path"] = path;
            }
            catch (Exception ex)
            {
                //MessageBox.Show( ex.Message);
            }
        }

        //private void Draw3(DataRow root)
        //{
        //    try
        //    {
        //        display.InnerHtml = display.InnerHtml + String.Format("<center><div style='border:solid green 1px;text-decoration:none; width:110px; height:120px; text-align:left;padding-left :10px;padding-top:10px;background-color:#FFAA00'><a href='NewTree.aspx?node={0}' style='text-decoration:none'><span style='color: red;'>{1}</span><BR/><span style='color: green'>{2}</span><BR/><span style='color: blue;'>{3}</span><br/><span style='color: crimson'>{4}</span></a></div></center>", root["UserID"].ToString(), root["UserID"].ToString(), root["ContactName"].ToString(), Convert.ToDateTime(root["RegistrationDate"]).ToString("dd/MM/yy"), root["MobileNumber"].ToString());

        //        if (BelongToPath(root["UserID"].ToString()))
        //        {
        //            DataTable childs = BusinessLogic.Customer.getChilds(root["UserID"].ToString());

        //            display.InnerHtml = display.InnerHtml + "<br/>|<hr/><table style='text-align:center' width='100%'><tr>";

        //            for (int x = 0; x < childs.Rows.Count; x++)
        //            {

        //                display.InnerHtml = display.InnerHtml + "<td valign='top'>";
        //                //Draw3(childs.Rows[x]["UserID"].ToString ());
        //                Draw3(childs.Rows[x]);

        //                display.InnerHtml = display.InnerHtml + "</td>";
        //            }

        //            display.InnerHtml = display.InnerHtml + "</tr></table>";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        private bool BelongToPath(string p)
        {
            try
            {
                string[] x = Session["Path"].ToString().Split(',');

                for (int i = 0; i < x.Length; i++)
                {

                    if (x[i].Equals(p))

                        return true;

                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ClearPath()
        {
            Session["Path"] = "";
        }
    }
}