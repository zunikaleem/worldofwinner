﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace mlm_web
{
    public partial class SentEPINs : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindOutbox();
            }
        }

        private void BindOutbox()
        {
            try
            {
                List<BusinessLogic.EPINInbox> sentEPINs = BusinessLogic.EPINInbox.GetSentEPINs(CurrentDomain.Session.CurrentCustomer.SystemUser);
                GridView2.DataSource = sentEPINs;
                GridView2.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}