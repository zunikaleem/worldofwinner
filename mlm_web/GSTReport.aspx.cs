﻿using System;
using System.Data;
using System.Web.UI;
using System.Linq;
using System.Globalization;

namespace mlm_web
{
    public partial class GSTReport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }

            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
        private void BindGrid()
        {
            CurrentDomain.Session.GSTReport = BusinessLogic.Customer.GetGSTReport();
            GridView1.DataSource = CurrentDomain.Session.GSTReport;
            GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.Footer)
            {
                var table = CurrentDomain.Session.GSTReport.AsEnumerable();

                e.Row.Cells[4].Text = table.Average(x => Convert.ToSingle(x[5])).ToString("n2");
                e.Row.Cells[5].Text = table.Sum(x => Convert.ToSingle(x[6])).ToString("C2", CultureInfo.CurrentCulture);
                e.Row.Cells[6].Text = table.Average(x => Convert.ToSingle(x[7])).ToString("n2");
                e.Row.Cells[7].Text = table.Average(x => Convert.ToSingle(x[8])).ToString("n2");
                e.Row.Cells[8].Text = table.Sum(x => Convert.ToSingle(x[9])).ToString("C2", CultureInfo.CurrentCulture);
                e.Row.Cells[9].Text = table.Sum(x => Convert.ToSingle(x[10])).ToString("C2", CultureInfo.CurrentCulture);
            }
        }
    }
}