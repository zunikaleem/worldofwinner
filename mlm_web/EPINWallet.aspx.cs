﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class EPINWallet : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    BindOutbox();
                    BindInbox();
                }

            }
            catch (Exception ex)
            {

            }
        }
        private void BindInbox()
        {
            try
            {
                string epinType = ddlEPINType.SelectedItem.Text;
                GridView1.DataSource = CurrentDomain.Session.CurrentSystemUser.GetEPINInbox(epinType);

                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;

            BindInbox();
            BindOutbox();
        }

        public void BindOutbox()
        {
            try
            {
                List<BusinessLogic.EPINInbox> sentEPINs = BusinessLogic.EPINInbox.GetSentEPINs(CurrentDomain.Session.CurrentSystemUser);
                GridView2.DataSource = sentEPINs;
                GridView2.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string epinNumber = e.Row.Cells[1].Text;

                if (BusinessLogic.EPIN.IsUsed(epinNumber))
                {
                    e.Row.BackColor = System.Drawing.Color.Pink;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.Honeydew;
                    e.Row.ForeColor = System.Drawing.Color.Green;
                }
            }
        }



        protected void ddlEPINType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string epinType = ddlEPINType.SelectedItem.Text;
                GridView1.DataSource = CurrentDomain.Session.CurrentSystemUser.GetEPINInbox(epinType);

                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}