﻿using mlm_lib.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace mlm_web
{
    public partial class DistributePayout : Page
    {
        List<BusinessLogic.Account> accounts = new List<BusinessLogic.Account>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                string userID = txtUserID.Text;


                BindGrid(userID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindGrid(string userID)
        {
            var systemUser = BusinessLogic.SystemUser.GetSystemUser(userID);
            var customer = systemUser.GetCustomer();
            accounts = BusinessLogic.Account.GetAccount(systemUser.SystemUserID);

            if (accounts.Count > 0)
            {
                GridView1.DataSource = accounts.OrderByDescending(o => o.UpdateDate);
                GridView1.DataBind();
                divPayouts.Visible = true;
                divTransferFund.Visible = true;
                CurrentDomain.Session.CurrentAccountSytsemUser = accounts.Select(o => o.SystemUser).First();
                BindTransferForm(CurrentDomain.Session.CurrentAccountSytsemUser);
            }
            else
            {
                MessageBox.Show(UpdatePanel1, "No records found.", MessageType.Info);
            }
        }

        private void BindTransferForm(BusinessLogic.SystemUser systemUser)
        {
            txtAccountHolderName.Text = systemUser.AccountHolderName;
            txtAccountNumber.Text = systemUser.AccountNumber;
            txtAccountType.Text = systemUser.AccountType;
            txtBankName.Text = systemUser.BankName;
            txtBranchName.Text = systemUser.BranchName;
            txtIFSCCode.Text = systemUser.IFSCCode;
            txtAmount.Text = string.Empty;
            txtTransactionComment.Text = string.Empty;
        }

        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtAmount.Text == string.Empty)
                    throw new Exception("Amount is required field");
                if (txtTransactionComment.Text == string.Empty)
                    throw new Exception("Transaction commment is required field");

                var amount = Convert.ToSingle(txtAmount.Text);
                if (Convert.ToSingle(txtAmount.Text) == 0)
                    throw new Exception("Amount should be greater than zero");

                if (amount > CurrentDomain.Session.NetBalance.GetValueOrDefault())
                    throw new Exception("Can't pay more than balance");

                BusinessLogic.Account account = new BusinessLogic.Account();
                account.DebitAmount = amount;
                account.SystemUser = CurrentDomain.Session.CurrentAccountSytsemUser;
                account.Narration = txtTransactionComment.Text;
                account.TransactionID = Guid.NewGuid().ToString();
                account.TransactionType = TransactionType.CommissionReceived;

                int x = account.Pay(CurrentDomain.Session.CurrentSystemUser);

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Amount paid successfully.", MessageType.Success);
                    BindGrid(CurrentDomain.Session.CurrentAccountSytsemUser.UserID);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to tranfer the amount. Please contact technical support.", MessageType.Error);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[6].Text == TransactionType.ReservedCommissionEarned)
                {
                    e.Row.BackColor = System.Drawing.Color.Pink;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                var totalCredit = accounts.Sum(o => o.CreditAmount);
                var totalDebit = accounts.Sum(o => o.DebitAmount);

                var totalBalance = totalCredit - totalDebit;

                var reservedBalance = accounts.Where(x => x.TransactionType == TransactionType.ReservedCommissionEarned).Sum(y => y.CreditAmount);

                var netBalance = totalBalance - reservedBalance;

                CurrentDomain.Session.NetBalance = netBalance;


                e.Row.Cells[4].Text = $"<b>{totalCredit.GetValueOrDefault().ToString("n2")}</i></b>";
                e.Row.Cells[5].Text = $"<b>{totalDebit.GetValueOrDefault().ToString("n2")}</i></b>";

                e.Row.Cells[3].Text = $"<b>Total Balance: {totalBalance.GetValueOrDefault().ToString("n2")}</b>" +
                                      $"<br/><b>Reserved Balance: {reservedBalance.GetValueOrDefault().ToString("n2")}</b>" +
                                      $"<br/><b>Net Balance: {netBalance.GetValueOrDefault().ToString("n2")}</b>";
            }
        }
    }
}