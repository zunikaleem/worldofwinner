﻿using System;
using System.Web.UI;

public partial class Logout : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClearSessionProperties();
        Session.Abandon();
        Response.Redirect("Signin.aspx");
    }
    private void ClearSessionProperties()
    {
        try
        {
            Type type = typeof(CurrentDomain.Session);
            foreach (var p in type.GetProperties())
            {
                p.SetValue(type, null, null);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}