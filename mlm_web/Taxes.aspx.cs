﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class Taxes : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                    BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindGrid()
        {
            GridView1.DataSource = BusinessLogic.Tax.GetAllTaxes();
            GridView1.DataBind();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                GridView1.EditIndex = e.NewEditIndex;

                BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                GridView1.EditIndex = -1;
                BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = GridView1.Rows[e.RowIndex];

                int taxID = Convert.ToInt32((row.FindControl("lblgTaxID") as Label).Text.Replace("'", ""));

                BusinessLogic.Tax tax = BusinessLogic.Tax.GetTax(taxID);

                tax.CalculateIn = (row.FindControl("txtgCalculateIn") as TextBox).Text.Replace("'", "");
                tax.TaxType = (row.FindControl("txtgTaxType") as TextBox).Text.Replace("'", "");
                tax.Value = Convert.ToSingle((row.FindControl("txtgValue") as TextBox).Text.Replace("'", ""));
                int x = tax.UpdateTax(CurrentDomain.Session.CurrentCustomer);

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Tax saved successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to update the Tax.", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
            finally
            {
                GridView1.EditIndex = -1;
                BindGrid();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessLogic.Tax tax = new BusinessLogic.Tax();

                tax.TaxType = txtTaxType.Text.Replace("'", "");
                tax.CalculateIn = txtCalculateIn.Text.Replace("'", "");
                tax.Value = Convert.ToSingle(txtValue.Text.Replace("'", ""));

                int x = tax.AddNewTax(CurrentDomain.Session.CurrentCustomer);

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Tax added successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to add tax.", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
            finally
            {
                BindGrid();
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = GridView1.Rows[e.RowIndex];

                int taxID = Convert.ToInt32((row.FindControl("lblgTaxID") as Label).Text.Replace("'", ""));

                BusinessLogic.Tax tax = BusinessLogic.Tax.GetTax(taxID);

                int x = tax.DeleteTax();

                if (x > 0)
                {
                    BindGrid();
                    MessageBox.Show(UpdatePanel1, "Tax delete successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to delete the tax.", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
    }
}