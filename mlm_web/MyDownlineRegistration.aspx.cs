﻿using Common.ExtensionMethods;
using mlm_lib.Shared;
using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class MyDownlineRegistration : Page
    {
        BusinessLogic.EPIN regEPIN = new BusinessLogic.EPIN();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    BindPackage();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindPackage()
        {
            try
            {
                Utility.Binder.BindDropDownList(ddlPackage, BusinessLogic.Package.getPackages(), "PackageID", "PackageName", "0", "--Select--");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlPackage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string packageName = ddlPackage.SelectedItem.Text;
                regEPIN = null;

                BusinessLogic.Package package = BusinessLogic.Package.GetPackage(packageName);

                if (package != null)
                {
                    if (package.PackageID != 0)
                    {
                        regEPIN = CurrentDomain.Session.CurrentSystemUser.GetSingleAvailableEPIN(package);

                        if (regEPIN != null)
                        {
                            if (regEPIN.EPINID != 0)
                            {
                                txtEPINNumber.Text = regEPIN.EPINNumber;
                                txtSponsorUser.Text = CurrentDomain.Session.CurrentCustomer.SystemUser.UserID;
                            }
                            else
                            {
                                throw new Exception("EPIN is not valid.");
                            }
                        }
                        else
                        {
                            throw new Exception("No EPIN found.");
                        }
                    }
                    else
                    {
                        throw new Exception("Package not found.");
                    }
                }
                else
                {
                    throw new Exception("Select valid package.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessLogic.Customer customer = new BusinessLogic.Customer();
                customer.SystemUser = new BusinessLogic.SystemUser();

                customer.SystemUser.ContactName = txtContactName.Text;

                customer.EmailID = txtEmail.Text;

                customer.SystemUser.MobileNumber = txtMobileNumber.Text;

                customer.Register(Convert.ToInt32(ddlPackage.SelectedValue), txtSponsorUser.Text, txtEPINNumber.Text, GenerateNewPassword());

                if (!customer.SystemUser.UserID.Equals(string.Empty))
                {
                    CurrentDomain.Session.RegisteredCustomer = customer;
                    var value = BusinessLogic.Configuration.GetConfiguration(ProjectConstant.SendSmsOnJoining).TryGet<bool>();
                    if (value == true)
                    {
                        SMSManager.SendSMS(CurrentDomain.Session.RegisteredCustomer.SystemUser.MobileNumber, Resources.LocalizedText.CompanyName + " welcomes you,kindly note your login id is " + CurrentDomain.Session.RegisteredCustomer.SystemUser.UserID + " and account password is " + CurrentDomain.Session.RegisteredCustomer.SystemUser.UserPassword + " Please visit " + Resources.LocalizedText.CompanyURL);
                        MessageBox.Show(UpdatePanel1, $"The user {customer.SystemUser.ContactName} registered successfully.Kindly note the user id : {customer.SystemUser.UserID} and password is been sent to provided mobile number", MessageType.Success);
                    }
                    else
                    {
                        MessageBox.Show(UpdatePanel1, $"The user {customer.SystemUser.ContactName} registered successfully.Kindly note the user id : {customer.SystemUser.UserID } and password is {customer.SystemUser.UserPassword}", MessageType.Success);
                    }
                }
                else
                {
                    throw new Exception($"Failed to register the customer {customer.SystemUser.ContactName}");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private string GenerateNewPassword()
        {
            try
            {
                Guid newGuid = Guid.NewGuid();

                string newGuidString = newGuid.ToString();
                return newGuid.ToString().Substring(0, 8);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}