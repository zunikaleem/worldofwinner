﻿using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class SendEPIN : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    BindPackage();
                    BindAvailableEPINs();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
                }
            }
        }

        private void BindAvailableEPINs()
        {
            GridView1.DataSource = CurrentDomain.Session.CurrentSystemUser.GetEPINInbox("Unused EPIN");

            GridView1.DataBind();
        }

        private void BindPackage()
        {
            Utility.Binder.BindDropDownList(ddlPackage, BusinessLogic.Package.getPackages(), "PackageID", "PackageName", "", "--Select Package--");
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                int quantity = Convert.ToInt16(txtQuantity.Text);
                string pacakge = ddlPackage.SelectedItem.Text;
                string userid = txtUserID.Text;

                int tranfer = CurrentDomain.Session.CurrentSystemUser.TranferEPIN(userid, pacakge, quantity);

                if (tranfer > 0)
                {
                    MessageBox.Show(UpdatePanel1, quantity.ToString() + " epins Transferred successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, quantity.ToString() + " Failed to transafer epins.", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
            finally
            {
                BindAvailableEPINs();
            }
        }
    }
}