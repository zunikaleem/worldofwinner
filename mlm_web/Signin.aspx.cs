﻿using mlm_lib.Shared;
using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class Signin : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CurrentDomain.Session.CompanyName = BusinessLogic.Configuration.GetConfiguration(ProjectConstant.CompanyName).ToString();
                lblCompanyName.Text = ltCompanyName.Text = CurrentDomain.Session.CompanyName;
            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessLogic.SystemUser systemUser = new BusinessLogic.SystemUser();
                string userID = txtUserId.Value;
                string password = txtPassword.Value;

                switch (BusinessLogic.SystemUser.DoLogin(userID, password, out systemUser))
                {
                    case BusinessLogic.LoginResult.Failed:
                        MessageBox.Show(UpdatePanel1, "Invalid password entered", MessageType.Info);
                        break;
                    case BusinessLogic.LoginResult.InvalidLogin:
                        MessageBox.Show(UpdatePanel1, "The entered user id is not registered", MessageType.Info);
                        break;
                    case BusinessLogic.LoginResult.Success:
                        CurrentDomain.Session.CurrentSystemUser = systemUser;
                        var customer = BusinessLogic.Customer.GetCustomer(systemUser);
                        if (customer != null)
                            CurrentDomain.Session.CurrentCustomer = customer;
                        Response.Redirect("TermAndCondition.aspx", false);
                        break;
                    default:
                        throw new Exception("Not all login check carried out");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
    }
}