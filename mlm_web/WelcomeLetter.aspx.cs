﻿using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class WelcomeLetter : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BusinessLogic.Customer customer = CurrentDomain.Session.CurrentCustomer;

                lblCustomerName.Text = customer.SystemUser.ContactName;
                lblUserID.Text = customer.SystemUser.UserID;
                lblRegistrationDate.Text = customer.RegistrationDate.ToString("dd-MMM-yyyy");

                lblBirthDate.Text = customer.DateOfBirth != null ? customer.DateOfBirth.Value.ToString("dd-MMM-yyyy") : "";
                lblAddress.Text = customer.Address;
                lblCity.Text = customer.City;
                lblEmailID.Text = customer.EmailID;

                lblNomineeName.Text = customer.NomineeName;
                lblNomineeRelationship.Text = customer.Relationship;
                lblPANNumber.Text = customer.SystemUser.PAN;

                BusinessLogic.Customer sponsor = BusinessLogic.Customer.GetCustomer(customer.SponsorID);

                lblSponsorUserID.Text = sponsor.SystemUser.UserID;
                lblSponsoredBy.Text = sponsor.SystemUser.ContactName;
                lblState.Text = customer.State;
                lblToday.Text = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")).Date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {

            }
        }
    }
}