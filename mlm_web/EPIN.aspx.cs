﻿using mlm_lib.Shared;
using System;
using System.Collections.Generic;
using System.Web.UI;
using ViewModels;

namespace mlm_web
{
    public partial class EPIN : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //(Master.FindControl("adminmenu") as HtmlGenericControl).Attributes.Add("class","treeview active");

                BindPackages();
                BindAdminSytsemUsers();
            }
        }

        private void BindAdminSytsemUsers()
        {
            //var accessGroup = BusinessLogic.AccessGroup.GetAccessGroup(AccessGroupConstant.Admin);
            //var systemUsers = BusinessLogic.SystemUser.GetSystemUsers(accessGroup);

            //var systemUserViewModelList = new List<SytsemUserViewModel>();

            //foreach(var systemUser in systemUsers)
            //{
            //    systemUserViewModelList.Add(new SytsemUserViewModel()
            //    {
            //        SystemUserID = systemUser.SystemUserID,
            //        SystemUserIDName = $"{systemUser.UserID}-{systemUser.}" 
            //    });
            //}

            //var accessGroup = BusinessLogic.AccessGroup.GetAccessGroup(AccessGroupConstant.AdminCustomer);
            //var adminSystemUsers = BusinessLogic.SystemUser.GetSystemUsers(accessGroup);
            //var customerDropdownViewModelList = new List<SystemUserDropdownViewModel>();
            //foreach (var adminSystemUser in adminSystemUsers)
            //{
            //    customerDropdownViewModelList.Add(new SystemUserDropdownViewModel()
            //    {
            //        SystemUserID = adminSystemUser.SystemUserID,
            //        ContactName = $"{adminSystemUser.UserID}-{adminSystemUser.ContactName}"
            //    });
            //}

            //Utility.Binder.BindDropDownList(ddlSystemUser, customerDropdownViewModelList, "SystemUserID", "ContactName", "", "--Select--");
        }

        private void BindPackages()
        {
            Utility.Binder.BindDropDownList(ddlPackage, BusinessLogic.Package.getPackages(), "PackageID", "PackageName", "", "--Select--");
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlPackage.SelectedIndex == 0) throw new Exception("Kindly Select the package");
                if (txtQuantity.Text == string.Empty) throw new Exception("Kindly enter the quantity");

                var packageID = Convert.ToInt32(ddlPackage.SelectedItem.Value);
                var quantity = Convert.ToInt32(txtQuantity.Text);
                //var adminUserID= Convert.ToInt32(ddlSystemUser.SelectedItem.Value);

                BusinessLogic.Package package = BusinessLogic.Package.getPackage(packageID);
                // BusinessLogic.SystemUser sendToAdminSystemUser = BusinessLogic.SystemUser.GetSystemUser(Convert.ToInt32(adminUserID));
                List<BusinessLogic.EPIN> epins = BusinessLogic.EPIN.GenerateEPIN(package, quantity, CurrentDomain.Session.CurrentSystemUser, CurrentDomain.Session.CurrentSystemUser);

                if (epins.Count > 0)
                {
                    GridView1.DataSource = epins;
                    GridView1.DataBind();

                    divEPINs.Visible = true;
                }
                else
                {
                    divEPINs.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}