﻿<%@ Page Title="Packages" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Packages.aspx.cs" Inherits="mlm_web.Packages" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Manage Packages
       
            <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Manage Packages</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Available Packages</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound" OnRowDeleting="GridView1_RowDeleting">
                                <Columns>
                                    <asp:CommandField ButtonType="Link"
                                        ShowEditButton="True">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="PackageID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgPackageID" runat="server" Text='<%# Bind("PackageID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Package Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgPackageName" runat="server" Text='<%# Bind("PackageName") %>'
                                                CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("PackageName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField HeaderText="Base Price">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgBasePrice" runat="server" Text='<%# Bind("BasePrice") %>'
                                                CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelBasePrice" runat="server" Text='<%# Bind("BasePrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SGST">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgSGST" runat="server" Text='<%# Bind("SGST") %>'
                                                CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1s" runat="server" Text='<%# Bind("SGST") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CGST">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgCGST" runat="server" Text='<%# Bind("CGST") %>'
                                                CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1c" runat="server" Text='<%# Bind("CGST") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Round Off">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgRoundOff" runat="server" Text='<%# Bind("RoundOff") %>'
                                                CssClass="form-control"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1r" runat="server" Text='<%# Bind("RoundOff") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cost">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>
                                        </ItemTemplate>
                                       <EditItemTemplate>
                                            <asp:Label ID="LabelCost" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtgDescription" runat="server" CssClass="form-control" Text='<%# Bind("Description") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Link" />
                                </Columns>
                            </asp:GridView>

                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <input type="button" value="Add New" id="btnNew" class="btn pull-right btn-primary" onclick="javascript: $('#modalAddPackage').modal('show')" data-toggle="modal" />
                    </div>

                </div>
            </section>

            <div class="example-modal">
                <div class="modal fade" id="modalAddPackage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header primary">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Add New  Package</h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-lg-12 form-horizontal">
                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            Package Name</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtPackageName" runat="server" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            Base Price</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtBasePrice" runat="server" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            SGST</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtSGST" runat="server" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            CGST</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtCGST" runat="server" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            Round Off</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtRoundOff" runat="server" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputusername" class="col-lg-4 control-label">
                                            Description</label><div class="col-lg-8">
                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                    </div
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" data-dismiss="modal"
                                    OnClick="btnSave_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
