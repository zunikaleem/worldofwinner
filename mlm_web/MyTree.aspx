﻿<%@ Page Title="My Tree" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyTree.aspx.cs" Inherits="mlm_web.MyTress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="content-header">
        <h1>Tree
       
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">My Business</a></li>
            <li class="active">My Tree</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tree View</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                </div>
            </div>

            <div class="box-body">
                <div class="row">
                    <div class="panel-body" style="overflow: auto;" id="display" runat="server">
                    </div>
                </div>

            </div>

        </div>
    </section>

</asp:Content>
