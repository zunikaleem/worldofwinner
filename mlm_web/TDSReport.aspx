﻿<%@ Page Title="TDS Report" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TDSReport.aspx.cs" Inherits="mlm_web.TDSReport" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <script>
        $(document).ready(function () {
            $('[id*=btnExport]').on('click', function () {
                ExportToExcel('GridView1');
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>TDS
       
            <small>Reports</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Admin</a></li>
                    <li><a href="#">Reports</a></li>
                    <li class="active">TDS Report</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">TDS Report</h3>
                        <button id="btnExport" value="Export" class="pull-right">Export</button>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  GridLines="None" AutoGenerateColumns="False" ShowFooter="true" OnRowDataBound="GridView1_RowDataBound">

                                <Columns>
                                    <asp:BoundField DataField="User ID" HeaderText="User ID" />
                                    <asp:BoundField DataField="Contact Name" HeaderText="Contact Name" />
                                    <asp:BoundField DataField="PAN No" HeaderText="PAN No." />
                                    <asp:BoundField DataField="Bank Name" HeaderText="Bank Name" />
                                    <asp:BoundField DataField="Branch Name" HeaderText="Branch Name" />
                                    <asp:BoundField DataField="Account Type" HeaderText="Account Type" />
                                    <asp:BoundField DataField="Account Number" HeaderText="Account Number" />
                                    <asp:BoundField DataField="Gross Amount" HeaderText="Gross Amount" DataFormatString="{0:0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TDS%" HeaderText="TDS%" DataFormatString="{0:0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Total TDS" HeaderText="Total TDS" DataFormatString="{0:0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                </Columns>
                                <FooterStyle Font-Bold="true" Font-Underline="true" />
                            </asp:GridView>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                </div>
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            $('[id*=btnExport]').on('click', function () {

                ExportToExcel('GridView1');
            });
        });

        function ExportToExcel(Id) {
            var tab_text = "<table border='2px'><tr>";
            var textRange;
            var j = 0;
            tab = document.getElementById(Id);
            var headerRow = $('[id*=GridView1] tr:first');
            tab_text += headerRow.html() + '</tr><tr>';
            var rows = $('[id*=GridView1] tr:not(:has(th))');
            for (j = 0; j < rows.length; j++) {
                if ($(rows[j]).css('display') != 'none') {
                    tab_text = tab_text + rows[j].innerHTML + "</tr>";
                }
            }
            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, Id + ".xls");
            }
            else {                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            }
            return (sa);
        }
    </script>
</asp:Content>
