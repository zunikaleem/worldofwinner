﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace mlm_web
{
    public partial class Packages : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    // (Master.FindControl("adminmenu") as HtmlGenericControl).Attributes.Add("class", "treeview active");
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindGrid()
        {
            try
            {
                List<BusinessLogic.Package> packages = BusinessLogic.Package.getPackages();
                GridView1.DataSource = packages;
                GridView1.DataBind();

                if (GridView1.Rows.Count > 0)
                {
                    GridView1.UseAccessibleHeader = true;
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                GridView1.EditIndex = e.NewEditIndex;

                BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                GridView1.EditIndex = -1;
                BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = GridView1.Rows[e.RowIndex];

                int packageID = Convert.ToInt32((row.FindControl("lblgPackageID") as Label).Text.Replace("'", ""));

                BusinessLogic.Package package = BusinessLogic.Package.getPackage(packageID);

                package.PackageName = (row.FindControl("txtgPackageName") as TextBox).Text.Replace("'", "");

                package.BasePrice = Convert.ToSingle((row.FindControl("txtgBasePrice") as TextBox).Text.Replace("'", ""));
                package.SGST = Convert.ToSingle((row.FindControl("txtgSGST") as TextBox).Text.Replace("'", ""));
                package.CGST = Convert.ToSingle((row.FindControl("txtgCGST") as TextBox).Text.Replace("'", ""));
                package.RoundOff = Convert.ToSingle((row.FindControl("txtgRoundOff") as TextBox).Text.Replace("'", ""));

                package.Description = (row.FindControl("txtgDescription") as TextBox).Text.Replace("'", "");

                int x = package.UpdatePackage(CurrentDomain.Session.CurrentCustomer);

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Package updated successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to update the package.", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
            finally
            {
                GridView1.EditIndex = -1;
                BindGrid();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessLogic.Package package = new BusinessLogic.Package();

                package.PackageName = txtPackageName.Text.Replace("'", "");
                package.BasePrice = Convert.ToSingle(txtBasePrice.Text.Replace("'", ""));
                package.SGST = Convert.ToSingle(txtSGST.Text.Replace("'", ""));
                package.CGST = Convert.ToSingle(txtCGST.Text.Replace("'", ""));
                package.RoundOff = Convert.ToSingle(txtRoundOff.Text.Replace("'", ""));
                package.Description = txtDescription.Text.Replace("'", "");

                int x = package.AddNewPackage(CurrentDomain.Session.CurrentCustomer);

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Package added successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to add package.", MessageType.Info);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
            finally
            {
                BindGrid();
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = GridView1.Rows[e.RowIndex];

                int packageID = Convert.ToInt32((row.FindControl("lblgPackageID") as Label).Text.Replace("'", ""));

                BusinessLogic.Package package = BusinessLogic.Package.getPackage(packageID);

                int x = package.DeletePackage();

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Package deleted successfully.", MessageType.Success);
                    BindGrid();
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to delete pacakge", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
    }
}