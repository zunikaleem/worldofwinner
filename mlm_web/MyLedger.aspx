﻿<%@ Page Title="My Ledger" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyLedger.aspx.cs" Inherits="mlm_web.MyLedger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>

    <asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <section class="content-header">
            <h1>Account
       
            <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li><a href="#">My Business</a></li>
                <li class="active">Ledger</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">


                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Ledger Balance</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form-horizontal">
                            <div class="table-responsive">
                                <asp:GridView ID="gvAccount" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  AutoGenerateColumns="False" GridLines="None" ShowFooter="true" OnRowDataBound="gvAccount_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="UpdateDate" HeaderText="Transaction Date" DataFormatString="{0:dd/MM/yy hh:mm}"></asp:BoundField>
                                        <asp:BoundField DataField="CreditAmount" HeaderText="Credit Amount" DataFormatString="{0:n2}"></asp:BoundField>
                                        <asp:BoundField DataField="DebitAmount" HeaderText="Debit Amount" DataFormatString="{0:n2}"></asp:BoundField>
                                        <asp:BoundField DataField="Narration" HeaderText="Comment"></asp:BoundField>
                                        <asp:BoundField DataField="TransactionType" HeaderText="Transaction Type"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle />
                                    <FooterStyle Font-Bold="true" Font-Underline="true" />
                                </asp:GridView>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
