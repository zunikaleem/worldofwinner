﻿using System;
using System.Data;
using System.Web.UI;

namespace mlm_web
{
    public partial class JoinedCustomers : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataTable payoutList = BusinessLogic.Customer.GetCustomers();

                GridView1.DataSource = payoutList;
                GridView1.DataBind();
                divPayouts.Visible = true;
            }
        }
    }
}