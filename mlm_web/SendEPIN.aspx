﻿<%@ Page Title="Send EPINs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SendEPIN.aspx.cs" Inherits="mlm_web.SendEPIN" %>

<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Send EPINs
       
            <small>Control</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">My Business</a></li>
                    <li><a href="#">EPIN Wallet</a></li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Send EPINs</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-md-4">
                                <label>Select Pacakge</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-list-alt"></i>

                                    </div>
                                    <asp:DropDownList ID="ddlPackage" runat="server" CssClass="form-control" required></asp:DropDownList>

                                </div>

                            </div>


                            <div class="form-group col-md-4">
                                <label>Enter User ID</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <asp:TextBox ID="txtUserID" runat="server" class="form-control" required placeholder="Enter user id of receiver"></asp:TextBox>
                                </div>

                            </div>

                            <div class="form-group col-md-4">
                                <label>EPIN Quantity</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtQuantity" Text="" runat="server" CssClass="form-control" TextMode="Number" required placeholder="How much epin you want to transfer?"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="reset" class="btn btn-danger" value="Reset" />
                        <asp:Button ID="btnSend" runat="server" Text="Send" CssClass="btn btn-success pull-right" OnClick="btnSend_Click" />
                    </div>
                </div>

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Available EPINs</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AllowPaging="True"  AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="PackageName" HeaderText="Package"></asp:BoundField>
                                    <asp:BoundField DataField="EPINNumber" HeaderText="EPIN Number"></asp:BoundField>
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Received Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}"></asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Right" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>

