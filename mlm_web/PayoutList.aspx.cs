﻿using System;
using System.Web.UI;
using System.Linq;
using System.Globalization;

namespace mlm_web
{
    public partial class PayoutList : Page
    {
        IOrderedEnumerable<ViewModel.PayoutBalanceList> payoutList = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                payoutList = BusinessLogic.SystemUser.GetPayoutBalnaceList().OrderBy(x => x.UserID);
                GridView1.DataSource = payoutList;
                GridView1.DataBind();
                divPayouts.Visible = true;
            }
        }

        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if(e.Row.RowType== System.Web.UI.WebControls.DataControlRowType.Footer)
            {
                e.Row.Cells[10].Text = payoutList.Sum(x=>x.TotalCredit).ToString("C2", CultureInfo.CurrentCulture);
                e.Row.Cells[11].Text = payoutList.Sum(x => x.TotalDebit).ToString("C2", CultureInfo.CurrentCulture);
                e.Row.Cells[12].Text = payoutList.Sum(x => x.Balance).ToString("C2", CultureInfo.CurrentCulture);
            }
        }
    }
}