﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using ViewModels;

namespace mlm_web
{
    public partial class MyPayoutReport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    var list = BusinessLogic.Payout.GetAchievedLevel(CurrentDomain.Session.CurrentCustomer);
                    var stages = new List<LevelViewModel>();
                    foreach (var item in list)
                    {
                        var stage = new LevelViewModel();
                        stage.Id = item;
                        stage.Name = "Level " + item.ToString();

                        stages.Add(stage);

                    }

                    Utility.Binder.BindDropDownList(ddlLevel, stages, "Id", "Name", "0", "--Select Level--");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
                }
            }

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime payoutFromDate = Convert.ToDateTime(txtFromDate.Text);
                DateTime payoutToDate = Convert.ToDateTime(txtToDate.Text);
                int level = Convert.ToInt32(ddlLevel.SelectedValue);

                DataTable payoutList = BusinessLogic.Payout.GetPayouts(payoutFromDate, payoutToDate, CurrentDomain.Session.CurrentCustomer, level);

                GridView1.DataSource = payoutList;
                GridView1.DataBind();
                ltPayoutCount.Text = payoutList.Rows.Count.ToString();
                divPayouts.Visible = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }
    }
}