﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControl_AjaxLoader"
    CodeBehind="AjaxLoader.ascx.cs" %>
<div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #FFFFFF; opacity: 0.7;">
    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/images/square.gif"
        AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 45%;" />
</div>
