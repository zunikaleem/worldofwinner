﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="mlm_web.UserControls.Menu" %>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><%=CurrentDomain.Session.CurrentSystemUser.UserID%></p>
                <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%if (CurrentDomain.Session.CurrentSystemUser.AccessGroup.AccessGroupName == mlm_lib.Shared.AccessGroupConstant.Customer)
                { %>
            <li class=" treeview" id="mybusinessmenu" runat="server">
                <a href="#">
                    <i class="fa fa-briefcase"></i><span><%=Resources.LocalizedText.MyBusiness %></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="CustomerDashboard.aspx"><i class="fa fa-dashboard"></i><%=Resources.LocalizedText.Dashboard %></a></li>
                    <li><a href="MyTree.aspx"><i class="fa fa-tree"></i><%=Resources.LocalizedText.MyTree %></a></li>
                    <li><a href="MyDownlineRegistration.aspx"><i class="fa fa-user"></i><%=Resources.LocalizedText.AddMember %></a></li>
                    <li><a href="MyDownline.aspx"><i class="fa fa-users"></i><%=Resources.LocalizedText.MyDownline %></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-google-wallet"></i><span><%=Resources.LocalizedText.EPINWallet %></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <%--<li><a href="RequestEPIN.aspx"><i class="fa fa-edit "></i><%=Resources.LocalizedText.RequestEPIN %></a></li>--%>
                            <li><a href="EPINWallet.aspx"><i class="fa fa-get-pocket"></i><%=Resources.LocalizedText.EPINInbox %></a></li>
                            <li><a href="SendEPIN.aspx"><i class="fa  fa-send"></i><%=Resources.LocalizedText.SendEPINs %></a></li>

                        </ul>
                    </li>
                    <li><a href="MyLedger.aspx"><i class="fa fa-rupee"></i><%=Resources.LocalizedText.MyLedger %></a></li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-tasks"></i><span><%=Resources.LocalizedText.Reports %></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="MyPayoutReport.aspx"><i class="fa fa-hospital-o"></i><%=Resources.LocalizedText.MyPayoutReport %></a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="treeview" id="profilemenu" runat="server">
                <a href="#">
                    <i class="fa fa-user"></i><span><%=Resources.LocalizedText.Profile %></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="UpdateProfile.aspx"><i class="fa fa-file"></i><%=Resources.LocalizedText.UpdateProfile %></a></li>
                    <li><a href="WelcomeLetter.aspx" target="_blank"><i class="fa fa-file"></i><%=Resources.LocalizedText.WelcomeLetter %></a></li>
                    <li><a href="ChangePassword.aspx"><i class="fa fa-lock"></i><%=Resources.LocalizedText.ChangePassword %></a></li>
                </ul>
            </li>
            <%} %>
            <%if (CurrentDomain.Session.CurrentSystemUser.AccessGroup.AccessGroupName == mlm_lib.Shared.AccessGroupConstant.SuperAdim)
                { %>
            <li class="treeview" id="adminmenu" runat="server">
                <a href="#">
                    <i class="fa fa-wrench"></i><span><%=Resources.LocalizedText.Admin %></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i><%=Resources.LocalizedText.AdminDashboard%></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gear"></i><span><%=Resources.LocalizedText.Configuration %></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="Packages.aspx"><i class="fa fa-tty"></i><%=Resources.LocalizedText.Packages %></a></li>
                        </ul>
                    </li>

                    <li><a href="EPIN.aspx"><i class="fa fa-plug"></i><%=Resources.LocalizedText.GenerateEPIN %></a></li>
                    <li><a href="EPINWallet.aspx"><i class="fa fa-get-pocket"></i><%=Resources.LocalizedText.EPINInbox %></a></li>
                            <li><a href="SendEPIN.aspx"><i class="fa  fa-send"></i><%=Resources.LocalizedText.SendEPINs %></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-rupee"></i><span><%=Resources.LocalizedText.Payout %></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="PayoutList.aspx"><i class="fa fa-money"></i><%=Resources.LocalizedText.PayoutList %></a></li>
                            <li><a href="DistributePayout.aspx"><i class="fa fa-area-chart"></i><%=Resources.LocalizedText.DistributePayout %></a></li>
                        </ul>
                    </li>
                    <%--<li><a href="EPINRequest.aspx"><i class="fa  fa-inbox"></i><%=Resources.LocalizedText.EPINRequest %></a></li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-user-plus"></i><span><%=Resources.LocalizedText.CustomerManagement %></span>
                            <span class="pull-right-cont
                                ainer">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="EditCustomer.aspx"><i class="fa  fa-clone"></i><%=Resources.LocalizedText.EditCustomer %></a></li>
                            <li><a href="MemberApproval.aspx"><i class="fa fa-plug"></i><%=Resources.LocalizedText.MemberApproval%></a></li>
                            <li><a href="JoinedCustomers.aspx"><i class="fa fa-sitemap"></i><%=Resources.LocalizedText.JoinedCustomers %></a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-columns"></i><span><%=Resources.LocalizedText.Reports %></span>
                            <span class="pull-right-cont
                                ainer">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="StatusReport.aspx"><i class="fa fa-calculator"></i><%=Resources.LocalizedText.StatusReport %></a></li>
                            <li><a href="GSTReport.aspx"><i class="fa  fa-clone"></i><%=Resources.LocalizedText.GSTReport %></a></li>
                            <li><a href="TDSReport.aspx"><i class="fa  fa-check-square-o"></i><%=Resources.LocalizedText.TDSReport %></a></li>
                            <li><a href="PayoutReport.aspx"><i class="fa fa-hospital-o"></i><%=Resources.LocalizedText.PayoutReport %></a></li>
                            <li><a href="LevelwisePayoutReport.aspx"><i class="fa fa-hospital-o"></i><%=Resources.LocalizedText.LevelwisePayout %></a></li>
                            <li><a href="LevelwiseReportByPAN.aspx"><i class="fa fa-hospital-o"></i><%=Resources.LocalizedText.LevelwisePayoutPAN %></a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <%} %>

            <%if (CurrentDomain.Session.CurrentSystemUser.AccessGroup.AccessGroupName == mlm_lib.Shared.AccessGroupConstant.CEOCustomer || 
                    CurrentDomain.Session.CurrentSystemUser.AccessGroup.AccessGroupName == mlm_lib.Shared.AccessGroupConstant.B2BCustomer)
                { %>
            <li class="treeview" id="Li1" runat="server">
                <a href="#">
                    <i class="fa fa-wrench"></i><span>My Business</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="UpdateProfile.aspx"><i class="fa fa-file"></i><%=Resources.LocalizedText.UpdateProfile %></a></li>
                    <li><a href="MyPayout.aspx"><i class="fa fa-hospital-o"></i><%=Resources.LocalizedText.MyPayoutReport %></a></li>
                    <li><a href="MyLedger.aspx"><i class="fa fa-rupee"></i><%=Resources.LocalizedText.MyLedger %></a></li>
                </ul>
            </li>
            <%} %>

            <%if (CurrentDomain.Session.CurrentSystemUser.AccessGroup.AccessGroupName == mlm_lib.Shared.AccessGroupConstant.CEOOwner
                    || CurrentDomain.Session.CurrentSystemUser.AccessGroup.AccessGroupName  == mlm_lib.Shared.AccessGroupConstant.B2BOwner)
                { %>
            <li class="treeview" id="Li2" runat="server">
                <a href="#">
                    <i class="fa fa-wrench"></i><span>My Business</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="UpdateProfile.aspx"><i class="fa fa-file"></i><%=Resources.LocalizedText.UpdateProfile %></a></li>
                    <li><a href="MyLedger.aspx"><i class="fa fa-rupee"></i><%=Resources.LocalizedText.MyLedger %></a></li>
                </ul>
            </li>
            <%} %>

            
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
