﻿<%@ Page Title="Distribute Payout" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DistributePayout.aspx.cs" Inherits="mlm_web.DistributePayout" %>
<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Payout 
       
            <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Admin</a></li>
                    <li><a href="#">Payout</a></li>
                    <li class="active">Distribute Payout</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search Form</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>User ID </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <asp:TextBox ID="txtUserID" runat="server" CssClass="form-control" placeholder="Enter userid"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <asp:Button ID="btnShow" runat="server" Text="Show Payout" CssClass="btn btn-info pull-right" OnClick="btnShow_Click" />
                    </div>
                </div>


                <div class="box box-info" id="divPayouts" runat="server" visible="false">
                    <div class="box-header with-border">
                        <h3 class="box-title">User Transactions</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="box-body form-horizontal">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True">
                                <Columns>
                                    <asp:BoundField DataField="UpdateDate" HeaderText="Transaction Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}" />
                                    <asp:BoundField DataField="SystemUserID" HeaderText="User ID" />
                                    <asp:BoundField DataField="ContactName" HeaderText="Contact Name" />
                                    <asp:BoundField DataField="Narration" HeaderText="Comment" />
                                    <asp:BoundField DataField="CreditAmount" HeaderText="Credit" DataFormatString="{0:0.00}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="DebitAmount" HeaderText="Debit" DataFormatString="{0:0.00}"   HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TransactionType" HeaderText ="TransactionType" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

                <div class="box box-info" id="divTransferFund" runat="server" visible="false">
                    <div class="box-header with-border">
                        <h3 class="box-title">Transfer Fund</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Account Type</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtAccountType" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Account Number</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtAccountNumber" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Account Holder Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtAccountHolderName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Bank Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtBankName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Branch  Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtBranchName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>IFSC Code</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtIFSCCode" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Amount</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" placeholder="Enter the amount"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Transaction Comment</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-circle"></i>
                                    </div>
                                    <asp:TextBox ID="txtTransactionComment" runat="server" CssClass="form-control" placeholder="Enter the comment"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <asp:Button ID="btnTransfer" runat="server" Text="Transfer Fund" CssClass="btn btn-info pull-right" OnClick="btnTransfer_Click" />
                    </div>
                </div>


            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- Page script -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", { "placeholder": "mm/dd/yyyy" });
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });

        });
    </script>
</asp:Content>
