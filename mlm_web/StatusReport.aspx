﻿<%@ Page Title="Staus Report" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatusReport.aspx.cs" Inherits="mlm_web.StatusReport" %>
<%@ Register Src="~/UserControls/AjaxLoader.ascx" TagName="Loader" TagPrefix="al" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <script>
        $(document).ready(function () {
            $('[id*=btnExport]').on('click', function () {
                ExportToExcel('GridView1');
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <al:Loader ID="Loader1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <h1>Status Report
       
            <small>Preview</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Admin</a></li>
                    <li><a href="#">Payout</a></li>
                    <li class="active">Daily Status Report</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search Form</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>From Date</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFromDate" runat="server" class="form-control" TextMode="Date" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label>To Date</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtToDate" runat="server" class="form-control" TextMode="Date" required></asp:TextBox>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Cancel</button>
                            <asp:Button ID="btnShow" runat="server" Text="Show Payout" CssClass="btn btn-info pull-right" OnClick="btnShow_Click" />
                        </div>
                    </div>
                </div>
                <div class="box box-info" id="divEPINSales" runat="server" visible="false">
                    <div class="box-header with-border">
                        <h3 class="box-title">EPIN Sales</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" id="btnExport"><i class="fa fa-file-excel-o"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>--%>
                    </div>
                    <div class="box-body form-horizontal">
                        <div class="table-responsive">
                            <asp:GridView ID="gvEPINSales" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AutoGenerateColumns="False" ShowFooter="True">
                                <Columns>
                                    <asp:BoundField DataField="PackageName" HeaderText="Package" />
                                    <asp:BoundField DataField="Cost" HeaderText="Cost" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TotalSale" HeaderText="Total Sale" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="box box-info" id="divReservedPayout" runat="server" visible="false">
                    <div class="box-header with-border">
                        <h3 class="box-title">Reserved Payout</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" id="btnExport"><i class="fa fa-file-excel-o"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>--%>
                    </div>
                    <div class="box-body form-horizontal">
                        <div class="table-responsive">
                            <asp:GridView ID="gvReservedPayout" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AutoGenerateColumns="False" ShowFooter="True">
                                <Columns>
                                    <asp:BoundField DataField="CustomerUserID" HeaderText="User ID" />
                                    <asp:BoundField DataField="GrossAmount" HeaderText="Gross Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TDS" HeaderText="TDS %" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TDSAmount" HeaderText="TDS Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge %" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ServiceChargeAmount" HeaderText="Service Charge Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="NetAmount" HeaderText="Net Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="box box-info" id="divRegularPayout" runat="server" visible="false">
                    <div class="box-header with-border">
                        <h3 class="box-title">Regular Payout</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" id="btnExport"><i class="fa fa-file-excel-o"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>--%>
                    </div>
                    <div class="box-body form-horizontal">
                        <div class="table-responsive">
                            <asp:GridView ID="gvRegularPayout" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AutoGenerateColumns="False" ShowFooter="True">
                                <Columns>
                                    <asp:BoundField DataField="UserID" HeaderText="User ID" />
                                    <asp:BoundField DataField="GrossAmount" HeaderText="Gross Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TDS" HeaderText="TDS %" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TDSAmount" HeaderText="TDS Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge %" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ServiceChargeAmount" HeaderText="Service Charge Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="NetAmount" HeaderText="Net Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TotalPaid" HeaderText="Total Paid" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Balance" HeaderText="Balance" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="box box-info" id="divFixedPayout" runat="server" visible="false">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fixed Payout</h3>
                        <%--<div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" id="btnExport"><i class="fa fa-file-excel-o"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>--%>
                    </div>
                    <div class="box-body form-horizontal">
                        <div class="table-responsive">
                            <asp:GridView ID="gvFixedPayout" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;" GridLines="None" AutoGenerateColumns="False" ShowFooter="True">
                                <Columns>
                                    <asp:BoundField DataField="UserID" HeaderText="User ID" />
                                    <asp:BoundField DataField="GrossAmount" HeaderText="Gross Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TDS" HeaderText="TDS %" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TDSAmount" HeaderText="TDS Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge %" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="ServiceChargeAmount" HeaderText="Service Charge Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="NetAmount" HeaderText="Net Amount" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="TotalPaid" HeaderText="Total Paid" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Balance" HeaderText="Balance" FooterStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-right" ItemStyle-HorizontalAlign="Right" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            $('[id*=btnExport]').on('click', function () {

                ExportToExcel('GridView1');
            });
        });

        function ExportToExcel(Id) {
            var tab_text = "<table border='2px'><tr>";
            var textRange;
            var j = 0;
            tab = document.getElementById(Id);
            var headerRow = $('[id*=GridView1] tr:first');
            tab_text += headerRow.html() + '</tr><tr>';
            var rows = $('[id*=GridView1] tr:not(:has(th))');
            for (j = 0; j < rows.length; j++) {
                if ($(rows[j]).css('display') != 'none') {
                    tab_text = tab_text + rows[j].innerHTML + "</tr>";
                }
            }
            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, Id + ".xls");
            }
            else {                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            }
            return (sa);
        }
    </script>
</asp:Content>
