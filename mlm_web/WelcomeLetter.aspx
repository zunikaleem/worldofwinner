﻿<%@ Page Title="Welcome Letter" Language="C#" AutoEventWireup="true" CodeBehind="WelcomeLetter.aspx.cs" Inherits="mlm_web.WelcomeLetter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Welcome Letter</title>
  <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="fonts/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="fonts/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css" />

    <style type="text/css">
        td
        {
            
            border-right : solid 1px black;
            border-bottom: solid 1px black;
            padding-left:10px;
            empty-cells:show;
            
        }
        table
        {
        	border-top: solid 1px black;
        	border-left : solid 1px black;
        	empty-cells:show;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-color: White">
        <table cellspacing="0" style="empty-cells:show">
            <tr>
                <td colspan="4" height="50">
                    <center><h2> <asp:Label ID="Label3" runat="server" Text="<%$ Resources:LocalizedText, CompanyName %>"></asp:Label></h2></center>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Customer Name" Style="font-weight: 700"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="User ID" Style="font-weight: 700"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblUserID" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                        Text="Registration Date"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblRegistrationDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label13" runat="server" style="font-weight: 700" 
                        Text="Date Of Birth"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblBirthDate" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Address" Style="font-weight: 700"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" style="font-weight: 700" Text="City"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label11" runat="server" style="font-weight: 700" Text="State"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblState" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label15" runat="server" style="font-weight: 700" 
                        Text="PAN Number"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblPANNumber" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label23" runat="server" style="font-weight: 700" Text="Email ID"></asp:Label>
                </td>
       
                <td>
                    <asp:Label ID="lblEmailID" runat="server"></asp:Label>
                </td>
       
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label16" runat="server" style="font-weight: 700" 
                        Text="Sponsored User ID"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblSponsorUserID" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label14" runat="server" style="font-weight: 700" 
                        Text="Sponsored By"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblSponsoredBy" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label24" runat="server" style="font-weight: 700" 
                        Text="Nominee Name"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblNomineeName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label25" runat="server" style="font-weight: 700" 
                        Text="Nominee Relationship"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblNomineeRelationship" runat="server"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td colspan="4">
                    
                    <br />
                    Dear Business Partner, Partner to introduce as many new Business Partners. 
                    <br />
                    Our Business Partners shall
                    be immensely benefited from the multiplier effect while introducing new Business
                    Partners. The Business Partners enrolled with <asp:Label ID="Label6" runat="server" Text="<%$ Resources:LocalizedText, CompanyName %>"></asp:Label>
                    can always recruit as many new Business Partners for expanding his network will
                    naturally help grow the Business Partners’ income multiplied several times. <b>
                    <br />
                    OUR EFFORT
                        IS TO MAKE TWO WINNERS</b>
                    <br />
                    We sell with an attitude to make two winners. A successful buyer and a satisfied
                    seller as network marketing are all about multiplication of business opportunities
                    through power of duplication. 
                    <br />
                    <br />
                                        Winning Regards, 
                    <br />
                    <br />
                    <b><asp:Label ID="Label4" runat="server" Text="<%$ Resources:LocalizedText, CompanyName %>"></asp:Label></b>
                    <br />
                    
                    <asp:Label ID="lblToday" runat="server"></asp:Label>
                </td>
            </tr>

        </table>
        
        <br />
        <center><input type="button" value="Print" onclick="javascript: window.print()" /></center>
    </div>
    </form>
</body>
</html>
