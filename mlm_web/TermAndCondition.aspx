﻿<%@ Page Title="Terms And Conditions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TermAndCondition.aspx.cs" Inherits="mlm_web.TermAndCondition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>
    <section class="content-header">
        <h1>Terms and Conditions
            <small>Read Only</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Term and Condition</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Terms and Conditions</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <ol>
                        <li>Dear Diamonds, this is our *Pre-Launching* phase.</li>
                        <li>Please maintain your diary, and keep a tracking record copy of all your data.</li>
                        <li>There will be update phase through-out the month.</li>
                    </ol>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-header with-border">
                <h3 class="box-title">नियम आणि अटी</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <ol>
                        <li>प्रिय डायमंड, हा आमचा * प्री-लॉन्चिंग * क्षण आहे।</li>
                        <li>कृपया आपली डायरी ठेवा, आपल्या सर्व डेटाची ट्रॅकिंग रेकॉर्ड ठेवा।</li>
                        <li>महिनाभर अद्यतनित होईल।</li>
                    </ol>
                </div>

            </div>
            <!-- /.box-body -->

              <div class="box-header with-border">
                <h3 class="box-title">नियम और शर्तें</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <ol>
                        <li>डियर डायमंड, यह हमारा * प्री-लॉन्चिंग * चरण है।</li>
                        <li>कृपया अपनी डायरी बनाए रखें, अपने सभी डेटा की एक ट्रैकिंग रिकॉर्ड कॉपी रखें।</li>
                        <li>पूरे महीने अपडेट चरण रहेगा।</li>
                    </ol>
                </div>
            </div>
            <div class="box-footer">
                <asp:Button ID="btnAccept" runat="server" Text="Accept" CssClass="btn btn-info pull-right" OnClick="btnAccept_Click" />
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
