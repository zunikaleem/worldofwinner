﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace mlm_web
{
    public partial class MemberApproval : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindGrid()
        {
            List<BusinessLogic.Customer> pendingCustomer = BusinessLogic.Customer.GetPendingApprovalCustomers();
            GridView1.DataSource = pendingCustomer.OrderBy(o => o.RegistrationDate).OrderByDescending(x => x.SystemUser.PAN);
            GridView1.DataBind();
        }
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                Button button = sender as Button;
                GridViewRow row = button.NamingContainer as GridViewRow;
                int customerID = Convert.ToInt32((row.FindControl("lblgCustomerID") as Label).Text.Replace("'", ""));

                int x = BusinessLogic.Customer.ApproveCustomer(customerID, CurrentDomain.Session.CurrentCustomer);

                if (x > 0)
                {
                    MessageBox.Show(UpdatePanel1, "Customer approved successfully.", MessageType.Success);
                }
                else
                {
                    MessageBox.Show(UpdatePanel1, "Failed to approve customer.", MessageType.Info);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
            finally
            {
                BindGrid();
            }
        }
    }
}