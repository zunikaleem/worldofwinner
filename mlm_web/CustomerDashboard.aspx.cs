﻿using mlm_lib.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class CustomerDashboard : Page
    {
        List<BusinessLogic.Customer> downLine = new List<BusinessLogic.Customer>();
        List<BusinessLogic.Payout> payouts = new List<BusinessLogic.Payout>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //     (Master.FindControl("mybusinessmenu") as HtmlGenericControl).Attributes.Add("class", "treeview active");
                    BindDownline();

                    CurrentDomain.Session.MyAccount = CurrentDomain.Session.CurrentCustomer.SystemUser
                        .GetAccounts()
                        .Where(x => x.TransactionType != TransactionType.FixedCommissionEarned).ToList();

                    float credit = Convert.ToSingle(CurrentDomain.Session.MyAccount.Sum(o => o.CreditAmount));
                    float debit = Convert.ToSingle(CurrentDomain.Session.MyAccount.Sum(o => o.DebitAmount));
                    float balance = (credit - debit);

                    ltLedgerBalance.Text = balance.ToString("n2");

                    gvAccount.DataSource = CurrentDomain.Session.MyAccount.OrderByDescending(o => o.UpdateBy).ToList();
                    gvAccount.DataBind();


                    payouts = CurrentDomain.Session.CurrentCustomer.MyPayouts().OrderByDescending(p => p.PayoutDate).ToList();
                    gvPayout.DataSource = payouts;
                    gvPayout.DataBind();

                    string epinCount = CurrentDomain.Session.CurrentCustomer.GetUnusedEPINInbox().Count.ToString();

                    ltEPINBalance.Text = epinCount;

                    List<BusinessLogic.Customer> wholeDownline = CurrentDomain.Session.CurrentCustomer.MyDownline();
                    ltCompleteDownline.Text = wholeDownline.Count.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, MessageType.Error);
            }
        }

        private void BindDownline()
        {
            downLine = CurrentDomain.Session.CurrentCustomer.MyDownLine().OrderByDescending(x => x.CustomerID).ToList();
            GridView1.DataSource = downLine;
            GridView1.DataBind();
            ltDirectSponsor.Text = downLine.Count().ToString();
            ltSponsorCount.Text = downLine.Count().ToString();
        }

        protected void gvAccount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                float credit = Convert.ToSingle(CurrentDomain.Session.MyAccount.Sum(o => o.CreditAmount));
                float debit = Convert.ToSingle(CurrentDomain.Session.MyAccount.Sum(o => o.DebitAmount));
                float balance = (credit - debit);

                e.Row.Cells[2].Text = credit.ToString("n2");
                e.Row.Cells[3].Text = debit.ToString("n2");
                e.Row.Cells[4].Text = "Balance : " + balance.ToString("n2");
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[1].Text == TransactionType.ReservedCommissionEarned)
                {
                    e.Row.BackColor = System.Drawing.Color.Pink;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void gvPayout_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[5].Text += "%";
                e.Row.Cells[6].Text += "%";

                if (e.Row.Cells[1].Text == PayoutType.Reserved)
                {
                    e.Row.BackColor = System.Drawing.Color.Pink;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                float netAmount = Convert.ToSingle(payouts.Sum(o => o.NetAmount));

                e.Row.Cells[7].Text = "Total : " + netAmount.ToString("n2");
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;

            BindDownline();
        }
    }
}