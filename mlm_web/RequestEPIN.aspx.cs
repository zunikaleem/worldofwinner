﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mlm_web
{
    public partial class RequestEPIN : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    BindPackage();
                    BindRequest();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }
        }

        private void BindPackage()
        {
            try
            {
                Utility.Binder.BindDropDownList(ddlPackage, BusinessLogic.Package.getPackages(), "PackageID", "PackageName", "", "--Select--");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindRequest()
        {
            try
            {
                List<BusinessLogic.EPINRequest> allRequest = CurrentDomain.Session.CurrentCustomer.MyEPINRequest().OrderByDescending(o => o.RequestDate).ToList();

                GridView1.DataSource = allRequest;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatus_grid = e.Row.FindControl("lblgStatus") as Label;
                if (lblStatus_grid != null)
                {
                    switch (lblStatus_grid.Text)
                    {
                        case "Transferred":
                            lblStatus_grid.Attributes.Add("class", "label label-success");
                            break;
                        case "Rejected":
                            lblStatus_grid.Attributes.Add("class", "label label-danger");
                            break;
                        case "":
                            lblStatus_grid.Attributes.Add("class", "label label-info");
                            lblStatus_grid.Text = "Request in queue";
                            break;
                        default:
                            lblStatus_grid.Attributes.Add("class", "label label-default");
                            break;
                    }
                }
            }
        }

        protected void btnRequest_Click1(object sender, EventArgs e)
        {
            try
            {
                if (fuTransaction.HasFile)
                {

                    string folderPath = Server.MapPath("~/UploadedTransactions/");

                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }

                    //SaveOnFTP();

                    BusinessLogic.Package package = BusinessLogic.Package.getPackage(Convert.ToInt32(ddlPackage.SelectedItem.Value));
                    int quantity = Convert.ToInt32(txtQuantity.Text);
                    string comment = txtComment.Text;
                    string transactionPath = fuTransaction.FileName;

                    int x = CurrentDomain.Session.CurrentSystemUser.RequestEPIN(package, quantity, transactionPath, comment);

                    if (x > 0)
                    {
                        ShowMessage("Your request raised successfully.", MessageType.Success);
                        BindRequest();
                    }
                    else
                    {
                        ShowMessage("Failed to raised your request at this time, please try again later.", MessageType.Info);
                    }
                }
                else
                {

                    ShowMessage("Please upload the transaction receiipt.", MessageType.Info);
                }

            }
            catch (Exception ex)
            {

                ShowMessage(ex.Message, MessageType.Error);
            }

        }

        private void ShowMessage(string message, MessageType type)
        {
            divStatus.Visible = true;
            divStatus.InnerText = message;
            divStatus.Attributes.Remove("class");
            switch (type)
            {
                case MessageType.Error:
                    divStatus.Attributes.Add("class", "alert alert-danger alert-dismissible form-group col-md-12");
                    divStatus.InnerHtml = "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
                                 "<h4><i class='icon fa fa-ban'></i>Error</h4>" + message;
                    break;
                case MessageType.Success:
                    divStatus.Attributes.Add("class", "alert alert-success alert-dismissible form-group col-md-12");
                    divStatus.InnerHtml = "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
                                 "<h4><i class='icon fa fa-check'></i>Success</h4>" + message;

                    break;
                default:
                    break;
            }
        }

        private void SaveOnFTP()
        {
            //FTP Server URL.
            string ftp = "ftp://admin.dreamachiver.com/";

            //FTP Folder name. Leave blank if you want to upload to root folder.
            string ftpFolder = "UploadedTransactions/";

            byte[] fileBytes = null;

            //Read the FileName and convert it to Byte array.
            string fileName = Path.GetFileName(fuTransaction.FileName);
            using (StreamReader fileStream = new StreamReader(fuTransaction.PostedFile.InputStream))
            {
                fileBytes = Encoding.UTF8.GetBytes(fileStream.ReadToEnd());
                fileStream.Close();
            }

            try
            {
                //Create FTP Request.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp + ftpFolder + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;

                //Enter FTP Server credentials.
                request.Credentials = new NetworkCredential("join.dream", "Rxm03!l9");
                request.ContentLength = fileBytes.Length;
                request.UsePassive = false;
                request.UseBinary = true;
                request.ServicePoint.ConnectionLimit = fileBytes.Length;
                request.EnableSsl = false;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileBytes, 0, fileBytes.Length);
                    requestStream.Close();
                }

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();


                response.Close();
            }
            catch (WebException ex)
            {
                throw new Exception((ex.Response as FtpWebResponse).StatusDescription);
            }
        }

        protected void hlbTransactionReceiipt_Click(object sender, EventArgs e)
        {
            string path = "UploadedTransactions" + "\\" + (sender as LinkButton).Text;
            imgReceipt.ImageUrl = path;
            string script = "javascript:$('#modalReceipt').modal('show');";
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "comment", script, true);
        }
    }
}