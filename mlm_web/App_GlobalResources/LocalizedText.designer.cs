//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class LocalizedText {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LocalizedText() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.LocalizedText", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Member.
        /// </summary>
        internal static string AddMember {
            get {
                return ResourceManager.GetString("AddMember", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin.
        /// </summary>
        internal static string Admin {
            get {
                return ResourceManager.GetString("Admin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin Dashboard.
        /// </summary>
        internal static string AdminDashboard {
            get {
                return ResourceManager.GetString("AdminDashboard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Password.
        /// </summary>
        internal static string ChangePassword {
            get {
                return ResourceManager.GetString("ChangePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Royal Bloom.
        /// </summary>
        internal static string CompanyName {
            get {
                return ResourceManager.GetString("CompanyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to https://diamond4u.trade/.
        /// </summary>
        internal static string CompanyURL {
            get {
                return ResourceManager.GetString("CompanyURL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configuration.
        /// </summary>
        internal static string Configuration {
            get {
                return ResourceManager.GetString("Configuration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer Management.
        /// </summary>
        internal static string CustomerManagement {
            get {
                return ResourceManager.GetString("CustomerManagement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dashboard.
        /// </summary>
        internal static string Dashboard {
            get {
                return ResourceManager.GetString("Dashboard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Distribute Payout.
        /// </summary>
        internal static string DistributePayout {
            get {
                return ResourceManager.GetString("DistributePayout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Customer.
        /// </summary>
        internal static string EditCustomer {
            get {
                return ResourceManager.GetString("EditCustomer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EPIN Inbox.
        /// </summary>
        internal static string EPINInbox {
            get {
                return ResourceManager.GetString("EPINInbox", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EPIN Request.
        /// </summary>
        internal static string EPINRequest {
            get {
                return ResourceManager.GetString("EPINRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EPIN Wallet.
        /// </summary>
        internal static string EPINWallet {
            get {
                return ResourceManager.GetString("EPINWallet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate EPIN.
        /// </summary>
        internal static string GenerateEPIN {
            get {
                return ResourceManager.GetString("GenerateEPIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate Payout.
        /// </summary>
        internal static string GeneratePayout {
            get {
                return ResourceManager.GetString("GeneratePayout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GST Report.
        /// </summary>
        internal static string GSTReport {
            get {
                return ResourceManager.GetString("GSTReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Joined Customers.
        /// </summary>
        internal static string JoinedCustomers {
            get {
                return ResourceManager.GetString("JoinedCustomers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Levelwise Payout.
        /// </summary>
        internal static string LevelwisePayout {
            get {
                return ResourceManager.GetString("LevelwisePayout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Levelwise Payout : PAN.
        /// </summary>
        internal static string LevelwisePayoutPAN {
            get {
                return ResourceManager.GetString("LevelwisePayoutPAN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Member Approval.
        /// </summary>
        internal static string MemberApproval {
            get {
                return ResourceManager.GetString("MemberApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Business.
        /// </summary>
        internal static string MyBusiness {
            get {
                return ResourceManager.GetString("MyBusiness", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Downline.
        /// </summary>
        internal static string MyDownline {
            get {
                return ResourceManager.GetString("MyDownline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MyLedger.
        /// </summary>
        internal static string MyLedger {
            get {
                return ResourceManager.GetString("MyLedger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Payout Report.
        /// </summary>
        internal static string MyPayoutReport {
            get {
                return ResourceManager.GetString("MyPayoutReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Tree.
        /// </summary>
        internal static string MyTree {
            get {
                return ResourceManager.GetString("MyTree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Packages.
        /// </summary>
        internal static string Packages {
            get {
                return ResourceManager.GetString("Packages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payout.
        /// </summary>
        internal static string Payout {
            get {
                return ResourceManager.GetString("Payout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payout List.
        /// </summary>
        internal static string PayoutList {
            get {
                return ResourceManager.GetString("PayoutList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payout Report.
        /// </summary>
        internal static string PayoutReport {
            get {
                return ResourceManager.GetString("PayoutReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile.
        /// </summary>
        internal static string Profile {
            get {
                return ResourceManager.GetString("Profile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reports.
        /// </summary>
        internal static string Reports {
            get {
                return ResourceManager.GetString("Reports", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request EPIN.
        /// </summary>
        internal static string RequestEPIN {
            get {
                return ResourceManager.GetString("RequestEPIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send EPINs.
        /// </summary>
        internal static string SendEPINs {
            get {
                return ResourceManager.GetString("SendEPINs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DT.
        /// </summary>
        internal static string ShortCompanyName {
            get {
                return ResourceManager.GetString("ShortCompanyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status Report.
        /// </summary>
        internal static string StatusReport {
            get {
                return ResourceManager.GetString("StatusReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Super Admin.
        /// </summary>
        internal static string SuperAdmin {
            get {
                return ResourceManager.GetString("SuperAdmin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to TDS Report.
        /// </summary>
        internal static string TDSReport {
            get {
                return ResourceManager.GetString("TDSReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transferred EPINs.
        /// </summary>
        internal static string TransferredEPINs {
            get {
                return ResourceManager.GetString("TransferredEPINs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Profile.
        /// </summary>
        internal static string UpdateProfile {
            get {
                return ResourceManager.GetString("UpdateProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome Letter.
        /// </summary>
        internal static string WelcomeLetter {
            get {
                return ResourceManager.GetString("WelcomeLetter", resourceCulture);
            }
        }
    }
}
