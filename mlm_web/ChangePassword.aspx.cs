﻿using System;
using System.Web.UI;

namespace mlm_web
{
    public partial class ChangePassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNewPassword.Text.Equals(txtConfirmPassword.Text))
                {
                    int x = CurrentDomain.Session.CurrentCustomer.ChangePassword(CurrentDomain.Session.CurrentCustomer.SystemUser.UserPassword, txtNewPassword.Text, txtConfirmPassword.Text);

                    if (x > 0)
                    {
                        MessageBox.Show(UpdatePanel1, "Password changed successfully.", MessageType.Success);
                    }
                    else
                    {
                        MessageBox.Show(UpdatePanel1, "Failed to update the password", MessageType.Info);
                    }
                }
                else
                {
                    throw new Exception("Password doesnt matched. Please try again.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(UpdatePanel1, ex.Message, MessageType.Error);
            }

        }
    }
}