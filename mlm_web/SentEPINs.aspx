﻿<%@ Page Title="Transferred EPINs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SentEPINs.aspx.cs" Inherits="mlm_web.SentEPINs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>EPIN Wallet
       
            <small>Transferred EPINs</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">My Business</a></li>
            <li><a href="#">EPIN Wallet</a></li>
            <li class="active">Transferred list</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Transferred EPIN List</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridView2" runat="server" CssClass="table table-bordered table-striped" Style="overflow-x: auto; white-space: nowrap;"  GridLines="None"  AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="PackageName" HeaderText="Package"></asp:BoundField>
                                    <asp:BoundField DataField="EPINNumber" HeaderText="EPIN Number"></asp:BoundField>
                                    <asp:BoundField DataField="TranferTo" HeaderText="Transfer To"></asp:BoundField>
                                    <asp:BoundField DataField="UpdateDate" HeaderText="Sent Date" DataFormatString="{0:dd/MM/yy hh:mm:ss}"></asp:BoundField>
                                </Columns>

                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Right" />
                            </asp:GridView>

                </div>
                <!-- /.table-responsive -->
            </div>

            

        </div>
    </section>

    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderFooter" runat="server">
</asp:Content>
