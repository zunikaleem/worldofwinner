﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mlm_lib.Shared
{
    public static class AssemblyInfo
    {
        public static string Version()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            return version;
        }
    }
}
