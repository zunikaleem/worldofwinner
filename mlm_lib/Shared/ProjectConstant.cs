﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mlm_lib.Shared
{
    public static class ProjectConstant
    {
        public const string TDS = "TDS";
        public const string ServiceCharge = "ServiceCharge";
        public const string SendSmsOnJoining = "SendSmsOnJoining";
        public const string BaseUserID = "BaseUserID";
        public const string DefaultSystemUserName = "Diamond Sys Diamond";
        public const string BaseUserIDPrefix = "BaseUserIDPrefix";
        public const string CompanyName = "CompanyName";
    }

    public static class AccessGroupConstant
    {
        public const string SuperAdim = "Super Admin"; 
        public const string Customer = "Customer"; 
        public const string B2BCustomer = "B2B Customer";
        public const string B2BOwner = "B2B Owner";
        public const string CEOOwner = "CEO Owner";
        public const string CEOCustomer = "CEO Customer";
    }

    public static class TransactionType
    {
        /// <summary>
        /// When company credited by selling product
        /// </summary>
        public const string ProductSold = "PRODUCT_SOLD";
        /// <summary>
        /// When Customer credited by earning commission
        /// </summary>
        public const string CommissionEarned = "COMMISSION_EARNED";
        /// <summary>
        /// When Customer credited by earning fixed commission.
        /// </summary>
        public const string FixedCommissionEarned = "FIXED_COMMISSION_EARNED";
        /// <summary>
        /// When company debited commission
        /// </summary>
        public const string CommissionPaid = "COMMISSION_PAID";
        /// <summary>
        /// When company debited fixed commission
        /// </summary>
        public const string FixedCommissionPaid = "FIXED_COMMISSION_PAID";
        public const string ReservedCommissionEarned = "RESERVED_COMMISSION_EARNED";
        public const string ReservedCommissionPaid = "RESERVED_COMMISSION_PAID";

        /// <summary>
        /// When customer/user account debited by paying amount
        /// </summary>
        public const string CommissionReceived = "COMMISSION_RECEIVED";

        public const string NotApplicable = "NOT_APPLICABLE";
    }

    public static class PayoutType
    {
        public const string Regular = "REGULAR";
        public const string Reserved = "RESERVED";
        public const string NotApplicable = "NOT_APPLICABLE";
    }
}
