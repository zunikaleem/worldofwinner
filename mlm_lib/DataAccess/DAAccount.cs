using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAAccount:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.Account>
	{
		public DAAccount(){}
		public DAAccount(Database database):base(database){}
		public DAAccount(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Accounts");
				InitializeParameter("@AccountID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Accounts",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("Accounts");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Accounts",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.Account obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("Accounts");
				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.Narration==null)
					InitializeNullParameter("@Narration",DbType.String);
				else
					InitializeParameter("@Narration",Convert.ToString(obj.Narration),DbType.String);

				if(obj.CreditAmount==null)
					InitializeNullParameter("@CreditAmount",DbType.Single);
				else
					InitializeParameter("@CreditAmount",Convert.ToSingle(obj.CreditAmount),DbType.Single);

				if(obj.DebitAmount==null)
					InitializeNullParameter("@DebitAmount",DbType.Single);
				else
					InitializeParameter("@DebitAmount",Convert.ToSingle(obj.DebitAmount),DbType.Single);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.TransactionType==null)
					InitializeNullParameter("@TransactionType",DbType.String);
				else
					InitializeParameter("@TransactionType",Convert.ToString(obj.TransactionType),DbType.String);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.Account obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("Accounts");
					InitializeParameter("@AccountID",Convert.ToInt32(obj.AccountID),DbType.Int32);

				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.Narration==null)
					InitializeNullParameter("@Narration",DbType.String);
				else
					InitializeParameter("@Narration",Convert.ToString(obj.Narration),DbType.String);

				if(obj.CreditAmount==null)
					InitializeNullParameter("@CreditAmount",DbType.Single);
				else
					InitializeParameter("@CreditAmount",Convert.ToSingle(obj.CreditAmount),DbType.Single);

				if(obj.DebitAmount==null)
					InitializeNullParameter("@DebitAmount",DbType.Single);
				else
					InitializeParameter("@DebitAmount",Convert.ToSingle(obj.DebitAmount),DbType.Single);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.TransactionType==null)
					InitializeNullParameter("@TransactionType",DbType.String);
				else
					InitializeParameter("@TransactionType",Convert.ToString(obj.TransactionType),DbType.String);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.Account obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Accounts");
				InitializeParameter("@AccountID",obj.AccountID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("Accounts");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Accounts",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("Accounts",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("Accounts",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
