using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DACustomer:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.Customer>
	{
		public DACustomer(){}
		public DACustomer(Database database):base(database){}
		public DACustomer(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Customers");
				InitializeParameter("@CustomerID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Customers",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("Customers");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Customers",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.Customer obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("Customers");
				if(obj.Address==null)
					InitializeNullParameter("@Address",DbType.String);
				else
					InitializeParameter("@Address",Convert.ToString(obj.Address),DbType.String);

				if(obj.City==null)
					InitializeNullParameter("@City",DbType.String);
				else
					InitializeParameter("@City",Convert.ToString(obj.City),DbType.String);

				if(obj.State==null)
					InitializeNullParameter("@State",DbType.String);
				else
					InitializeParameter("@State",Convert.ToString(obj.State),DbType.String);

				if(obj.DateOfBirth==null)
					InitializeNullParameter("@DateOfBirth",DbType.DateTime);
				else
					InitializeParameter("@DateOfBirth",Convert.ToDateTime(obj.DateOfBirth),DbType.DateTime);

				if(obj.SponsorID==null)
					InitializeNullParameter("@SponsorID",DbType.Int32);
				else
					InitializeParameter("@SponsorID",Convert.ToInt32(obj.SponsorID),DbType.Int32);

				InitializeParameter("@EPINID",obj.EPIN.EPINID,DbType.Int32);
				if(obj.RegistrationDate==null)
					InitializeNullParameter("@RegistrationDate",DbType.DateTime);
				else
					InitializeParameter("@RegistrationDate",Convert.ToDateTime(obj.RegistrationDate),DbType.DateTime);

				if(obj.Title==null)
					InitializeNullParameter("@Title",DbType.String);
				else
					InitializeParameter("@Title",Convert.ToString(obj.Title),DbType.String);

				if(obj.Gender==null)
					InitializeNullParameter("@Gender",DbType.String);
				else
					InitializeParameter("@Gender",Convert.ToString(obj.Gender),DbType.String);

				if(obj.MaritalStatus==null)
					InitializeNullParameter("@MaritalStatus",DbType.String);
				else
					InitializeParameter("@MaritalStatus",Convert.ToString(obj.MaritalStatus),DbType.String);

				if(obj.OtherContact==null)
					InitializeNullParameter("@OtherContact",DbType.String);
				else
					InitializeParameter("@OtherContact",Convert.ToString(obj.OtherContact),DbType.String);

				if(obj.PostalCode==null)
					InitializeNullParameter("@PostalCode",DbType.String);
				else
					InitializeParameter("@PostalCode",Convert.ToString(obj.PostalCode),DbType.String);

				if(obj.EmailID==null)
					InitializeNullParameter("@EmailID",DbType.String);
				else
					InitializeParameter("@EmailID",Convert.ToString(obj.EmailID),DbType.String);

				if(obj.LockStatus==null)
					InitializeNullParameter("@LockStatus",DbType.Int32);
				else
					InitializeParameter("@LockStatus",Convert.ToInt32(obj.LockStatus),DbType.Int32);

				if(obj.NomineeName==null)
					InitializeNullParameter("@NomineeName",DbType.String);
				else
					InitializeParameter("@NomineeName",Convert.ToString(obj.NomineeName),DbType.String);

				if(obj.Relationship==null)
					InitializeNullParameter("@Relationship",DbType.String);
				else
					InitializeParameter("@Relationship",Convert.ToString(obj.Relationship),DbType.String);

				if(obj.NomineeGender==null)
					InitializeNullParameter("@NomineeGender",DbType.String);
				else
					InitializeParameter("@NomineeGender",Convert.ToString(obj.NomineeGender),DbType.String);

				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.Customer obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("Customers");
					InitializeParameter("@CustomerID",Convert.ToInt32(obj.CustomerID),DbType.Int32);

				if(obj.Address==null)
					InitializeNullParameter("@Address",DbType.String);
				else
					InitializeParameter("@Address",Convert.ToString(obj.Address),DbType.String);

				if(obj.City==null)
					InitializeNullParameter("@City",DbType.String);
				else
					InitializeParameter("@City",Convert.ToString(obj.City),DbType.String);

				if(obj.State==null)
					InitializeNullParameter("@State",DbType.String);
				else
					InitializeParameter("@State",Convert.ToString(obj.State),DbType.String);

				if(obj.DateOfBirth==null)
					InitializeNullParameter("@DateOfBirth",DbType.DateTime);
				else
					InitializeParameter("@DateOfBirth",Convert.ToDateTime(obj.DateOfBirth),DbType.DateTime);

				if(obj.SponsorID==null)
					InitializeNullParameter("@SponsorID",DbType.Int32);
				else
					InitializeParameter("@SponsorID",Convert.ToInt32(obj.SponsorID),DbType.Int32);

				InitializeParameter("@EPINID",obj.EPIN.EPINID,DbType.Int32);
				if(obj.RegistrationDate==null)
					InitializeNullParameter("@RegistrationDate",DbType.DateTime);
				else
					InitializeParameter("@RegistrationDate",Convert.ToDateTime(obj.RegistrationDate),DbType.DateTime);

				if(obj.Title==null)
					InitializeNullParameter("@Title",DbType.String);
				else
					InitializeParameter("@Title",Convert.ToString(obj.Title),DbType.String);

				if(obj.Gender==null)
					InitializeNullParameter("@Gender",DbType.String);
				else
					InitializeParameter("@Gender",Convert.ToString(obj.Gender),DbType.String);

				if(obj.MaritalStatus==null)
					InitializeNullParameter("@MaritalStatus",DbType.String);
				else
					InitializeParameter("@MaritalStatus",Convert.ToString(obj.MaritalStatus),DbType.String);

				if(obj.OtherContact==null)
					InitializeNullParameter("@OtherContact",DbType.String);
				else
					InitializeParameter("@OtherContact",Convert.ToString(obj.OtherContact),DbType.String);

				if(obj.PostalCode==null)
					InitializeNullParameter("@PostalCode",DbType.String);
				else
					InitializeParameter("@PostalCode",Convert.ToString(obj.PostalCode),DbType.String);

				if(obj.EmailID==null)
					InitializeNullParameter("@EmailID",DbType.String);
				else
					InitializeParameter("@EmailID",Convert.ToString(obj.EmailID),DbType.String);

				if(obj.LockStatus==null)
					InitializeNullParameter("@LockStatus",DbType.Int32);
				else
					InitializeParameter("@LockStatus",Convert.ToInt32(obj.LockStatus),DbType.Int32);

				if(obj.NomineeName==null)
					InitializeNullParameter("@NomineeName",DbType.String);
				else
					InitializeParameter("@NomineeName",Convert.ToString(obj.NomineeName),DbType.String);

				if(obj.Relationship==null)
					InitializeNullParameter("@Relationship",DbType.String);
				else
					InitializeParameter("@Relationship",Convert.ToString(obj.Relationship),DbType.String);

				if(obj.NomineeGender==null)
					InitializeNullParameter("@NomineeGender",DbType.String);
				else
					InitializeParameter("@NomineeGender",Convert.ToString(obj.NomineeGender),DbType.String);

				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.Customer obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Customers");
				InitializeParameter("@CustomerID",obj.CustomerID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("Customers");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Customers",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("Customers",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("Customers",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
