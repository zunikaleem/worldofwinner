using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DASystemUser:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.SystemUser>
	{
		public DASystemUser(){}
		public DASystemUser(Database database):base(database){}
		public DASystemUser(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("SystemUsers");
				InitializeParameter("@SystemUserID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("SystemUsers",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("SystemUsers");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("SystemUsers",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.SystemUser obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("SystemUsers");
				InitializeParameter("@AccessGroupID",obj.AccessGroup.AccessGroupID,DbType.Int32);
				if(obj.RegistrationDate==null)
					InitializeNullParameter("@RegistrationDate",DbType.DateTime);
				else
					InitializeParameter("@RegistrationDate",Convert.ToDateTime(obj.RegistrationDate),DbType.DateTime);

				if(obj.UserID==null)
					InitializeNullParameter("@UserID",DbType.String);
				else
					InitializeParameter("@UserID",Convert.ToString(obj.UserID),DbType.String);

				if(obj.UserPassword==null)
					InitializeNullParameter("@UserPassword",DbType.String);
				else
					InitializeParameter("@UserPassword",Convert.ToString(obj.UserPassword),DbType.String);

				if(obj.ContactName==null)
					InitializeNullParameter("@ContactName",DbType.String);
				else
					InitializeParameter("@ContactName",Convert.ToString(obj.ContactName),DbType.String);

				if(obj.MobileNumber==null)
					InitializeNullParameter("@MobileNumber",DbType.String);
				else
					InitializeParameter("@MobileNumber",Convert.ToString(obj.MobileNumber),DbType.String);

				if(obj.PAN==null)
					InitializeNullParameter("@PAN",DbType.String);
				else
					InitializeParameter("@PAN",Convert.ToString(obj.PAN),DbType.String);

				if(obj.BankName==null)
					InitializeNullParameter("@BankName",DbType.String);
				else
					InitializeParameter("@BankName",Convert.ToString(obj.BankName),DbType.String);

				if(obj.BranchName==null)
					InitializeNullParameter("@BranchName",DbType.String);
				else
					InitializeParameter("@BranchName",Convert.ToString(obj.BranchName),DbType.String);

				if(obj.IFSCCode==null)
					InitializeNullParameter("@IFSCCode",DbType.String);
				else
					InitializeParameter("@IFSCCode",Convert.ToString(obj.IFSCCode),DbType.String);

				if(obj.AccountType==null)
					InitializeNullParameter("@AccountType",DbType.String);
				else
					InitializeParameter("@AccountType",Convert.ToString(obj.AccountType),DbType.String);

				if(obj.AccountNumber==null)
					InitializeNullParameter("@AccountNumber",DbType.String);
				else
					InitializeParameter("@AccountNumber",Convert.ToString(obj.AccountNumber),DbType.String);

				if(obj.AccountHolderName==null)
					InitializeNullParameter("@AccountHolderName",DbType.String);
				else
					InitializeParameter("@AccountHolderName",Convert.ToString(obj.AccountHolderName),DbType.String);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.SystemUser obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("SystemUsers");
				InitializeParameter("@AccessGroupID",obj.AccessGroup.AccessGroupID,DbType.Int32);
				if(obj.RegistrationDate==null)
					InitializeNullParameter("@RegistrationDate",DbType.DateTime);
				else
					InitializeParameter("@RegistrationDate",Convert.ToDateTime(obj.RegistrationDate),DbType.DateTime);

					InitializeParameter("@SystemUserID",Convert.ToInt32(obj.SystemUserID),DbType.Int32);

				if(obj.UserID==null)
					InitializeNullParameter("@UserID",DbType.String);
				else
					InitializeParameter("@UserID",Convert.ToString(obj.UserID),DbType.String);

				if(obj.UserPassword==null)
					InitializeNullParameter("@UserPassword",DbType.String);
				else
					InitializeParameter("@UserPassword",Convert.ToString(obj.UserPassword),DbType.String);

				if(obj.ContactName==null)
					InitializeNullParameter("@ContactName",DbType.String);
				else
					InitializeParameter("@ContactName",Convert.ToString(obj.ContactName),DbType.String);

				if(obj.MobileNumber==null)
					InitializeNullParameter("@MobileNumber",DbType.String);
				else
					InitializeParameter("@MobileNumber",Convert.ToString(obj.MobileNumber),DbType.String);

				if(obj.PAN==null)
					InitializeNullParameter("@PAN",DbType.String);
				else
					InitializeParameter("@PAN",Convert.ToString(obj.PAN),DbType.String);

				if(obj.BankName==null)
					InitializeNullParameter("@BankName",DbType.String);
				else
					InitializeParameter("@BankName",Convert.ToString(obj.BankName),DbType.String);

				if(obj.BranchName==null)
					InitializeNullParameter("@BranchName",DbType.String);
				else
					InitializeParameter("@BranchName",Convert.ToString(obj.BranchName),DbType.String);

				if(obj.IFSCCode==null)
					InitializeNullParameter("@IFSCCode",DbType.String);
				else
					InitializeParameter("@IFSCCode",Convert.ToString(obj.IFSCCode),DbType.String);

				if(obj.AccountType==null)
					InitializeNullParameter("@AccountType",DbType.String);
				else
					InitializeParameter("@AccountType",Convert.ToString(obj.AccountType),DbType.String);

				if(obj.AccountNumber==null)
					InitializeNullParameter("@AccountNumber",DbType.String);
				else
					InitializeParameter("@AccountNumber",Convert.ToString(obj.AccountNumber),DbType.String);

				if(obj.AccountHolderName==null)
					InitializeNullParameter("@AccountHolderName",DbType.String);
				else
					InitializeParameter("@AccountHolderName",Convert.ToString(obj.AccountHolderName),DbType.String);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.SystemUser obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("SystemUsers");
				InitializeParameter("@SystemUserID",obj.SystemUserID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("SystemUsers");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("SystemUsers",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("SystemUsers",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("SystemUsers",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
