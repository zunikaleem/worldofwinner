using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAPackage:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.Package>
	{
		public DAPackage(){}
		public DAPackage(Database database):base(database){}
		public DAPackage(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Packages");
				InitializeParameter("@PackageID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Packages",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("Packages");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Packages",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.Package obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("Packages");
				if(obj.PackageName==null)
					InitializeNullParameter("@PackageName",DbType.String);
				else
					InitializeParameter("@PackageName",Convert.ToString(obj.PackageName),DbType.String);

				if(obj.BasePrice==null)
					InitializeNullParameter("@BasePrice",DbType.Single);
				else
					InitializeParameter("@BasePrice",Convert.ToSingle(obj.BasePrice),DbType.Single);

				if(obj.SGST==null)
					InitializeNullParameter("@SGST",DbType.Single);
				else
					InitializeParameter("@SGST",Convert.ToSingle(obj.SGST),DbType.Single);

				if(obj.CGST==null)
					InitializeNullParameter("@CGST",DbType.Single);
				else
					InitializeParameter("@CGST",Convert.ToSingle(obj.CGST),DbType.Single);

				if(obj.RoundOff==null)
					InitializeNullParameter("@RoundOff",DbType.Single);
				else
					InitializeParameter("@RoundOff",Convert.ToSingle(obj.RoundOff),DbType.Single);

				if(obj.Cost==null)
					InitializeNullParameter("@Cost",DbType.Single);
				else
					InitializeParameter("@Cost",Convert.ToSingle(obj.Cost),DbType.Single);

				if(obj.Description==null)
					InitializeNullParameter("@Description",DbType.String);
				else
					InitializeParameter("@Description",Convert.ToString(obj.Description),DbType.String);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.Package obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("Packages");
					InitializeParameter("@PackageID",Convert.ToInt32(obj.PackageID),DbType.Int32);

				if(obj.PackageName==null)
					InitializeNullParameter("@PackageName",DbType.String);
				else
					InitializeParameter("@PackageName",Convert.ToString(obj.PackageName),DbType.String);

				if(obj.BasePrice==null)
					InitializeNullParameter("@BasePrice",DbType.Single);
				else
					InitializeParameter("@BasePrice",Convert.ToSingle(obj.BasePrice),DbType.Single);

				if(obj.SGST==null)
					InitializeNullParameter("@SGST",DbType.Single);
				else
					InitializeParameter("@SGST",Convert.ToSingle(obj.SGST),DbType.Single);

				if(obj.CGST==null)
					InitializeNullParameter("@CGST",DbType.Single);
				else
					InitializeParameter("@CGST",Convert.ToSingle(obj.CGST),DbType.Single);

				if(obj.RoundOff==null)
					InitializeNullParameter("@RoundOff",DbType.Single);
				else
					InitializeParameter("@RoundOff",Convert.ToSingle(obj.RoundOff),DbType.Single);

				if(obj.Cost==null)
					InitializeNullParameter("@Cost",DbType.Single);
				else
					InitializeParameter("@Cost",Convert.ToSingle(obj.Cost),DbType.Single);

				if(obj.Description==null)
					InitializeNullParameter("@Description",DbType.String);
				else
					InitializeParameter("@Description",Convert.ToString(obj.Description),DbType.String);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.Package obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Packages");
				InitializeParameter("@PackageID",obj.PackageID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("Packages");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Packages",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("Packages",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
