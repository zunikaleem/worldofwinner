using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAEPINRequest:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.EPINRequest>
	{
		public DAEPINRequest(){}
		public DAEPINRequest(Database database):base(database){}
		public DAEPINRequest(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("EPINRequest");
				InitializeParameter("@EPINRequestID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("EPINRequest",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("EPINRequest");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("EPINRequest",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.EPINRequest obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("EPINRequest");
				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.Quantity==null)
					InitializeNullParameter("@Quantity",DbType.Int32);
				else
					InitializeParameter("@Quantity",Convert.ToInt32(obj.Quantity),DbType.Int32);

				if(obj.RequestDate==null)
					InitializeNullParameter("@RequestDate",DbType.DateTime);
				else
					InitializeParameter("@RequestDate",Convert.ToDateTime(obj.RequestDate),DbType.DateTime);

				if(obj.Status==null)
					InitializeNullParameter("@Status",DbType.String);
				else
					InitializeParameter("@Status",Convert.ToString(obj.Status),DbType.String);

				if(obj.ProcessDate==null)
					InitializeNullParameter("@ProcessDate",DbType.DateTime);
				else
					InitializeParameter("@ProcessDate",Convert.ToDateTime(obj.ProcessDate),DbType.DateTime);

				if(obj.TransferredQuantity==null)
					InitializeNullParameter("@TransferredQuantity",DbType.Int32);
				else
					InitializeParameter("@TransferredQuantity",Convert.ToInt32(obj.TransferredQuantity),DbType.Int32);

				if(obj.TransactionPath==null)
					InitializeNullParameter("@TransactionPath",DbType.String);
				else
					InitializeParameter("@TransactionPath",Convert.ToString(obj.TransactionPath),DbType.String);

				if(obj.UserComment==null)
					InitializeNullParameter("@UserComment",DbType.String);
				else
					InitializeParameter("@UserComment",Convert.ToString(obj.UserComment),DbType.String);

				if(obj.AdminComment==null)
					InitializeNullParameter("@AdminComment",DbType.String);
				else
					InitializeParameter("@AdminComment",Convert.ToString(obj.AdminComment),DbType.String);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.EPINRequest obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("EPINRequest");
					InitializeParameter("@EPINRequestID",Convert.ToInt32(obj.EPINRequestID),DbType.Int32);

				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.Quantity==null)
					InitializeNullParameter("@Quantity",DbType.Int32);
				else
					InitializeParameter("@Quantity",Convert.ToInt32(obj.Quantity),DbType.Int32);

				if(obj.RequestDate==null)
					InitializeNullParameter("@RequestDate",DbType.DateTime);
				else
					InitializeParameter("@RequestDate",Convert.ToDateTime(obj.RequestDate),DbType.DateTime);

				if(obj.Status==null)
					InitializeNullParameter("@Status",DbType.String);
				else
					InitializeParameter("@Status",Convert.ToString(obj.Status),DbType.String);

				if(obj.ProcessDate==null)
					InitializeNullParameter("@ProcessDate",DbType.DateTime);
				else
					InitializeParameter("@ProcessDate",Convert.ToDateTime(obj.ProcessDate),DbType.DateTime);

				if(obj.TransferredQuantity==null)
					InitializeNullParameter("@TransferredQuantity",DbType.Int32);
				else
					InitializeParameter("@TransferredQuantity",Convert.ToInt32(obj.TransferredQuantity),DbType.Int32);

				if(obj.TransactionPath==null)
					InitializeNullParameter("@TransactionPath",DbType.String);
				else
					InitializeParameter("@TransactionPath",Convert.ToString(obj.TransactionPath),DbType.String);

				if(obj.UserComment==null)
					InitializeNullParameter("@UserComment",DbType.String);
				else
					InitializeParameter("@UserComment",Convert.ToString(obj.UserComment),DbType.String);

				if(obj.AdminComment==null)
					InitializeNullParameter("@AdminComment",DbType.String);
				else
					InitializeParameter("@AdminComment",Convert.ToString(obj.AdminComment),DbType.String);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.EPINRequest obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("EPINRequest");
				InitializeParameter("@EPINRequestID",obj.EPINRequestID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("EPINRequest");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("EPINRequest",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("EPINRequest",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("EPINRequest",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
