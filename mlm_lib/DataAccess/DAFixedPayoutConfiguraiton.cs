using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAFixedPayoutConfiguraiton:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.FixedPayoutConfiguraiton>
	{
		public DAFixedPayoutConfiguraiton(){}
		public DAFixedPayoutConfiguraiton(Database database):base(database){}
		public DAFixedPayoutConfiguraiton(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("FixedPayoutConfiguraitons");
				InitializeParameter("@FixedPayoutConfigurationID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("FixedPayoutConfiguraitons",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("FixedPayoutConfiguraitons");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("FixedPayoutConfiguraitons",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.FixedPayoutConfiguraiton obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("FixedPayoutConfiguraitons");
				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.Enable==null)
					InitializeNullParameter("@Enable",DbType.Boolean);
				else
					InitializeParameter("@Enable",Convert.ToBoolean(obj.Enable),DbType.Boolean);

				if(obj.FixedAmount==null)
					InitializeNullParameter("@FixedAmount",DbType.Single);
				else
					InitializeParameter("@FixedAmount",Convert.ToSingle(obj.FixedAmount),DbType.Single);

				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.FixedPayoutConfiguraiton obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("FixedPayoutConfiguraitons");
				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.Enable==null)
					InitializeNullParameter("@Enable",DbType.Boolean);
				else
					InitializeParameter("@Enable",Convert.ToBoolean(obj.Enable),DbType.Boolean);

				if(obj.FixedAmount==null)
					InitializeNullParameter("@FixedAmount",DbType.Single);
				else
					InitializeParameter("@FixedAmount",Convert.ToSingle(obj.FixedAmount),DbType.Single);

					InitializeParameter("@FixedPayoutConfigurationID",Convert.ToInt32(obj.FixedPayoutConfigurationID),DbType.Int32);

				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.FixedPayoutConfiguraiton obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("FixedPayoutConfiguraitons");
				InitializeParameter("@FixedPayoutConfigurationID",obj.FixedPayoutConfigurationID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("FixedPayoutConfiguraitons");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("FixedPayoutConfiguraitons",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("FixedPayoutConfiguraitons",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("FixedPayoutConfiguraitons",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
