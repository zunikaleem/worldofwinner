using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DASMSLog:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.SMSLog>
	{
		public DASMSLog(){}
		public DASMSLog(Database database):base(database){}
		public DASMSLog(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("SMSLog");
				InitializeParameter("@SMSLogID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("SMSLog",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("SMSLog");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("SMSLog",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.SMSLog obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("SMSLog");
				if(obj.MobileNumber==null)
					InitializeNullParameter("@MobileNumber",DbType.String);
				else
					InitializeParameter("@MobileNumber",Convert.ToString(obj.MobileNumber),DbType.String);

				if(obj.MessasgeText==null)
					InitializeNullParameter("@MessasgeText",DbType.String);
				else
					InitializeParameter("@MessasgeText",Convert.ToString(obj.MessasgeText),DbType.String);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.Status==null)
					InitializeNullParameter("@Status",DbType.String);
				else
					InitializeParameter("@Status",Convert.ToString(obj.Status),DbType.String);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.SMSLog obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("SMSLog");
					InitializeParameter("@SMSLogID",Convert.ToInt32(obj.SMSLogID),DbType.Int32);

				if(obj.MobileNumber==null)
					InitializeNullParameter("@MobileNumber",DbType.String);
				else
					InitializeParameter("@MobileNumber",Convert.ToString(obj.MobileNumber),DbType.String);

				if(obj.MessasgeText==null)
					InitializeNullParameter("@MessasgeText",DbType.String);
				else
					InitializeParameter("@MessasgeText",Convert.ToString(obj.MessasgeText),DbType.String);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.Status==null)
					InitializeNullParameter("@Status",DbType.String);
				else
					InitializeParameter("@Status",Convert.ToString(obj.Status),DbType.String);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.SMSLog obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("SMSLog");
				InitializeParameter("@SMSLogID",obj.SMSLogID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("SMSLog");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("SMSLog",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("SMSLog",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
