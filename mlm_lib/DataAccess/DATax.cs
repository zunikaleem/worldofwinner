using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DATax:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.Tax>
	{
		public DATax(){}
		public DATax(Database database):base(database){}
		public DATax(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Taxes");
				InitializeParameter("@TaxID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Taxes",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("Taxes");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Taxes",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.Tax obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("Taxes");
				if(obj.TaxType==null)
					InitializeNullParameter("@TaxType",DbType.String);
				else
					InitializeParameter("@TaxType",Convert.ToString(obj.TaxType),DbType.String);

				if(obj.CalculateIn==null)
					InitializeNullParameter("@CalculateIn",DbType.String);
				else
					InitializeParameter("@CalculateIn",Convert.ToString(obj.CalculateIn),DbType.String);

				if(obj.Value==null)
					InitializeNullParameter("@Value",DbType.Single);
				else
					InitializeParameter("@Value",Convert.ToSingle(obj.Value),DbType.Single);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.Tax obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("Taxes");
					InitializeParameter("@TaxID",Convert.ToInt32(obj.TaxID),DbType.Int32);

				if(obj.TaxType==null)
					InitializeNullParameter("@TaxType",DbType.String);
				else
					InitializeParameter("@TaxType",Convert.ToString(obj.TaxType),DbType.String);

				if(obj.CalculateIn==null)
					InitializeNullParameter("@CalculateIn",DbType.String);
				else
					InitializeParameter("@CalculateIn",Convert.ToString(obj.CalculateIn),DbType.String);

				if(obj.Value==null)
					InitializeNullParameter("@Value",DbType.Single);
				else
					InitializeParameter("@Value",Convert.ToSingle(obj.Value),DbType.Single);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.Tax obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Taxes");
				InitializeParameter("@TaxID",obj.TaxID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("Taxes");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Taxes",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("Taxes",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
