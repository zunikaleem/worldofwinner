using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAPayoutSlab:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.PayoutSlab>
	{
		public DAPayoutSlab(){}
		public DAPayoutSlab(Database database):base(database){}
		public DAPayoutSlab(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("PayoutSlabs");
				InitializeParameter("@PayoutSlabID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("PayoutSlabs",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("PayoutSlabs");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("PayoutSlabs",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.PayoutSlab obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("PayoutSlabs");
				if(obj.TotalSale==null)
					InitializeNullParameter("@TotalSale",DbType.Single);
				else
					InitializeParameter("@TotalSale",Convert.ToSingle(obj.TotalSale),DbType.Single);

				if(obj.Level==null)
					InitializeNullParameter("@Level",DbType.Int32);
				else
					InitializeParameter("@Level",Convert.ToInt32(obj.Level),DbType.Int32);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.PayoutSlab obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("PayoutSlabs");
				if(obj.TotalSale==null)
					InitializeNullParameter("@TotalSale",DbType.Single);
				else
					InitializeParameter("@TotalSale",Convert.ToSingle(obj.TotalSale),DbType.Single);

				if(obj.Level==null)
					InitializeNullParameter("@Level",DbType.Int32);
				else
					InitializeParameter("@Level",Convert.ToInt32(obj.Level),DbType.Int32);

					InitializeParameter("@PayoutSlabID",Convert.ToInt32(obj.PayoutSlabID),DbType.Int32);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.PayoutSlab obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("PayoutSlabs");
				InitializeParameter("@PayoutSlabID",obj.PayoutSlabID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("PayoutSlabs");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("PayoutSlabs",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("PayoutSlabs",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("PayoutSlabs",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
