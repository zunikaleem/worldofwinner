using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAConfiguration:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.Configuration>
	{
		public DAConfiguration(){}
		public DAConfiguration(Database database):base(database){}
		public DAConfiguration(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Configurations");
				InitializeParameter("@ConfigurationID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Configurations",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("Configurations");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("Configurations",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.Configuration obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("Configurations");
				if(obj.ConfigKey==null)
					InitializeNullParameter("@ConfigKey",DbType.String);
				else
					InitializeParameter("@ConfigKey",Convert.ToString(obj.ConfigKey),DbType.String);

				if(obj.ConfigType==null)
					InitializeNullParameter("@ConfigType",DbType.String);
				else
					InitializeParameter("@ConfigType",Convert.ToString(obj.ConfigType),DbType.String);

				if(obj.ConfigValue==null)
					InitializeNullParameter("@ConfigValue",DbType.String);
				else
					InitializeParameter("@ConfigValue",Convert.ToString(obj.ConfigValue),DbType.String);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.Configuration obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("Configurations");
				if(obj.ConfigKey==null)
					InitializeNullParameter("@ConfigKey",DbType.String);
				else
					InitializeParameter("@ConfigKey",Convert.ToString(obj.ConfigKey),DbType.String);

				if(obj.ConfigType==null)
					InitializeNullParameter("@ConfigType",DbType.String);
				else
					InitializeParameter("@ConfigType",Convert.ToString(obj.ConfigType),DbType.String);

					InitializeParameter("@ConfigurationID",Convert.ToInt32(obj.ConfigurationID),DbType.Int32);

				if(obj.ConfigValue==null)
					InitializeNullParameter("@ConfigValue",DbType.String);
				else
					InitializeParameter("@ConfigValue",Convert.ToString(obj.ConfigValue),DbType.String);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.Configuration obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Configurations");
				InitializeParameter("@ConfigurationID",obj.ConfigurationID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("Configurations");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("Configurations",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("Configurations",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("Configurations",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
