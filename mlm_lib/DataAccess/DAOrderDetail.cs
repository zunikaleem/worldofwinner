using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAOrderDetail:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.OrderDetail>
	{
		public DAOrderDetail(){}
		public DAOrderDetail(Database database):base(database){}
		public DAOrderDetail(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("OrderDetails");
				InitializeParameter("@OrderDetailID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("OrderDetails",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("OrderDetails");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("OrderDetails",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.OrderDetail obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("OrderDetails");
				if(obj.BasePrice==null)
					InitializeNullParameter("@BasePrice",DbType.Single);
				else
					InitializeParameter("@BasePrice",Convert.ToSingle(obj.BasePrice),DbType.Single);

				if(obj.CGST==null)
					InitializeNullParameter("@CGST",DbType.Single);
				else
					InitializeParameter("@CGST",Convert.ToSingle(obj.CGST),DbType.Single);

				InitializeParameter("@OrderID",obj.Order.OrderID,DbType.Int32);
				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.SGST==null)
					InitializeNullParameter("@SGST",DbType.Single);
				else
					InitializeParameter("@SGST",Convert.ToSingle(obj.SGST),DbType.Single);

				if(obj.TaxIncluded==null)
					InitializeNullParameter("@TaxIncluded",DbType.Boolean);
				else
					InitializeParameter("@TaxIncluded",Convert.ToBoolean(obj.TaxIncluded),DbType.Boolean);

				if(obj.Total==null)
					InitializeNullParameter("@Total",DbType.Single);
				else
					InitializeParameter("@Total",Convert.ToSingle(obj.Total),DbType.Single);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.OrderDetail obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("OrderDetails");
				if(obj.BasePrice==null)
					InitializeNullParameter("@BasePrice",DbType.Single);
				else
					InitializeParameter("@BasePrice",Convert.ToSingle(obj.BasePrice),DbType.Single);

				if(obj.CGST==null)
					InitializeNullParameter("@CGST",DbType.Single);
				else
					InitializeParameter("@CGST",Convert.ToSingle(obj.CGST),DbType.Single);

					InitializeParameter("@OrderDetailID",Convert.ToInt32(obj.OrderDetailID),DbType.Int32);

				InitializeParameter("@OrderID",obj.Order.OrderID,DbType.Int32);
				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.SGST==null)
					InitializeNullParameter("@SGST",DbType.Single);
				else
					InitializeParameter("@SGST",Convert.ToSingle(obj.SGST),DbType.Single);

				if(obj.TaxIncluded==null)
					InitializeNullParameter("@TaxIncluded",DbType.Boolean);
				else
					InitializeParameter("@TaxIncluded",Convert.ToBoolean(obj.TaxIncluded),DbType.Boolean);

				if(obj.Total==null)
					InitializeNullParameter("@Total",DbType.Single);
				else
					InitializeParameter("@Total",Convert.ToSingle(obj.Total),DbType.Single);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.OrderDetail obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("OrderDetails");
				InitializeParameter("@OrderDetailID",obj.OrderDetailID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("OrderDetails");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("OrderDetails",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("OrderDetails",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("OrderDetails",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
