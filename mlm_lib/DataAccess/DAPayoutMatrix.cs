using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAPayoutMatrix:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.PayoutMatrix>
	{
		public DAPayoutMatrix(){}
		public DAPayoutMatrix(Database database):base(database){}
		public DAPayoutMatrix(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("PayoutMatrix");
				InitializeParameter("@PayoutMatrixId",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("PayoutMatrix",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("PayoutMatrix");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("PayoutMatrix",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.PayoutMatrix obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("PayoutMatrix");
				if(obj.Amount==null)
					InitializeNullParameter("@Amount",DbType.Single);
				else
					InitializeParameter("@Amount",Convert.ToSingle(obj.Amount),DbType.Single);

				if(obj.Level==null)
					InitializeNullParameter("@Level",DbType.Int32);
				else
					InitializeParameter("@Level",Convert.ToInt32(obj.Level),DbType.Int32);

				if(obj.PackageId==null)
					InitializeNullParameter("@PackageId",DbType.Int32);
				else
					InitializeParameter("@PackageId",Convert.ToInt32(obj.PackageId),DbType.Int32);

				if(obj.Release==null)
					InitializeNullParameter("@Release",DbType.Int32);
				else
					InitializeParameter("@Release",Convert.ToInt32(obj.Release),DbType.Int32);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.PayoutMatrix obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("PayoutMatrix");
				if(obj.Amount==null)
					InitializeNullParameter("@Amount",DbType.Single);
				else
					InitializeParameter("@Amount",Convert.ToSingle(obj.Amount),DbType.Single);

				if(obj.Level==null)
					InitializeNullParameter("@Level",DbType.Int32);
				else
					InitializeParameter("@Level",Convert.ToInt32(obj.Level),DbType.Int32);

				if(obj.PackageId==null)
					InitializeNullParameter("@PackageId",DbType.Int32);
				else
					InitializeParameter("@PackageId",Convert.ToInt32(obj.PackageId),DbType.Int32);

					InitializeParameter("@PayoutMatrixId",Convert.ToInt32(obj.PayoutMatrixId),DbType.Int32);

				if(obj.Release==null)
					InitializeNullParameter("@Release",DbType.Int32);
				else
					InitializeParameter("@Release",Convert.ToInt32(obj.Release),DbType.Int32);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.PayoutMatrix obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("PayoutMatrix");
				InitializeParameter("@PayoutMatrixId",obj.PayoutMatrixId,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("PayoutMatrix");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("PayoutMatrix",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("PayoutMatrix",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("PayoutMatrix",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
