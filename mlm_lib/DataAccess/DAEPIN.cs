using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAEPIN:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.EPIN>
	{
		public DAEPIN(){}
		public DAEPIN(Database database):base(database){}
		public DAEPIN(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("EPINs");
				InitializeParameter("@EPINID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("EPINs",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("EPINs");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("EPINs",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.EPIN obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("EPINs");
				if(obj.EPINNumber==null)
					InitializeNullParameter("@EPINNumber",DbType.String);
				else
					InitializeParameter("@EPINNumber",Convert.ToString(obj.EPINNumber),DbType.String);

				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.SystemUserID==null)
					InitializeNullParameter("@SystemUserID",DbType.Int32);
				else
					InitializeParameter("@SystemUserID",Convert.ToInt32(obj.SystemUserID),DbType.Int32);

				if(obj.TransactionDate==null)
					InitializeNullParameter("@TransactionDate",DbType.DateTime);
				else
					InitializeParameter("@TransactionDate",Convert.ToDateTime(obj.TransactionDate),DbType.DateTime);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.EPIN obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("EPINs");
					InitializeParameter("@EPINID",Convert.ToInt32(obj.EPINID),DbType.Int32);

				if(obj.EPINNumber==null)
					InitializeNullParameter("@EPINNumber",DbType.String);
				else
					InitializeParameter("@EPINNumber",Convert.ToString(obj.EPINNumber),DbType.String);

				InitializeParameter("@PackageID",obj.Package.PackageID,DbType.Int32);
				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.SystemUserID==null)
					InitializeNullParameter("@SystemUserID",DbType.Int32);
				else
					InitializeParameter("@SystemUserID",Convert.ToInt32(obj.SystemUserID),DbType.Int32);

				if(obj.TransactionDate==null)
					InitializeNullParameter("@TransactionDate",DbType.DateTime);
				else
					InitializeParameter("@TransactionDate",Convert.ToDateTime(obj.TransactionDate),DbType.DateTime);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.EPIN obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("EPINs");
				InitializeParameter("@EPINID",obj.EPINID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("EPINs");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("EPINs",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("EPINs",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("EPINs",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
