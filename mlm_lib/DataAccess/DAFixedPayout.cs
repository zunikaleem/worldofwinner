using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DataAccess
{
	public class DAFixedPayout:DataAccess.DABasis,DataAccess.IDBase<BusinessLogic.FixedPayout>
	{
		public DAFixedPayout(){}
		public DAFixedPayout(Database database):base(database){}
		public DAFixedPayout(string projectCode):base(projectCode){}
		public DataTable Select(int primaryKey)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("FixedPayouts");
				InitializeParameter("@FixedPayoutID",primaryKey,DbType.Int32);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable Select(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("FixedPayouts",condition);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.SelectAll("FixedPayouts");
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public DataTable SelectAll(int top)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Select("FixedPayouts",top);
				DataTable table=new DataTable();
				Adapter.SelectCommand=Command;
				Adapter.Fill(table);
				return table;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				Dispose();
			}
		}

		public int Insert(BusinessLogic.FixedPayout obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Insert("FixedPayouts");
				InitializeParameter("@BySystemUserID",obj.BySystemUser.SystemUserID,DbType.Int32);
				if(obj.GrossAmount==null)
					InitializeNullParameter("@GrossAmount",DbType.Single);
				else
					InitializeParameter("@GrossAmount",Convert.ToSingle(obj.GrossAmount),DbType.Single);

				if(obj.NetAmount==null)
					InitializeNullParameter("@NetAmount",DbType.Single);
				else
					InitializeParameter("@NetAmount",Convert.ToSingle(obj.NetAmount),DbType.Single);

				if(obj.PayoutDate==null)
					InitializeNullParameter("@PayoutDate",DbType.DateTime);
				else
					InitializeParameter("@PayoutDate",Convert.ToDateTime(obj.PayoutDate),DbType.DateTime);

				if(obj.ServiceCharge==null)
					InitializeNullParameter("@ServiceCharge",DbType.Single);
				else
					InitializeParameter("@ServiceCharge",Convert.ToSingle(obj.ServiceCharge),DbType.Single);

				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.TDS==null)
					InitializeNullParameter("@TDS",DbType.Single);
				else
					InitializeParameter("@TDS",Convert.ToSingle(obj.TDS),DbType.Single);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.ServiceChargeAmount==null)
					InitializeNullParameter("@ServiceChargeAmount",DbType.Single);
				else
					InitializeParameter("@ServiceChargeAmount",Convert.ToSingle(obj.ServiceChargeAmount),DbType.Single);

				if(obj.TDSAmount==null)
					InitializeNullParameter("@TDSAmount",DbType.Single);
				else
					InitializeParameter("@TDSAmount",Convert.ToSingle(obj.TDSAmount),DbType.Single);

				return Convert.ToInt32(Command.ExecuteScalar());
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Update(BusinessLogic.FixedPayout obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Update("FixedPayouts");
				InitializeParameter("@BySystemUserID",obj.BySystemUser.SystemUserID,DbType.Int32);
					InitializeParameter("@FixedPayoutID",Convert.ToInt32(obj.FixedPayoutID),DbType.Int32);

				if(obj.GrossAmount==null)
					InitializeNullParameter("@GrossAmount",DbType.Single);
				else
					InitializeParameter("@GrossAmount",Convert.ToSingle(obj.GrossAmount),DbType.Single);

				if(obj.NetAmount==null)
					InitializeNullParameter("@NetAmount",DbType.Single);
				else
					InitializeParameter("@NetAmount",Convert.ToSingle(obj.NetAmount),DbType.Single);

				if(obj.PayoutDate==null)
					InitializeNullParameter("@PayoutDate",DbType.DateTime);
				else
					InitializeParameter("@PayoutDate",Convert.ToDateTime(obj.PayoutDate),DbType.DateTime);

				if(obj.ServiceCharge==null)
					InitializeNullParameter("@ServiceCharge",DbType.Single);
				else
					InitializeParameter("@ServiceCharge",Convert.ToSingle(obj.ServiceCharge),DbType.Single);

				InitializeParameter("@SystemUserID",obj.SystemUser.SystemUserID,DbType.Int32);
				if(obj.TDS==null)
					InitializeNullParameter("@TDS",DbType.Single);
				else
					InitializeParameter("@TDS",Convert.ToSingle(obj.TDS),DbType.Single);

				if(obj.TransactionID==null)
					InitializeNullParameter("@TransactionID",DbType.String);
				else
					InitializeParameter("@TransactionID",Convert.ToString(obj.TransactionID),DbType.String);

				if(obj.UpdateBy==null)
					InitializeNullParameter("@UpdateBy",DbType.Int32);
				else
					InitializeParameter("@UpdateBy",Convert.ToInt32(obj.UpdateBy),DbType.Int32);

				if(obj.UpdateDate==null)
					InitializeNullParameter("@UpdateDate",DbType.DateTime);
				else
					InitializeParameter("@UpdateDate",Convert.ToDateTime(obj.UpdateDate),DbType.DateTime);

				if(obj.ServiceChargeAmount==null)
					InitializeNullParameter("@ServiceChargeAmount",DbType.Single);
				else
					InitializeParameter("@ServiceChargeAmount",Convert.ToSingle(obj.ServiceChargeAmount),DbType.Single);

				if(obj.TDSAmount==null)
					InitializeNullParameter("@TDSAmount",DbType.Single);
				else
					InitializeParameter("@TDSAmount",Convert.ToSingle(obj.TDSAmount),DbType.Single);

				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(BusinessLogic.FixedPayout obj)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("FixedPayouts");
				InitializeParameter("@FixedPayoutID",obj.FixedPayoutID,DbType.Int32);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int DeleteAll()
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.DeleteAll("FixedPayouts");
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public int Delete(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Delete("FixedPayouts",condition);
				return Command.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object IsExist(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.IsExist("FixedPayouts",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
		public object Count(string condition)
		{
			try
			{
				Command.CommandText=XMLQueryGenerator.Count("FixedPayouts",condition);
				return Command.ExecuteScalar();
			}
			catch(Exception ex)
			{
				throw  ex;
			}
			finally
			{
				Dispose();
			}
		}
	}
}
