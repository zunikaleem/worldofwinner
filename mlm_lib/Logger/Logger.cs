﻿// ---------------------------------------------------------------------
// File Name:   Logger.cs
// Copyrights:  Copyright Gilbarco Veeder-Root, Inc. 2010
// Written By:  Paul D. McSwain
// Notes:       None
//
// Change History
//  Date        Who         Def         Description
//	09/14/2011  mcswainp    ????????    Initial coding
// ---------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Win32;


namespace Utility
{
    /// <summary>
    ///   Class encapsulating the System Recovery application's logging requirements.
    /// </summary>
    [TypeLibType(TypeLibTypeFlags.FHidden)]
    [ComVisible(false)]
    public sealed class Logger
    {
        /// <summary>
        ///   Available priority levels for logged messages.
        /// </summary>
        /// <remarks>
        ///   Similar to the Unix convention, the lower the ordinal value of the priority
        ///   the greater its actual priority.  So, at the time of this writing, ERROR is the
        ///   most important/severe while TRACE is the least.
        /// </remarks>
        public enum Priority
        {
            /// <summary>
            ///   An error event, this indicates a significant problem.
            /// </summary>
            ERROR,

            /// <summary>
            ///   A warning event.  This indicates a something that is not immediately significant,
            ///   but may signify conditions that could cause a future problem.
            /// </summary>
            WARNING,

            /// <summary>
            ///   An informational event.  This indicates a significant, successful operation.
            /// </summary>
            INFO,

            /// <summary>
            ///   A trace event.  This indicates progress or information useful for debugging
            ///   or other technical activity that does not concern the user.  These events
            ///   are placed in log files only, not the Windows Event Log.
            /// </summary>
            TRACE
        }

        #region Private Constants

        //private const string LOG_SOURCE_NAME = "DBMA";
        /// <summary>
        /// Windows event log type
        /// </summary>
        private const string WIN_APP_LOG_NAME = "Application";

        //private const string LOG_FILE_PREFIX = "dbmalog_";
        /// <summary>
        /// Extension to use for log file name
        /// </summary>
        private const string LOG_FILE_EXTENSION = ".log";
        /// <summary>
        /// Formatting for log file name
        /// </summary>
        private const string LOG_FILE_DATE_FORMAT = "yyyyMMdd";
        /// <summary>
        /// Formatting for log file name
        /// </summary>
        private const string LOG_FILE_DATE_WILDCARD = "????????";
        /// <summary>
        /// Default days we wait before moving to the 'alt' prefix name.
        /// </summary>
        private const int LOG_FILE_ALT_AGE = 3;
        /// <summary>
        /// Registry key for logging configuration
        /// </summary>
        private const string _registryKey = @"SOFTWARE\Gilbarco\ASM";

        #endregion

        #region Private Data

        /// <summary>
        /// Locking object for multithread access to the logging object
        /// </summary>
        private readonly static object logLocker = new object();
        /// <summary>
        /// Instance through which all the classes services are accessed.
        /// </summary>
        private readonly static Logger log;

        /// <summary>
        /// Reference to the windows event log
        /// </summary>
        private EventLog windowsEventLog;
        /// <summary>
        /// Folder to write log file to
        /// </summary>
        private string filePath;
        /// <summary>
        /// Log file prefix
        /// </summary>
        private string logPrefix;
        /// <summary>
        /// Log file alternative prefix
        /// </summary>
        private string logAltPrefix;
        /// <summary>
        /// Windows event source name
        /// </summary>
        private string logSource;
        /// <summary>
        /// Number of days to keep log files
        /// </summary>
        private int maxAgeOfLogFilesInDays;
        /// <summary>
        /// Number of days before the file will be renamed to its alternative prefix
        /// </summary>
        private int maxAgeOfLogFilesToRenameInDays;
        /// <summary>
        /// Trace level to applied to file log
        /// </summary>
        private Priority fileTraceLevel = Priority.INFO;
        /// <summary>
        /// Trace level to applied to application event log
        /// </summary>
        private Priority eventTraceLevel = Priority.INFO;
        /// <summary>
        /// If true, log messages will be written to the event log
        /// </summary>
        private bool logToWinEventLog = true;
        /// <summary>
        /// If true, log messages will be written to the log file
        /// </summary>
        private bool logToFile = true;
        /// <summary>
        /// Flag indicating whether logging is enabled
        /// </summary>
        private bool loggingEnabled = true;
        /// <summary>
        /// Locking object used to prevent multithreaded problems when
        /// accessing the read timer.
        /// </summary>
        private static object readTimerLock = new object();
        /// <summary>
        /// Timer used to run periodically read logging configuration
        /// </summary>
        private static System.Timers.Timer registryReadTimer;
        /// <summary>
        /// The interval for the registry read timer.
        /// Default = 90 seconds (or 90000 milliseconds).
        /// </summary>
        private static int timerInterval = 90000;
        /// <summary>
        /// Determines if the thread ID will show up in the log file lines
        /// </summary>
        private static bool showThreadId = true;

        #endregion

        #region Constructors

        /// <summary>
        ///   Static constructor, initalizes the logger instance through which the
        ///   classes services are accessed.
        /// </summary>
        static Logger()
        {
            const int MAX_LOG_FILE_AGE = 30;
            log = new Logger(@"D:\Gilbarco\logs", MAX_LOG_FILE_AGE);
        }

        /// <summary>
        ///   Creates a new instance of the SCFLogger class.  If the specified log file
        ///   directory does not exist, it will be created.  Log files older than the
        ///   specified number of days will be deleted when a new log file is created.
        /// </summary>
        /// <param name="logFilePath">
        ///   Log files are created in this directory.
        /// </param>
        /// <param name="maxAgeInDays">
        ///   Maximum age of the log files in days before they will be deleted.
        /// </param>
        private Logger(string logFilePath, int maxAgeInDays)
        {
            maxAgeOfLogFilesInDays = maxAgeInDays;
            if (maxAgeInDays > LOG_FILE_ALT_AGE)
            {
                maxAgeOfLogFilesToRenameInDays = LOG_FILE_ALT_AGE;
            }
            else
            {
                maxAgeOfLogFilesToRenameInDays = maxAgeInDays;
            }
            filePath = logFilePath;
            windowsEventLog = null;
            logToWinEventLog = true;
            lock (readTimerLock)
            {
                registryReadTimer = new System.Timers.Timer();
                registryReadTimer.Interval = timerInterval;
                registryReadTimer.Elapsed += new System.Timers.ElapsedEventHandler(registryReadTimer_Elapsed);
                registryReadTimer.Start();
            }
            UpdateLoggingConfig();
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        ///   Adds a message to the current day's log file.  The message will also be added to
        ///   the Windows Application Event Log if the priority is ERROR, WARNING, or INFO.
        ///   All messages are prepended with the calling object and method in parenthesis.
        /// </summary>
        /// <param name="priority">
        ///   Priority level associated with the message.
        /// </param>
        /// <param name="message">
        ///   The message to be logged.
        /// </param>
        public static void putMessage(Priority priority, string message)
        {
            //if an error or info type, write to the console.
            if (priority == Priority.ERROR || priority == Priority.INFO || priority == Priority.WARNING)
            {
                //call private method
                writeMessage(priority, message, true, null);
            }
            else
            {
                //call private method
                writeMessage(priority, message, false, null);
            }
        }

        /// <summary>
        ///   Adds a message to the current day's log file.  The message will also be added to
        ///   the Windows Application Event Log if the priority is ERROR, WARNING, or INFO.
        ///   All messages are prepended with the calling object and method in parenthesis.
        /// </summary>
        /// <param name="priority">
        ///   Priority level associated with the message.
        /// </param>
        /// <param name="message">
        ///   The message to be logged.
        /// </param>
        /// <param name="writeToConsole">Write the message to the console.</param>
        public static void putMessage(Priority priority, string message, bool writeToConsole)
        {
            //call private method
            writeMessage(priority, message, writeToConsole, null);
        }

        /// <summary>
        ///   Adds a message to the current day's log file.  The message will also be added to
        ///   the Windows Application Event Log if the priority is ERROR, WARNING, or INFO.
        ///   All messages are prepended with the calling object and method in parenthesis.
        /// </summary>
        /// <param name="priority">
        ///   Priority level associated with the message.
        /// </param>
        /// <param name="message">
        ///   The message to be logged.
        /// </param>
        /// <param name="writeToConsole">Write the message to the console.</param>
        /// <param name="exception">Exception to be written to the (log along with message)</param>
        public static void putMessage(Priority priority, string message, bool writeToConsole, Exception exception)
        {
            //call private method
            writeMessage(priority, message, writeToConsole, exception);
        }

        /// <summary>
        ///   Adds a message to the current day's log file.  The message will also be added to
        ///   the Windows Application Event Log if the priority is ERROR, WARNING, or INFO.
        ///   All messages are prepended with the calling object and method in parenthesis.
        /// </summary>
        /// <param name="priority">
        ///   Priority level associated with the message.
        /// </param>
        /// <param name="message">
        ///   The message to be logged.
        /// </param>
        /// <param name="exception">Exception to be written to the (log along with message)</param>
        public static void putMessage(Priority priority, string message, Exception exception)
        {
            //call private method
            writeMessage(priority, message, false, exception);
        }

        #endregion

        #region Public Static Properties

        /// <summary>
        /// Gets the path to the log file.
        /// </summary>
        public static String logFilePath
        {
            get
            {
                lock (logLocker)
                {
                    return (log.filePath + @"\" + log.getTodaysLogFileName());
                }
            }
        }

        /// <summary>
        /// Gets or sets the Alternative log prefix name.  Logs that are "old" are renamed to this prefix (typically does not include a "_" char.
        /// </summary>
        public static String logAltPrefixName
        {
            get
            {
                lock (logLocker)
                {
                    return (log.logAltPrefix);
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.logAltPrefix = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the log prefix name.  This name appears before the date/time string identifying the log file.
        /// </summary>
        public static String logPrefixName
        {
            get
            {
                lock (logLocker)
                {
                    return (log.logPrefix);
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.logPrefix = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the string value to place in the event log under the Source column.
        /// </summary>
        public static String logSourceName
        {
            get
            {
                lock (logLocker)
                {
                    return (log.logSource);
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.logSource = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the folder in which the log will be written
        /// </summary>
        public static String logFolder
        {
            get
            {
                lock (logLocker)
                {
                    return (log.filePath);
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.filePath = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of days of log files to keep
        /// </summary>
        public static int logMaxFileAge
        {
            get
            {
                lock (logLocker)
                {
                    return (log.maxAgeOfLogFilesInDays);
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.maxAgeOfLogFilesInDays = value;
                    if (value > LOG_FILE_ALT_AGE)
                    {
                        log.maxAgeOfLogFilesToRenameInDays = LOG_FILE_ALT_AGE;
                    }
                    else
                    {
                        log.maxAgeOfLogFilesToRenameInDays = value;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets minimum priority of events sent to the file log
        /// </summary>
        public static Priority logFileTraceLevel
        {
            get
            {
                lock (logLocker)
                {
                    return (log.fileTraceLevel);
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.fileTraceLevel = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets minimum priority of events sent to the windows application event log
        /// </summary>
        public static Priority logEventTraceLevel
        {
            get
            {
                lock (logLocker)
                {
                    return (log.eventTraceLevel);
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.eventTraceLevel = value;
                }
            }
        }

        /// <summary>
        /// Should we Log this data to the Windows Event Log?
        /// </summary>
        public static bool LogToWinEventLog
        {
            get
            {
                lock (logLocker)
                {
                    return log.logToWinEventLog;
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.logToWinEventLog = value;
                }
            }
        }

        /// <summary>
        /// Should we log this data to a file?
        /// </summary>
        public static bool LogToFile
        {
            get
            {
                lock (logLocker)
                {
                    return log.logToFile;
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.logToFile = value;
                }
            }
        }

        /// <summary>
        /// Turns on or off all logging
        /// </summary>
        /// <value>bool.  If set to false, all logging data is discarded</value>
        public static bool Enabled
        {
            get
            {
                lock (logLocker)
                {
                    return log.loggingEnabled;
                }
            }
            set
            {
                lock (logLocker)
                {
                    log.loggingEnabled = value;
                }
            }
        }

        /// <summary>
        /// Specifies the interval to use when setting up the registry read timer.
        /// </summary>
        public static int RegistryReadTimerInterval
        {
            get { return timerInterval; }
            set
            {
                // if the value is less than 1000ms, we multiple it by 1000
                // (assume values less than 1000 are in seconds not ms).
                if (value < 1000)
                {
                    timerInterval = value * 1000;
                }
                else
                {
                    timerInterval = value;
                }
                lock (readTimerLock)
                {
                    if (registryReadTimer != null)
                    {
                        registryReadTimer.Stop();
                        registryReadTimer.Interval = timerInterval;
                        registryReadTimer.Start();
                    }
                }
            }
        }

        /// <summary>
        /// Specifies if the ThreadID is written to the log file with the messages.
        /// </summary>
        public static bool ShowThreadId { get { return showThreadId; } set { showThreadId = value; } }

        #endregion

        #region Private Methods

        /// <summary>
        /// Private method to do the work for the public putMessage methods.
        /// </summary>
        /// <param name="priority">Priority of the message.</param>
        /// <param name="message">Text representation of the message.</param>
        /// <param name="writeToConsole">Write the message to the console.</param>
        /// <param name="exception">Exception to be included in message text.</param>
        private static void writeMessage(Priority priority, string message, bool writeToConsole, Exception exception)
        {
            if (log == null)
            {
                return;
            }

            if ((log.loggingEnabled == false) || ((log.logToFile == false) && (log.logToWinEventLog == false)))
            {
                return;
            }

            String exceptionText = "";
            if (exception == null)
            {
                exceptionText = "";
            }
            else
            {
                exceptionText = "\r\n" + exception.ToString();
            }

            if (null != log)
            {
                //Prepend the calling routine to the message.
                StackTrace callStack = new StackTrace();
                StackFrame frame = null;
                MethodBase method = null;

                try
                {
                    //Put the message to the Windows Event Log.
                    if (log.logToWinEventLog == true)
                    {
                        log.putIntoWindowsEventLog(priority, message + " " + exceptionText);
                    }
                }
                catch
                {
                    // Well, not sure what to do about this, can't really log it for anyone ?!?
                }

                try
                {
                    // Here, we're attempting to make sure that we get a some
                    // method information that is not from this class.
                    StackFrame[] callStackFrameList = callStack.GetFrames();
                    // search through the call stack frame list until we
                    // find a frame that is not in this file.
                    foreach (StackFrame singleFrame in callStackFrameList)
                    {
                        // get the file name for the frame
                        string frameFile = singleFrame.GetMethod().Name.ToUpper();
                        // is the file name the same as this file?
                        if (frameFile != "WRITEMESSAGE" && frameFile != "PUTMESSAGE")
                        {
                            // NO!  store the frame
                            frame = singleFrame;
                            break;
                        }
                    }
                    //frame = callStack.GetFrame(1);
                    method = frame.GetMethod();
                }
                catch
                {
                    frame = null;
                    method = null;
                }

                string tracedMessage = "";

                try
                {
                    tracedMessage += "(" + method.DeclaringType.Name;
                }
                catch
                {
                    // in the case that we didn't get any declaring type information
                    // we need to put something there that lets us know
                    tracedMessage += "([unknown type]";
                }

                try
                {
                    tracedMessage += "." + method.Name + ") ";
                }
                catch
                {
                    // in the case that we didn't get any method information
                    // we need some idea what the stack looked like, so
                    // we dump the whole stack
                    tracedMessage += callStack.ToString() + ")  ";
                }

                tracedMessage += message + exceptionText;

                lock (logLocker)
                {
                    //Put the message to the log file.
                    if (log.logToFile == true)
                    {
                        log.putIntoLogFile(priority, tracedMessage);
                    }

                    //display message in the console window.

                    if (writeToConsole == true)
                    {
                        Console.WriteLine(message);
                    }
                }
            }
        }

        /// <summary>
        ///   Generates a log file of the format "scf_yyyyMMdd.txt" where 'yyyy' is the four
        ///   digit year, 'MM' is the two digit month, and 'dd' is the two digit day of the
        ///   month as given by the system's current local time.
        /// </summary>
        /// <returns>
        ///   The filename for the current day.
        /// </returns>
        private string getTodaysLogFileName()
        {
            DateTime now = DateTime.Today;
            if (logPrefix == null)
            {
                logPrefix = "ASM_";
            }
            if (logAltPrefix == null)
            {
                logAltPrefix = logPrefix;
            }
            string name = logPrefix + now.ToString(LOG_FILE_DATE_FORMAT) + LOG_FILE_EXTENSION;

            return (name);
        }

        /// <summary>
        ///   Adds the specified message to the log file.  If a log file does not exist for the
        ///   current day, one will be created (and sufficiently old log files will be removed).
        /// </summary>
        /// <param name="priority">
        ///   Priority level associated with the message.
        /// </param>
        /// <param name="message">
        ///   The message to be logged.
        /// </param>
        private void putIntoLogFile(Priority priority, string message)
        {
            if (priority > fileTraceLevel)
            {
                return;
            }

            //Annotate the string for easier reading/searching.
            string fancyMessage = "[" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "] ";
            // if we need to show the thread ID in the line
            if (showThreadId)
            {
                // add it in between the date and the method information.
                fancyMessage += "(tid:" + System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() + ") ";
            }
            fancyMessage += message;
            switch (priority)
            {
                case Priority.ERROR:
                    fancyMessage = "!! " + fancyMessage;
                    break;

                case Priority.WARNING:
                    fancyMessage = "?? " + fancyMessage;
                    break;

                case Priority.INFO:
                    fancyMessage = ".. " + fancyMessage;
                    break;

                case Priority.TRACE:
                    fancyMessage = "-> " + fancyMessage;
                    break;

                default:
                    fancyMessage = "## " + fancyMessage;
                    break;
            }

            //Check whether the requested file path exists; attempt to create it if it doesn't.
            if (!Directory.Exists(filePath))
            {
                try
                {
                    Directory.CreateDirectory(filePath);
                }
                catch
                {
                    //ignore exceptions
                }
            }

            lock (this)
            {
                //Check to see if today's log file exists, if not create it.
                string logFileName = filePath + @"\" + getTodaysLogFileName();
                if (!File.Exists(logFileName))
                {
                    //Create the file and immediately close it so that the StreamWriter
                    //  below will not have problems accessing it.
                    FileStream file = null;
                    try
                    {
                        file = File.Create(logFileName);
                    }
                    catch
                    {
                        //ignore exceptions
                    }
                    finally
                    {
                        if (null != file)
                        {
                            file.Close();
                        }
                    }

                    //When creating a new log file we also want to delete old ones.
                    deleteOldLogFiles();
                }

                //Add the message to the log file.
                StreamWriter writer = null;
                try
                {
                    writer = File.AppendText(logFileName);
                    writer.WriteLine(fancyMessage);
                }
                catch
                {
                    //ignore exceptions
                }
                finally
                {
                    if (null != writer)
                    {
                        try
                        {
                            writer.Close();
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        /// <summary>
        ///   Adds the specified message to the Windows Event Log, if the specified
        ///   priority indicates it should be.
        /// </summary>
        /// <param name="priority">
        ///   Priority level associated with the message.
        /// </param>
        /// <param name="message">
        ///   The message to be logged.
        /// </param>
        private void putIntoWindowsEventLog(Priority priority, string message)
        {
            if (priority > eventTraceLevel)
            {
                return;
            }

            //check the source parameter for null
            if (logSource == null)
            {
                logSource = "ASMLogParser";
            }
            //Initialize the Windows Event Log instance if it hasn't been already.
            if (null == windowsEventLog)
            {
                //Make sure the System Restore source exists in the Windows Application Log world.
                try
                {
                    if (!EventLog.SourceExists(logSourceName))
                    {
                        EventLog.CreateEventSource(logSourceName, WIN_APP_LOG_NAME);
                    }
                }
                catch
                {
                    //ignore exceptions
                }

                //Create and set upthe Windows Application Log instance.
                windowsEventLog = new EventLog();
                windowsEventLog.Source = logSourceName;
            }

            try
            {
                EventLogEntryType logType = EventLogEntryType.Error;

                switch (priority)
                {
                    case Priority.ERROR:
                        logType = EventLogEntryType.Error;
                        break;

                    case Priority.WARNING:
                        logType = EventLogEntryType.Warning;
                        break;

                    case Priority.INFO:
                        logType = EventLogEntryType.Information;
                        break;

                    case Priority.TRACE:
                        logType = EventLogEntryType.Information;
                        break;
                }
                windowsEventLog.WriteEntry(message, logType);
            }
            catch
            {
                //ignore exceptions
            }
        }

        /// <summary>
        ///   Removes log files older than the instance's specified max age.
        /// </summary>
        private void deleteOldLogFiles()
        {
            try
            {
                //Get the list of files in the log directory that match the log filename format.
                DirectoryInfo logDirInfo;
                FileInfo[] logFiles;

                DateTime today = DateTime.Today;
                DateTime creationTime;
                TimeSpan fileAge;

                String myAltPrefix = logAltPrefix;

                if (logAltPrefix == "")
                {
                    //logAltPrefix is empty - so skip the "rename to altPrefix" section and go to the just "Delete Old Files" section.
                    myAltPrefix = logPrefix;
                }
                else
                {
                    if (logAltPrefix.Equals(logPrefix))
                    {
                        // if the LogAltPrefix and logPrefix are the same - there should be no deleting/renaming required. 
                        // Skip down to the "Delete Old Files" section.
                    }
                    else
                    {
                        //Get the list of files in the log directory that match the log filename format.
                        logDirInfo = new DirectoryInfo(filePath);
                        logFiles = logDirInfo.GetFiles(logPrefix + LOG_FILE_DATE_WILDCARD + LOG_FILE_EXTENSION);

                        putIntoWindowsEventLog(Priority.INFO, "LogFiles has " + logFiles.Length + " files to process");

                        foreach (FileInfo aFile in logFiles)
                        {
                            creationTime = aFile.CreationTime;
                            fileAge = today.Subtract(creationTime);
                            if (fileAge.Days > maxAgeOfLogFilesToRenameInDays)
                            {
                                String newFileName = filePath + "\\" + aFile.Name.Replace(logPrefix, logAltPrefix);
                                putIntoWindowsEventLog(Priority.INFO, "Renaming " + aFile.Name + " to " + newFileName + "  days:" + fileAge.Days.ToString());
                                if (File.Exists(newFileName))
                                {
                                    File.Delete(newFileName);
                                }
                                aFile.MoveTo(newFileName);
                            }
                            else
                            {
                                //writeMessage(Priority.INFO, "File is " + aFile.Name + "  days:" + fileAge.Days.ToString(), true, null);
                            }
                        }
                    }
                }

                // DELETE OLD FILES and make the magic happen !

                //Get the list of files in the log directory that match the log filename format.
                logDirInfo = new DirectoryInfo(filePath);
                logFiles = logDirInfo.GetFiles(myAltPrefix + LOG_FILE_DATE_WILDCARD + LOG_FILE_EXTENSION);

                //Go through the list of files and remove any that are older than the specified
                // time interval.
                putIntoWindowsEventLog(Priority.INFO, myAltPrefix + " LogFiles has " + logFiles.Length + " files to process");

                foreach (FileInfo aFile in logFiles)
                {
                    creationTime = aFile.CreationTime;
                    fileAge = today.Subtract(creationTime);
                    if (fileAge.Days > maxAgeOfLogFilesInDays)
                    {
                        putIntoWindowsEventLog(Priority.INFO, "DELETING " + aFile.Name + " it is " + fileAge.Days + " old.");
                        aFile.Delete();
                    }
                    else
                    {
                        //writeMessage(Priority.INFO, "Keeping  " + aFile.Name + " it is " + fileAge.Days + " old.", true, null);
                    }
                }
            }
            catch (Exception exc)
            {
                //ignore exceptions
                putIntoWindowsEventLog(Priority.INFO, "Got an exception " + exc);
            }
        }

        /// <summary>
        /// Reads trace parameters from the registry and loads them into the logging object
        /// </summary>
        /// <remarks>A timer should call this method once every minute.</remarks>
        private void UpdateLoggingConfig()
        {
            int fileLevel;
            int eventLevel;
            bool loggingChange = false;

            //Set Logger the the current settings from the registry for trace level

            try
            {
                RegistryKey key = Registry.LocalMachine.CreateSubKey(_registryKey);
                string fileLevelText = (string)key.GetValue("FileTraceLevel");
                string eventLevelText = (string)key.GetValue("EventTraceLevel");
                string logToFileText = (string)key.GetValue("LogToFile");
                string logToEventText = (string)key.GetValue("LogToEvent");
                string showThreadIdText = (string)key.GetValue("ShowThreadID");

                bool toFile = false;
                lock (logLocker)
                {
                    toFile = this.logToFile;
                }

                if (!string.IsNullOrEmpty(logToFileText))
                {
                    logToFileText = logToFileText.Trim();

                    if ((String.Compare(logToFileText, "1", true) == 0) || (String.Compare(logToFileText, "true", true) == 0) || (String.Compare(logToFileText, "yes", true) == 0))
                    {
                        if (toFile != true)
                        {
                            loggingChange = true;
                            lock (logLocker)
                            {
                                toFile = this.logToFile = true;
                            }
                        }
                    }
                    else
                    {
                        if (toFile != false)
                        {
                            lock (logLocker)
                            {
                                toFile = this.logToFile = false;
                            }
                            loggingChange = true;
                        }
                    }
                }

                bool toEventLog = false;
                lock (logLocker)
                {
                    toEventLog = this.logToWinEventLog;
                }

                if (!string.IsNullOrEmpty(logToEventText))
                {
                    logToEventText = logToEventText.Trim();
                    if ((String.Compare(logToEventText, "1", true) == 0) || (String.Compare(logToEventText, "true", true) == 0) || (String.Compare(logToFileText, "yes", true) == 0))
                    {
                        if (toEventLog != true)
                        {
                            lock (logLocker)
                            {
                                toEventLog = this.logToWinEventLog = true;
                            }
                            loggingChange = true;
                        }
                    }
                    else
                    {
                        if (toEventLog != false)
                        {
                            lock (logLocker)
                            {
                                toEventLog = this.logToWinEventLog = false;
                            }
                            loggingChange = true;
                        }
                    }
                }

                Priority filePriority = Priority.ERROR;
                lock (logLocker)
                {
                    filePriority = this.fileTraceLevel;
                }

                if (int.TryParse(fileLevelText, out fileLevel) == true)
                {
                    if (filePriority != (Logger.Priority)fileLevel)
                    {
                        lock (logLocker)
                        {
                            this.fileTraceLevel = (Logger.Priority)fileLevel;
                        }
                        loggingChange = true;
                    }
                }

                Priority eventPriority = Priority.ERROR;
                lock (logLocker)
                {
                    eventPriority = this.eventTraceLevel;
                }
                if (int.TryParse(eventLevelText, out eventLevel) == true)
                {
                    if (eventPriority != (Logger.Priority)eventLevel)
                    {
                        lock (logLocker)
                        {
                            this.eventTraceLevel = (Logger.Priority)eventLevel;
                        }
                        loggingChange = true;
                    }
                }

                if (showThreadIdText != null &&
                    (showThreadIdText.ToUpper() == "FALSE" ||
                    showThreadIdText.ToUpper() == "0" ||
                    showThreadIdText.ToUpper() == "NO"))
                {
                    showThreadId = false;
                }
                else
                {
                    showThreadId = true;
                }
            }
            catch (Exception ex)
            {
                // we could be in the constructor, so we need to make sure
                // that we have a log object before we try to write to it
                if (log != null)
                {
                    Logger.putMessage(Logger.Priority.TRACE, "Cannot read registry for log configuration", true, ex);
                }
            }

            // If it changed, note it
            // we could be in the constructor, so we need to make sure
            // that we have a log object before we try to write to it
            if (loggingChange == true && log != null)
            {
                Logger.putMessage(Logger.Priority.WARNING, "Logging change:\r\n" +
                    "Log to file=" + Logger.LogToFile.ToString() + "\r\n" +
                    "Log to event log=" + Logger.LogToFile.ToString() + "\r\n" +
                    "File trace level=" + Logger.logFileTraceLevel.ToString() + "\r\n" +
                    "Event trace level=" + Logger.logEventTraceLevel.ToString());
            }
        }

        /// <summary>
        /// Event handler for when the read timer fires.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void registryReadTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (readTimerLock)
            {
                registryReadTimer.Stop();
                UpdateLoggingConfig();
                registryReadTimer.Start();
            }
        }

        #endregion
    }
}
