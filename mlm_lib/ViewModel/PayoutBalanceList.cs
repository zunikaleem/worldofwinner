﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class PayoutBalanceList
    {
        public string UserID { get; set; }
        public string ContactName { get; set; }
        public string MobileNumber { get; set; }
        public string PANNumber { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IFCCode { get; set; }
        public string AccountType { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNumber { get; set; }
        public decimal TotalCredit{ get; set; }
        public decimal TotalDebit { get; set; }
        public decimal Balance { get; set; }
    }
}
