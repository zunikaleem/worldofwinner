﻿using System.Data;
using System.Collections.Generic;

/// <summary>
/// Summary description for ICrud
/// </summary>
namespace BusinessLogic
{
    public interface IBase<T>
    {
        T Select();
        T Select(int primaryKey);
        List<T> Select(string condition);
        List<T> SelectAll();
        List<T> SelectAll(int top);
        int Insert();
        int Update();
        int Delete();
        int DeleteAll();
        int Delete(string condition);
        void Validate(BusinessLogic.OperationMode mode);
        void MapEntity(List<T> list,DataTable table);
        void MapEntity(T obj,DataRow dr);
    }
}