﻿using System;

/// <summary>
/// Summary description for BusinessBase
/// </summary>
namespace BusinessLogic
{
    [Serializable]
    public abstract class BusinessBase:IDisposable
    {
        public void Dispose()
        {
           
        }
    }

    public enum OperationMode
    {
        Insert,
        Update,
        Delete
    }
   
}
