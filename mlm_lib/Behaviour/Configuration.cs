﻿using System.Linq;

namespace BusinessLogic
{
    public partial class Configuration
    {
        public static string GetConfiguration(string configKey)
        {
            var value = Select($"Where ConfigKey='{configKey}'").FirstOrDefault();

            if (value != null)
            {
                return value.ConfigValue;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
