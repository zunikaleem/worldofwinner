﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.ExtensionMethods;
using mlm_lib.Shared;

namespace BusinessLogic
{
    public partial class EPIN
    {
        public string PackageName
        {
            get
            {
                return Package.PackageName;
            }
        }
        public static List<EPIN> GenerateEPIN(Package package, int quantity, SystemUser generatedBy, SystemUser sendToSystemUser)
        {
            var epins = new List<EPIN>();
            for (int i = 0; i < quantity; i++)
            {
                var epin = new EPIN(
                    Guid.NewGuid().ToString().ToUpper(), package, generatedBy.SystemUserID, DateTime.Now.IST(), sendToSystemUser.SystemUserID, DateTime.Now.IST());

                int x = epin.Insert();

                if (x > 0)
                    epins.Add(epin);
            }

            return epins;
        }

        [Obsolete("Deprecated")]
        private static EPIN NewEPIN()
        {
            var epin = new EPIN();

            List<EPIN> epins = Select("Where EPINID=(Select Max(EPINID) from EPINs)");
            EPIN lastEPIN;
            if (epins.Count > 0)
                lastEPIN = epins[0];
            else
            {
                lastEPIN = new EPIN();
            }

            //GET EPIN Number
            epin.EPINNumber = Guid.NewGuid().ToString();

            return epin;
        }
        [Obsolete("Deprecated")]
        private static bool IsSerialOrPasswordPreset(string join) => Select($"Where EPINPassword='{join}' or EPINNumber='{join}'").Any() ? true : false;

        public static List<EPIN> GetUsedEPINs(int packageID) => Select($"Where EPINID in (Select EPINID from Customers) and PackageID={packageID.ToString()}");

        public static List<EPIN> GetUsedEPINs() => Select("Where EPINID in (Select EPINID from Customers)");

        public static List<EPIN> GetUnusedEPINs(int packageID) => Select($"Where EPINID not in (Select EPINID from Customers) and PackageID={packageID.ToString()}");

        public static List<EPIN> GetUnusedEPINs() => Select("Where EPINID not in (Select EPINID from Customers) ");

        internal static EPIN GetEPIN(string serialNumber, string epinNumber, string epinPassword)
        {
            List<EPIN> epins = Select($"Where SerialNumber='{serialNumber}' and epinNumber='{epinNumber}' and EPINPassword='{epinPassword}'");

            if (epins.Any())
                return epins.FirstOrDefault();
            else
                return new EPIN();
        }

        internal static EPIN GetEPIN(string epinNumber, int packageID)
        {
            List<EPIN> epins = Select($"Where epinNumber='{epinNumber}' and PackageID={packageID}");

            if (epins.Any()) return epins.FirstOrDefault();
            else
                return new EPIN();
        }

        internal bool IsUsed()
        {
            var epin = Select(EPINID);

            if (epin.EPINID > 0)
            {
                var epins = Select($"Where EPINID={epin.EPINID} and EPINID in (Select EPINID from Customers)");

                if (epins.Any()) return true;
                else return false;
            }
            else
            {
                throw new Exception("Invalid epin");
            }
        }

        /// <summary>
        /// Valid epin and not in use
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {

            var epin = GetEPIN(EPINNumber, Package.PackageID);


            ///No EPIN found
            if (epin.EPINID == 0)
            {
                return false;
            }
            else
            {
                //EPIN found 
                //Let check this epin into Customer Table
                if (Customer.IsEPINUsed(epin))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        internal bool IsValid(int packageID)
        {
            var epin = GetEPIN(EPINNumber, packageID);


            ///No EPIN found
            if (epin.EPINID == 0)
            {
                return false;
            }
            else
            {
                //EPIN found 
                //Let check this epin into Customer Table
                if (Customer.IsEPINUsed(epin))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        internal static List<EPIN> GetEPINInbox(SystemUser systemUser, string epinType)
        {
            switch (epinType)
            {
                case "Used EPIN":
                    return Select($"Where SystemUserID={systemUser.SystemUserID.ToString()}  AND EPINID in (Select EPINID from Customers)");

                case "Unused EPIN":
                    return Select($"Where SystemUserID={systemUser.SystemUserID.ToString()} AND EPINID not in (Select EPINID from Customers)");
                default:
                    return new List<EPIN>();
            }
        }

        public static bool IsUsed(string epinNumber) => Select($"Where EPINNumber='{epinNumber}' and EPINID in (Select EPINID from Customers)").Any();

        internal static List<EPIN> GetEPINInbox(Customer customer, Package package) => Select($"Where CustomerID={customer.CustomerID.ToString()} And PackageID={package.PackageID.ToString()}");

        internal static List<EPIN> GetUnusedEPINs(int pacakgeID, int systemUserID) => Select($"Where PackageID={pacakgeID.ToString()} and SystemUserID = {systemUserID.ToString()} And EPINID Not In (Select EPINID From Customers)");

        public int TransferEPIN(SystemUser transferToSystemUser)
        {
            SystemUserID = transferToSystemUser.SystemUserID;
            TransactionDate = DateTime.Now.IST();
            return Update();
        }

        public static int GetEPINCount() => EPIN.SelectAll().Count;


        public static List<EPIN> GetAllEPINs() => SelectAll();


        public static int GetGenerateEPINCount() => Convert.ToInt32(Count("1=1"));

        public static object UsedEPINsCount() => Convert.ToInt32(Count($"EPINID in (Select EPINID from Customers c)"));

        //EPINID not in (Select EPINID from Customers)

        public static object UnUsedEPINsCount() => Convert.ToInt32(Count("EPINID not in (Select EPINID from Customers)"));

    }
}
