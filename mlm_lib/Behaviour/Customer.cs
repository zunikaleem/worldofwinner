﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Common;
using mlm_lib.Shared;
using System.Threading;
using Common.ExtensionMethods;

namespace BusinessLogic
{
    public partial class Customer
    {
        public string UserID
        {
            get
            {
                return SystemUser.UserID;
            }
        }
        public string ContactName
        {
            get
            {
                return SystemUser.ContactName;
            }
        }
        public string MobileNumber
        {
            get
            {
                return SystemUser.MobileNumber;
            }
        }
        public static Customer GetCustomer(int customerID) => Select(customerID);

        public static Customer GetCustomer(SystemUser systemUser)
        {
            var customer = Select($"Where SystemUserID={systemUser.SystemUserID}").FirstOrDefault();

            return customer;
        }

        public static List<Customer> GetPendingApprovalCustomers() => Select("Where CustomerID not in (Select CustomerID from CustomerApprovals)");


        public List<Customer> MyDownLine() => Select($"Where SponsorID={CustomerID}");
        public List<Customer> MyDownLine(int pageSize, int pageNumber)
        {
            var customers = Select($"Where SponsorID={CustomerID}");

            return customers.Skip(pageNumber).Take(pageSize).ToList();
        }


        public static int ApproveCustomer(int customerID, Customer updatedBy)
        {
            Customer customer = GetCustomer(customerID);
            if (customer.CustomerID != 0)
            {
                return CustomerApproval.approve(customer, updatedBy);
            }
            else
            {
                throw new Exception("Invalid customer id");
            }
        }

        public static Customer GetCustomer(string userID)
        {
            var customer = Select($"Where UserID='{userID}'").FirstOrDefault();

            return customer ?? new Customer();
        }

        public int ChangePassword(string oldPassword, string newPassword, string confirmPassword)
        {

            oldPassword = oldPassword.Replace("'", "");
            newPassword = newPassword.Replace("'", "");
            confirmPassword = confirmPassword.Replace("'", "");

            if (HasSpecialCharecters(newPassword))
            {
                if (oldPassword.Trim() != string.Empty && oldPassword.Trim() == SystemUser.UserPassword)
                {
                    if (newPassword.Trim() != string.Empty && confirmPassword != string.Empty)
                    {
                        if (newPassword.Trim() == confirmPassword.Trim())
                        {
                            if (newPassword.Trim() != oldPassword.Trim())
                            {
                                if (newPassword.Trim().Length > 6)
                                {
                                    this.SystemUser.UserPassword = newPassword;

                                    return Update();
                                }
                                else
                                {
                                    throw new Exception("The password lenght should be greater than 6 charecter.");
                                }
                            }
                            else
                            {
                                throw new Exception("Old password and new password should not be same.");
                            }
                        }
                        else
                        {
                            throw new Exception("New password and confirm password should be same.");
                        }
                    }
                    else
                    {
                        throw new Exception("New password and confirm password are required fields.");
                    }
                }
                else
                {
                    throw new Exception("Old password is required field,and it should be identical with existing password.");
                }
            }
            else
            {
                throw new Exception("At least one special charecter is required.");
            }
        }

        public bool IsApproved() => CustomerApproval.isApproved(this);


        private bool HasSpecialCharecters(string password)
        {
            try
            {
                if (
                    password.Contains("~") ||
                    password.Contains("`") ||
                    password.Contains("!") ||
                    password.Contains("@") ||
                    password.Contains("#") ||
                    password.Contains("$") ||
                    password.Contains("%") ||
                    password.Contains("^") ||
                    password.Contains("&") ||
                    password.Contains("*") ||
                    password.Contains("(") ||
                    password.Contains(")") ||
                    password.Contains("-") ||
                    password.Contains("_") ||
                    password.Contains("=") ||
                    password.Contains("+") ||
                    password.Contains(",") ||
                    password.Contains("<") ||
                    password.Contains(".") ||
                    password.Contains(">") ||
                    password.Contains("?") ||
                    password.Contains("/") ||
                    password.Contains(";") ||
                    password.Contains(":") ||
                    password.Contains("'") ||
                    password.Contains("\"") ||
                    password.Contains("[") ||
                    password.Contains("{") ||
                    password.Contains("]") ||
                    password.Contains("}") ||
                    password.Contains("|") ||
                    password.Contains("\\")
                )
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static bool IsEPINUsed(EPIN epin) => Select($"Where EPINID={epin.EPINID.ToString()}").Any();


        public string Register(int packageID, string sponsorUserID, string epinSerialNumber, string password)
        {
            //validate epin
            var epin = EPIN.GetEPIN(epinSerialNumber, packageID);

            if (epin.EPINID == 0)
                throw new Exception("Invalid epin entered");

            if (!epin.IsValid())
                throw new Exception("Enter epin is not valid");

            EPIN = epin;

            var sponsorSystemUser = SystemUser.GetSystemUser(sponsorUserID);
            var sponsorCustomer = GetCustomer(sponsorSystemUser);

            SponsorID = sponsorCustomer.CustomerID;
            if (SponsorID == 0) throw new Exception("Invalid Sponsor User ID");

            var accessGroup = AccessGroup.GetAccessGroup(AccessGroupConstant.SuperAdim);
            var companySytemUser = SystemUser.GetSystemUsers(accessGroup).FirstOrDefault();

            try
            {
                while (mlm_lib.Shared.Monitor.IsLocked == true)
                {
                    Thread.Sleep(1000);
                }

                mlm_lib.Shared.Monitor.IsLocked = true;

                RegistrationDate = DateTime.Now.IST();

                //using (var db = new DataAccess.DABasis(DataAccess.Database.Master))
                //{
                //    if (db.Connection.State == ConnectionState.Closed)
                //        db.Connection.Open();
                //db.Transaction = db.Connection.BeginTransaction(IsolationLevel.RepeatableRead);

                try
                {
                    //InsertCustomer(db);
                    var transactionID = Guid.NewGuid().ToString();

                    SystemUser = InsertSytsemUser(SystemUser.ContactName, SystemUser.MobileNumber, password, transactionID);

                    TransactionID = transactionID;
                    Insert();

                    if (CustomerID == 0) throw new Exception("Failed to create customer");



                    var orderDetail = InsertOrder(this, transactionID);

                    //CreditToCompany(orderDetail, companySytemUser, transactionID);

                    var payoutList = Payout.GeneratePayout(this, transactionID);

                    foreach (var payout in payoutList)
                    {
                        InsertAccount(payout, companySytemUser, transactionID);
                    }

                    var totalFixedPaid = InsertFixedPayout(transactionID, companySytemUser, this.SystemUser);
                    var totalPayoutDistributed = payoutList.Sum(x => x.NetAmount);

                    var leftOverBalance = orderDetail.Total - totalFixedPaid - totalPayoutDistributed;



                    //db.Transaction.Commit();
                }
                catch (SqlException ex)
                {
                    //db.Transaction.Rollback();
                }
                finally
                {
                    //db.Connection.Close();

                }
                //}

                return SystemUser.UserID;
            }
            catch (Exception ex)
            {
                mlm_lib.Shared.Monitor.IsLocked = false;
                throw ex;
            }
            finally
            {
                mlm_lib.Shared.Monitor.IsLocked = false;
            }
        }

        private void CreditToCompany(OrderDetail orderDetail, SystemUser companyUser, string transactionID)
        {
            Account account = new Account
            {
                CreditAmount = orderDetail.Total,
                SystemUser = companyUser,
                Narration = $"Account credited by sold of package {orderDetail.Package.PackageName} to customer {orderDetail.Order.Customer.SystemUser.UserID}",
                TransactionID = transactionID,
                TransactionType = TransactionType.ProductSold
            };

            account.Pay(orderDetail.Order.Customer.SystemUser);
        }

        private OrderDetail InsertOrder(Customer customer, string transactionID)
        {
            Order order = new Order
            {
                Customer = customer,
                TransactionID = transactionID
            };

            var orderDetails = order.CreateOrder(EPIN);

            return orderDetails;
        }

        private float InsertFixedPayout(string transactionID, SystemUser companyUser, SystemUser joinedCustomer)
        {
            var fixedPayoutConfigurationList = FixedPayoutConfiguraiton.GetFixedPayoutConfiguraiton(EPIN.Package)
                .Where(x => x.SystemUser.SystemUserID != 0 && x.Enable == true);

            float totalPay = 0;

            var tdsPercent = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.TDS));
            var serviceChargePercent = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.ServiceCharge));

            foreach (var config in fixedPayoutConfigurationList)
            {

                FixedPayout fixedPayout = new FixedPayout()
                {
                    BySystemUser = joinedCustomer,
                    GrossAmount = config.FixedAmount,
                    NetAmount = (config.FixedAmount) - ((tdsPercent / 100 * config.FixedAmount) + (serviceChargePercent / 100 * config.FixedAmount)),
                    PayoutDate = DateTime.Now.IST(),
                    ServiceCharge = serviceChargePercent,
                    ServiceChargeAmount = (serviceChargePercent / 100 * config.FixedAmount),
                    SystemUser = config.SystemUser,
                    TDS = tdsPercent,
                    TDSAmount = (tdsPercent / 100 * config.FixedAmount),
                    TransactionID = transactionID,
                    UpdateBy = joinedCustomer.SystemUserID,
                    UpdateDate = DateTime.Now.IST()
                };

                int x = fixedPayout.InsertFixedPayout();

                if (x > 0)
                {
                    Account creditAccount = new Account()
                    {
                        CreditAmount = fixedPayout.NetAmount,
                        SystemUser = config.SystemUser,
                        Narration = $"Received fixed amount by joining of {UserID}",
                        TransactionID = transactionID,
                        TransactionType = TransactionType.FixedCommissionEarned
                    };

                    creditAccount.Pay(this.SystemUser);

                    totalPay += Convert.ToSingle(creditAccount.CreditAmount);
                }
            }

            return totalPay;
        }

        private SystemUser InsertSytsemUser(string contactName, string mobileNumber, string password, string transactionID)
        {
            var systemUser = new SystemUser
            {
                AccessGroup = AccessGroup.GetAccessGroup(AccessGroupConstant.Customer),
                RegistrationDate = RegistrationDate,
                UserID = GetNewUserID(),
                UserPassword = password,
                ContactName = contactName,
                MobileNumber = mobileNumber,
                TransactionID = transactionID
            };

            systemUser.InsertSytsemUser();
            return systemUser;
        }

        private void InsertAccount(Payout payout, SystemUser companySystemUser, string transactionID)
        {
            var totalJoining = payout.Customer.MyDownLine(payout.AtStage).Count;

            Account creditAccount = new Account()
            {
                CreditAmount = payout.NetAmount,
                SystemUser = payout.Customer.SystemUser,
                UpdateBy = CustomerID,
                UpdateDate = DateTime.Now.IST(),
                Narration = $"Amount credited at level {payout.AtStage} by {payout.SponsorToUserID} [{totalJoining} new e-Store] , Ref. #{payout.PayoutID}",
                TransactionID = transactionID,
                TransactionType = payout.PayoutType == PayoutType.Regular
                ? TransactionType.CommissionEarned
                : TransactionType.ReservedCommissionEarned
            };
            switch (payout.PayoutType)
            {
                case PayoutType.Regular:
                    creditAccount.TransactionType = TransactionType.CommissionEarned;
                    break;
                case PayoutType.Reserved:
                    creditAccount.TransactionType = TransactionType.ReservedCommissionEarned;
                    break;
                case PayoutType.NotApplicable:
                    creditAccount.TransactionType = TransactionType.NotApplicable;
                    break;
            }

            creditAccount.Pay(this.SystemUser);

            //Account debitAccount = new Account
            //{
            //    DebitAmount = creditAccount.CreditAmount,
            //    SystemUser = companySystemUser,
            //    UpdateBy = CustomerID,
            //    UpdateDate = DateTime.Now.IST(),
            //    Narration = $"Commission paid to {payout.Customer.SystemUser.UserID} for level {payout.AtStage}",
            //    TransactionID = transactionID,
            //    TransactionType = payout.PayoutType == PayoutType.Regular
            //    ? TransactionType.CommissionPaid
            //    : TransactionType.ReservedCommissionPaid
            //};

            //debitAccount.Pay(this.SystemUser);
        }

        private void InsertCustomer(DataAccess.DABasis db)
        {
            try
            {
                var insertCustomer = $"Insert Into Customers(ContactName,EmailID,MobileNumber,UserID,UserPassword,SponsorID,EPINID,RegistrationDate) " +
                                            $"Values(@ContactName,@EmailID,@MobileNumber,@UserID,@UserPassword,@SponsorID,@EPINID,@RegistrationDate);SELECT SCOPE_IDENTITY();";

                var paramArray = Array.CreateInstance(typeof(SqlParameter), 8);
                var param_EmailID = new SqlParameter("@EmailID", EmailID);
                var param_UserID = new SqlParameter("@UserID", SystemUser.UserID);
                var param_UserPassword = new SqlParameter("@UserPassword", SystemUser.UserPassword);
                var param_SponsorID = new SqlParameter("@SponsorID", SponsorID);
                var param_EPINID = new SqlParameter("@EPINID", EPIN.EPINID);
                var param_RegistraionDate = new SqlParameter("@RegistrationDate", RegistrationDate);

                paramArray.SetValue(param_EmailID, 1);
                paramArray.SetValue(param_UserID, 3);
                paramArray.SetValue(param_UserPassword, 4);
                paramArray.SetValue(param_SponsorID, 5);
                paramArray.SetValue(param_EPINID, 6);
                paramArray.SetValue(param_RegistraionDate, 7);

                CustomerID = Convert.ToInt32(db.ExecuteScaler(insertCustomer, paramArray));
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public static Customer GetRootCustomer()
        {
            var customer = Select("Where SponsorId=0").FirstOrDefault();

            if (customer != null)
                return customer;
            else
                return new Customer();
        }

        private static string GetNewUserID()
        {
            var customers = Select($"Where SystemUserID in (Select SystemUserID from SystemUsers s Inner Join AccessGroups a on s.AccessGroupID=a.AccessGroupID Where a.AccessGroupName='Customer')");
            var newCustomerID = customers.Count + Configuration.GetConfiguration(ProjectConstant.BaseUserID).TryGet<int>();

            var prefix = Configuration.GetConfiguration(ProjectConstant.BaseUserIDPrefix);

            return prefix + newCustomerID.ToString();
        }

        public static Customer GetLastCustomer()
        {
            var customers = Select("Where CustomerID=(Select Max(CustomerID) from Customers)");

            return customers.Any() ? customers.FirstOrDefault() : new Customer();
        }

        public DataRow GetTreeDetails()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);

            string query = $"Select " +
                "dbo.GetUserID(i.SystemUserID) as UserID," +
                "(Select dbo.GetUserID(SystemUserID) from Customers Where CustomerID=i.sponsorid) as SponsorUserID," +
                "dbo.GetMobileNumber(i.SystemUserID) MobileNumber," +
                "i.RegistrationDate," +
                "dbo.GetContactName(i.SystemUserID) ContactName," +
                "(Select count(*) from Customers c where c.SponsorID=i.CustomerID) as TotalChild " +
                "From Customers i  " +
                $"Where dbo.GetUserID(SystemUserID)='{SystemUser.UserID}'";

            DataTable table = db.ExecuteSelectQuery(query);

            if (table.Rows.Count == 1)
            {
                return table.Rows[0];
            }
            else
            {
                return table.NewRow();
            }
        }

        public DataTable GetChilds()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);

            string query = $"Select " +
                "c.CustomerID," +
                "dbo.GetMobileNumber(c.SystemUserID) MobileNumber," +
                "dbo.GetContactName(c.SystemUserID) ContactName," +
                "dbo.GetUserID(c.SystemUserID) as UserID," +
                "c.SponsorID," +
                "c.RegistrationDate," +
                "(Select dbo.GetUserID(i.SystemUserID) from Customers i where c.SponsorID=i.CustomerID) as SponsorUserID, " +
                "(Select count(*) from Customers n where n.SponsorID=c.CustomerID) as TotalChild " +
                $"From Customers c Where c.SponsorID=(Select CustomerID from Customers Where CustomerID='{CustomerID}')";

            DataTable table = db.ExecuteSelectQuery(query);

            return table;
        }

        public static DataTable GetChilds(string userID)
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);

            var systemUser = BusinessLogic.SystemUser.GetSystemUser(userID);
            var customer = BusinessLogic.Customer.GetCustomer(systemUser);

            string query = $"Select " +
                            "c.CustomerID," +
                            "dbo.GetMobileNumber(i.SystemUserID) MobileNumber," +
                            "dbo.GetContactName(i.SystemUserID) ContactName," +
                            "dbo.GetUserID(i.SystemUserID) as UserID," +
                            "c.SponsorID," +
                            "c.RegistrationDate," +
                            "(Select i.UserID from Customers i where c.SponsorID=i.CustomerID) as SponsorUserID," +
                            "(Select count(*) from Customers n where n.SponsorID=c.CustomerID) as TotalChild " +
                            $"From Customers c Where c.SponsorID=(Select CustomerID from Customers Where CustomerID='{customer.CustomerID}')";

            DataTable table = db.ExecuteSelectQuery(query);

            return table;
        }

        public static string GetParent(string userID)
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            string query = $"Select (Select i.UserID from Customers i where c.SponsorID=i.CustomerID) as SponsorUserID from Customers c where c.UserID='{userID}'";
            return Convert.ToString(db.ExecuteScaler(query));
        }



        public static DataTable GetCustomerDashBoard(DateTime startDate, DateTime endDate)
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);

            var paramArray = Array.CreateInstance(typeof(SqlParameter), 2);
            var param_StartDate = new SqlParameter("@StartDate", DateTime.Now.IST().AddYears(-1));
            var param_EndDate = new SqlParameter("@EndDate", DateTime.Now.IST());

            paramArray.SetValue(param_StartDate, 0);
            paramArray.SetValue(param_EndDate, 1);

            return db.ExecuteStoredProcedure("CustomerDashboard", paramArray);
        }

        public int UpdateCustomer()
        {
            try
            {
                Update();

                this.SystemUser.UpdateSystemUser();
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }

        internal static List<Customer> GetCustomerForPayoutout(DateTime registrationDate) => Select($"Where convert(datetime,Convert(varchar(10),RegistrationDate,101))='{registrationDate.ToString("yyyy-MM-dd")}'");

        //covered in epinmanagement


        //coverted in epinmanagement
        public List<EPIN> GetEPINInbox(BusinessLogic.Package package) => EPIN.GetEPINInbox(this, package);



        public List<Payout> MyPayouts() => Payout.GetPayouts(this);

        public List<EPIN> GetUnusedEPINInbox()
        {

            var packages = Package.getPackages();
            var epins = new List<EPIN>();
            foreach (Package package in packages)
            {
                epins.AddRange(EPIN.GetUnusedEPINs(package.PackageID, this.CustomerID));
            }
            return epins;

        }



        public List<Customer> MyDownLine(int level)
        {
            List<Customer> customers = new List<Customer>();
            customers.Add(this);

            for (int i = 1; i <= level; i++)
            {

                string[] ids = customers.Select(o => o.CustomerID.ToString()).ToArray();


                string strIDs = string.Join(",", ids);

                if (strIDs == string.Empty)
                    return new List<BusinessLogic.Customer>();

                customers = Select($"Where SponsorID in ({strIDs})");
            }

            return customers;
        }

        public List<Customer> MyDownline()
        {

            var customers = new List<Customer>();
            customers.Add(this);
            var returnCustomers = new List<Customer>();

            while (true)
            {

                string[] customerIds = customers.Select(o => o.CustomerID.ToString()).ToArray();


                string strIDs = string.Join(",", customerIds);

                if (strIDs == string.Empty)
                    return returnCustomers;

                customers = Select($"Where SponsorID in ({strIDs})");

                returnCustomers.AddRange(customers);
            }

        }


        public List<EPINRequest> MyEPINRequest() => EPINRequest.GetEPINRequest(this);



        public static DataTable GetGSTReport()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            return db.ExecuteStoredProcedure("GSTReport");
        }

        public static DataTable GetTDSReport()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            return db.ExecuteStoredProcedure("TDSReport");
        }

        public static int GetCustomerCount() => Convert.ToInt32(Count($"SystemUserID in (Select SystemUserID from SystemUsers s Inner Join AccessGroups a on s.AccessGroupID=a.AccessGroupID Where a.AccessGroupName='{AccessGroupConstant.Customer}')"));


        public static float TotalEarning()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            return Convert.ToSingle(db.ExecuteStoredProcedure("TotalEarning").Rows[0][0]);
        }

        public static DataTable GetCustomers()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);

            return db.ExecuteStoredProcedure("JoinedCustomers");

        }
        /// <summary>
        /// Returns the customer joined between these dates
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static List<Customer> GetCustomers(string startDate,string endDate)
        {
            return Select($"Where RegistrationDate Between '{startDate.ToString()}' and '{endDate.ToString()}'");
        }

        public static List<ViewModel.PayoutBalanceList> GetPayoutBalnaceList()
        {
            var customers = SelectAll();
            var payoutBalanceList = new List<ViewModel.PayoutBalanceList>();

            foreach (var customer in customers)
            {
                var accounts = customer.SystemUser.GetAccounts();

                var totalCredit = accounts.Sum(x => x.CreditAmount);
                var totalDebit = accounts.Sum(x => x.DebitAmount);
                var balance = totalCredit - totalDebit;

                if (balance > 0)
                {
                    payoutBalanceList.Add(new ViewModel.PayoutBalanceList()
                    {
                        AccountHolderName = customer.SystemUser.AccountHolderName,
                        AccountNumber = customer.SystemUser.AccountNumber,
                        AccountType = customer.SystemUser.AccountType,
                        Balance = (decimal)balance,
                        BankName = customer.SystemUser.BankName,
                        BranchName = customer.SystemUser.BranchName,
                        ContactName = customer.SystemUser.ContactName,
                        IFCCode = customer.SystemUser.IFSCCode,
                        MobileNumber = customer.SystemUser.MobileNumber,
                        PANNumber = customer.SystemUser.PAN,
                        TotalCredit = (decimal)totalCredit,
                        TotalDebit = (decimal)totalDebit,
                        UserID = customer.SystemUser.UserID
                    });
                }
            }

            return payoutBalanceList;
        }

        public static List<ViewModel.PayoutBalanceList> GetPayoutBalnaceList2()
        {
            var systemUsers = SelectAll();
            var payoutBalanceList = new List<ViewModel.PayoutBalanceList>();

            foreach (var systemUser in systemUsers)
            {
                var accounts = systemUser.SystemUser.GetAccounts();

                var totalCredit = accounts.Sum(x => x.CreditAmount);
                var totalDebit = accounts.Sum(x => x.DebitAmount);
                var balance = totalCredit - totalDebit;

                if (balance > 0)
                {
                    payoutBalanceList.Add(new ViewModel.PayoutBalanceList()
                    {
                        AccountHolderName = systemUser.SystemUser.AccountHolderName,
                        AccountNumber = systemUser.SystemUser.AccountNumber,
                        AccountType = systemUser.SystemUser.AccountType,
                        Balance = (decimal)balance,
                        BankName = systemUser.SystemUser.BankName,
                        BranchName = systemUser.SystemUser.BranchName,
                        ContactName = systemUser.SystemUser.ContactName,
                        IFCCode = systemUser.SystemUser.IFSCCode,
                        MobileNumber = systemUser.SystemUser.MobileNumber,
                        PANNumber = systemUser.SystemUser.PAN,
                        TotalCredit = (decimal)totalCredit,
                        TotalDebit = (decimal)totalDebit,
                        UserID = systemUser.SystemUser.UserID
                    });
                }
            }

            return payoutBalanceList;
        }

        public static List<Customer> GetCustomers(AccessGroup accessGroup)
        {
            var systemUser = SystemUser.GetSystemUsers(accessGroup);

            string[] ids = systemUser.Select(o => o.SystemUserID.ToString()).ToArray();

            string strIDs = string.Join(",", ids);

            if (strIDs == string.Empty) return new List<Customer>();

            return Select($"Where SystemUserID in ({strIDs})");
        }

        public List<Payout> GetPayouts()
        {
            return Payout.GetPayouts(this);
        }
    }

    public enum LoginResult
    {
        InvalidLogin,
        Success,
        Failed,
    }
}



