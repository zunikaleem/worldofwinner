﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Common.ExtensionMethods;
using DataAccess;
using mlm_lib.Shared;

namespace BusinessLogic
{
    public partial class Payout
    {
        public string Level
        {
            get
            {
                return AtStage.ToString();
            }
        }

        public static List<int> GetAchievedLevel(Customer currentCustomer)
        {
            var payoutStage = Select("Where CustomerID=" + currentCustomer.CustomerID).Select(x => x.AtStage).Distinct();

            return payoutStage.ToList();

        }

        public string CustomerUserID
        {
            get
            {
                return Customer.SystemUser.UserID;
            }
        }
        public string SponsorToUserID
        {
            get
            {
                return SponsorTo.SystemUser.UserID;
            }
        }

        public static List<Payout> generatePayout(float stage1Commission, float stage2Commission, float stage3Commission, float stage4Commission, float stage5Commission, float stage6Commission, DateTime registrationDate, float serviceCharge, float tds, Customer generatedBy)
        {
            try
            {
                var customers = Customer.GetCustomerForPayoutout(registrationDate);
                var payouts = new List<Payout>();

                foreach (Customer customer in customers)
                {
                    var sponsor = Customer.GetCustomer(customer.SponsorID);
                    for (int i = 1; i <= 6; i++)
                    {
                        if (sponsor.CustomerID != 0)
                        {
                            var payout = new Payout();
                            payout.AtStage = i;
                            payout.Customer = sponsor;
                            payout.SponsorTo = customer;
                            payout.TDS = tds;
                            payout.ServiceCharge = serviceCharge;
                            payout.GenerateDate = DateTime.Now.IST();
                            payout.PayoutDate = registrationDate;

                            //payout.Stage1Commission = stage1Commission;
                            //payout.Stage2Commission = stage2Commission;
                            //payout.Stage3Commission = stage3Commission;
                            //payout.Stage4Commission = stage4Commission;
                            //payout.Stage5Commission = stage5Commission;
                            //payout.Stage6Commission = stage6Commission;

                            #region stages
                            switch (payout.AtStage)
                            {
                                case 1:
                                    payout.GrossAmount = Convert.ToSingle(customer.EPIN.Package.Cost) * Convert.ToSingle(stage1Commission / 100.0);
                                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
                                    break;
                                case 2:
                                    payout.GrossAmount = Convert.ToSingle(customer.EPIN.Package.Cost) * Convert.ToSingle(stage2Commission / 100.0);
                                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
                                    break;
                                case 3:
                                    payout.GrossAmount = Convert.ToSingle(customer.EPIN.Package.Cost) * Convert.ToSingle(stage3Commission / 100.0);
                                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
                                    break;
                                case 4:
                                    payout.GrossAmount = Convert.ToSingle(customer.EPIN.Package.Cost) * Convert.ToSingle(stage4Commission / 100.0);
                                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
                                    break;
                                case 5:
                                    payout.GrossAmount = Convert.ToSingle(customer.EPIN.Package.Cost) * Convert.ToSingle(stage5Commission / 100.0);
                                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
                                    break;
                                case 6:
                                    payout.GrossAmount = Convert.ToSingle(customer.EPIN.Package.Cost) * Convert.ToSingle(stage6Commission / 100.0);
                                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
                                    break;
                                default:
                                    break;
                            }
                            #endregion

                            if (!payout.IsPresent())
                            {
                                payout.UpdateBy = generatedBy.CustomerID;
                                payout.UpdateDate = DateTime.Now.IST();
                                payout.GenerateDate = DateTime.Now.IST();
                                int x = payout.Insert();
                            }
                            payouts.Add(payout);
                        }
                        else
                        {
                            break;
                        }
                        sponsor = Customer.GetCustomer(sponsor.SponsorID);
                    }
                }

                return payouts;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        private bool IsPresent() => Select($"Where CustomerID={Customer.CustomerID.ToString()} and AtStage={AtStage.ToString()} and SponsorToID={SponsorTo.CustomerID.ToString()}").Any();


        internal static List<Payout> GetPayouts(Customer customer) => Select($"Where CustomerID={customer.CustomerID}");

        public static DataTable GetPayouts(DateTime fromDate, DateTime toDate)
        {
            var db = new DataAccess.DABasis(Database.Master);

            Array paramArray = Array.CreateInstance(typeof(SqlParameter), 2);
            var param_FromDate = new SqlParameter("@FromDate", fromDate);
            var param_ToDate = new SqlParameter("@ToDate", toDate);
            paramArray.SetValue(param_FromDate, 0);
            paramArray.SetValue(param_ToDate, 1);

            return db.ExecuteStoredProcedureDS("GetPayoutByDate", paramArray).Tables[0];
        }

        public static List<Payout> GetPayouts(string fromDate, string toDate,string payoutType)
        {
            var payouts = Select($"Where PayoutDate Between '{fromDate}' and '{toDate}' and PayoutType='{payoutType}'");

            return payouts;
        }
        public static DataTable GetPayouts(DateTime fromDate, DateTime toDate, Customer customer, int atStage)
        {

            var db = new DataAccess.DABasis(DataAccess.Database.Master);

            Array paramArray = Array.CreateInstance(typeof(SqlParameter), 4);
            var param_FromDate = new SqlParameter("@FromDate", fromDate);
            var param_ToDate = new SqlParameter("@ToDate", toDate);
            var param_CustomerID = new SqlParameter("@CustomerID", customer.CustomerID);
            var param_AtStage = new SqlParameter("@AtStage", atStage);
            paramArray.SetValue(param_FromDate, 0);
            paramArray.SetValue(param_ToDate, 1);
            paramArray.SetValue(param_CustomerID, 2);
            paramArray.SetValue(param_AtStage, 3);

            return db.ExecuteStoredProcedureDS("GetCustomerPayoutByDate", paramArray).Tables[0];

        }

        internal static List<Payout> GeneratePayout(Customer customer, DABasis db)
        {
            var sponsor = Customer.GetCustomer(customer.SponsorID);

            var stage = 1;
            var payouts = new List<Payout>();
            while (sponsor.CustomerID != 0)
            {
                var payout = new Payout();
                payout.AtStage = stage;
                payout.Customer = sponsor;
                payout.GenerateDate = DateTime.Now.IST();
                payout.SponsorTo = customer;

                CalculateAmount(payout, customer.EPIN);

                if (payout.GrossAmount > 0)
                {
                    payout.ServiceCharge = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.ServiceCharge));
                    payout.TDS = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.TDS));
                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);

                }
                else
                {
                    payout.NetAmount = 0;
                    payout.ServiceCharge = 0;
                    payout.TDS = 0;
                }
                payout.PayoutDate = DateTime.Now.IST();

                if (!payout.IsPresent())
                {
                    payout.UpdateBy = customer.CustomerID;
                    payout.UpdateDate = DateTime.Now.IST();
                    payout.GenerateDate = DateTime.Now.IST();
                    int x = payout.Insert();

                    //InsertPayout(payout, db);

                    payouts.Add(payout);
                }

                sponsor = Customer.GetCustomer(sponsor.SponsorID);
                stage++;
            }

            return payouts;
        }

        private static void InsertPayout(Payout payout, DABasis db)
        {
            try
            {
                var insertPayout = $"INSERT INTO [dbo].[Payouts] " +
                                   "([PayoutDate]" +
                                   ",[CustomerID]" +
                                   ",[SponsorToID]" +
                                   ",[AtStage]" +
                                   ",[ServiceCharge]" +
                                   ",[TDS]" +
                                   ",[GrossAmount]" +
                                   ",[NetAmount]" +
                                   ",[GenerateDate]" +
                                   ",[UpdateBy]" +
                                   ",[UpdateDate])" +
                                   "VALUES" +
                                   "(@PayoutDate" +
                                   ",@CustomerID" +
                                   ",@SponsorToID" +
                                   ",@AtStage" +
                                   ",@ServiceCharge" +
                                   ",@TDS" +
                                   ",@GrossAmount" +
                                   ",@NetAmount" +
                                   ",@GenerateDate" +
                                   ",@UpdateBy" +
                                   ",@UpdateDate); SELECT SCOPE_IDENTITY(); ";

                var paramArray = Array.CreateInstance(typeof(SqlParameter), 10);
                var param_PayoutDate = new SqlParameter("@PayoutDate", payout.PayoutDate);
                var param_CustomerID = new SqlParameter("@CustomerID", payout.Customer.CustomerID);
                var param_SponsorToID = new SqlParameter("@SponsorToID", payout.SponsorTo.CustomerID);
                var param_AtStage = new SqlParameter("@AtStage", payout.AtStage);
                var param_ServiceCharge = new SqlParameter("@ServiceCharge", payout.ServiceCharge);
                var param_TDS = new SqlParameter("@TDS", payout.TDS);
                var param_GrossAmount = new SqlParameter("@GrossAmount", payout.GrossAmount);
                var param_NetAmount = new SqlParameter("@NetAmount", payout.NetAmount);
                var param_GenerateDate = new SqlParameter("@GenerateDate", payout.GenerateDate);
                var param_UpdateBy = new SqlParameter("@UpdateBy", payout.UpdateBy);
                var param_UpdateDate = new SqlParameter("@UpdateBy", payout.UpdateDate);

                paramArray.SetValue(param_PayoutDate, 0);
                paramArray.SetValue(param_CustomerID, 1);
                paramArray.SetValue(param_SponsorToID, 2);
                paramArray.SetValue(param_AtStage, 3);
                paramArray.SetValue(param_ServiceCharge, 4);
                paramArray.SetValue(param_TDS, 5);
                paramArray.SetValue(param_GrossAmount, 6);
                paramArray.SetValue(param_NetAmount, 7);
                paramArray.SetValue(param_GenerateDate, 8);
                paramArray.SetValue(param_UpdateBy, 9);
                paramArray.SetValue(param_UpdateDate, 10);

                payout.PayoutID = (int)db.ExecuteScaler(insertPayout, paramArray);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        internal static List<Payout> GeneratePayout(Customer customer, string transactionID)
        {
            var sponsor = Customer.GetCustomer(customer.SponsorID);
            var stage = 1;
            var payouts = new List<Payout>();
            while (sponsor.CustomerID != 0)
            {
                var payout = new Payout();
                payout.AtStage = stage;
                payout.Customer = sponsor;
                payout.GenerateDate = DateTime.Now.IST();
                payout.SponsorTo = customer;

                CalculateAmount2(payout, customer.EPIN);

                payout.PayoutDate = DateTime.Now.IST();

                if (!payout.IsPresent())
                {
                    payout.UpdateBy = customer.CustomerID;
                    payout.UpdateDate = DateTime.Now.IST();
                    payout.GenerateDate = DateTime.Now.IST();
                    payout.TransactionID = transactionID;
                    int x = payout.Insert();
                    payouts.Add(payout);
                }

                sponsor = Customer.GetCustomer(sponsor.SponsorID);
                stage++;
            }

            return payouts;
        }

        private static void CalculateAmount(Payout payout, EPIN epin)
        {
            var matrix = epin.Package.GetPayoutMatrix();

            int stage = payout.AtStage % 10;

            int slab = (payout.AtStage / 10);

            var totalDownline = payout.Customer.MyDownLine(stage).Count;

            if (totalDownline > matrix.Where(x => x.Level == stage).FirstOrDefault().Release)
            {
                payout.PayoutType = mlm_lib.Shared.PayoutType.Regular;
            }
            else
            {
                payout.PayoutType = mlm_lib.Shared.PayoutType.Reserved;
            }

            // Does it completed previous slab 

            if (slab > 0)
            {
                var payoutSlab = PayoutSlab.GetSlab((slab), epin.Package);
            }
            else
            {

                payout.GrossAmount = Convert.ToSingle(matrix.Where(x => x.Level == stage).FirstOrDefault().Amount);
                payout.ServiceCharge = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.ServiceCharge));
                payout.TDS = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.TDS));
                payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
            }
        }

        private static void CalculateAmount2(Payout payout, EPIN epin)
        {
            var package = epin.Package;
            var payoutMatrix = package.GetPayoutMatrix();
            var payoutSlab = package.GetPayoutSlab();

            //Does previous slab/stage acheived

            var currentSlab = payout.AtStage % 10 == 0 ? (payout.AtStage / 10) - 1 : (payout.AtStage / 10);

            var totalDownline = payout.Customer.MyDownLine(payout.AtStage).Count;

            if (currentSlab == 0)
            {
                //No need to check for previous slab

                if (totalDownline >= payoutMatrix.Where(x => x.Level == payout.AtStage).FirstOrDefault().Release)
                {
                    payout.PayoutType = mlm_lib.Shared.PayoutType.Regular;
                }
                else
                {
                    payout.PayoutType = mlm_lib.Shared.PayoutType.Reserved;
                }

                payout.GrossAmount = Convert.ToSingle(payoutMatrix.Where(x => x.Level == payout.AtStage).FirstOrDefault().Amount);
                payout.ServiceCharge = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.ServiceCharge));
                payout.ServiceChargeAmount = (payout.ServiceCharge/ 100 * payout.GrossAmount);
                payout.TDS = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.TDS));
                payout.TDSAmount = (payout.TDS / 100 * payout.GrossAmount);
                payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
            }
            else
            {
                // check does he qualified for this slab.

                var previousSlab = currentSlab - 1;

                var previouStageToCheck = previousSlab * 10 + payoutSlab.Level;

                var toalEarningAtPreviousStage = Payout.GetPayouts(payout.Customer, previouStageToCheck).Sum(x => x.GrossAmount);

                if (toalEarningAtPreviousStage < payoutSlab.TotalSale)
                {
                    payout.GrossAmount = 0;
                    payout.ServiceCharge = 0;
                    payout.TDS = 0;
                    payout.NetAmount = 0;
                    payout.PayoutType = mlm_lib.Shared.PayoutType.NotApplicable;
                }
                else
                {
                    var stage = (payout.AtStage % 10 == 0) ? 10 : payout.AtStage % 10;

                    if (totalDownline > payoutMatrix.Where(x => x.Level == stage).FirstOrDefault().Release)
                    {
                        payout.PayoutType = mlm_lib.Shared.PayoutType.Regular;
                    }
                    else
                    {
                        payout.PayoutType = mlm_lib.Shared.PayoutType.Reserved;
                    }

                    payout.GrossAmount = Convert.ToSingle(payoutMatrix.Where(x => x.Level == (payout.AtStage % 10)).FirstOrDefault().Amount);
                    payout.ServiceCharge = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.ServiceCharge));
                    payout.ServiceChargeAmount = (payout.ServiceCharge / 100 * payout.GrossAmount);
                    payout.TDS = Convert.ToSingle(Configuration.GetConfiguration(ProjectConstant.TDS));
                    payout.TDSAmount = (payout.TDS / 100 * payout.GrossAmount);
                    payout.NetAmount = payout.GrossAmount - (payout.TDS / 100 * payout.GrossAmount) - (payout.ServiceCharge / 100 * payout.GrossAmount);
                }
            }
        }

        private static List<Payout> GetPayouts(Customer customer, int stage)
        {
            return Select($"Where CustomerID={customer.CustomerID} And AtStage={stage}");
        }

        public static float GetTotalTDS()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            var value = db.ExecuteStoredProcedure("TotalTDS").Rows[0][0];
            if (DBNull.Value == value)
                return 0;
            else
                return Convert.ToSingle(value);
        }

        public static float GetTotalServiceCharge()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            var value = db.ExecuteStoredProcedure("TotalServiceCharge").Rows[0][0];
            if (DBNull.Value == value)
                return 0;
            else
                return Convert.ToSingle(value);
        }

        public static DataTable GetLevelwisePayouts()
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            return db.ExecuteStoredProcedure("LevelWisePayout");
        }

        public static DataTable GetLevelwisePayouts(string panNumber)
        {
            var db = new DataAccess.DABasis(DataAccess.Database.Master);
            Array paramArray = Array.CreateInstance(typeof(SqlParameter), 1);
            SqlParameter param_PANNumber = new SqlParameter("@PANNumber", panNumber);

            paramArray.SetValue(param_PANNumber, 0);

            return db.ExecuteStoredProcedure("LevelWisePayoutByPAN" +
                "", paramArray);
        }
    }
}
