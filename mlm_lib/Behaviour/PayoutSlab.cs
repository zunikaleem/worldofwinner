﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class PayoutSlab
    {
        public static PayoutSlab GetSlab(int slab,BusinessLogic.Package package)
        {
            var payoutSlab = Select($"Where slab={slab} and PackageID={package.PackageID}").FirstOrDefault();

            return payoutSlab;
        }

        public static PayoutSlab GetPayoutSlab(Package package)
        {
           return Select($"Where PackageID={package.PackageID.ToString()}").FirstOrDefault();
        }
    }
}
