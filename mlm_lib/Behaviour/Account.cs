﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Common.ExtensionMethods;
using DataAccess;
using System.Linq;

namespace BusinessLogic
{
    public partial class Account
    {
        public string SystemUserID
        {
            get
            {
                return SystemUser.UserID;
            }
        }
        public string ContactName
        {
            get
            {
                return SystemUser.ContactName;
            }
        }
        public static List<Account> GetAccount(int systemUserID)
        {
            try
            {
                if (systemUserID != 0)
                {
                    return Select($"Where SystemUserID={systemUserID}");
                }
                else
                {
                    throw new Exception($"Invalid User provided.Please enter valid user id.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Account> GetAccounts(int systemUserID,string startDate,string endDate,string transactionType)
        {
            return Select($"Where SystemUserID={systemUserID.ToString()} and UpdateDate Between '{startDate}' and '{endDate}' and TransactionType='{transactionType}'");
        }

        public int Pay(SystemUser updateBy)
        {
            try
            {
                UpdateBy = updateBy.SystemUserID;
                UpdateDate = DateTime.Now.IST();

                return Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public float? GetBalance(SystemUser systemUser)
        {
            var accounts = GetAccount(systemUser.SystemUserID);

            var credit = accounts.Sum(x => x.CreditAmount);
            var debit = accounts.Sum(x => x.DebitAmount);

            return (credit - debit);

        }
        public static float TotalDistribution()
        {
            var db = new DABasis(Database.Master);
            var value = db.ExecuteStoredProcedure("TotalDistribution").Rows[0][0];

            if (DBNull.Value == value)
                return 0;
            else
                return Convert.ToSingle(value);
        }

        internal void InsertAccount(DABasis db)
        {
            try
            {
                var insertAccount = $"INSERT INTO [dbo].[Accounts] " +
                                   "([CustomerID]" +
                                   ",[Narration]" +
                                   ",[CreditAmount]" +
                                   ",[DebitAmount]" +
                                   ",[UpdateBy]" +
                                   ",[UpdateDate])" +
                                   "VALUES " +
                                   "(@CustomerID," +
                                   ",@Narration," +
                                   ",@CreditAmount," +
                                   ",@DebitAmount," +
                                   ",@UpdateBy," +
                                   ",@UpdateDate); SELECT SCOPE_IDENTITY();";

                var paramArray = Array.CreateInstance(typeof(SqlParameter), 6);
                var param_CustomerID = new SqlParameter("@SytsemUserID", SystemUser.SystemUserID);
                var param_Narration = new SqlParameter("@Narration", Narration);
                var param_CreditAmount = new SqlParameter("@CreditAmount", CreditAmount);
                var param_DebitAmount = new SqlParameter("@DebitAmount", DebitAmount);
                var param_UpdateBy = new SqlParameter("@UpdateBy", UpdateBy);
                var param_UpdateDate = new SqlParameter("@UpdateBy", UpdateDate);

                paramArray.SetValue(param_CustomerID, 0);
                paramArray.SetValue(param_Narration, 1);
                paramArray.SetValue(param_CreditAmount, 2);
                paramArray.SetValue(param_DebitAmount, 3);
                paramArray.SetValue(param_UpdateBy, 4);
                paramArray.SetValue(param_UpdateDate, 5);

                AccountID = (int)db.ExecuteScaler(insertAccount, paramArray);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
