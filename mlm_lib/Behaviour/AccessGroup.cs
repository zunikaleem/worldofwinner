﻿using System.Linq;

namespace BusinessLogic
{
    public partial class AccessGroup
    {
        public AccessGroup(string accessGroupName)
        {
            AccessGroupName = accessGroupName;
        }

        public static AccessGroup GetAccessGroup(string accessGroupName)
        {
            var accessGroup = Select($"Where AccessGroupName='{accessGroupName}'").FirstOrDefault();

            return accessGroup;
        }
    }
}
