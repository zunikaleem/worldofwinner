﻿using System;
using System.Collections.Generic;
using Common.ExtensionMethods;

namespace BusinessLogic
{
    public partial class EPINRequest
    {
        public string PackageName
        {
            get
            {
                return Package.PackageName;
            }
        }

        public string SystemUserID
        {
            get
            {
                return SystemUser.UserID;
            }
        }
        //public string ContactName
        //{
        //    get
        //    {

        //        return System.ContactName;
        //    }
        //}
        //public string ContactNumber
        //{
        //    get
        //    {
        //        return Customer.MobileNumber;
        //    }
        //}


        public static List<EPINRequest> GetEPINRequest(Customer customer) => Select($"Where CustomerID = {customer.CustomerID.ToString()}");

        internal static int placeRequest(Package package, int quantity, SystemUser systemUser, string transactionPath, string comment)
        {

            var epinRequest = new EPINRequest();
            epinRequest.SystemUser = systemUser;
            epinRequest.Package = package;
            epinRequest.Quantity = quantity;
            epinRequest.UserComment = comment;
            epinRequest.TransactionPath = transactionPath;
            epinRequest.RequestDate = DateTime.Now.IST();

            return epinRequest.Insert();

        }

        public static List<EPINRequest> GetAllPendingRequest() => Select("Where ProcessDate is null and Status is null");

        public static EPINRequest GetEPINRequest(int epinRequestID) => Select(epinRequestID);

        public int TransferEPIN(SystemUser systemUser)
        {
            int quantity = systemUser.TranferEPIN(systemUser.UserID, Package.PackageName, TransferredQuantity.GetValueOrDefault());

            ProcessDate = DateTime.Now.IST();
            Status = "Transferred";

            Update();

            return quantity;
        }

        public static List<EPINRequest> GetProcessedRequest() => Select("Where ProcessDate is not null and Status is not null");


        public int RejectRequest()
        {
            ProcessDate = DateTime.Now.IST();
            Status = "Rejected";

            return Update();
        }
    }
}
