﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class FixedPayout
    {
        public int InsertFixedPayout()
        {
            return Insert();
        }

        public static List<FixedPayout> GetPayouts(string startDate, string endDate)
        {
            var payouts = Select($"Where PayoutDate Between '{startDate}' and '{endDate}'");

            return payouts;
        }
    }
}
