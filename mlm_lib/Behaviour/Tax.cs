﻿using System;
using Common.ExtensionMethods;

namespace BusinessLogic
{
    public partial class Tax
    {
        public static object GetAllTaxes() => SelectAll();
        public static Tax GetTax(int taxID) => Select(taxID);
        public int UpdateTax(Customer customer)
        {
            UpdateBy = customer.CustomerID;
            UpdateDate = DateTime.Now.IST();

            return Update();
        }
        public int AddNewTax(Customer customer)
        {
            UpdateBy = customer.CustomerID;
            UpdateDate = DateTime.Now.IST();

            return Insert();
        }

        public int DeleteTax() => Delete();
    }
}
