﻿using System;
using System.Collections.Generic;

namespace BusinessLogic
{
    public partial class EPINInbox
    {
        public string PackageName
        {
            get
            {
                return EPIN.PackageName;
            }
        }
        public string TranferTo
        {
            get
            {
                return ToSystemUser.UserID;
            }
        }
        public string EPINNumber
        {
            get
            {
                return EPIN.EPINNumber;
            }
        }
        public static int InsertToEPINInbox(EPIN epin, SystemUser fromSystemUser, SystemUser toSystemUser, SystemUser updateBy)
        {
            var epinInbox = new EPINInbox(epin, fromSystemUser, toSystemUser, updateBy.SystemUserID, TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")));

            return epinInbox.Insert();
        }

        public static List<EPINInbox> GetSentEPINs(SystemUser systemUser) => Select($"Where FromSystemUserID={systemUser.SystemUserID.ToString()}");

    }
}
