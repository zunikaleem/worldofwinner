﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.ExtensionMethods;

namespace BusinessLogic
{
    public partial class Package
    {

        public static List<Package> getPackages()
        {
            try
            {
                return SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Package getPackage(int packageID)
        {
            try
            {
                return Select(packageID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdatePackage(Customer customer)
        {
            Cost = Convert.ToSingle((BasePrice) + (CGST / 100.0 * BasePrice) + (SGST / 100.0 * BasePrice) + (RoundOff));
            UpdateBy = customer.CustomerID;
            UpdateDate = DateTime.Now.IST();

            return Update();
        }


        public int AddNewPackage(Customer customer)
        {
            Cost = Convert.ToSingle((BasePrice) + (CGST / 100.0 * BasePrice) + (SGST / 100.0 * BasePrice) + (RoundOff));
            UpdateBy = customer.CustomerID;
            UpdateDate = DateTime.Now.IST();

            return Insert();
        }

        public int DeletePackage()
        {
            var usedEPINS = EPIN.GetUsedEPINs(PackageID);
            if (usedEPINS.Count == 0)
            {
                return Delete();
            }
            else
            {
                throw new Exception("The package is in use, can't delete the package " + PackageName);
            }
        }

        public static Package GetPackage(string packageName) => Select($"Where PackageName='{packageName}'").FirstOrDefault();

        public List<PayoutMatrix> GetPayoutMatrix()
        {
            return PayoutMatrix.GetPayoutMatrix(this);
        }

        public PayoutSlab GetPayoutSlab()
        {
            return PayoutSlab.GetPayoutSlab(this);
        }

    }
}
