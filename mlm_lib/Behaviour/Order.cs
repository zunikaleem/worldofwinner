﻿using Common.ExtensionMethods;
using System;

namespace BusinessLogic
{
    public partial class Order
    {
        public OrderDetail CreateOrder(EPIN epin)
        {
            OrderDate = DateTime.Now.IST();

            Insert();

            OrderDetail orderDetail = new OrderDetail
            {
                BasePrice = epin.Package.BasePrice,
                CGST = epin.Package.CGST,
                Order = this,
                Package = epin.Package,
                SGST = epin.Package.SGST,
                TaxIncluded = true,
                Total = epin.Package.Cost,
                TransactionID = TransactionID
            };

            orderDetail.CreateOrderDetails();

            return orderDetail;
        }
    }
}
