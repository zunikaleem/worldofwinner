﻿using mlm_lib.Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic
{
    public partial class SystemUser
    {

        public static LoginResult DoLogin(string userID, string password, out SystemUser systemUser)
        {
            systemUser = GetSystemUser(userID);

            if (systemUser.SystemUserID == 0) return LoginResult.InvalidLogin;

            if (systemUser.UserPassword != password) return LoginResult.Failed;

            return LoginResult.Success;
        }

        public Customer GetCustomer()
        {
            var customer = Customer.GetCustomer(this);

            if (customer != null)
                return customer;
            else
                return new Customer();

        }

        public static SystemUser GetSystemUser(string userID)
        {
            var users = Select($"where UserID='{userID}'");

            if (users.Any()) return users.First();
            else
                return new SystemUser();
        }

        public static SystemUser GetSystemUser(int systemUserID) => Select($"Where SystemUserID={systemUserID}").FirstOrDefault();


        public static List<SystemUser> GetSystemUsers(AccessGroup accessGroup) => Select($"Where AccessGroupID={accessGroup.AccessGroupID}");


        internal int InsertSytsemUser() => Insert();


        public int TranferEPIN(string transferToUserID, string pacakgeName, int quantity)
        {
            var tranferToSystemUser = SystemUser.GetSystemUser(transferToUserID);

            var tranferQuantity = 0;

            if (tranferToSystemUser.SystemUserID == 0)
                throw new Exception("Invalid receiver.");
            if (tranferToSystemUser.SystemUserID == this.SystemUserID)
                throw new Exception("Cant transfer to yourself.");

            var package = Package.GetPackage(pacakgeName);

            if (package.PackageID == 0)
                throw new Exception("Invalid Package");

            var unUsedEpinInbox = this.GetUnusedEPINInbox(package);

            if (unUsedEpinInbox.Count >= quantity)
            {
                for (int count = 0; count < quantity; count++)
                {
                    int x = unUsedEpinInbox[count].TransferEPIN(tranferToSystemUser);
                    if (x > 0)
                    {
                        int y = EPINInbox.InsertToEPINInbox(unUsedEpinInbox[count], this, tranferToSystemUser, this);

                        tranferQuantity++;
                    }
                }
            }
            else
            {
                var message = unUsedEpinInbox.Count == 0
                    ? $"You don't have any epin of type {package.PackageName}"
                    : $"You have only {unUsedEpinInbox.Count.ToString()} epin(s) available";
                throw new Exception(message);
            }

            //number of epin transfered
            return tranferQuantity;
        }

        public List<EPIN> GetUnusedEPINInbox(Package package) => EPIN.GetUnusedEPINs(package.PackageID, this.SystemUserID);

        public EPIN GetSingleAvailableEPIN(Package package) => GetUnusedEPINInbox(package).FirstOrDefault();

        public int RequestEPIN(Package package, int quantity, string transactionPath, string comment) => EPINRequest.placeRequest(package, quantity, this, transactionPath, comment);

        public List<Account> GetAccounts() => Account.GetAccount(SystemUserID);

        public List<Account> GetAccounts(string startDate, string endDate, string transactionType) => Account.GetAccounts(this.SystemUserID,startDate, endDate, transactionType);

        internal static SystemUser GetCompanyUser()
        {
            var accessGroup = AccessGroup.GetAccessGroup(AccessGroupConstant.SuperAdim);
            var systemuser = SystemUser.GetSystemUsers(accessGroup).FirstOrDefault();

            return systemuser;
        }

        public List<EPIN> GetEPINInbox(string epinType) => EPIN.GetEPINInbox(this, epinType);

        internal int UpdateSystemUser()
        {
            return Update();
        }

        public static List<ViewModel.PayoutBalanceList> GetPayoutBalnaceList()
        {
            var systemUsers = SelectAll();
            var payoutBalanceList = new List<ViewModel.PayoutBalanceList>();

            foreach (var systemUser in systemUsers)
            {
                var accounts = systemUser.GetAccounts();

                var totalCredit = accounts.Sum(x => x.CreditAmount);
                var totalDebit = accounts.Sum(x => x.DebitAmount);
                var balance = totalCredit - totalDebit;

                if (balance > 0)
                {
                    payoutBalanceList.Add(new ViewModel.PayoutBalanceList()
                    {
                        AccountHolderName = systemUser.AccountHolderName,
                        AccountNumber = systemUser.AccountNumber,
                        AccountType = systemUser.AccountType,
                        Balance = (decimal)balance,
                        BankName = systemUser.BankName,
                        BranchName = systemUser.BranchName,
                        ContactName = systemUser.ContactName,
                        IFCCode = systemUser.IFSCCode,
                        MobileNumber = systemUser.MobileNumber,
                        PANNumber = systemUser.PAN,
                        TotalCredit = (decimal)totalCredit,
                        TotalDebit = (decimal)totalDebit,
                        UserID = systemUser.UserID
                    });
                }
            }

            return payoutBalanceList;
        }
    }
}
