﻿using System;
using Common.ExtensionMethods;

namespace BusinessLogic
{
    public partial class CustomerApproval
    {
        internal static int approve(Customer customer, Customer updateBy)
        {
            var obj = new CustomerApproval();
            obj.Approved = true;
            obj.Customer = customer;
            obj.UpdateBy = updateBy.CustomerID;
            obj.UpdateDate = DateTime.Now.IST();

            return obj.Insert();
        }
    }
}