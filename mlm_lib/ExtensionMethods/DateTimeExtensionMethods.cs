﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ExtensionMethods
{
    public static class DateTimeExtensionMethods
    {
        public static DateTime IST(this DateTime dateTime)
        {
            DateTime val = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            return val;
        }
    }
}
