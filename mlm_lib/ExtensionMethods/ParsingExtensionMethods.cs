﻿using System;
using System.Globalization;
using System.Xml.XPath;
using static System.ComponentModel.TypeDescriptor;

namespace Common.ExtensionMethods
{
    public static class ParsingExt
    {
        public static T? TryGet<T>(this string s, CultureInfo cultureInfo = null) where T : struct
        {
            if (string.IsNullOrWhiteSpace(s))
                return null;

            try
            {
                if (typeof(T) == typeof(int))
                {
                    if (int.TryParse(s, NumberStyles.Integer, cultureInfo, out int result))
                        return result as T?;
                    else
                        return null;
                }

                var tc = GetConverter(typeof(T));
                return cultureInfo != null ? (T)tc.ConvertFrom(null, cultureInfo, s) : (T)tc.ConvertFrom(s);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static XPathExpression ToXPath(this string xpath)
        {
            try
            {
                return XPathExpression.Compile(xpath);
            }
            catch (XPathException)
            {
                throw new ArgumentException($"XPath [{xpath}] is invalid");
            }
        }
    }
}
