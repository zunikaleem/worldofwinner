using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class SMSLog:BusinessLogic.BusinessBase
	{

		public int SMSLogID { get;  set;}
		public string MobileNumber { get;  set;}
		public string MessasgeText { get;  set;}
		public DateTime UpdateDate { get;  set;}
		public string Status { get;  set;}

		public SMSLog(){}

		public SMSLog(string MobileNumber,string MessasgeText,DateTime UpdateDate,string Status)
		{
			this.MobileNumber=MobileNumber;
			this.MessasgeText=MessasgeText;
			this.UpdateDate=UpdateDate;
			this.Status=Status;
		}

		public SMSLog(int SMSLogID,string MobileNumber,string MessasgeText,DateTime UpdateDate,string Status)
		{
			this.SMSLogID=SMSLogID;
			this.MobileNumber=MobileNumber;
			this.MessasgeText=MessasgeText;
			this.UpdateDate=UpdateDate;
			this.Status=Status;
		}

		#region IBase Members

		private BusinessLogic.SMSLog Select()
		{
			try
			{
				BusinessLogic.SMSLog obj=new SMSLog();
				DataTable table=(new DataAccess.DASMSLog(DataAccess.Database.Master)).Select(this.SMSLogID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.SMSLog Select(int primaryKey)
		{
			try
			{
				BusinessLogic.SMSLog obj=new SMSLog();
				DataTable table=(new DataAccess.DASMSLog(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.SMSLog> Select(string condition)
		{
			try
			{
				List<BusinessLogic.SMSLog> objs=new List<BusinessLogic.SMSLog>();
				DataTable table=(new DataAccess.DASMSLog(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.SMSLog> SelectAll()
		{
			try
			{
				List<BusinessLogic.SMSLog> objs=new List<BusinessLogic.SMSLog>();
				DataTable table=(new DataAccess.DASMSLog(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.SMSLog> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.SMSLog> objs=new List<BusinessLogic.SMSLog>();
				DataTable table=(new DataAccess.DASMSLog(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.SMSLogID= (new DataAccess.DASMSLog(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DASMSLog(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DASMSLog(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DASMSLog(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DASMSLog(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DASMSLog(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.SMSLog obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.MobileNumber).Trim().Equals(string.Empty)) 
							throw new Exception("The MobileNumber should not be left blank.");
						if (Convert.ToString(obj.MessasgeText).Trim().Equals(string.Empty)) 
							throw new Exception("The MessasgeText should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.SMSLogID).Trim().Equals(string.Empty)) 
							throw new Exception("The SMSLogID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.SMSLog> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.SMSLog obj = new BusinessLogic.SMSLog();
					//Add Custom code here

					obj.SMSLogID=Convert.ToInt32(dr["SMSLogID"]);

					if(dr["MobileNumber"]==DBNull.Value)
						obj.MobileNumber=null;
					else
						obj.MobileNumber=Convert.ToString(dr["MobileNumber"]);

					if(dr["MessasgeText"]==DBNull.Value)
						obj.MessasgeText=null;
					else
						obj.MessasgeText=Convert.ToString(dr["MessasgeText"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					if(dr["Status"]==DBNull.Value)
						obj.Status=null;
					else
						obj.Status=Convert.ToString(dr["Status"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.SMSLog obj, DataRow dr)
		{
			try
			{
					obj.SMSLogID=Convert.ToInt32(dr["SMSLogID"]);

				if(dr["MobileNumber"]==DBNull.Value)
					obj.MobileNumber=null;
				else
					obj.MobileNumber=Convert.ToString(dr["MobileNumber"]);

				if(dr["MessasgeText"]==DBNull.Value)
					obj.MessasgeText=null;
				else
					obj.MessasgeText=Convert.ToString(dr["MessasgeText"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

				if(dr["Status"]==DBNull.Value)
					obj.Status=null;
				else
					obj.Status=Convert.ToString(dr["Status"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
