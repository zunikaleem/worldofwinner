using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class Package:BusinessLogic.BusinessBase
	{

		public int PackageID { get; private set;}
		public string PackageName { get;  set;}
		public float BasePrice { get; set;}
		public float SGST { get;  set;}
		public float CGST { get;  set;}
		public float RoundOff { get;  set;}
		public float Cost { get;  private set;}
		public string Description { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}

		public Package(){}

		public Package(string PackageName,float BasePrice,float SGST,float CGST,float RoundOff,float Cost,string Description,int UpdateBy,DateTime UpdateDate)
		{
			this.PackageName=PackageName;
			this.BasePrice=BasePrice;
			this.SGST=SGST;
			this.CGST=CGST;
			this.RoundOff=RoundOff;
			this.Cost=Cost;
			this.Description=Description;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		public Package(int PackageID,string PackageName,float BasePrice,float SGST,float CGST,float RoundOff,float Cost,string Description,int UpdateBy,DateTime UpdateDate)
		{
			this.PackageID=PackageID;
			this.PackageName=PackageName;
			this.BasePrice=BasePrice;
			this.SGST=SGST;
			this.CGST=CGST;
			this.RoundOff=RoundOff;
			this.Cost=Cost;
			this.Description=Description;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		#region IBase Members

		private BusinessLogic.Package Select()
		{
			try
			{
				BusinessLogic.Package obj=new Package();
				DataTable table=(new DataAccess.DAPackage(DataAccess.Database.Master)).Select(this.PackageID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.Package Select(int primaryKey)
		{
			try
			{
				BusinessLogic.Package obj=new Package();
				DataTable table=(new DataAccess.DAPackage(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Package> Select(string condition)
		{
			try
			{
				List<BusinessLogic.Package> objs=new List<BusinessLogic.Package>();
				DataTable table=(new DataAccess.DAPackage(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Package> SelectAll()
		{
			try
			{
				List<BusinessLogic.Package> objs=new List<BusinessLogic.Package>();
				DataTable table=(new DataAccess.DAPackage(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Package> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.Package> objs=new List<BusinessLogic.Package>();
				DataTable table=(new DataAccess.DAPackage(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.PackageID= (new DataAccess.DAPackage(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAPackage(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAPackage(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAPackage(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAPackage(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAPackage(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.Package obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.PackageName).Trim().Equals(string.Empty)) 
							throw new Exception("The PackageName should not be left blank.");
						if (Convert.ToString(obj.BasePrice).Trim().Equals(string.Empty)) 
							throw new Exception("The BasePrice should not be left blank.");
						if (Convert.ToString(obj.SGST).Trim().Equals(string.Empty)) 
							throw new Exception("The SGST should not be left blank.");
						if (Convert.ToString(obj.CGST).Trim().Equals(string.Empty)) 
							throw new Exception("The CGST should not be left blank.");
						if (Convert.ToString(obj.RoundOff).Trim().Equals(string.Empty)) 
							throw new Exception("The RoundOff should not be left blank.");
						if (Convert.ToString(obj.Cost).Trim().Equals(string.Empty)) 
							throw new Exception("The Cost should not be left blank.");
						if (Convert.ToString(obj.Description).Trim().Equals(string.Empty)) 
							throw new Exception("The Description should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.PackageID).Trim().Equals(string.Empty)) 
							throw new Exception("The PackageID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.Package> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.Package obj = new BusinessLogic.Package();
					//Add Custom code here

					obj.PackageID=Convert.ToInt32(dr["PackageID"]);

					if(dr["PackageName"]==DBNull.Value)
						obj.PackageName=null;
					else
						obj.PackageName=Convert.ToString(dr["PackageName"]);

					obj.BasePrice=Convert.ToSingle(dr["BasePrice"]);

					obj.SGST=Convert.ToSingle(dr["SGST"]);

					obj.CGST=Convert.ToSingle(dr["CGST"]);

					obj.RoundOff=Convert.ToSingle(dr["RoundOff"]);

					obj.Cost=Convert.ToSingle(dr["Cost"]);

					if(dr["Description"]==DBNull.Value)
						obj.Description=null;
					else
						obj.Description=Convert.ToString(dr["Description"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.Package obj, DataRow dr)
		{
			try
			{
					obj.PackageID=Convert.ToInt32(dr["PackageID"]);

				if(dr["PackageName"]==DBNull.Value)
					obj.PackageName=null;
				else
					obj.PackageName=Convert.ToString(dr["PackageName"]);

					obj.BasePrice=Convert.ToSingle(dr["BasePrice"]);

					obj.SGST=Convert.ToSingle(dr["SGST"]);

					obj.CGST=Convert.ToSingle(dr["CGST"]);

					obj.RoundOff=Convert.ToSingle(dr["RoundOff"]);

					obj.Cost=Convert.ToSingle(dr["Cost"]);

				if(dr["Description"]==DBNull.Value)
					obj.Description=null;
				else
					obj.Description=Convert.ToString(dr["Description"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
        #endregion
    }
}
