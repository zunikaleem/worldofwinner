using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class AccessGroup:BusinessLogic.BusinessBase
	{

		public int AccessGroupID { get;  set;}
		public string AccessGroupName { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}

		public AccessGroup(){}

		public AccessGroup(string AccessGroupName,int UpdateBy,DateTime UpdateDate)
		{
			this.AccessGroupName=AccessGroupName;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		public AccessGroup(int AccessGroupID,string AccessGroupName,int UpdateBy,DateTime UpdateDate)
		{
			this.AccessGroupID=AccessGroupID;
			this.AccessGroupName=AccessGroupName;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		#region IBase Members

		private BusinessLogic.AccessGroup Select()
		{
			try
			{
				BusinessLogic.AccessGroup obj=new AccessGroup();
				DataTable table=(new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Select(this.AccessGroupID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.AccessGroup Select(int primaryKey)
		{
			try
			{
				BusinessLogic.AccessGroup obj=new AccessGroup();
				DataTable table=(new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.AccessGroup> Select(string condition)
		{
			try
			{
				List<BusinessLogic.AccessGroup> objs=new List<BusinessLogic.AccessGroup>();
				DataTable table=(new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.AccessGroup> SelectAll()
		{
			try
			{
				List<BusinessLogic.AccessGroup> objs=new List<BusinessLogic.AccessGroup>();
				DataTable table=(new DataAccess.DAAccessGroup(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.AccessGroup> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.AccessGroup> objs=new List<BusinessLogic.AccessGroup>();
				DataTable table=(new DataAccess.DAAccessGroup(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.AccessGroupID= (new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAAccessGroup(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAAccessGroup(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAAccessGroup(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.AccessGroup obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.AccessGroupName).Trim().Equals(string.Empty)) 
							throw new Exception("The AccessGroupName should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.AccessGroupID).Trim().Equals(string.Empty)) 
							throw new Exception("The AccessGroupID should not be left blank.");
						if (Convert.ToString(obj.AccessGroupName).Trim().Equals(string.Empty)) 
							throw new Exception("The AccessGroupName should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.AccessGroup> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.AccessGroup obj = new BusinessLogic.AccessGroup();
					//Add Custom code here

					obj.AccessGroupID=Convert.ToInt32(dr["AccessGroupID"]);

					if(dr["AccessGroupName"]==DBNull.Value)
						obj.AccessGroupName=null;
					else
						obj.AccessGroupName=Convert.ToString(dr["AccessGroupName"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.AccessGroup obj, DataRow dr)
		{
			try
			{
					obj.AccessGroupID=Convert.ToInt32(dr["AccessGroupID"]);

				if(dr["AccessGroupName"]==DBNull.Value)
					obj.AccessGroupName=null;
				else
					obj.AccessGroupName=Convert.ToString(dr["AccessGroupName"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
