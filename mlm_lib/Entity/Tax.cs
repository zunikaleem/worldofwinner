using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class Tax:BusinessLogic.BusinessBase
	{

		public int TaxID { get;  set;}
		public string TaxType { get;  set;}
		public string CalculateIn { get;  set;}
		public float Value { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}

		public Tax(){}

		public Tax(string TaxType,string CalculateIn,float Value,int UpdateBy,DateTime UpdateDate)
		{
			this.TaxType=TaxType;
			this.CalculateIn=CalculateIn;
			this.Value=Value;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		public Tax(int TaxID,string TaxType,string CalculateIn,float Value,int UpdateBy,DateTime UpdateDate)
		{
			this.TaxID=TaxID;
			this.TaxType=TaxType;
			this.CalculateIn=CalculateIn;
			this.Value=Value;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		#region IBase Members

		private BusinessLogic.Tax Select()
		{
			try
			{
				BusinessLogic.Tax obj=new Tax();
				DataTable table=(new DataAccess.DATax(DataAccess.Database.Master)).Select(this.TaxID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.Tax Select(int primaryKey)
		{
			try
			{
				BusinessLogic.Tax obj=new Tax();
				DataTable table=(new DataAccess.DATax(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Tax> Select(string condition)
		{
			try
			{
				List<BusinessLogic.Tax> objs=new List<BusinessLogic.Tax>();
				DataTable table=(new DataAccess.DATax(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Tax> SelectAll()
		{
			try
			{
				List<BusinessLogic.Tax> objs=new List<BusinessLogic.Tax>();
				DataTable table=(new DataAccess.DATax(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Tax> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.Tax> objs=new List<BusinessLogic.Tax>();
				DataTable table=(new DataAccess.DATax(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.TaxID= (new DataAccess.DATax(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DATax(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DATax(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DATax(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DATax(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DATax(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.Tax obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.TaxType).Trim().Equals(string.Empty)) 
							throw new Exception("The TaxType should not be left blank.");
						if (Convert.ToString(obj.CalculateIn).Trim().Equals(string.Empty)) 
							throw new Exception("The CalculateIn should not be left blank.");
						if (Convert.ToString(obj.Value).Trim().Equals(string.Empty)) 
							throw new Exception("The Value should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.TaxID).Trim().Equals(string.Empty)) 
							throw new Exception("The TaxID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.Tax> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.Tax obj = new BusinessLogic.Tax();
					//Add Custom code here

					obj.TaxID=Convert.ToInt32(dr["TaxID"]);

					if(dr["TaxType"]==DBNull.Value)
						obj.TaxType=null;
					else
						obj.TaxType=Convert.ToString(dr["TaxType"]);

					if(dr["CalculateIn"]==DBNull.Value)
						obj.CalculateIn=null;
					else
						obj.CalculateIn=Convert.ToString(dr["CalculateIn"]);

					obj.Value=Convert.ToSingle(dr["Value"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.Tax obj, DataRow dr)
		{
			try
			{
					obj.TaxID=Convert.ToInt32(dr["TaxID"]);

				if(dr["TaxType"]==DBNull.Value)
					obj.TaxType=null;
				else
					obj.TaxType=Convert.ToString(dr["TaxType"]);

				if(dr["CalculateIn"]==DBNull.Value)
					obj.CalculateIn=null;
				else
					obj.CalculateIn=Convert.ToString(dr["CalculateIn"]);

					obj.Value=Convert.ToSingle(dr["Value"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
