using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class EPIN:BusinessLogic.BusinessBase
	{

		public int EPINID { get; private set;}
		public string EPINNumber { get;  set;}
		public BusinessLogic.Package Package { get;  set;}
		public int UpdateBy { get; private set;}
		public DateTime UpdateDate { get; private set;}
		public int SystemUserID { get;  set;}
		public DateTime? TransactionDate { get; private set;}

		public EPIN(){}

		public EPIN(string EPINNumber,BusinessLogic.Package Package,int UpdateBy,DateTime UpdateDate,int SystemUserID,DateTime TransactionDate)
		{
			this.EPINNumber=EPINNumber;
			this.Package=Package;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.SystemUserID=SystemUserID;
			this.TransactionDate=TransactionDate;
		}

		public EPIN(int EPINID,string EPINNumber,BusinessLogic.Package Package,int UpdateBy,DateTime UpdateDate,int SystemUserID,DateTime TransactionDate)
		{
			this.EPINID=EPINID;
			this.EPINNumber=EPINNumber;
			this.Package=Package;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.SystemUserID=SystemUserID;
			this.TransactionDate=TransactionDate;
		}

		#region IBase Members

		private BusinessLogic.EPIN Select()
		{
			try
			{
				BusinessLogic.EPIN obj=new EPIN();
				DataTable table=(new DataAccess.DAEPIN(DataAccess.Database.Master)).Select(this.EPINID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.EPIN Select(int primaryKey)
		{
			try
			{
				BusinessLogic.EPIN obj=new EPIN();
				DataTable table=(new DataAccess.DAEPIN(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPIN> Select(string condition)
		{
			try
			{
				List<BusinessLogic.EPIN> objs=new List<BusinessLogic.EPIN>();
				DataTable table=(new DataAccess.DAEPIN(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPIN> SelectAll()
		{
			try
			{
				List<BusinessLogic.EPIN> objs=new List<BusinessLogic.EPIN>();
				DataTable table=(new DataAccess.DAEPIN(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPIN> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.EPIN> objs=new List<BusinessLogic.EPIN>();
				DataTable table=(new DataAccess.DAEPIN(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.EPINID= (new DataAccess.DAEPIN(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAEPIN(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAEPIN(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAEPIN(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAEPIN(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAEPIN(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAEPIN(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.EPIN obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.EPINNumber).Trim().Equals(string.Empty )) 
							throw new Exception("The EPINNumber should not be left blank.");
						if (Convert.ToString(obj.Package.PackageID).Trim().Equals(string.Empty )) 
							throw new Exception("The Package should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateDate should not be left blank.");
						if (Convert.ToString(obj.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUserID should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.EPINID).Trim().Equals(string.Empty )) 
							throw new Exception("The EPINID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.EPIN> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.EPIN obj = new BusinessLogic.EPIN();
					//Add Custom code here

					obj.EPINID=Convert.ToInt32(dr["EPINID"]);

					if(dr["EPINNumber"]==DBNull.Value)
						obj.EPINNumber=null;
					else
						obj.EPINNumber=Convert.ToString(dr["EPINNumber"]);

					obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					obj.SystemUserID=Convert.ToInt32(dr["SystemUserID"]);

					if(dr["TransactionDate"]==DBNull.Value)
						obj.TransactionDate=null;
					else
						obj.TransactionDate=Convert.ToDateTime(dr["TransactionDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.EPIN obj, DataRow dr)
		{
			try
			{
					obj.EPINID=Convert.ToInt32(dr["EPINID"]);

				if(dr["EPINNumber"]==DBNull.Value)
					obj.EPINNumber=null;
				else
					obj.EPINNumber=Convert.ToString(dr["EPINNumber"]);

				obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					obj.SystemUserID=Convert.ToInt32(dr["SystemUserID"]);

				if(dr["TransactionDate"]==DBNull.Value)
					obj.TransactionDate=null;
				else
					obj.TransactionDate=Convert.ToDateTime(dr["TransactionDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
