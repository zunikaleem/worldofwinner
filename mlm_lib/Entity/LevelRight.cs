using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class LevelRight:BusinessLogic.BusinessBase
	{

		public int LevelRightID {get;set;}
		public BusinessLogic.Customer AdminID {get;set;}

		public LevelRight(){}

		public LevelRight(BusinessLogic.Customer AdminID)
		{
			this.AdminID=AdminID;
		}

		public LevelRight(int LevelRightID,BusinessLogic.Customer AdminID)
		{
			this.LevelRightID=LevelRightID;
			this.AdminID=AdminID;
		}

		#region IBase Members

		private BusinessLogic.LevelRight Select()
		{
			try
			{
				BusinessLogic.LevelRight obj=new LevelRight();
				DataTable table=(new DataAccess.DALevelRight(DataAccess.Database.Master)).Select(this.LevelRightID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.LevelRight Select(int primaryKey)
		{
			try
			{
				BusinessLogic.LevelRight obj=new LevelRight();
				DataTable table=(new DataAccess.DALevelRight(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.LevelRight> Select(string condition)
		{
			try
			{
				List<BusinessLogic.LevelRight> objs=new List<BusinessLogic.LevelRight>();
				DataTable table=(new DataAccess.DALevelRight(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.LevelRight> SelectAll()
		{
			try
			{
				List<BusinessLogic.LevelRight> objs=new List<BusinessLogic.LevelRight>();
				DataTable table=(new DataAccess.DALevelRight(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.LevelRight> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.LevelRight> objs=new List<BusinessLogic.LevelRight>();
				DataTable table=(new DataAccess.DALevelRight(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.LevelRightID= (new DataAccess.DALevelRight(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DALevelRight(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DALevelRight(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DALevelRight(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DALevelRight(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.LevelRight obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.LevelRightID).Trim().Equals(string.Empty)) 
							throw new Exception("The LevelRightID should not be left blank.");
						if (Convert.ToString(obj.AdminID.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The AdminID should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.LevelRightID).Trim().Equals(string.Empty)) 
							throw new Exception("The LevelRightID should not be left blank.");
						if (Convert.ToString(obj.AdminID.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The AdminID should not be left blank.");

						break;
					case OperationMode.Delete :
						if (Convert.ToString(obj.LevelRightID).Trim().Equals(string.Empty)) 
							throw new Exception("The LevelRightID should not be left blank.");
						if (Convert.ToString(obj.AdminID.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The AdminID should not be left blank.");

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.LevelRight> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.LevelRight obj = new BusinessLogic.LevelRight();
					//Add Custom code here

					obj.LevelRightID=Convert.ToInt32(dr["LevelRightID"]);

					obj.AdminID=BusinessLogic.Customer.Select(Convert.ToInt32(dr["AdminID"]));
					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.LevelRight obj, DataRow dr)
		{
			try
			{
					obj.LevelRightID=Convert.ToInt32(dr["LevelRightID"]);

				obj.AdminID=BusinessLogic.Customer.Select(Convert.ToInt32(dr["AdminID"]));
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
