using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class EPINRequest:BusinessLogic.BusinessBase
	{

		public int EPINRequestID { get;  set;}
		public BusinessLogic.SystemUser SystemUser { get;  set;}
		public BusinessLogic.Package Package { get;  set;}
		public int Quantity { get;  set;}
		public DateTime RequestDate { get;  set;}
		public string Status { get;  set;}
		public DateTime? ProcessDate { get;  set;}
		public int? TransferredQuantity { get;  set;}
		public string TransactionPath { get;  set;}
		public string UserComment { get;  set;}
		public string AdminComment { get;  set;}

		public EPINRequest(){}

		public EPINRequest(BusinessLogic.SystemUser SystemUser,BusinessLogic.Package Package,int Quantity,DateTime RequestDate,string Status,DateTime ProcessDate,int TransferredQuantity,string TransactionPath,string UserComment,string AdminComment)
		{
			this.SystemUser=SystemUser;
			this.Package=Package;
			this.Quantity=Quantity;
			this.RequestDate=RequestDate;
			this.Status=Status;
			this.ProcessDate=ProcessDate;
			this.TransferredQuantity=TransferredQuantity;
			this.TransactionPath=TransactionPath;
			this.UserComment=UserComment;
			this.AdminComment=AdminComment;
		}

		public EPINRequest(int EPINRequestID,BusinessLogic.SystemUser SystemUser,BusinessLogic.Package Package,int Quantity,DateTime RequestDate,string Status,DateTime ProcessDate,int TransferredQuantity,string TransactionPath,string UserComment,string AdminComment)
		{
			this.EPINRequestID=EPINRequestID;
			this.SystemUser=SystemUser;
			this.Package=Package;
			this.Quantity=Quantity;
			this.RequestDate=RequestDate;
			this.Status=Status;
			this.ProcessDate=ProcessDate;
			this.TransferredQuantity=TransferredQuantity;
			this.TransactionPath=TransactionPath;
			this.UserComment=UserComment;
			this.AdminComment=AdminComment;
		}

		#region IBase Members

		private BusinessLogic.EPINRequest Select()
		{
			try
			{
				BusinessLogic.EPINRequest obj=new EPINRequest();
				DataTable table=(new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Select(this.EPINRequestID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.EPINRequest Select(int primaryKey)
		{
			try
			{
				BusinessLogic.EPINRequest obj=new EPINRequest();
				DataTable table=(new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPINRequest> Select(string condition)
		{
			try
			{
				List<BusinessLogic.EPINRequest> objs=new List<BusinessLogic.EPINRequest>();
				DataTable table=(new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPINRequest> SelectAll()
		{
			try
			{
				List<BusinessLogic.EPINRequest> objs=new List<BusinessLogic.EPINRequest>();
				DataTable table=(new DataAccess.DAEPINRequest(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPINRequest> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.EPINRequest> objs=new List<BusinessLogic.EPINRequest>();
				DataTable table=(new DataAccess.DAEPINRequest(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.EPINRequestID= (new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAEPINRequest(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAEPINRequest(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAEPINRequest(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.EPINRequest obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.SystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUser should not be left blank.");
						if (Convert.ToString(obj.Package.PackageID).Trim().Equals(string.Empty )) 
							throw new Exception("The Package should not be left blank.");
						if (Convert.ToString(obj.Quantity).Trim().Equals(string.Empty )) 
							throw new Exception("The Quantity should not be left blank.");
						if (Convert.ToString(obj.RequestDate).Trim().Equals(string.Empty )) 
							throw new Exception("The RequestDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.EPINRequestID).Trim().Equals(string.Empty )) 
							throw new Exception("The EPINRequestID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.EPINRequest> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.EPINRequest obj = new BusinessLogic.EPINRequest();
					//Add Custom code here

					obj.EPINRequestID=Convert.ToInt32(dr["EPINRequestID"]);

					obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
					obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.Quantity=Convert.ToInt32(dr["Quantity"]);

					obj.RequestDate=Convert.ToDateTime(dr["RequestDate"]);

					if(dr["Status"]==DBNull.Value)
						obj.Status=null;
					else
						obj.Status=Convert.ToString(dr["Status"]);

					if(dr["ProcessDate"]==DBNull.Value)
						obj.ProcessDate=null;
					else
						obj.ProcessDate=Convert.ToDateTime(dr["ProcessDate"]);

					if(dr["TransferredQuantity"]==DBNull.Value)
						obj.TransferredQuantity=null;
					else
						obj.TransferredQuantity=Convert.ToInt32(dr["TransferredQuantity"]);

					if(dr["TransactionPath"]==DBNull.Value)
						obj.TransactionPath=null;
					else
						obj.TransactionPath=Convert.ToString(dr["TransactionPath"]);

					if(dr["UserComment"]==DBNull.Value)
						obj.UserComment=null;
					else
						obj.UserComment=Convert.ToString(dr["UserComment"]);

					if(dr["AdminComment"]==DBNull.Value)
						obj.AdminComment=null;
					else
						obj.AdminComment=Convert.ToString(dr["AdminComment"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.EPINRequest obj, DataRow dr)
		{
			try
			{
					obj.EPINRequestID=Convert.ToInt32(dr["EPINRequestID"]);

				obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
				obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.Quantity=Convert.ToInt32(dr["Quantity"]);

					obj.RequestDate=Convert.ToDateTime(dr["RequestDate"]);

				if(dr["Status"]==DBNull.Value)
					obj.Status=null;
				else
					obj.Status=Convert.ToString(dr["Status"]);

				if(dr["ProcessDate"]==DBNull.Value)
					obj.ProcessDate=null;
				else
					obj.ProcessDate=Convert.ToDateTime(dr["ProcessDate"]);

				if(dr["TransferredQuantity"]==DBNull.Value)
					obj.TransferredQuantity=null;
				else
					obj.TransferredQuantity=Convert.ToInt32(dr["TransferredQuantity"]);

				if(dr["TransactionPath"]==DBNull.Value)
					obj.TransactionPath=null;
				else
					obj.TransactionPath=Convert.ToString(dr["TransactionPath"]);

				if(dr["UserComment"]==DBNull.Value)
					obj.UserComment=null;
				else
					obj.UserComment=Convert.ToString(dr["UserComment"]);

				if(dr["AdminComment"]==DBNull.Value)
					obj.AdminComment=null;
				else
					obj.AdminComment=Convert.ToString(dr["AdminComment"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
