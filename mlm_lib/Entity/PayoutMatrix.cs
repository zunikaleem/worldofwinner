using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class PayoutMatrix:BusinessLogic.BusinessBase
	{

		public float Amount { get;  set;}
		public int Level { get;  set;}
		public int PackageId { get;  set;}
		public int PayoutMatrixId { get;  set;}
		public int Release { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}

		public PayoutMatrix(){}

		public PayoutMatrix(float Amount,int Level,int PackageId,int Release,int UpdateBy,DateTime UpdateDate)
		{
			this.Amount=Amount;
			this.Level=Level;
			this.PackageId=PackageId;
			this.Release=Release;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		public PayoutMatrix(float Amount,int Level,int PackageId,int PayoutMatrixId,int Release,int UpdateBy,DateTime UpdateDate)
		{
			this.Amount=Amount;
			this.Level=Level;
			this.PackageId=PackageId;
			this.PayoutMatrixId=PayoutMatrixId;
			this.Release=Release;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		#region IBase Members

		private BusinessLogic.PayoutMatrix Select()
		{
			try
			{
				BusinessLogic.PayoutMatrix obj=new PayoutMatrix();
				DataTable table=(new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Select(this.PayoutMatrixId);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.PayoutMatrix Select(int primaryKey)
		{
			try
			{
				BusinessLogic.PayoutMatrix obj=new PayoutMatrix();
				DataTable table=(new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.PayoutMatrix> Select(string condition)
		{
			try
			{
				List<BusinessLogic.PayoutMatrix> objs=new List<BusinessLogic.PayoutMatrix>();
				DataTable table=(new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.PayoutMatrix> SelectAll()
		{
			try
			{
				List<BusinessLogic.PayoutMatrix> objs=new List<BusinessLogic.PayoutMatrix>();
				DataTable table=(new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.PayoutMatrix> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.PayoutMatrix> objs=new List<BusinessLogic.PayoutMatrix>();
				DataTable table=(new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.PayoutMatrixId= (new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAPayoutMatrix(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.PayoutMatrix obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.Amount).Trim().Equals(string.Empty)) 
							throw new Exception("The Amount should not be left blank.");
						if (Convert.ToString(obj.Level).Trim().Equals(string.Empty)) 
							throw new Exception("The Level should not be left blank.");
						if (Convert.ToString(obj.PackageId).Trim().Equals(string.Empty)) 
							throw new Exception("The PackageId should not be left blank.");
						if (Convert.ToString(obj.Release).Trim().Equals(string.Empty)) 
							throw new Exception("The Release should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.PayoutMatrixId).Trim().Equals(string.Empty)) 
							throw new Exception("The PayoutMatrixId should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.PayoutMatrix> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.PayoutMatrix obj = new BusinessLogic.PayoutMatrix();
					//Add Custom code here

					obj.Amount=Convert.ToSingle(dr["Amount"]);

					obj.Level=Convert.ToInt32(dr["Level"]);

					obj.PackageId=Convert.ToInt32(dr["PackageId"]);

					obj.PayoutMatrixId=Convert.ToInt32(dr["PayoutMatrixId"]);

					obj.Release=Convert.ToInt32(dr["Release"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.PayoutMatrix obj, DataRow dr)
		{
			try
			{
					obj.Amount=Convert.ToSingle(dr["Amount"]);

					obj.Level=Convert.ToInt32(dr["Level"]);

					obj.PackageId=Convert.ToInt32(dr["PackageId"]);

					obj.PayoutMatrixId=Convert.ToInt32(dr["PayoutMatrixId"]);

					obj.Release=Convert.ToInt32(dr["Release"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
