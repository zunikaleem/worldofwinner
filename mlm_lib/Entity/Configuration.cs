using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class Configuration:BusinessLogic.BusinessBase
	{

		public string ConfigKey { get;  set;}
		public string ConfigType { get;  set;}
		public int ConfigurationID { get;  set;}
		public string ConfigValue { get;  set;}

		public Configuration(){}

		public Configuration(string ConfigKey,string ConfigType,string ConfigValue)
		{
			this.ConfigKey=ConfigKey;
			this.ConfigType=ConfigType;
			this.ConfigValue=ConfigValue;
		}

		public Configuration(string ConfigKey,string ConfigType,int ConfigurationID,string ConfigValue)
		{
			this.ConfigKey=ConfigKey;
			this.ConfigType=ConfigType;
			this.ConfigurationID=ConfigurationID;
			this.ConfigValue=ConfigValue;
		}

		#region IBase Members

		private BusinessLogic.Configuration Select()
		{
			try
			{
				BusinessLogic.Configuration obj=new Configuration();
				DataTable table=(new DataAccess.DAConfiguration(DataAccess.Database.Master)).Select(this.ConfigurationID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.Configuration Select(int primaryKey)
		{
			try
			{
				BusinessLogic.Configuration obj=new Configuration();
				DataTable table=(new DataAccess.DAConfiguration(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Configuration> Select(string condition)
		{
			try
			{
				List<BusinessLogic.Configuration> objs=new List<BusinessLogic.Configuration>();
				DataTable table=(new DataAccess.DAConfiguration(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Configuration> SelectAll()
		{
			try
			{
				List<BusinessLogic.Configuration> objs=new List<BusinessLogic.Configuration>();
				DataTable table=(new DataAccess.DAConfiguration(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Configuration> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.Configuration> objs=new List<BusinessLogic.Configuration>();
				DataTable table=(new DataAccess.DAConfiguration(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.ConfigurationID= (new DataAccess.DAConfiguration(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAConfiguration(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAConfiguration(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAConfiguration(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAConfiguration(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAConfiguration(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAConfiguration(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.Configuration obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.ConfigKey).Trim().Equals(string.Empty)) 
							throw new Exception("The ConfigKey should not be left blank.");
						if (Convert.ToString(obj.ConfigValue).Trim().Equals(string.Empty)) 
							throw new Exception("The ConfigValue should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.ConfigKey).Trim().Equals(string.Empty)) 
							throw new Exception("The ConfigKey should not be left blank.");
						if (Convert.ToString(obj.ConfigurationID).Trim().Equals(string.Empty)) 
							throw new Exception("The ConfigurationID should not be left blank.");
						if (Convert.ToString(obj.ConfigValue).Trim().Equals(string.Empty)) 
							throw new Exception("The ConfigValue should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.Configuration> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.Configuration obj = new BusinessLogic.Configuration();
					//Add Custom code here

					if(dr["ConfigKey"]==DBNull.Value)
						obj.ConfigKey=null;
					else
						obj.ConfigKey=Convert.ToString(dr["ConfigKey"]);

					if(dr["ConfigType"]==DBNull.Value)
						obj.ConfigType=null;
					else
						obj.ConfigType=Convert.ToString(dr["ConfigType"]);

					obj.ConfigurationID=Convert.ToInt32(dr["ConfigurationID"]);

					if(dr["ConfigValue"]==DBNull.Value)
						obj.ConfigValue=null;
					else
						obj.ConfigValue=Convert.ToString(dr["ConfigValue"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.Configuration obj, DataRow dr)
		{
			try
			{
				if(dr["ConfigKey"]==DBNull.Value)
					obj.ConfigKey=null;
				else
					obj.ConfigKey=Convert.ToString(dr["ConfigKey"]);

				if(dr["ConfigType"]==DBNull.Value)
					obj.ConfigType=null;
				else
					obj.ConfigType=Convert.ToString(dr["ConfigType"]);

					obj.ConfigurationID=Convert.ToInt32(dr["ConfigurationID"]);

				if(dr["ConfigValue"]==DBNull.Value)
					obj.ConfigValue=null;
				else
					obj.ConfigValue=Convert.ToString(dr["ConfigValue"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
