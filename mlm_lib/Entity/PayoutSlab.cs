using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class PayoutSlab:BusinessLogic.BusinessBase
	{

		public float TotalSale { get;  set;}
		public int Level { get;  set;}
		public int PayoutSlabID { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}
		public BusinessLogic.Package Package { get;  set;}

		public PayoutSlab(){}

		public PayoutSlab(float TotalSale,int Level,int UpdateBy,DateTime UpdateDate,BusinessLogic.Package Package)
		{
			this.TotalSale=TotalSale;
			this.Level=Level;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.Package=Package;
		}

		public PayoutSlab(float TotalSale,int Level,int PayoutSlabID,int UpdateBy,DateTime UpdateDate,BusinessLogic.Package Package)
		{
			this.TotalSale=TotalSale;
			this.Level=Level;
			this.PayoutSlabID=PayoutSlabID;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.Package=Package;
		}

		#region IBase Members

		private BusinessLogic.PayoutSlab Select()
		{
			try
			{
				BusinessLogic.PayoutSlab obj=new PayoutSlab();
				DataTable table=(new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Select(this.PayoutSlabID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.PayoutSlab Select(int primaryKey)
		{
			try
			{
				BusinessLogic.PayoutSlab obj=new PayoutSlab();
				DataTable table=(new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.PayoutSlab> Select(string condition)
		{
			try
			{
				List<BusinessLogic.PayoutSlab> objs=new List<BusinessLogic.PayoutSlab>();
				DataTable table=(new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.PayoutSlab> SelectAll()
		{
			try
			{
				List<BusinessLogic.PayoutSlab> objs=new List<BusinessLogic.PayoutSlab>();
				DataTable table=(new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.PayoutSlab> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.PayoutSlab> objs=new List<BusinessLogic.PayoutSlab>();
				DataTable table=(new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.PayoutSlabID= (new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAPayoutSlab(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.PayoutSlab obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.TotalSale).Trim().Equals(string.Empty )) 
							throw new Exception("The TotalSale should not be left blank.");
						if (Convert.ToString(obj.Level).Trim().Equals(string.Empty )) 
							throw new Exception("The Level should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateDate should not be left blank.");
						if (Convert.ToString(obj.Package.PackageID).Trim().Equals(string.Empty )) 
							throw new Exception("The Package should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.TotalSale).Trim().Equals(string.Empty )) 
							throw new Exception("The TotalSale should not be left blank.");
						if (Convert.ToString(obj.Level).Trim().Equals(string.Empty )) 
							throw new Exception("The Level should not be left blank.");
						if (Convert.ToString(obj.PayoutSlabID).Trim().Equals(string.Empty )) 
							throw new Exception("The PayoutSlabID should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateDate should not be left blank.");
						if (Convert.ToString(obj.Package.PackageID).Trim().Equals(string.Empty )) 
							throw new Exception("The Package should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.PayoutSlab> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.PayoutSlab obj = new BusinessLogic.PayoutSlab();
					//Add Custom code here

					obj.TotalSale=Convert.ToSingle(dr["TotalSale"]);

					obj.Level=Convert.ToInt32(dr["Level"]);

					obj.PayoutSlabID=Convert.ToInt32(dr["PayoutSlabID"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.PayoutSlab obj, DataRow dr)
		{
			try
			{
					obj.TotalSale=Convert.ToSingle(dr["TotalSale"]);

					obj.Level=Convert.ToInt32(dr["Level"]);

					obj.PayoutSlabID=Convert.ToInt32(dr["PayoutSlabID"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

				obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
