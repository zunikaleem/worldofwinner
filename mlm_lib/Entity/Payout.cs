using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class Payout:BusinessLogic.BusinessBase
	{

		public int AtStage { get;  set;}
		public BusinessLogic.Customer Customer { get;  set;}
		public DateTime GenerateDate { get;  set;}
		public float GrossAmount { get;  set;}
		public float NetAmount { get;  set;}
		public DateTime PayoutDate { get;  set;}
		public int PayoutID { get;  set;}
		public float ServiceCharge { get;  set;}
		public BusinessLogic.Customer SponsorTo { get;  set;}
		public float TDS { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}
		public string TransactionID { get;  set;}
		public string PayoutType { get;  set;}
		public float ServiceChargeAmount { get;  set;}
		public float TDSAmount { get;  set;}

		public Payout(){}

		public Payout(int AtStage,BusinessLogic.Customer Customer,DateTime GenerateDate,float GrossAmount,float NetAmount,DateTime PayoutDate,float ServiceCharge,BusinessLogic.Customer SponsorTo,float TDS,int UpdateBy,DateTime UpdateDate,string TransactionID,string PayoutType,float ServiceChargeAmount,float TDSAmount)
		{
			this.AtStage=AtStage;
			this.Customer=Customer;
			this.GenerateDate=GenerateDate;
			this.GrossAmount=GrossAmount;
			this.NetAmount=NetAmount;
			this.PayoutDate=PayoutDate;
			this.ServiceCharge=ServiceCharge;
			this.SponsorTo=SponsorTo;
			this.TDS=TDS;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.TransactionID=TransactionID;
			this.PayoutType=PayoutType;
			this.ServiceChargeAmount=ServiceChargeAmount;
			this.TDSAmount=TDSAmount;
		}

		public Payout(int AtStage,BusinessLogic.Customer Customer,DateTime GenerateDate,float GrossAmount,float NetAmount,DateTime PayoutDate,int PayoutID,float ServiceCharge,BusinessLogic.Customer SponsorTo,float TDS,int UpdateBy,DateTime UpdateDate,string TransactionID,string PayoutType,float ServiceChargeAmount,float TDSAmount)
		{
			this.AtStage=AtStage;
			this.Customer=Customer;
			this.GenerateDate=GenerateDate;
			this.GrossAmount=GrossAmount;
			this.NetAmount=NetAmount;
			this.PayoutDate=PayoutDate;
			this.PayoutID=PayoutID;
			this.ServiceCharge=ServiceCharge;
			this.SponsorTo=SponsorTo;
			this.TDS=TDS;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.TransactionID=TransactionID;
			this.PayoutType=PayoutType;
			this.ServiceChargeAmount=ServiceChargeAmount;
			this.TDSAmount=TDSAmount;
		}

		#region IBase Members

		private BusinessLogic.Payout Select()
		{
			try
			{
				BusinessLogic.Payout obj=new Payout();
				DataTable table=(new DataAccess.DAPayout(DataAccess.Database.Master)).Select(this.PayoutID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.Payout Select(int primaryKey)
		{
			try
			{
				BusinessLogic.Payout obj=new Payout();
				DataTable table=(new DataAccess.DAPayout(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Payout> Select(string condition)
		{
			try
			{
				List<BusinessLogic.Payout> objs=new List<BusinessLogic.Payout>();
				DataTable table=(new DataAccess.DAPayout(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Payout> SelectAll()
		{
			try
			{
				List<BusinessLogic.Payout> objs=new List<BusinessLogic.Payout>();
				DataTable table=(new DataAccess.DAPayout(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Payout> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.Payout> objs=new List<BusinessLogic.Payout>();
				DataTable table=(new DataAccess.DAPayout(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.PayoutID= (new DataAccess.DAPayout(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAPayout(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAPayout(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAPayout(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAPayout(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAPayout(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAPayout(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.Payout obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.AtStage).Trim().Equals(string.Empty )) 
							throw new Exception("The AtStage should not be left blank.");
						if (Convert.ToString(obj.Customer.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The Customer should not be left blank.");
						if (Convert.ToString(obj.GenerateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The GenerateDate should not be left blank.");
						if (Convert.ToString(obj.GrossAmount).Trim().Equals(string.Empty )) 
							throw new Exception("The GrossAmount should not be left blank.");
						if (Convert.ToString(obj.NetAmount).Trim().Equals(string.Empty )) 
							throw new Exception("The NetAmount should not be left blank.");
						if (Convert.ToString(obj.PayoutDate).Trim().Equals(string.Empty )) 
							throw new Exception("The PayoutDate should not be left blank.");
						if (Convert.ToString(obj.ServiceCharge).Trim().Equals(string.Empty )) 
							throw new Exception("The ServiceCharge should not be left blank.");
						if (Convert.ToString(obj.SponsorTo.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The SponsorTo should not be left blank.");
						if (Convert.ToString(obj.TDS).Trim().Equals(string.Empty )) 
							throw new Exception("The TDS should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateDate should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty )) 
							throw new Exception("The TransactionID should not be left blank.");
						if (Convert.ToString(obj.PayoutType).Trim().Equals(string.Empty )) 
							throw new Exception("The PayoutType should not be left blank.");
						if (Convert.ToString(obj.ServiceChargeAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The ServiceChargeAmount should not be left blank.");
						if (Convert.ToString(obj.TDSAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The TDSAmount should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.PayoutID).Trim().Equals(string.Empty )) 
							throw new Exception("The PayoutID should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty )) 
							throw new Exception("The TransactionID should not be left blank.");
						if (Convert.ToString(obj.PayoutType).Trim().Equals(string.Empty )) 
							throw new Exception("The PayoutType should not be left blank.");
						if (Convert.ToString(obj.ServiceChargeAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The ServiceChargeAmount should not be left blank.");
						if (Convert.ToString(obj.TDSAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The TDSAmount should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.Payout> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.Payout obj = new BusinessLogic.Payout();
					//Add Custom code here

					obj.AtStage=Convert.ToInt32(dr["AtStage"]);

					obj.Customer=BusinessLogic.Customer.Select(Convert.ToInt32(dr["CustomerID"]));
					obj.GenerateDate=Convert.ToDateTime(dr["GenerateDate"]);

					obj.GrossAmount=Convert.ToSingle(dr["GrossAmount"]);

					obj.NetAmount=Convert.ToSingle(dr["NetAmount"]);

					obj.PayoutDate=Convert.ToDateTime(dr["PayoutDate"]);

					obj.PayoutID=Convert.ToInt32(dr["PayoutID"]);

					obj.ServiceCharge=Convert.ToSingle(dr["ServiceCharge"]);

					obj.SponsorTo=BusinessLogic.Customer.Select(Convert.ToInt32(dr["SponsorToID"]));
					obj.TDS=Convert.ToSingle(dr["TDS"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					if(dr["TransactionID"]==DBNull.Value)
						obj.TransactionID=null;
					else
						obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					if(dr["PayoutType"]==DBNull.Value)
						obj.PayoutType=null;
					else
						obj.PayoutType=Convert.ToString(dr["PayoutType"]);

					obj.ServiceChargeAmount=Convert.ToSingle(dr["ServiceChargeAmount"]);

					obj.TDSAmount=Convert.ToSingle(dr["TDSAmount"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.Payout obj, DataRow dr)
		{
			try
			{
					obj.AtStage=Convert.ToInt32(dr["AtStage"]);

				obj.Customer=BusinessLogic.Customer.Select(Convert.ToInt32(dr["CustomerID"]));
					obj.GenerateDate=Convert.ToDateTime(dr["GenerateDate"]);

					obj.GrossAmount=Convert.ToSingle(dr["GrossAmount"]);

					obj.NetAmount=Convert.ToSingle(dr["NetAmount"]);

					obj.PayoutDate=Convert.ToDateTime(dr["PayoutDate"]);

					obj.PayoutID=Convert.ToInt32(dr["PayoutID"]);

					obj.ServiceCharge=Convert.ToSingle(dr["ServiceCharge"]);

				obj.SponsorTo=BusinessLogic.Customer.Select(Convert.ToInt32(dr["SponsorToID"]));
					obj.TDS=Convert.ToSingle(dr["TDS"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

				if(dr["TransactionID"]==DBNull.Value)
					obj.TransactionID=null;
				else
					obj.TransactionID=Convert.ToString(dr["TransactionID"]);

				if(dr["PayoutType"]==DBNull.Value)
					obj.PayoutType=null;
				else
					obj.PayoutType=Convert.ToString(dr["PayoutType"]);

					obj.ServiceChargeAmount=Convert.ToSingle(dr["ServiceChargeAmount"]);

					obj.TDSAmount=Convert.ToSingle(dr["TDSAmount"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
