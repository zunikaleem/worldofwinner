using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class FixedPayout:BusinessLogic.BusinessBase
	{

		public BusinessLogic.SystemUser BySystemUser { get;  set;}
		public int FixedPayoutID { get;  set;}
		public float GrossAmount { get;  set;}
		public float NetAmount { get;  set;}
		public DateTime PayoutDate { get;  set;}
		public float ServiceCharge { get;  set;}
		public BusinessLogic.SystemUser SystemUser { get;  set;}
		public float TDS { get;  set;}
		public string TransactionID { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}
		public float ServiceChargeAmount { get;  set;}
		public float TDSAmount { get;  set;}

		public FixedPayout(){}

		public FixedPayout(BusinessLogic.SystemUser BySystemUser,float GrossAmount,float NetAmount,DateTime PayoutDate,float ServiceCharge,BusinessLogic.SystemUser SystemUser,float TDS,string TransactionID,int UpdateBy,DateTime UpdateDate,float ServiceChargeAmount,float TDSAmount)
		{
			this.BySystemUser=BySystemUser;
			this.GrossAmount=GrossAmount;
			this.NetAmount=NetAmount;
			this.PayoutDate=PayoutDate;
			this.ServiceCharge=ServiceCharge;
			this.SystemUser=SystemUser;
			this.TDS=TDS;
			this.TransactionID=TransactionID;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.ServiceChargeAmount=ServiceChargeAmount;
			this.TDSAmount=TDSAmount;
		}

		public FixedPayout(BusinessLogic.SystemUser BySystemUser,int FixedPayoutID,float GrossAmount,float NetAmount,DateTime PayoutDate,float ServiceCharge,BusinessLogic.SystemUser SystemUser,float TDS,string TransactionID,int UpdateBy,DateTime UpdateDate,float ServiceChargeAmount,float TDSAmount)
		{
			this.BySystemUser=BySystemUser;
			this.FixedPayoutID=FixedPayoutID;
			this.GrossAmount=GrossAmount;
			this.NetAmount=NetAmount;
			this.PayoutDate=PayoutDate;
			this.ServiceCharge=ServiceCharge;
			this.SystemUser=SystemUser;
			this.TDS=TDS;
			this.TransactionID=TransactionID;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.ServiceChargeAmount=ServiceChargeAmount;
			this.TDSAmount=TDSAmount;
		}

		#region IBase Members

		private BusinessLogic.FixedPayout Select()
		{
			try
			{
				BusinessLogic.FixedPayout obj=new FixedPayout();
				DataTable table=(new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Select(this.FixedPayoutID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.FixedPayout Select(int primaryKey)
		{
			try
			{
				BusinessLogic.FixedPayout obj=new FixedPayout();
				DataTable table=(new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.FixedPayout> Select(string condition)
		{
			try
			{
				List<BusinessLogic.FixedPayout> objs=new List<BusinessLogic.FixedPayout>();
				DataTable table=(new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.FixedPayout> SelectAll()
		{
			try
			{
				List<BusinessLogic.FixedPayout> objs=new List<BusinessLogic.FixedPayout>();
				DataTable table=(new DataAccess.DAFixedPayout(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.FixedPayout> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.FixedPayout> objs=new List<BusinessLogic.FixedPayout>();
				DataTable table=(new DataAccess.DAFixedPayout(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.FixedPayoutID= (new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAFixedPayout(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAFixedPayout(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAFixedPayout(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.FixedPayout obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.BySystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The BySystemUser should not be left blank.");
						if (Convert.ToString(obj.GrossAmount).Trim().Equals(string.Empty )) 
							throw new Exception("The GrossAmount should not be left blank.");
						if (Convert.ToString(obj.NetAmount).Trim().Equals(string.Empty )) 
							throw new Exception("The NetAmount should not be left blank.");
						if (Convert.ToString(obj.PayoutDate).Trim().Equals(string.Empty )) 
							throw new Exception("The PayoutDate should not be left blank.");
						if (Convert.ToString(obj.ServiceCharge).Trim().Equals(string.Empty )) 
							throw new Exception("The ServiceCharge should not be left blank.");
						if (Convert.ToString(obj.SystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUser should not be left blank.");
						if (Convert.ToString(obj.TDS).Trim().Equals(string.Empty )) 
							throw new Exception("The TDS should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty )) 
							throw new Exception("The TransactionID should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");
						if (Convert.ToString(obj.ServiceChargeAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The ServiceChargeAmount should not be left blank.");
						if (Convert.ToString(obj.TDSAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The TDSAmount should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.BySystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The BySystemUser should not be left blank.");
						if (Convert.ToString(obj.FixedPayoutID).Trim().Equals(string.Empty )) 
							throw new Exception("The FixedPayoutID should not be left blank.");
						if (Convert.ToString(obj.GrossAmount).Trim().Equals(string.Empty )) 
							throw new Exception("The GrossAmount should not be left blank.");
						if (Convert.ToString(obj.NetAmount).Trim().Equals(string.Empty )) 
							throw new Exception("The NetAmount should not be left blank.");
						if (Convert.ToString(obj.PayoutDate).Trim().Equals(string.Empty )) 
							throw new Exception("The PayoutDate should not be left blank.");
						if (Convert.ToString(obj.ServiceCharge).Trim().Equals(string.Empty )) 
							throw new Exception("The ServiceCharge should not be left blank.");
						if (Convert.ToString(obj.SystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUser should not be left blank.");
						if (Convert.ToString(obj.TDS).Trim().Equals(string.Empty )) 
							throw new Exception("The TDS should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty )) 
							throw new Exception("The TransactionID should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");
						if (Convert.ToString(obj.ServiceChargeAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The ServiceChargeAmount should not be left blank.");
						if (Convert.ToString(obj.TDSAmount).Trim().Equals(string.Empty)) 
							throw new Exception("The TDSAmount should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.FixedPayout> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.FixedPayout obj = new BusinessLogic.FixedPayout();
					//Add Custom code here

					obj.BySystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["BySystemUserID"]));
					obj.FixedPayoutID=Convert.ToInt32(dr["FixedPayoutID"]);

					obj.GrossAmount=Convert.ToSingle(dr["GrossAmount"]);

					obj.NetAmount=Convert.ToSingle(dr["NetAmount"]);

					obj.PayoutDate=Convert.ToDateTime(dr["PayoutDate"]);

					obj.ServiceCharge=Convert.ToSingle(dr["ServiceCharge"]);

					obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
					obj.TDS=Convert.ToSingle(dr["TDS"]);

					if(dr["TransactionID"]==DBNull.Value)
						obj.TransactionID=null;
					else
						obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					obj.ServiceChargeAmount=Convert.ToSingle(dr["ServiceChargeAmount"]);

					obj.TDSAmount=Convert.ToSingle(dr["TDSAmount"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.FixedPayout obj, DataRow dr)
		{
			try
			{
				obj.BySystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["BySystemUserID"]));
					obj.FixedPayoutID=Convert.ToInt32(dr["FixedPayoutID"]);

					obj.GrossAmount=Convert.ToSingle(dr["GrossAmount"]);

					obj.NetAmount=Convert.ToSingle(dr["NetAmount"]);

					obj.PayoutDate=Convert.ToDateTime(dr["PayoutDate"]);

					obj.ServiceCharge=Convert.ToSingle(dr["ServiceCharge"]);

				obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
					obj.TDS=Convert.ToSingle(dr["TDS"]);

				if(dr["TransactionID"]==DBNull.Value)
					obj.TransactionID=null;
				else
					obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					obj.ServiceChargeAmount=Convert.ToSingle(dr["ServiceChargeAmount"]);

					obj.TDSAmount=Convert.ToSingle(dr["TDSAmount"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
