using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class SystemUser:BusinessLogic.BusinessBase
	{

		public BusinessLogic.AccessGroup AccessGroup { get;  set;}
		public DateTime RegistrationDate { get;  set;}
		public int SystemUserID { get;  set;}
		public string UserID { get;  set;}
		public string UserPassword { get;  set;}
		public string ContactName { get;  set;}
		public string MobileNumber { get;  set;}
		public string PAN { get;  set;}
		public string BankName { get;  set;}
		public string BranchName { get;  set;}
		public string IFSCCode { get;  set;}
		public string AccountType { get;  set;}
		public string AccountNumber { get;  set;}
		public string AccountHolderName { get;  set;}
		public string TransactionID { get;  set;}

		public SystemUser(){}

		public SystemUser(BusinessLogic.AccessGroup AccessGroup,DateTime RegistrationDate,string UserID,string UserPassword,string ContactName,string MobileNumber,string PAN,string BankName,string BranchName,string IFSCCode,string AccountType,string AccountNumber,string AccountHolderName,string TransactionID)
		{
			this.AccessGroup=AccessGroup;
			this.RegistrationDate=RegistrationDate;
			this.UserID=UserID;
			this.UserPassword=UserPassword;
			this.ContactName=ContactName;
			this.MobileNumber=MobileNumber;
			this.PAN=PAN;
			this.BankName=BankName;
			this.BranchName=BranchName;
			this.IFSCCode=IFSCCode;
			this.AccountType=AccountType;
			this.AccountNumber=AccountNumber;
			this.AccountHolderName=AccountHolderName;
			this.TransactionID=TransactionID;
		}

		public SystemUser(BusinessLogic.AccessGroup AccessGroup,DateTime RegistrationDate,int SystemUserID,string UserID,string UserPassword,string ContactName,string MobileNumber,string PAN,string BankName,string BranchName,string IFSCCode,string AccountType,string AccountNumber,string AccountHolderName,string TransactionID)
		{
			this.AccessGroup=AccessGroup;
			this.RegistrationDate=RegistrationDate;
			this.SystemUserID=SystemUserID;
			this.UserID=UserID;
			this.UserPassword=UserPassword;
			this.ContactName=ContactName;
			this.MobileNumber=MobileNumber;
			this.PAN=PAN;
			this.BankName=BankName;
			this.BranchName=BranchName;
			this.IFSCCode=IFSCCode;
			this.AccountType=AccountType;
			this.AccountNumber=AccountNumber;
			this.AccountHolderName=AccountHolderName;
			this.TransactionID=TransactionID;
		}

		#region IBase Members

		private BusinessLogic.SystemUser Select()
		{
			try
			{
				BusinessLogic.SystemUser obj=new SystemUser();
				DataTable table=(new DataAccess.DASystemUser(DataAccess.Database.Master)).Select(this.SystemUserID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.SystemUser Select(int primaryKey)
		{
			try
			{
				BusinessLogic.SystemUser obj=new SystemUser();
				DataTable table=(new DataAccess.DASystemUser(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.SystemUser> Select(string condition)
		{
			try
			{
				List<BusinessLogic.SystemUser> objs=new List<BusinessLogic.SystemUser>();
				DataTable table=(new DataAccess.DASystemUser(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.SystemUser> SelectAll()
		{
			try
			{
				List<BusinessLogic.SystemUser> objs=new List<BusinessLogic.SystemUser>();
				DataTable table=(new DataAccess.DASystemUser(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.SystemUser> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.SystemUser> objs=new List<BusinessLogic.SystemUser>();
				DataTable table=(new DataAccess.DASystemUser(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.SystemUserID= (new DataAccess.DASystemUser(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DASystemUser(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DASystemUser(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DASystemUser(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DASystemUser(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DASystemUser(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DASystemUser(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.SystemUser obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.AccessGroup.AccessGroupID).Trim().Equals(string.Empty )) 
							throw new Exception("The AccessGroup should not be left blank.");
						if (Convert.ToString(obj.RegistrationDate).Trim().Equals(string.Empty )) 
							throw new Exception("The RegistrationDate should not be left blank.");
						if (Convert.ToString(obj.UserID).Trim().Equals(string.Empty )) 
							throw new Exception("The UserID should not be left blank.");
						if (Convert.ToString(obj.UserPassword).Trim().Equals(string.Empty )) 
							throw new Exception("The UserPassword should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.AccessGroup.AccessGroupID).Trim().Equals(string.Empty )) 
							throw new Exception("The AccessGroup should not be left blank.");
						if (Convert.ToString(obj.RegistrationDate).Trim().Equals(string.Empty )) 
							throw new Exception("The RegistrationDate should not be left blank.");
						if (Convert.ToString(obj.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUserID should not be left blank.");
						if (Convert.ToString(obj.UserID).Trim().Equals(string.Empty )) 
							throw new Exception("The UserID should not be left blank.");
						if (Convert.ToString(obj.UserPassword).Trim().Equals(string.Empty )) 
							throw new Exception("The UserPassword should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.SystemUser> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.SystemUser obj = new BusinessLogic.SystemUser();
					//Add Custom code here

					obj.AccessGroup=BusinessLogic.AccessGroup.Select(Convert.ToInt32(dr["AccessGroupID"]));
					obj.RegistrationDate=Convert.ToDateTime(dr["RegistrationDate"]);

					obj.SystemUserID=Convert.ToInt32(dr["SystemUserID"]);

					if(dr["UserID"]==DBNull.Value)
						obj.UserID=null;
					else
						obj.UserID=Convert.ToString(dr["UserID"]);

					if(dr["UserPassword"]==DBNull.Value)
						obj.UserPassword=null;
					else
						obj.UserPassword=Convert.ToString(dr["UserPassword"]);

					if(dr["ContactName"]==DBNull.Value)
						obj.ContactName=null;
					else
						obj.ContactName=Convert.ToString(dr["ContactName"]);

					if(dr["MobileNumber"]==DBNull.Value)
						obj.MobileNumber=null;
					else
						obj.MobileNumber=Convert.ToString(dr["MobileNumber"]);

					if(dr["PAN"]==DBNull.Value)
						obj.PAN=null;
					else
						obj.PAN=Convert.ToString(dr["PAN"]);

					if(dr["BankName"]==DBNull.Value)
						obj.BankName=null;
					else
						obj.BankName=Convert.ToString(dr["BankName"]);

					if(dr["BranchName"]==DBNull.Value)
						obj.BranchName=null;
					else
						obj.BranchName=Convert.ToString(dr["BranchName"]);

					if(dr["IFSCCode"]==DBNull.Value)
						obj.IFSCCode=null;
					else
						obj.IFSCCode=Convert.ToString(dr["IFSCCode"]);

					if(dr["AccountType"]==DBNull.Value)
						obj.AccountType=null;
					else
						obj.AccountType=Convert.ToString(dr["AccountType"]);

					if(dr["AccountNumber"]==DBNull.Value)
						obj.AccountNumber=null;
					else
						obj.AccountNumber=Convert.ToString(dr["AccountNumber"]);

					if(dr["AccountHolderName"]==DBNull.Value)
						obj.AccountHolderName=null;
					else
						obj.AccountHolderName=Convert.ToString(dr["AccountHolderName"]);

					if(dr["TransactionID"]==DBNull.Value)
						obj.TransactionID=null;
					else
						obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.SystemUser obj, DataRow dr)
		{
			try
			{
				obj.AccessGroup=BusinessLogic.AccessGroup.Select(Convert.ToInt32(dr["AccessGroupID"]));
					obj.RegistrationDate=Convert.ToDateTime(dr["RegistrationDate"]);

					obj.SystemUserID=Convert.ToInt32(dr["SystemUserID"]);

				if(dr["UserID"]==DBNull.Value)
					obj.UserID=null;
				else
					obj.UserID=Convert.ToString(dr["UserID"]);

				if(dr["UserPassword"]==DBNull.Value)
					obj.UserPassword=null;
				else
					obj.UserPassword=Convert.ToString(dr["UserPassword"]);

				if(dr["ContactName"]==DBNull.Value)
					obj.ContactName=null;
				else
					obj.ContactName=Convert.ToString(dr["ContactName"]);

				if(dr["MobileNumber"]==DBNull.Value)
					obj.MobileNumber=null;
				else
					obj.MobileNumber=Convert.ToString(dr["MobileNumber"]);

				if(dr["PAN"]==DBNull.Value)
					obj.PAN=null;
				else
					obj.PAN=Convert.ToString(dr["PAN"]);

				if(dr["BankName"]==DBNull.Value)
					obj.BankName=null;
				else
					obj.BankName=Convert.ToString(dr["BankName"]);

				if(dr["BranchName"]==DBNull.Value)
					obj.BranchName=null;
				else
					obj.BranchName=Convert.ToString(dr["BranchName"]);

				if(dr["IFSCCode"]==DBNull.Value)
					obj.IFSCCode=null;
				else
					obj.IFSCCode=Convert.ToString(dr["IFSCCode"]);

				if(dr["AccountType"]==DBNull.Value)
					obj.AccountType=null;
				else
					obj.AccountType=Convert.ToString(dr["AccountType"]);

				if(dr["AccountNumber"]==DBNull.Value)
					obj.AccountNumber=null;
				else
					obj.AccountNumber=Convert.ToString(dr["AccountNumber"]);

				if(dr["AccountHolderName"]==DBNull.Value)
					obj.AccountHolderName=null;
				else
					obj.AccountHolderName=Convert.ToString(dr["AccountHolderName"]);

				if(dr["TransactionID"]==DBNull.Value)
					obj.TransactionID=null;
				else
					obj.TransactionID=Convert.ToString(dr["TransactionID"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
