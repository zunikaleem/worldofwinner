using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class EPINInbox:BusinessLogic.BusinessBase
	{

		public int EPINInboxID { get;  set;}
		public BusinessLogic.EPIN EPIN { get;  set;}
		public BusinessLogic.SystemUser FromSystemUser { get;  set;}
		public BusinessLogic.SystemUser ToSystemUser { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}

		public EPINInbox(){}

		public EPINInbox(BusinessLogic.EPIN EPIN,BusinessLogic.SystemUser FromSystemUser,BusinessLogic.SystemUser ToSystemUser,int UpdateBy,DateTime UpdateDate)
		{
			this.EPIN=EPIN;
			this.FromSystemUser=FromSystemUser;
			this.ToSystemUser=ToSystemUser;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		public EPINInbox(int EPINInboxID,BusinessLogic.EPIN EPIN,BusinessLogic.SystemUser FromSystemUser,BusinessLogic.SystemUser ToSystemUser,int UpdateBy,DateTime UpdateDate)
		{
			this.EPINInboxID=EPINInboxID;
			this.EPIN=EPIN;
			this.FromSystemUser=FromSystemUser;
			this.ToSystemUser=ToSystemUser;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		#region IBase Members

		private BusinessLogic.EPINInbox Select()
		{
			try
			{
				BusinessLogic.EPINInbox obj=new EPINInbox();
				DataTable table=(new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Select(this.EPINInboxID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.EPINInbox Select(int primaryKey)
		{
			try
			{
				BusinessLogic.EPINInbox obj=new EPINInbox();
				DataTable table=(new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPINInbox> Select(string condition)
		{
			try
			{
				List<BusinessLogic.EPINInbox> objs=new List<BusinessLogic.EPINInbox>();
				DataTable table=(new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPINInbox> SelectAll()
		{
			try
			{
				List<BusinessLogic.EPINInbox> objs=new List<BusinessLogic.EPINInbox>();
				DataTable table=(new DataAccess.DAEPINInbox(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.EPINInbox> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.EPINInbox> objs=new List<BusinessLogic.EPINInbox>();
				DataTable table=(new DataAccess.DAEPINInbox(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.EPINInboxID= (new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAEPINInbox(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAEPINInbox(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAEPINInbox(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.EPINInbox obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.EPIN.EPINID).Trim().Equals(string.Empty )) 
							throw new Exception("The EPIN should not be left blank.");
						if (Convert.ToString(obj.FromSystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The FromSystemUser should not be left blank.");
						if (Convert.ToString(obj.ToSystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The ToSystemUser should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.EPINInboxID).Trim().Equals(string.Empty )) 
							throw new Exception("The EPINInboxID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.EPINInbox> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.EPINInbox obj = new BusinessLogic.EPINInbox();
					//Add Custom code here

					obj.EPINInboxID=Convert.ToInt32(dr["EPINInboxID"]);

					obj.EPIN=BusinessLogic.EPIN.Select(Convert.ToInt32(dr["EPINID"]));
					obj.FromSystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["FromSystemUserID"]));
					obj.ToSystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["ToSystemUserID"]));
					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.EPINInbox obj, DataRow dr)
		{
			try
			{
					obj.EPINInboxID=Convert.ToInt32(dr["EPINInboxID"]);

				obj.EPIN=BusinessLogic.EPIN.Select(Convert.ToInt32(dr["EPINID"]));
				obj.FromSystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["FromSystemUserID"]));
				obj.ToSystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["ToSystemUserID"]));
					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
