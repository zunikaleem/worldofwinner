using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class Order:BusinessLogic.BusinessBase
	{

		public BusinessLogic.Customer Customer { get;  set;}
		public DateTime OrderDate { get;  set;}
		public int OrderID { get;  set;}
		public string TransactionID { get;  set;}

		public Order(){}

		public Order(BusinessLogic.Customer Customer,DateTime OrderDate,string TransactionID)
		{
			this.Customer=Customer;
			this.OrderDate=OrderDate;
			this.TransactionID=TransactionID;
		}

		public Order(BusinessLogic.Customer Customer,DateTime OrderDate,int OrderID,string TransactionID)
		{
			this.Customer=Customer;
			this.OrderDate=OrderDate;
			this.OrderID=OrderID;
			this.TransactionID=TransactionID;
		}

		#region IBase Members

		private BusinessLogic.Order Select()
		{
			try
			{
				BusinessLogic.Order obj=new Order();
				DataTable table=(new DataAccess.DAOrder(DataAccess.Database.Master)).Select(this.OrderID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.Order Select(int primaryKey)
		{
			try
			{
				BusinessLogic.Order obj=new Order();
				DataTable table=(new DataAccess.DAOrder(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Order> Select(string condition)
		{
			try
			{
				List<BusinessLogic.Order> objs=new List<BusinessLogic.Order>();
				DataTable table=(new DataAccess.DAOrder(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Order> SelectAll()
		{
			try
			{
				List<BusinessLogic.Order> objs=new List<BusinessLogic.Order>();
				DataTable table=(new DataAccess.DAOrder(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Order> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.Order> objs=new List<BusinessLogic.Order>();
				DataTable table=(new DataAccess.DAOrder(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.OrderID= (new DataAccess.DAOrder(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAOrder(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAOrder(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAOrder(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAOrder(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAOrder(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAOrder(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.Order obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.Customer.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The Customer should not be left blank.");
						if (Convert.ToString(obj.OrderDate).Trim().Equals(string.Empty )) 
							throw new Exception("The OrderDate should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.Customer.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The Customer should not be left blank.");
						if (Convert.ToString(obj.OrderDate).Trim().Equals(string.Empty )) 
							throw new Exception("The OrderDate should not be left blank.");
						if (Convert.ToString(obj.OrderID).Trim().Equals(string.Empty )) 
							throw new Exception("The OrderID should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.Order> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.Order obj = new BusinessLogic.Order();
					//Add Custom code here

					obj.Customer=BusinessLogic.Customer.Select(Convert.ToInt32(dr["CustomerID"]));
					obj.OrderDate=Convert.ToDateTime(dr["OrderDate"]);

					obj.OrderID=Convert.ToInt32(dr["OrderID"]);

					if(dr["TransactionID"]==DBNull.Value)
						obj.TransactionID=null;
					else
						obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.Order obj, DataRow dr)
		{
			try
			{
				obj.Customer=BusinessLogic.Customer.Select(Convert.ToInt32(dr["CustomerID"]));
					obj.OrderDate=Convert.ToDateTime(dr["OrderDate"]);

					obj.OrderID=Convert.ToInt32(dr["OrderID"]);

				if(dr["TransactionID"]==DBNull.Value)
					obj.TransactionID=null;
				else
					obj.TransactionID=Convert.ToString(dr["TransactionID"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
