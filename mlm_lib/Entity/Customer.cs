using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class Customer:BusinessLogic.BusinessBase
	{

		public int CustomerID { get; private set;}
		public string Address { get;  set;}
		public string City { get;  set;}
		public string State { get;  set;}
		public DateTime? DateOfBirth { get;  set;}
		public int SponsorID { get;  set;}
		public BusinessLogic.EPIN EPIN { get;  set;}
		public DateTime RegistrationDate { get;  set;}
		public string Title { get;  set;}
		public string Gender { get;  set;}
		public string MaritalStatus { get;  set;}
		public string OtherContact { get;  set;}
		public string PostalCode { get;  set;}
		public string EmailID { get;  set;}
		public int? LockStatus { get;  set;}
		public string NomineeName { get;  set;}
		public string Relationship { get;  set;}
		public string NomineeGender { get;  set;}
		public BusinessLogic.SystemUser SystemUser { get;  set;}
		public string TransactionID { get;  set;}

		public Customer(){}

		public Customer(string Address,string City,string State,DateTime DateOfBirth,int SponsorID,BusinessLogic.EPIN EPIN,DateTime RegistrationDate,string Title,string Gender,string MaritalStatus,string OtherContact,string PostalCode,string EmailID,int LockStatus,string NomineeName,string Relationship,string NomineeGender,BusinessLogic.SystemUser SystemUser,string TransactionID)
		{
			this.Address=Address;
			this.City=City;
			this.State=State;
			this.DateOfBirth=DateOfBirth;
			this.SponsorID=SponsorID;
			this.EPIN=EPIN;
			this.RegistrationDate=RegistrationDate;
			this.Title=Title;
			this.Gender=Gender;
			this.MaritalStatus=MaritalStatus;
			this.OtherContact=OtherContact;
			this.PostalCode=PostalCode;
			this.EmailID=EmailID;
			this.LockStatus=LockStatus;
			this.NomineeName=NomineeName;
			this.Relationship=Relationship;
			this.NomineeGender=NomineeGender;
			this.SystemUser=SystemUser;
			this.TransactionID=TransactionID;
		}

		public Customer(int CustomerID,string Address,string City,string State,DateTime DateOfBirth,int SponsorID,BusinessLogic.EPIN EPIN,DateTime RegistrationDate,string Title,string Gender,string MaritalStatus,string OtherContact,string PostalCode,string EmailID,int LockStatus,string NomineeName,string Relationship,string NomineeGender,BusinessLogic.SystemUser SystemUser,string TransactionID)
		{
			this.CustomerID=CustomerID;
			this.Address=Address;
			this.City=City;
			this.State=State;
			this.DateOfBirth=DateOfBirth;
			this.SponsorID=SponsorID;
			this.EPIN=EPIN;
			this.RegistrationDate=RegistrationDate;
			this.Title=Title;
			this.Gender=Gender;
			this.MaritalStatus=MaritalStatus;
			this.OtherContact=OtherContact;
			this.PostalCode=PostalCode;
			this.EmailID=EmailID;
			this.LockStatus=LockStatus;
			this.NomineeName=NomineeName;
			this.Relationship=Relationship;
			this.NomineeGender=NomineeGender;
			this.SystemUser=SystemUser;
			this.TransactionID=TransactionID;
		}

		#region IBase Members

		private BusinessLogic.Customer Select()
		{
			try
			{
				BusinessLogic.Customer obj=new Customer();
				DataTable table=(new DataAccess.DACustomer(DataAccess.Database.Master)).Select(this.CustomerID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.Customer Select(int primaryKey)
		{
			try
			{
				BusinessLogic.Customer obj=new Customer();
				DataTable table=(new DataAccess.DACustomer(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Customer> Select(string condition)
		{
			try
			{
				List<BusinessLogic.Customer> objs=new List<BusinessLogic.Customer>();
				DataTable table=(new DataAccess.DACustomer(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Customer> SelectAll()
		{
			try
			{
				List<BusinessLogic.Customer> objs=new List<BusinessLogic.Customer>();
				DataTable table=(new DataAccess.DACustomer(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Customer> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.Customer> objs=new List<BusinessLogic.Customer>();
				DataTable table=(new DataAccess.DACustomer(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.CustomerID= (new DataAccess.DACustomer(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DACustomer(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DACustomer(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DACustomer(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DACustomer(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DACustomer(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DACustomer(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.Customer obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.SponsorID).Trim().Equals(string.Empty )) 
							throw new Exception("The SponsorID should not be left blank.");
						if (Convert.ToString(obj.EPIN.EPINID).Trim().Equals(string.Empty )) 
							throw new Exception("The EPIN should not be left blank.");
						if (Convert.ToString(obj.RegistrationDate).Trim().Equals(string.Empty )) 
							throw new Exception("The RegistrationDate should not be left blank.");
						if (Convert.ToString(obj.EmailID).Trim().Equals(string.Empty )) 
							throw new Exception("The EmailID should not be left blank.");
						if (Convert.ToString(obj.SystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUser should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The CustomerID should not be left blank.");
						if (Convert.ToString(obj.SystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUser should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.Customer> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.Customer obj = new BusinessLogic.Customer();
					//Add Custom code here

					obj.CustomerID=Convert.ToInt32(dr["CustomerID"]);

					if(dr["Address"]==DBNull.Value)
						obj.Address=null;
					else
						obj.Address=Convert.ToString(dr["Address"]);

					if(dr["City"]==DBNull.Value)
						obj.City=null;
					else
						obj.City=Convert.ToString(dr["City"]);

					if(dr["State"]==DBNull.Value)
						obj.State=null;
					else
						obj.State=Convert.ToString(dr["State"]);

					if(dr["DateOfBirth"]==DBNull.Value)
						obj.DateOfBirth=null;
					else
						obj.DateOfBirth=Convert.ToDateTime(dr["DateOfBirth"]);

					obj.SponsorID=Convert.ToInt32(dr["SponsorID"]);

					obj.EPIN=BusinessLogic.EPIN.Select(Convert.ToInt32(dr["EPINID"]));
					obj.RegistrationDate=Convert.ToDateTime(dr["RegistrationDate"]);

					if(dr["Title"]==DBNull.Value)
						obj.Title=null;
					else
						obj.Title=Convert.ToString(dr["Title"]);

					if(dr["Gender"]==DBNull.Value)
						obj.Gender=null;
					else
						obj.Gender=Convert.ToString(dr["Gender"]);

					if(dr["MaritalStatus"]==DBNull.Value)
						obj.MaritalStatus=null;
					else
						obj.MaritalStatus=Convert.ToString(dr["MaritalStatus"]);

					if(dr["OtherContact"]==DBNull.Value)
						obj.OtherContact=null;
					else
						obj.OtherContact=Convert.ToString(dr["OtherContact"]);

					if(dr["PostalCode"]==DBNull.Value)
						obj.PostalCode=null;
					else
						obj.PostalCode=Convert.ToString(dr["PostalCode"]);

					if(dr["EmailID"]==DBNull.Value)
						obj.EmailID=null;
					else
						obj.EmailID=Convert.ToString(dr["EmailID"]);

					if(dr["LockStatus"]==DBNull.Value)
						obj.LockStatus=null;
					else
						obj.LockStatus=Convert.ToInt32(dr["LockStatus"]);

					if(dr["NomineeName"]==DBNull.Value)
						obj.NomineeName=null;
					else
						obj.NomineeName=Convert.ToString(dr["NomineeName"]);

					if(dr["Relationship"]==DBNull.Value)
						obj.Relationship=null;
					else
						obj.Relationship=Convert.ToString(dr["Relationship"]);

					if(dr["NomineeGender"]==DBNull.Value)
						obj.NomineeGender=null;
					else
						obj.NomineeGender=Convert.ToString(dr["NomineeGender"]);

					obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
					if(dr["TransactionID"]==DBNull.Value)
						obj.TransactionID=null;
					else
						obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.Customer obj, DataRow dr)
		{
			try
			{
					obj.CustomerID=Convert.ToInt32(dr["CustomerID"]);

				if(dr["Address"]==DBNull.Value)
					obj.Address=null;
				else
					obj.Address=Convert.ToString(dr["Address"]);

				if(dr["City"]==DBNull.Value)
					obj.City=null;
				else
					obj.City=Convert.ToString(dr["City"]);

				if(dr["State"]==DBNull.Value)
					obj.State=null;
				else
					obj.State=Convert.ToString(dr["State"]);

				if(dr["DateOfBirth"]==DBNull.Value)
					obj.DateOfBirth=null;
				else
					obj.DateOfBirth=Convert.ToDateTime(dr["DateOfBirth"]);

					obj.SponsorID=Convert.ToInt32(dr["SponsorID"]);

				obj.EPIN=BusinessLogic.EPIN.Select(Convert.ToInt32(dr["EPINID"]));
					obj.RegistrationDate=Convert.ToDateTime(dr["RegistrationDate"]);

				if(dr["Title"]==DBNull.Value)
					obj.Title=null;
				else
					obj.Title=Convert.ToString(dr["Title"]);

				if(dr["Gender"]==DBNull.Value)
					obj.Gender=null;
				else
					obj.Gender=Convert.ToString(dr["Gender"]);

				if(dr["MaritalStatus"]==DBNull.Value)
					obj.MaritalStatus=null;
				else
					obj.MaritalStatus=Convert.ToString(dr["MaritalStatus"]);

				if(dr["OtherContact"]==DBNull.Value)
					obj.OtherContact=null;
				else
					obj.OtherContact=Convert.ToString(dr["OtherContact"]);

				if(dr["PostalCode"]==DBNull.Value)
					obj.PostalCode=null;
				else
					obj.PostalCode=Convert.ToString(dr["PostalCode"]);

				if(dr["EmailID"]==DBNull.Value)
					obj.EmailID=null;
				else
					obj.EmailID=Convert.ToString(dr["EmailID"]);

				if(dr["LockStatus"]==DBNull.Value)
					obj.LockStatus=null;
				else
					obj.LockStatus=Convert.ToInt32(dr["LockStatus"]);

				if(dr["NomineeName"]==DBNull.Value)
					obj.NomineeName=null;
				else
					obj.NomineeName=Convert.ToString(dr["NomineeName"]);

				if(dr["Relationship"]==DBNull.Value)
					obj.Relationship=null;
				else
					obj.Relationship=Convert.ToString(dr["Relationship"]);

				if(dr["NomineeGender"]==DBNull.Value)
					obj.NomineeGender=null;
				else
					obj.NomineeGender=Convert.ToString(dr["NomineeGender"]);

				obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
				if(dr["TransactionID"]==DBNull.Value)
					obj.TransactionID=null;
				else
					obj.TransactionID=Convert.ToString(dr["TransactionID"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
