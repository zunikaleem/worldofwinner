using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class CustomerApproval:BusinessLogic.BusinessBase
	{

		public int CustomerApprovalID { get;  set;}
		public BusinessLogic.Customer Customer { get;  set;}
		public bool Approved { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}

		public CustomerApproval(){}

		public CustomerApproval(BusinessLogic.Customer Customer,bool Approved,int UpdateBy,DateTime UpdateDate)
		{
			this.Customer=Customer;
			this.Approved=Approved;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		public CustomerApproval(int CustomerApprovalID,BusinessLogic.Customer Customer,bool Approved,int UpdateBy,DateTime UpdateDate)
		{
			this.CustomerApprovalID=CustomerApprovalID;
			this.Customer=Customer;
			this.Approved=Approved;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		#region IBase Members

		private BusinessLogic.CustomerApproval Select()
		{
			try
			{
				BusinessLogic.CustomerApproval obj=new CustomerApproval();
				DataTable table=(new DataAccess.DACustomerApproval(DataAccess.Database.Master)).Select(this.CustomerApprovalID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.CustomerApproval Select(int primaryKey)
		{
			try
			{
				BusinessLogic.CustomerApproval obj=new CustomerApproval();
				DataTable table=(new DataAccess.DACustomerApproval(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.CustomerApproval> Select(string condition)
		{
			try
			{
				List<BusinessLogic.CustomerApproval> objs=new List<BusinessLogic.CustomerApproval>();
				DataTable table=(new DataAccess.DACustomerApproval(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.CustomerApproval> SelectAll()
		{
			try
			{
				List<BusinessLogic.CustomerApproval> objs=new List<BusinessLogic.CustomerApproval>();
				DataTable table=(new DataAccess.DACustomerApproval(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.CustomerApproval> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.CustomerApproval> objs=new List<BusinessLogic.CustomerApproval>();
				DataTable table=(new DataAccess.DACustomerApproval(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.CustomerApprovalID= (new DataAccess.DACustomerApproval(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DACustomerApproval(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DACustomerApproval(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DACustomerApproval(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DACustomerApproval(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DACustomerApproval(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.CustomerApproval obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.Customer.CustomerID).Trim().Equals(string.Empty )) 
							throw new Exception("The Customer should not be left blank.");
						if (Convert.ToString(obj.Approved).Trim().Equals(string.Empty)) 
							throw new Exception("The Approved should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty)) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.CustomerApprovalID).Trim().Equals(string.Empty)) 
							throw new Exception("The CustomerApprovalID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        internal static bool isApproved(Customer customer)
        {
            try
            {
                List<CustomerApproval> list = Select("Where CustomerID=" + customer.CustomerID);
                if(list.Count==1)
                { return true; }
                else { return false; }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void MapEntity(List<BusinessLogic.CustomerApproval> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.CustomerApproval obj = new BusinessLogic.CustomerApproval();
					//Add Custom code here

					obj.CustomerApprovalID=Convert.ToInt32(dr["CustomerApprovalID"]);

					obj.Customer=BusinessLogic.Customer.Select(Convert.ToInt32(dr["CustomerID"]));
					obj.Approved=Convert.ToBoolean(dr["Approved"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.CustomerApproval obj, DataRow dr)
		{
			try
			{
					obj.CustomerApprovalID=Convert.ToInt32(dr["CustomerApprovalID"]);

				obj.Customer=BusinessLogic.Customer.Select(Convert.ToInt32(dr["CustomerID"]));
					obj.Approved=Convert.ToBoolean(dr["Approved"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
