using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class OrderDetail:BusinessLogic.BusinessBase
	{

		public float BasePrice { get;  set;}
		public float CGST { get;  set;}
		public int OrderDetailID { get;  set;}
		public BusinessLogic.Order Order { get;  set;}
		public BusinessLogic.Package Package { get;  set;}
		public float SGST { get;  set;}
		public bool TaxIncluded { get;  set;}
		public float Total { get;  set;}
		public string TransactionID { get;  set;}

		public OrderDetail(){}

		public OrderDetail(float BasePrice,float CGST,BusinessLogic.Order Order,BusinessLogic.Package Package,float SGST,bool TaxIncluded,float Total,string TransactionID)
		{
			this.BasePrice=BasePrice;
			this.CGST=CGST;
			this.Order=Order;
			this.Package=Package;
			this.SGST=SGST;
			this.TaxIncluded=TaxIncluded;
			this.Total=Total;
			this.TransactionID=TransactionID;
		}

		public OrderDetail(float BasePrice,float CGST,int OrderDetailID,BusinessLogic.Order Order,BusinessLogic.Package Package,float SGST,bool TaxIncluded,float Total,string TransactionID)
		{
			this.BasePrice=BasePrice;
			this.CGST=CGST;
			this.OrderDetailID=OrderDetailID;
			this.Order=Order;
			this.Package=Package;
			this.SGST=SGST;
			this.TaxIncluded=TaxIncluded;
			this.Total=Total;
			this.TransactionID=TransactionID;
		}

		#region IBase Members

		private BusinessLogic.OrderDetail Select()
		{
			try
			{
				BusinessLogic.OrderDetail obj=new OrderDetail();
				DataTable table=(new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Select(this.OrderDetailID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.OrderDetail Select(int primaryKey)
		{
			try
			{
				BusinessLogic.OrderDetail obj=new OrderDetail();
				DataTable table=(new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.OrderDetail> Select(string condition)
		{
			try
			{
				List<BusinessLogic.OrderDetail> objs=new List<BusinessLogic.OrderDetail>();
				DataTable table=(new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.OrderDetail> SelectAll()
		{
			try
			{
				List<BusinessLogic.OrderDetail> objs=new List<BusinessLogic.OrderDetail>();
				DataTable table=(new DataAccess.DAOrderDetail(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.OrderDetail> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.OrderDetail> objs=new List<BusinessLogic.OrderDetail>();
				DataTable table=(new DataAccess.DAOrderDetail(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.OrderDetailID= (new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAOrderDetail(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAOrderDetail(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAOrderDetail(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.OrderDetail obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.BasePrice).Trim().Equals(string.Empty )) 
							throw new Exception("The BasePrice should not be left blank.");
						if (Convert.ToString(obj.CGST).Trim().Equals(string.Empty )) 
							throw new Exception("The CGST should not be left blank.");
						if (Convert.ToString(obj.Order.OrderID).Trim().Equals(string.Empty )) 
							throw new Exception("The Order should not be left blank.");
						if (Convert.ToString(obj.Package.PackageID).Trim().Equals(string.Empty )) 
							throw new Exception("The Package should not be left blank.");
						if (Convert.ToString(obj.SGST).Trim().Equals(string.Empty )) 
							throw new Exception("The SGST should not be left blank.");
						if (Convert.ToString(obj.TaxIncluded).Trim().Equals(string.Empty )) 
							throw new Exception("The TaxIncluded should not be left blank.");
						if (Convert.ToString(obj.Total).Trim().Equals(string.Empty )) 
							throw new Exception("The Total should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.BasePrice).Trim().Equals(string.Empty )) 
							throw new Exception("The BasePrice should not be left blank.");
						if (Convert.ToString(obj.CGST).Trim().Equals(string.Empty )) 
							throw new Exception("The CGST should not be left blank.");
						if (Convert.ToString(obj.OrderDetailID).Trim().Equals(string.Empty )) 
							throw new Exception("The OrderDetailID should not be left blank.");
						if (Convert.ToString(obj.Order.OrderID).Trim().Equals(string.Empty )) 
							throw new Exception("The Order should not be left blank.");
						if (Convert.ToString(obj.Package.PackageID).Trim().Equals(string.Empty )) 
							throw new Exception("The Package should not be left blank.");
						if (Convert.ToString(obj.SGST).Trim().Equals(string.Empty )) 
							throw new Exception("The SGST should not be left blank.");
						if (Convert.ToString(obj.TaxIncluded).Trim().Equals(string.Empty )) 
							throw new Exception("The TaxIncluded should not be left blank.");
						if (Convert.ToString(obj.Total).Trim().Equals(string.Empty )) 
							throw new Exception("The Total should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.OrderDetail> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.OrderDetail obj = new BusinessLogic.OrderDetail();
					//Add Custom code here

					obj.BasePrice=Convert.ToSingle(dr["BasePrice"]);

					obj.CGST=Convert.ToSingle(dr["CGST"]);

					obj.OrderDetailID=Convert.ToInt32(dr["OrderDetailID"]);

					obj.Order=BusinessLogic.Order.Select(Convert.ToInt32(dr["OrderID"]));
					obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.SGST=Convert.ToSingle(dr["SGST"]);

					obj.TaxIncluded=Convert.ToBoolean(dr["TaxIncluded"]);

					obj.Total=Convert.ToSingle(dr["Total"]);

					if(dr["TransactionID"]==DBNull.Value)
						obj.TransactionID=null;
					else
						obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.OrderDetail obj, DataRow dr)
		{
			try
			{
					obj.BasePrice=Convert.ToSingle(dr["BasePrice"]);

					obj.CGST=Convert.ToSingle(dr["CGST"]);

					obj.OrderDetailID=Convert.ToInt32(dr["OrderDetailID"]);

				obj.Order=BusinessLogic.Order.Select(Convert.ToInt32(dr["OrderID"]));
				obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.SGST=Convert.ToSingle(dr["SGST"]);

					obj.TaxIncluded=Convert.ToBoolean(dr["TaxIncluded"]);

					obj.Total=Convert.ToSingle(dr["Total"]);

				if(dr["TransactionID"]==DBNull.Value)
					obj.TransactionID=null;
				else
					obj.TransactionID=Convert.ToString(dr["TransactionID"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
