using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class Account:BusinessLogic.BusinessBase
	{

		public int AccountID { get;  set;}
		public BusinessLogic.SystemUser SystemUser { get;  set;}
		public string Narration { get;  set;}
		public float? CreditAmount { get;  set;}
		public float? DebitAmount { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}
		public string TransactionType { get;  set;}
		public string TransactionID { get;  set;}

		public Account(){}

		public Account(BusinessLogic.SystemUser SystemUser,string Narration,float CreditAmount,float DebitAmount,int UpdateBy,DateTime UpdateDate,string TransactionType,string TransactionID)
		{
			this.SystemUser=SystemUser;
			this.Narration=Narration;
			this.CreditAmount=CreditAmount;
			this.DebitAmount=DebitAmount;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.TransactionType=TransactionType;
			this.TransactionID=TransactionID;
		}

		public Account(int AccountID,BusinessLogic.SystemUser SystemUser,string Narration,float CreditAmount,float DebitAmount,int UpdateBy,DateTime UpdateDate,string TransactionType,string TransactionID)
		{
			this.AccountID=AccountID;
			this.SystemUser=SystemUser;
			this.Narration=Narration;
			this.CreditAmount=CreditAmount;
			this.DebitAmount=DebitAmount;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
			this.TransactionType=TransactionType;
			this.TransactionID=TransactionID;
		}

		#region IBase Members

		private BusinessLogic.Account Select()
		{
			try
			{
				BusinessLogic.Account obj=new Account();
				DataTable table=(new DataAccess.DAAccount(DataAccess.Database.Master)).Select(this.AccountID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.Account Select(int primaryKey)
		{
			try
			{
				BusinessLogic.Account obj=new Account();
				DataTable table=(new DataAccess.DAAccount(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Account> Select(string condition)
		{
			try
			{
				List<BusinessLogic.Account> objs=new List<BusinessLogic.Account>();
				DataTable table=(new DataAccess.DAAccount(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Account> SelectAll()
		{
			try
			{
				List<BusinessLogic.Account> objs=new List<BusinessLogic.Account>();
				DataTable table=(new DataAccess.DAAccount(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.Account> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.Account> objs=new List<BusinessLogic.Account>();
				DataTable table=(new DataAccess.DAAccount(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.AccountID= (new DataAccess.DAAccount(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAAccount(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAAccount(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAAccount(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAAccount(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAAccount(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAAccount(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.Account obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.SystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUser should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateDate should not be left blank.");
						if (Convert.ToString(obj.TransactionType).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionType should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.AccountID).Trim().Equals(string.Empty )) 
							throw new Exception("The AccountID should not be left blank.");
						if (Convert.ToString(obj.TransactionType).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionType should not be left blank.");
						if (Convert.ToString(obj.TransactionID).Trim().Equals(string.Empty)) 
							throw new Exception("The TransactionID should not be left blank.");

						break;
					case OperationMode.Delete :

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.Account> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.Account obj = new BusinessLogic.Account();
					//Add Custom code here

					obj.AccountID=Convert.ToInt32(dr["AccountID"]);

					obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
					if(dr["Narration"]==DBNull.Value)
						obj.Narration=null;
					else
						obj.Narration=Convert.ToString(dr["Narration"]);

					if(dr["CreditAmount"]==DBNull.Value)
						obj.CreditAmount=null;
					else
						obj.CreditAmount=Convert.ToSingle(dr["CreditAmount"]);

					if(dr["DebitAmount"]==DBNull.Value)
						obj.DebitAmount=null;
					else
						obj.DebitAmount=Convert.ToSingle(dr["DebitAmount"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					if(dr["TransactionType"]==DBNull.Value)
						obj.TransactionType=null;
					else
						obj.TransactionType=Convert.ToString(dr["TransactionType"]);

					if(dr["TransactionID"]==DBNull.Value)
						obj.TransactionID=null;
					else
						obj.TransactionID=Convert.ToString(dr["TransactionID"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.Account obj, DataRow dr)
		{
			try
			{
					obj.AccountID=Convert.ToInt32(dr["AccountID"]);

				obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
				if(dr["Narration"]==DBNull.Value)
					obj.Narration=null;
				else
					obj.Narration=Convert.ToString(dr["Narration"]);

				if(dr["CreditAmount"]==DBNull.Value)
					obj.CreditAmount=null;
				else
					obj.CreditAmount=Convert.ToSingle(dr["CreditAmount"]);

				if(dr["DebitAmount"]==DBNull.Value)
					obj.DebitAmount=null;
				else
					obj.DebitAmount=Convert.ToSingle(dr["DebitAmount"]);

					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

				if(dr["TransactionType"]==DBNull.Value)
					obj.TransactionType=null;
				else
					obj.TransactionType=Convert.ToString(dr["TransactionType"]);

				if(dr["TransactionID"]==DBNull.Value)
					obj.TransactionID=null;
				else
					obj.TransactionID=Convert.ToString(dr["TransactionID"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
