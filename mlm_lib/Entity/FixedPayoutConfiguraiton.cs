using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BusinessLogic
{
	public partial class FixedPayoutConfiguraiton:BusinessLogic.BusinessBase
	{

		public BusinessLogic.SystemUser SystemUser { get;  set;}
		public bool Enable { get;  set;}
		public float FixedAmount { get;  set;}
		public int FixedPayoutConfigurationID { get;  set;}
		public BusinessLogic.Package Package { get;  set;}
		public int UpdateBy { get;  set;}
		public DateTime UpdateDate { get;  set;}

		public FixedPayoutConfiguraiton(){}

		public FixedPayoutConfiguraiton(BusinessLogic.SystemUser SystemUser,bool Enable,float FixedAmount,BusinessLogic.Package Package,int UpdateBy,DateTime UpdateDate)
		{
			this.SystemUser=SystemUser;
			this.Enable=Enable;
			this.FixedAmount=FixedAmount;
			this.Package=Package;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		public FixedPayoutConfiguraiton(BusinessLogic.SystemUser SystemUser,bool Enable,float FixedAmount,int FixedPayoutConfigurationID,BusinessLogic.Package Package,int UpdateBy,DateTime UpdateDate)
		{
			this.SystemUser=SystemUser;
			this.Enable=Enable;
			this.FixedAmount=FixedAmount;
			this.FixedPayoutConfigurationID=FixedPayoutConfigurationID;
			this.Package=Package;
			this.UpdateBy=UpdateBy;
			this.UpdateDate=UpdateDate;
		}

		#region IBase Members

		private BusinessLogic.FixedPayoutConfiguraiton Select()
		{
			try
			{
				BusinessLogic.FixedPayoutConfiguraiton obj=new FixedPayoutConfiguraiton();
				DataTable table=(new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Select(this.FixedPayoutConfigurationID);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		internal static BusinessLogic.FixedPayoutConfiguraiton Select(int primaryKey)
		{
			try
			{
				BusinessLogic.FixedPayoutConfiguraiton obj=new FixedPayoutConfiguraiton();
				DataTable table=(new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Select(primaryKey);
				if (table.Rows.Count > 0)
				{
					MapEntity(obj, table.Rows[0]);
				}
				return obj;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.FixedPayoutConfiguraiton> Select(string condition)
		{
			try
			{
				List<BusinessLogic.FixedPayoutConfiguraiton> objs=new List<BusinessLogic.FixedPayoutConfiguraiton>();
				DataTable table=(new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Select(condition);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.FixedPayoutConfiguraiton> SelectAll()
		{
			try
			{
				List<BusinessLogic.FixedPayoutConfiguraiton> objs=new List<BusinessLogic.FixedPayoutConfiguraiton>();
				DataTable table=(new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).SelectAll();
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static List<BusinessLogic.FixedPayoutConfiguraiton> SelectAll(int top)
		{
			try
			{
				List<BusinessLogic.FixedPayoutConfiguraiton> objs=new List<BusinessLogic.FixedPayoutConfiguraiton>();
				DataTable table=(new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).SelectAll(top);
				if (table.Rows.Count > 0)
				{
					MapEntity(objs, table);
				}
				return objs;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Insert()
		{
			try
			{
				Validate(OperationMode.Insert,this);
				this.FixedPayoutConfigurationID= (new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Insert(this);
				return 1;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Update()
		{
			try
			{
				Validate(OperationMode.Update,this);
				int x = (new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Update(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private int Delete()
		{
			try
			{
				Validate(OperationMode.Delete,this);
				int x = (new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Delete(this);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int DeleteAll()
		{
			try
			{
				int x = (new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).DeleteAll();
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static int Delete(string condition)
		{
			try
			{
				int x = (new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Delete(condition);
				return x;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object IsExist(string condition)
		{
			try
			{
				return (new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).IsExist(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static object Count(string condition)
		{
			try
			{
				return (new DataAccess.DAFixedPayoutConfiguraiton(DataAccess.Database.Master)).Count(condition);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void Validate(OperationMode mode,BusinessLogic.FixedPayoutConfiguraiton obj)
		{
			try
			{
				switch(mode)
				{
					case OperationMode.Insert :
						if (Convert.ToString(obj.SystemUser.SystemUserID).Trim().Equals(string.Empty )) 
							throw new Exception("The SystemUser should not be left blank.");
						if (Convert.ToString(obj.Enable).Trim().Equals(string.Empty )) 
							throw new Exception("The Enable should not be left blank.");
						if (Convert.ToString(obj.FixedAmount).Trim().Equals(string.Empty )) 
							throw new Exception("The FixedAmount should not be left blank.");
						if (Convert.ToString(obj.Package.PackageID).Trim().Equals(string.Empty )) 
							throw new Exception("The Package should not be left blank.");
						if (Convert.ToString(obj.UpdateBy).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateBy should not be left blank.");
						if (Convert.ToString(obj.UpdateDate).Trim().Equals(string.Empty )) 
							throw new Exception("The UpdateDate should not be left blank.");

						break;
					case OperationMode.Update :
						if (Convert.ToString(obj.FixedPayoutConfigurationID).Trim().Equals(string.Empty )) 
							throw new Exception("The FixedPayoutConfigurationID should not be left blank.");

						break;
					case OperationMode.Delete :
						if (Convert.ToString(obj.FixedPayoutConfigurationID).Trim().Equals(string.Empty )) 
							throw new Exception("The FixedPayoutConfigurationID should not be left blank.");

						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(List<BusinessLogic.FixedPayoutConfiguraiton> list, DataTable table)
		{
			try
			{
				foreach(DataRow dr in table.Rows)
				{
					BusinessLogic.FixedPayoutConfiguraiton obj = new BusinessLogic.FixedPayoutConfiguraiton();
					//Add Custom code here

					obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
					obj.Enable=Convert.ToBoolean(dr["Enable"]);

					obj.FixedAmount=Convert.ToSingle(dr["FixedAmount"]);

					obj.FixedPayoutConfigurationID=Convert.ToInt32(dr["FixedPayoutConfigurationID"]);

					obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

					list.Add(obj);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static void MapEntity(BusinessLogic.FixedPayoutConfiguraiton obj, DataRow dr)
		{
			try
			{
				obj.SystemUser=BusinessLogic.SystemUser.Select(Convert.ToInt32(dr["SystemUserID"]));
					obj.Enable=Convert.ToBoolean(dr["Enable"]);

					obj.FixedAmount=Convert.ToSingle(dr["FixedAmount"]);

					obj.FixedPayoutConfigurationID=Convert.ToInt32(dr["FixedPayoutConfigurationID"]);

				obj.Package=BusinessLogic.Package.Select(Convert.ToInt32(dr["PackageID"]));
					obj.UpdateBy=Convert.ToInt32(dr["UpdateBy"]);

					obj.UpdateDate=Convert.ToDateTime(dr["UpdateDate"]);

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
