If Exists (Select object_id from SYS.Procedures Where object_name(object_id)='TDSReport')
Begin
	Drop Procedure dbo.TDSReport
END
GO
Create Procedure TDSReport    
AS Begin    
Select     
 s.UserID [User ID],s.ContactName [Contact Name],s.PAN [PAN No],s.BankName [Bank Name],s.BranchName [Branch Name],s.AccountType [Account Type], s.AccountNumber [Account Number],  
 sum(p.GrossAmount) [Gross Amount],AVG(p.TDS) AS [TDS%],Sum(p.TDSAmount) AS  [Total TDS],Sum(p.NetAmount) AS [Net Amount]
From   Payouts p    
Inner Join Customers c    
On p.CustomerID =c.CustomerID    
Inner Join SystemUsers s  
On s.SystemUserID=c.SystemUserID  
Where p.GrossAmount > 0 and p.TDS>0
Group by s.userid,s.contactname,s.pan,s.bankname,s.branchname,s.accounttype,s.accountnumber

UNION 

Select     
 s.UserID [User ID],s.ContactName [Contact Name],s.PAN [PAN No],s.BankName [Bank Name],s.BranchName [Branch Name],s.AccountType [Account Type], s.AccountNumber [Account Number],  
 sum(p.GrossAmount) [Gross Amount],AVG(p.TDS) AS [TDS%],Sum(p.TDSAmount) AS  [Total TDS],Sum(p.NetAmount) AS [Net Amount]
From   FixedPayouts p    
   Inner Join SystemUsers s  
On s.SystemUserID=p.SystemUserID  
Where p.GrossAmount > 0 and p.TDS>0
Group by s.userid,s.contactname,s.pan,s.bankname,s.branchname,s.accounttype,s.accountnumber

Order by s.UserID    
END 
