IF EXISTS (SELECT * FROM sys.procedures  WHERE object_id= OBJECT_ID(N'dbo.TotalServiceCharge'))
BEGIN
	DROP PROCEDURE dbo.TotalServiceCharge
END 
GO 

Create Procedure TotalServiceCharge
AS
Begin
	Declare @FixedPayoutServiceCharge  float
	Declare @PayoutServiceCharge  float

	Select @FixedPayoutServiceCharge = ISNULL(Sum(ServiceChargeAmount),0) From FixedPayouts
	Select @PayoutServiceCharge = ISNULL(Sum(ServiceChargeAmount),0) From Payouts

	Select @FixedPayoutServiceCharge + @PayoutServiceCharge as TotalTDS
END