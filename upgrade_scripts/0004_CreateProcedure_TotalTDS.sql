IF EXISTS (SELECT * FROM sys.procedures  WHERE object_id= OBJECT_ID(N'dbo.TotalTDS'))
BEGIN
	DROP PROCEDURE dbo.TotalTDS
END 
GO 

Create Procedure TotalTDS
AS
Begin
	Declare @FixedPayouTDS  float
	Declare @PayoutTDS  float

	Select @FixedPayoutDS = ISNULL(Sum(TDSAmount),0) From FixedPayouts
	Select @PayoutTDS = ISNULL(Sum(TDSAmount),0) From Payouts

	Select @FixedPayoutDS+@PayoutTDS as TotalTDS
END
