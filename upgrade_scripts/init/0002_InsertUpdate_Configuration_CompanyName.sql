Declare @ConfigKey varchar(50)
Declare @ConfigVale varchar(50)
SET @ConfigVale = 'Royal Bloom'
SET @ConfigKey = 'CompanyName'
IF Not Exists (Select ConfigurationID from Configurations Where ConfigKey=@ConfigKey)
BEGIN
	Insert Into Configurations values(@ConfigKey,@ConfigVale,null)
END
ELSE
BEGIN
	Update Configurations SET ConfigValue=@ConfigVale Where ConfigKey=@ConfigKey
End

