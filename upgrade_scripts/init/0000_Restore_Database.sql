USE [master]
RESTORE DATABASE [Diamond4You_13] 
FROM  DISK = N'D:\Kaleem Sir 27-10-20\Database\upgrade_scripts\201208_BaseConfiguration.bak' 
WITH  FILE = 1,  
MOVE N'dreamachiver' 
TO N'D:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Diamond4You_13.mdf',  
MOVE N'dreamachiver_log' 
TO N'D:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Diamond4You_13_log.ldf',  
NOUNLOAD,  
STATS = 5
GO
