Declare @ConfigKey varchar(50)
Declare @ConfigVale varchar(50)
SET @ConfigVale = '70000001'
SET @ConfigKey = 'BaseUserID'
IF Not Exists (Select ConfigurationID from Configurations Where ConfigKey=@ConfigKey)
BEGIN
	Insert Into Configurations values(@ConfigKey,@ConfigVale,null)
END
ELSE
BEGIN
	Update Configurations SET ConfigValue=@ConfigVale Where ConfigKey=@ConfigKey
End

