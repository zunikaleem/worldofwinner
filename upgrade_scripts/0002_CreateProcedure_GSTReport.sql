If Exists (Select object_id from SYS.Procedures Where object_name(object_id)='GSTREPORT')
Begin
	Drop Procedure dbo.GSTReport
END
GO  
Create Procedure GSTReport    
AS begin    
	Select    
		dbo.GetConfigValue('GSTNumber') as GSTIN,    
		s.ContactName 'Receiver Name',    
		s.UserID 'User ID',    
		c.RegistrationDate 'Invoice Date',    
		'Regular' as 'Invoice Type',    
		(p.cgst + p.sgst) 'Rate %',    
		p.baseprice 'Taxable Amount',    
		p.cgst 'CGST%',    
		p.sgst 'SGST%',    
		p.roundoff 'Round Off',    
		p.cgst/100 * p.baseprice +p.sgst/100 * p.baseprice 'Total GST'    
	From   
		Customers c    
	Inner Join   
		EPINs e     
	On   
		c.EPINID = e.EPINID     
	Inner Join   
		Packages p    
	On   
		e.PackageID = p.PackageID   
	Inner Join   
		SystemUsers s  
	On   
		s.SystemUserID = c.SystemUserID  
	Inner Join 
		AccessGroups ag
	On 
		ag.AccessGroupID=s.AccessGroupID
	Where 
		ag.AccessGroupName='Customer'
	Order By   
		c.RegistrationDate    
End 