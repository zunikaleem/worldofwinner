If Exists (Select Routine_Name from INFORMATION_SCHEMA.ROUTINES Where Routine_Name = 'GetConfigValue' and ROUTINE_TYPE = 'FUNCTION')
Begin
	Drop Procedure dbo.GetConfigValue
END
GO

CREATE Function [dbo].[GetConfigValue](@ConfigKey nvarchar(30)) Returns nvarchar(50)  
AS 
Begin  
	Declare @ConfigValue nvarchar(59)  
	Select   
		@ConfigValue = ConfigValue
	From   
		Configurations   
	Where   
		ConfigKey=@ConfigKey
	
	Return @ConfigValue  
End