If Exists (Select * from sys.procedures where Name='GSTReport')
Begin
	Drop Procedure GSTReport  
End

Go

Create Procedure GSTReport  
AS begin  
Select  
	'12GEOPS0823BBZH' as GSTIN,  
	s.ContactName 'Receiver Name',  
	c.State 'Place to Supply',  
	s.UserID 'User ID',  
	c.RegistrationDate 'Invoice Date',  
	'Regular' as 'Invoice Type',  
	(p.cgst + p.sgst) 'Rate %',  
	p.baseprice 'Taxable Amount',  
	p.cgst 'CGST%',  
	p.sgst 'SGST%',  
	p.roundoff 'Round Off',  
	p.cgst/100 * p.baseprice +p.sgst/100 * p.baseprice 'Total GST'  
From 
	Customers c  
Inner Join 
	EPINs e   
On 
	c.EPINID = e.EPINID   
Inner Join 
	Packages p  
On 
	e.PackageID = p.PackageID 
Inner Join 
	SystemUsers s
On 
	s.SystemUserID = c.SystemUserID
Order By 
	c.RegistrationDate  
End 