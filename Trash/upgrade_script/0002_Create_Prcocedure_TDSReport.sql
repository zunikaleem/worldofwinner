If Exists (Select * from sys.procedures where Name='TDSReport')
Begin
	Drop Procedure TDSReport  
End

Go

Create Procedure TDSReport  
AS Begin  
Select   
	p.PayoutDate [Payout Date],  
	s.UserID [User ID],  
	s.ContactName [Contact Name],  
	s.PAN [PAN No],  
	s.BankName [Bank Name],  
	s.BranchName [Branch Name],  
	s.AccountType [Account Type],  
	s.AccountNumber [Account Number],
	p.AtStage [Level],
	p.GrossAmount [Gross Amount],  
	convert(varchar,p.TDS)+'%' [TDS],  
	(p.tds/100)* p.GrossAmount [Total TDS]  
From 
	Payouts p  
Inner Join 
Customers c  
On 
	p.CustomerID =c.CustomerID  
Inner Join SystemUsers s
On
	s.SystemUserID=c.SystemUserID
Order by p.PayoutDate  
END  
  