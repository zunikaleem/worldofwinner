Declare @TransactionID varchar(50)
SET @TransactionID='292fbd08-e46d-4b8e-a456-7be2ca33b3c2'
Delete from OrderDetails Where TransactionID = @TransactionID
Delete from Orders Where TransactionID = @TransactionID
Delete from Payouts Where TransactionID = @TransactionID
Delete from FixedPayouts Where TransactionID = @TransactionID
Delete from Accounts Where TransactionID = @TransactionID
Delete from Customers Where TransactionID = @TransactionID
Delete from SystemUsers where TransactionID = @TransactionID