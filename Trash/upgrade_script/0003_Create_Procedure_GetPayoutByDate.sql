If Exists (Select * from sys.procedures where Name='GetPayoutByDate')
Begin
	Drop Procedure GetPayoutByDate  
End

Go

CREATE Proc GetPayoutByDate      
(      
 @FromDate datetime  ,    
 @ToDate datetime      
)      
As      
Begin      
	Select 
		p.PayoutDate,s.UserID,s.PAN, 
		s.ContactName,s.BankName,s.BranchName,
		s.IFSCCode,s.AccountType,s.AccountHolderName,
		s.AccountNumber,p.TDs,p.ServiceCharge,
		SUM(p.GrossAmount) AS GrossAmount,
		SUM(p.NetAmount) as NetAmount      
	From 
		Payouts p      
	Inner Join 
		Customers c      
	On 
		p.CustomerID =c.CustomerID  
	Inner Join SystemUsers s
	On
		s.SystemUserID=c.SystemUserID
	Where 
		Convert(datetime,Convert(varchar(10),PayoutDate,101)) between Convert(datetime,Convert(varchar(10),@FromDate,101)) and  Convert(datetime,Convert(varchar(10),@ToDate,101))      
	Group By 
		p.PayoutDate,s.UserID,s.PAN,
		s.ContactName,s.BankName,s.BranchName,
		s.IFSCCode,s.AccountType,s.AccountHolderName,
		s.AccountNumber,p.TDs,p.ServiceCharge      
END