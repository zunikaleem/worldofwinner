If Exists (Select Name from sys.triggers where Name='OnPayoutMatrixInsertUpdate')
DROP TRIGGER OnPayoutMatrixInsertUpdate
GO
Create Trigger [OnPayoutMatrixInsertUpdate]
On [dbo].[PayoutMatrix]
After Insert,Update
AS Begin
	Insert Into PayoutMatrixLogs (PayoutMatrixId,PackageID,[Level],Release,Amount,UpdateBy,UpdateDate)
	Select PayoutMatrixId,PackageID,[Level],Release,Amount,UpdateBy,GETDATE() from Inserted
End


	