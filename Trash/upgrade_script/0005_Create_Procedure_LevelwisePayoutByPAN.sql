If Exists (Select * from sys.procedures where Name='LevelWisePayoutByPAN')
Begin
	Drop Procedure LevelWisePayoutByPAN  
End

Go

CREATE Procedure LevelWisePayoutByPAN(@PANNumber varchar(20))    
As    
Begin          
IF EXISTS (SELECT * FROM sys.tables WHERE name='##pivot') DROP TABLE ##pivot                    

Declare @select varchar(8000),@sumfunc varchar(100),@pivot varchar(100),@table varchar(max)                    

SET @select ='Select UserID as [UserID] from #Matrix group by UserID'                    
SET @sumfunc='AVG(Total)'                    
SET @pivot='AtStage'                    
          
Create Table #Matrix          
(          
	UserID varchar(50),
	AtStage varchar(50),
	Total float    
)          
          
Insert Into #Matrix          
          
SELECT 
	s.UserID ,  
	'Level '+CONVERT(varchar,atstage) as Level,    
	Round(Sum(NetAmount),2) AS Total                  
FROM   
	Payouts     
Inner Join 
	Customers c     
On 
	c.CustomerID =Payouts.CustomerID   
Inner Join 
	SystemUsers s
On 
	s.SystemUserID = c.SystemUserID
Where 
	s.PAN=@PANNumber          
GROUP BY s.UserID,AtStage         
Order by s.UserID,AtStage desc     
              
SET @table='#Matrix'            
          
DECLARE @sql varchar(max), @delim varchar(1)                    
          
SET NOCOUNT ON                    
          
SET ANSI_WARNINGS OFF                    
          
EXEC ('SELECT ' + @pivot + ' AS [pivot] INTO ##pivot FROM ' + @table + ' WHERE 1=2')                    
          
EXEC ('INSERT INTO ##pivot SELECT DISTINCT ' + @pivot + ' FROM ' + @table + ' WHERE ' + @pivot + ' Is Not Null')                    
          
SELECT @sql='',  @sumfunc=stuff(@sumfunc, len(@sumfunc), 1, ' END)' )                    
    
SELECT @delim=CASE Sign( CharIndex('char', data_type)+CharIndex('date', data_type) )                     
          
WHEN 0 THEN '' ELSE '''' END                     
          
FROM tempdb.information_schema.columns                     
          
WHERE table_name='##pivot' AND column_name='pivot'                    
          
SELECT @sql=@sql + '''' + convert(varchar(100), [pivot]) + ''' = ' +      stuff(@sumfunc,charindex( '(', @sumfunc )+1, 0, ' CASE ' + @pivot + ' WHEN '       + @delim + convert(varchar(100), [pivot]) + @delim + ' THEN ' ) + ', ' FROM ##pivot               
          
DROP TABLE ##pivot                    
          
If len(@sql)>1 Begin                    
          
	SELECT @sql=left(@sql, len(@sql)-1)                    
          
	SELECT @select=stuff(@select, charindex(' FROM ', @select)+1, 0, ', ' + @sql + ' ')                    
          
Print @Select                    
          
EXEC (@select)            
          
Drop Table #Matrix                  
          
End              
          
SET ANSI_WARNINGS ON                    
End      
          
    
    
    
  