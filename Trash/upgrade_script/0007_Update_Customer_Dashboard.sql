
ALTER PROCEDURE [dbo].[CustomerDashboard]  
(  
	@startDate smalldatetime,  
	@endDate smalldatetime  
)  
AS  
	create table #XC  
	(  
		smmId [int],  
		city nvarchar(50),  
		today [int],  
		yesterday [int],  
		currentweek [int],  
		previousweek [int],  
		currentmonth [int],  
		previousmonth [int],  
		currentyear [int],  
		previousyear [int],  
		specifieddate [int],  
		total [int]  
	)  
	Declare @today int  
	Declare @yesterday int  
	Declare @currentweek int  
	Declare @previousweek int  
	Declare @currentmonth int  
	Declare @previousmonth int  
	Declare @currentyear int  
	Declare @previousyear int  
	Declare @specifieddate int  
	Declare @total int  
	Declare @smmID int  
	DECLARE @city varchar(50)
	
	DECLARE @getCity CURSOR  
	SET @smmID=1  
	SET @getCity = CURSOR FOR SELECT distinct city FROM Customers where city is not null  
	OPEN @getCity  
	FETCH NEXT FROM @getCity INTO @city  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		Select @today=   COUNT(isnull(c.city,1)) 
		From Customers c
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and CONVERT(CHAR(10),c.RegistrationDate,120)=Convert(char(10),[dbo].[getistdate] (),120) and c.city=@city
  
		Select @yesterday=  COUNT(isnull(c.city,1)) 
		from Customers c
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and CONVERT(CHAR(10),c.RegistrationDate,120)=Convert(char(10),dateadd("d",-1,[dbo].[getistdate] ()),120) and c.city=@city  
  
		Select @currentweek= COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  c.RegistrationDate Between Convert(varchar, DateAdd(dd, -(DatePart(dw, dbo.getistdate()) - 1), dbo.getistdate()), 101) and Convert(varchar, DateAdd(dd, (7 - DatePart(dw, dbo.getistdate())), dbo.getistdate()), 101) and c.city=@city  
  
		Select @previousweek= COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  c.RegistrationDate Between Convert(varchar, DateAdd(dd, -(DatePart(dw, dbo.getistdate()) + 6), dbo.getistdate()), 101) and Convert(varchar, DateAdd(dd, ( - DatePart(dw, dbo.getistdate())), dbo.getistdate()), 101) and c.city=@city  
  
		Select @currentmonth= COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  c.RegistrationDate between DATEADD(m, DATEDIFF(m, 0, dbo.getistdate()), 0) and DATEADD(m, DATEDIFF(m, 0, dbo.getistdate())+1, 0)-1 and c.city=@city  
  
		Select @previousmonth= COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  c.RegistrationDate between DATEADD(mm, DATEDIFF(mm,0,DATEADD(mm,-1,dbo.getistdate())), 0) and DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,dbo.getistdate()),0)) and c.city=@city  
  
		Select @currentyear= COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  datepart("yy",c.RegistrationDate) =datepart("yy",dbo.getistdate()) and c.city=@city  
  
		Select @previousyear= COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  datepart("yy",c.RegistrationDate) =(datepart("yy",dbo.getistdate())-1) and c.city=@city  
  
		Select @specifieddate= COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  Convert(varchar(10),c.RegistrationDate,101) Between Convert(varchar(10),@startdate,101) and Convert(varchar(10),@enddate,101) and c.city=@city  
  
		Select @total=   COUNT(isnull(c.city,1)) 
		from Customers c 
		Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
		Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
		Where ag.AccessGroupName='Customer' and  c.city=@city  
  
		Insert   
		#XC(smmID,city,today,yesterday,currentweek,previousweek,currentmonth,previousmonth,currentyear,previousyear,specifieddate,total)  
		Values  
		(@smmID,@city,@today,@yesterday,@currentweek,@previousweek,@currentmonth,@previousmonth,@currentyear,@previousyear,@specifieddate,@total)  
  
		Set @smmID=@smmID+1  
		
		FETCH NEXT FROM @getCity INTO @City  
	END  
   
	CLOSE @getCity  
	DEALLOCATE @getCity  
  
	Select @today=   COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   CONVERT(CHAR(10),c.RegistrationDate,120)=Convert(char(10),[dbo].[getistdate] (),120) and c.city is null  

	Select @yesterday=  COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   CONVERT(CHAR(10),c.RegistrationDate,120)=Convert(char(10),dateadd("d",-1,[dbo].[getistdate] ()),120) and c.city is null  

	Select @currentweek= COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   c.RegistrationDate Between Convert(varchar, DateAdd(dd, -(DatePart(dw, dbo.getistdate()) - 1), dbo.getistdate()), 101) and Convert(varchar, DateAdd(dd, (7 - DatePart(dw, dbo.getistdate())), dbo.getistdate()), 101) and c.city is null  

	Select @previousweek= COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   c.RegistrationDate Between Convert(varchar, DateAdd(dd, -(DatePart(dw, dbo.getistdate()) + 6), dbo.getistdate()), 101) and Convert(varchar, DateAdd(dd, ( - DatePart(dw, dbo.getistdate())), dbo.getistdate()), 101) and c.city is null  

	Select @currentmonth= COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   c.RegistrationDate between DATEADD(m, DATEDIFF(m, 0, dbo.getistdate()), 0) and DATEADD(m, DATEDIFF(m, 0, dbo.getistdate())+1, 0)-1 and c.city is null  

	Select @previousmonth= COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   c.RegistrationDate between DATEADD(mm, DATEDIFF(mm,0,DATEADD(mm,-1,dbo.getistdate())), 0) and DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,dbo.getistdate()),0)) and c.city is null  

	Select @currentyear= COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   datepart("yy",c.RegistrationDate) =datepart("yy",dbo.getistdate()) and c.city is null  

	Select @previousyear= COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   datepart("yy",c.RegistrationDate) =(datepart("yy",dbo.getistdate())-1) and c.city is null  

	Select @specifieddate= COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   Convert(varchar(10),c.RegistrationDate,101) Between Convert(varchar(10),@startdate,101) and Convert(varchar(10),@enddate,101) and c.city is null  

	Select @total=   COUNT(isnull(c.city,1)) 
	from Customers c
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID =s.AccessGroupID 
	Where ag.AccessGroupName='Customer' and   c.city is null  
  
	Insert   
	#XC(smmID,city,today,yesterday,currentweek,previousweek,currentmonth,previousmonth,currentyear,previousyear,specifieddate,total)  
	Values  (@smmID,'Not Defined',@today,@yesterday,@currentweek,@previousweek,@currentmonth,@previousmonth,@currentyear,@previousyear,@specifieddate,@total)  
  
	Set @smmID=@smmID+1  
  
	Declare @todayTotal int  
	Declare @yesterdayTotal int  
	Declare @currentWeekTotal int  
	Declare @previousWeekTotal int  
	Declare @currentMonthTotal int  
	Declare @previousMonthTotal int  
	Declare @currentYearTotal int  
	Declare @previousYearTotal int  
	Declare @specifiedDateTotal int  
	Declare @totalTotal int  
  
	Select @todayTotal=sum(today) from #XC  
	Select @yesterdayTotal=sum(yesterday) from #XC  
	Select @currentWeekTotal=sum(currentweek) from #XC  
	Select @previousWeekTotal=sum(previousweek) from #XC  
	Select @currentMonthTotal=sum(currentmonth) from #XC  
	Select @previousMonthTotal=sum(previousmonth) from #XC  
	Select @currentYearTotal=sum(currentyear) from #XC  
	Select @previousYearTotal=sum(previousyear) from #XC  
	Select @specifiedDateTotal=sum(specifieddate) from #XC  
	Select @totalTotal=sum(total) from #XC  

	Insert #XC(smmID,city,today,yesterday,currentweek,previousweek,currentmonth,previousmonth,currentyear,previousyear,specifieddate,total)  
	Values  
	(@smmID,'All Cities',@todayTotal,@yesterdayTotal,@currentweekTotal,@previousweekTotal,@currentmonthTotal,@previousmonthTotal,@currentyearTotal,@previousyearTotal,@specifieddateTotal,@totalTotal)  

	select  
	isnull(city,'Not Defined') 'City',today 'Today',yesterday 'Yesterday',currentweek 'Current Week',  
	previousweek 'Previous Week',currentmonth 'Current Month',previousmonth 'Previous Month',currentyear 'Current Year',  
	previousyear 'Previous Year',specifieddate 'Between Date',total 'Total' from #XC  
	order by today desc,yesterday desc,currentweek desc,previousweek desc,currentmonth desc,previousmonth desc,currentyear desc,previousyear desc  

drop table #XC  
RETURN  
