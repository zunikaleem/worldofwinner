IF EXISTS (SELECT * FROM sys.procedures  WHERE object_id= OBJECT_ID(N'dbo.TotalServiceCharge'))
BEGIN
	DROP PROCEDURE dbo.TotalServiceCharge
END 
GO 

Create Procedure TotalServiceCharge
AS
Begin
	Declare @FixedPayoutTDS  float
	Declare @PayoutTDS  float

	Select @FixedPayoutTDS = Sum(ServiceChargeAmount) From FixedPayouts
	Select @PayoutTDS = Sum(ServiceChargeAmount) From Payouts

	Select @FixedPayoutTDS + @PayoutTDS as TotalTDS
END