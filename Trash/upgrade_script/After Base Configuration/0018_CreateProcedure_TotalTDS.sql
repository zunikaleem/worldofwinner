IF EXISTS (SELECT * FROM sys.procedures  WHERE object_id= OBJECT_ID(N'dbo.TotalTDS'))
BEGIN
	DROP PROCEDURE dbo.TotalTDS
END 
GO 

Create Procedure TotalTDS
AS
Begin
	Declare @FixedPayoutDS  float
	Declare @PayoutTDS  float

	Select @FixedPayoutDS = Sum(TDSAmount) From FixedPayouts
	Select @PayoutTDS = Sum(TDSAmount) From Payouts

	Select @FixedPayoutDS+@PayoutTDS as TotalTDS
END
