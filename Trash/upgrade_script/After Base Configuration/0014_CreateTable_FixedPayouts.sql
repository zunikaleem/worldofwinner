IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='FixedPayouts')
BEGIN
	CREATE TABLE [dbo].[FixedPayouts](
		[FixedPayoutID] [int] IDENTITY(1,1) NOT NULL,
		[SystemUserID] [int] NOT NULL,
		[BySystemUserID] [int] NOT NULL,
		[PayoutDate] [datetime] NOT NULL,
		[TDS] [float] NOT NULL,
		[ServiceCharge] [float] NOT NULL,
		[GrossAmount] [float] NOT NULL,
		[NetAmount] [float] NOT NULL,
		[TransactionID] [nvarchar](50) NOT NULL,
		[UpdateBy] [int] NOT NULL,
		[UpdateDate] [datetime] NOT NULL,
	 CONSTRAINT [PK_FixedPayouts] PRIMARY KEY CLUSTERED 
	(
		[FixedPayoutID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END


