IF EXISTS (SELECT * FROM sys.procedures  WHERE object_id= OBJECT_ID(N'dbo.TotalDistribution'))
BEGIN
	DROP PROCEDURE dbo.TotalDistribution
END 
GO 

Create Procedure TotalDistribution
AS
Begin
	Declare @FixedPayoutDistribution  float
	Declare @PayoutDistribution  float

	Select @FixedPayoutDistribution = Sum(NetAmount) From FixedPayouts
	Select @PayoutDistribution = Sum(NetAmount) From Payouts

	Select @FixedPayoutDistribution + @PayoutDistribution as TotalTDS
END

