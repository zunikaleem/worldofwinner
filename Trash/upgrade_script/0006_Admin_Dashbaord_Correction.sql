Update SystemUsers SET AccessGroupID = 4 Where AccessGroupID=3
GO
Update AccessGroups SET AccessGroupName = 'Admin Customer' Where AccessGroupName ='Admin'
GO
Update AccessGroups SET AccessGroupName = 'Leader Customer' Where AccessGroupName ='Company Leader'
GO 
ALTER Procedure TotalEarning  
As  
Begin  
	Select isnull(SUM(p.Cost),0) from Customers c   
	Inner join EPINs e on c.EPINID=e.EPINID  
	Inner join Packages p on p.PackageID =e.PackageID  
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups a on a.AccessGroupID = s.AccessGroupID
	Where a.AccessGroupName = 'Customer'
End
GO
Update EPINS SET PackageID = 6 Where EPINID in (270,271,272,273,274)
GO
ALTER Proc TotalTDS  
AS Begin  
	Select isnull(Sum(p.TDS/100 * p.GrossAmount),0)   
	from Payouts p
	Inner Join Customers c on c.CustomerID = p.CustomerID
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups a on a.AccessGroupID = s.AccessGroupID
	Where a.AccessGroupName ='Customer'
end
GO
ALTER Proc TotalServiceCharge  
AS Begin  
	Select isnull(Sum(ServiceCharge/100 * GrossAmount),0)   
	from Payouts p
	Inner Join Customers c on c.CustomerID = p.CustomerID
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups a on a.AccessGroupID = s.AccessGroupID
	Where a.AccessGroupName ='Customer'  
end  
GO
ALTER Procedure TotalDistribution  
As Begin  
	Select SUM(DebitAmount) from Accounts a
	Inner Join Payouts p on p.TransactionID = a.TransactionID
	Inner Join Customers c on c.CustomerID = p.PayoutID
	Inner Join SystemUsers s on s.SystemUserID = c.SystemUserID
	Inner Join AccessGroups ag on ag.AccessGroupID = s.AccessGroupID
	Where ag.AccessGroupName ='Customer'
End