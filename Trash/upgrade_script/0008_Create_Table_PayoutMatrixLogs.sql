If Not Exists (Select Name from sys.tables where Name='PayoutMatrixLogs')
Begin
	CREATE TABLE [dbo].[PayoutMatrixLogs]
	(
		[PayoutMatrixLogID] [int] IDENTITY(1,1) NOT NULL,
		[PayoutMatrixID] [int] NOT NULL,
		[PackageID] [int] NOT NULL,
		[Level] [int] NOT NULL,
		[Release] [int] NOT NULL,
		[Amount] [decimal](18, 0) NOT NULL,
		[UpdateBy] [int] NOT NULL,
		[UpdateDate] [datetime] NOT NULL,
		PRIMARY KEY CLUSTERED 
		(
			[PayoutMatrixLogID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
End


