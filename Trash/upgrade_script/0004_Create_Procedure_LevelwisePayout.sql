If Exists (Select * from sys.procedures where Name='LevelWisePayout')
Begin
	Drop Procedure LevelWisePayout  
End

Go

Create Procedure LevelWisePayout    
As  Begin          
 IF EXISTS (SELECT * FROM sys.tables WHERE name='##pivot')   
  DROP TABLE ##pivot                    
   
 Declare @select varchar(8000),@sumfunc varchar(100),@pivot varchar(100),@table varchar(max)                    
  
 SET @select ='Select m.UserID as [User ID] from #Matrix m  group by m.UserID'                    
 SET @sumfunc='AVG(Total)'         
 SET @pivot='AtStage'                    
  
 Create Table #Matrix          
 (        
  UserID varchar(50),AtStage varchar(50),Total float  
 )          
  
 Insert Into #Matrix          
  
 SELECT   
  s.UserID ,     
  'Level '+CONVERT(varchar,atstage) [Level],   
  Round(Sum(p.NetAmount),2) AS Total  
 FROM       
  Payouts p  
 Inner Join     
  Customers c     
 on     
  c.CustomerID =p.CustomerID 
 Inner Join SystemUsers s
 On	
	s.SystemUserID = c.SystemUserID
 GROUP BY     
  s.UserID,p.AtStage         
 ORDER BY  
  s.UserID,AtStage desc     
  
 SET @table='#Matrix'            
  
 DECLARE @sql varchar(max), @delim varchar(1)                    
 SET NOCOUNT ON                    
 SET ANSI_WARNINGS OFF                    
  
 EXEC ('SELECT ' + @pivot + ' AS [pivot] INTO ##pivot FROM ' + @table + ' WHERE 1=2')                    
  
 EXEC ('INSERT INTO ##pivot SELECT DISTINCT ' + @pivot + ' FROM ' + @table + ' WHERE ' + @pivot + ' Is Not Null')                    
  
 SELECT @sql='',  @sumfunc=stuff(@sumfunc, len(@sumfunc), 1, ' END)' )                    
  
 SELECT @delim=CASE Sign( CharIndex('char', data_type)+CharIndex('date', data_type) )                     
 WHEN 0 THEN '' ELSE '''' END                     
 FROM tempdb.information_schema.columns                     
 WHERE table_name='##pivot' AND column_name='pivot'      
  
 SELECT @sql=@sql + '''' + convert(varchar(100), [pivot]) + ''' = ' +      stuff(@sumfunc,charindex( '(', @sumfunc )+1, 0, ' CASE ' + @pivot + ' WHEN '       + @delim + convert(varchar(100), [pivot]) + @delim + ' THEN ' ) + ', ' FROM ##pivot              
  
   
 DROP TABLE ##pivot        
   
 IF LEN(@sql)>1 begin                    
  SELECT @sql=left(@sql, len(@sql)-1)                    
  SELECT @select=stuff(@select, charindex(' FROM ', @select)+1, 0, ', ' + @sql + ' ')                    
    
  PRINT @Select                    
    
  EXEC (@select)            
    
  DROP TABLE #Matrix                  
  
 END              
  
 SET ANSI_WARNINGS ON                    
END      
  
  
  
  