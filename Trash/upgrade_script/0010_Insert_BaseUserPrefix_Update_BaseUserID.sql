If Not Exists (Select ConfigurationID from Configurations Where ConfigKey='BaseUserIDPrefix')
Begin
	Insert Into Configurations (ConfigKey,ConfigValue)
	Values ('BaseUserIDPrefix','A')
End

If Exists (Select ConfigurationID from Configurations Where ConfigKey='BaseUserID')
Begin
	Update Configurations SET ConfigValue ='10000001'
	Where ConfigKey='BaseUserID'
End
