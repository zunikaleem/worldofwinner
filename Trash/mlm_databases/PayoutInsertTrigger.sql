USE [Diamond4You]
GO
/****** Object:  Trigger [dbo].[OnPayoutInserted]    Script Date: 11-10-2020 20:57:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Trigger [dbo].[OnPayoutInserted]
On [dbo].[Payouts]
After Insert
AS
Begin
	Declare @CustomerID as int
	Declare @PayoutID as int 
	Declare @Narration as varchar(1000)
	Declare @CreditAmount as float
	Declare @UpdateBy int
	Declare @UpdateDate datetime
	Declare @Stage int
	Declare @PayoutDate datetime
	Declare @SponsorToID int 
	Declare @SponsorToUserID varchar(20)
	
	Select 
		@CustomerID=CustomerID,
		@PayoutID=PayoutID,
		@CreditAmount=NetAmount,
		@UpdateBy=UpdateBy,
		@UpdateDate=UpdateDate, 
		@Stage=AtStage, 
		@PayoutDate=PayoutDate,
		@SponsorToID=SponsorToID
	from 
		inserted
	
	Select @SponsorToUserID=UserID from Customers Where CustomerID=@SponsorToID
	
	SET @Narration='Amount credited at level '+Convert(varchar(11),(@Stage)) +' by '+@SponsorToUserID +' Ref:' + Convert(varchar(11),(@PayoutID))
	 
	Insert Into Accounts(CustomerID,Narration,CreditAmount,UpdateBy,UpdateDate)
	values(@CustomerID,@Narration,@CreditAmount,@UpdateBy,@UpdateDate)
end
