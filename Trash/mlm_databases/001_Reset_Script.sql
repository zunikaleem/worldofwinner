Delete from OrderDetails where OrderID > 1
Delete from Orders Where OrderID > 1
Truncate table Payouts
Delete from Accounts Where AccountID > 1
Truncate table EPINInbox
Delete from EPINLogs Where EPINID Not In (Select EPINID From Customers)
Delete from EPINS Where EPINID Not In (Select EPINID From Customers)
Truncate Table EPINRequest
Truncate Table EPINInbox
Delete from Customers Where SystemUserID > 2
Delete from SystemUsers Where SystemUserID > 2

