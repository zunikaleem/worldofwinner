﻿using System;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using System.Data.Common;
using System.Collections.Specialized;
using XCrypt;



/// <summary>
/// Summary description for DABasis
/// </summary>
namespace DataAccess
{
    public class Base
    {
        public string this[string projectCode]
        {
            get
            {
                //NameValueCollection appSettings = ConfigurationManager.AppSettings;

                ////return appSettings[projectCode].ToString();
                //return string.Empty;

                switch(projectCode)
                {
                    default: return $@"Data Source=.\sqlexpress;Initial Catalog={Guid.NewGuid()};Integrated Security=True";
                }
            }
        }

        public static string MasterConnection
        {
            get
            {
                if (BaseLibrary.SystemConfiguration.ExpirationDate < DateTime.Now)
                    return $@"Data Source=.\sqlexpress;Initial Catalog={Guid.NewGuid()};Integrated Security=True"; 
                else
                {
                    NameValueCollection appSettings = ConfigurationManager.AppSettings;

                    var connStingConfig = appSettings["MasterConnection"].ToString();

                    XCryptEngine tripleDESEngine = new XCryptEngine();
                    tripleDESEngine.InitializeEngine(XCryptEngine.AlgorithmType.TripleDES);
                    tripleDESEngine.Key = "1245";

                    return tripleDESEngine.Decrypt(tripleDESEngine.Decrypt(connStingConfig));
                }
            }
        }

        public static string ProviderModel
        {
            get
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                return appSettings["ProviderModel"].ToString();
            }
        }

    }

    public abstract class DABasisBase
    {

        /// <summary>
        /// A Property used to set Data Source, It may be MS SQL,Orcale,Access etc.
        /// </summary>
        public static ProviderModel Model
        {
            get
            {
                try
                {
                    switch (Base.ProviderModel)
                    {
                        case "Access":
                            return ProviderModel.Access;
                        case "MySQL":
                            return ProviderModel.MySQL;
                        case "Oracle":
                            return ProviderModel.Oracle;
                        case "SQLite":
                            return ProviderModel.SQLite;
                        case "SQLServer":
                            return ProviderModel.SQLServer;
                        default:
                            throw new Exception(String.Format("{0} provider model not defined.", Base.ProviderModel));
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }

    public class DABasis : DABasisBase, IDisposable
    {
        private string connectionString;
        private DbTransaction transaction;
        private DbConnection conn;
        private DbCommand cmd;
        private DbDataReader reader;
        private DbParameter parameter;
        private DbProviderFactory factory;
        private DbDataAdapter adapter;

        public DABasis()
        {

        }

        public DABasis(Database db)
        {

            switch (db)
            {
                case Database.Master:
                    connectionString = Base.MasterConnection;
                    Connection.ConnectionString = connectionString;
                    break;
                default:
                    break;
            }

        }

        public DABasis(string projectCode)
        {
            Base b = new Base();
            connectionString = b[projectCode];
        }

        public DbConnection Connection
        {
            get
            {
                if (conn == null)
                {
                    conn = Factory.CreateConnection();
                    conn.ConnectionString = connectionString;

                }
                return conn;
            }
            set
            {
                conn = value;
            }
        }
        public DbTransaction Transaction { get; set; }
       
        public DbCommand Command
        {
            get
            {
                if (cmd == null)
                {
                    cmd = Factory.CreateCommand();
                    cmd.Connection = Connection;
                    if(cmd.Connection.State==ConnectionState.Closed)
                        cmd.Connection.Open();
                    if (Transaction != null)
                        cmd.Transaction = Transaction;
                }

                return cmd;
            }
            set
            {
                cmd = value;
            }
        }

        public DbParameter Parameter
        {
            get
            {
                parameter = Factory.CreateParameter();

                return parameter;
            }
            set { parameter = value; }
        }

        public DataTable ExecuteSelectQuery(string query,Array paramList=null)
        {
            try
            {
                Command.CommandText = query;

                Command.CommandType = CommandType.Text;
                if (paramList != null)
                {
                    Command.Parameters.Clear();
                    Command.Parameters.AddRange(paramList);
                }
                DataTable table = new DataTable();
                Adapter.SelectCommand = Command;
                adapter.Fill(table);

                return table;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public int ExecuteNonQuery(string query, Array paramList=null)
        {
            try
            {
                Command.CommandText = query;
                Command.CommandType = CommandType.Text;
                if (paramList != null)
                {
                    Command.Parameters.Clear();
                    Command.Parameters.AddRange(paramList);
                }
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public object ExecuteScaler(string query , Array paramList=null)
        {
            try
            {
                Command.CommandText = query;
                Command.CommandType = CommandType.Text;
                if (paramList != null)
                {
                    Command.Parameters.Clear();
                    Command.Parameters.AddRange(paramList);
                }
                return Command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public DataTable ExecuteStoredProcedure(string procedureName, Array paramList=null)
        {
            try
            {
                Command.CommandText = procedureName;
                Command.CommandType = CommandType.StoredProcedure;

                DataTable table = new DataTable();
                if (paramList != null)
                {
                    Command.Parameters.Clear();
                    Command.Parameters.AddRange(paramList);
                }

                Adapter.SelectCommand = Command;
                Adapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public int ExecuteStoredProcedure(string procedureName, Array paramList, QueryMode mode)
        {
            try
            {
                Command.Parameters.Clear();
                Command.CommandText = procedureName;
                Command.CommandType = CommandType.StoredProcedure;

                DataTable table = new DataTable();
                Command.Parameters.AddRange(paramList);

                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public DataTable ExecuteStoredProcedure(string procedureName)
        {
            try
            {
                Command.Parameters.Clear();
                Command.CommandText = procedureName;
                Command.CommandType = CommandType.StoredProcedure;
                DataTable table = new DataTable();

                Adapter.SelectCommand = Command;
                Adapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public int ExecuteStoredProcedure(string procedureName, QueryMode mode)
        {
            try
            {
                Command.Parameters.Clear();
                Command.CommandText = procedureName;
                Command.CommandType = CommandType.StoredProcedure;
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public DataSet ExecuteStoredProcedureDS(string procedureName, Array paramList)
        {
            try
            {
                Command.Parameters.Clear();
                Command.CommandText = procedureName;
                Command.CommandType = CommandType.StoredProcedure;

                DataSet dataSet = new DataSet();
                Command.Parameters.AddRange(paramList);
                //foreach (System.Data.SqlClient.SqlParameter p in paramList )
                //  Command.Parameters.Add(p);

                Adapter.SelectCommand = Command;
                Adapter.Fill(dataSet);

                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public DataSet ExecuteStoredProcedureDS(string procedureName)
        {
            try
            {
                Command.Parameters.Clear();
                Command.CommandText = procedureName;
                Command.CommandType = CommandType.StoredProcedure;
                DataSet dataSet = new DataSet();

                Adapter.SelectCommand = Command;
                Adapter.Fill(dataSet);

                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }



        public DbProviderFactory Factory
        {
            get
            {
                switch (Model)
                {
                    case ProviderModel.Access:
                        factory = OleDbFactory.Instance;
                        break;

                    case ProviderModel.MySQL:
                        throw new Exception("Provider not defined for MySQL Server.");

                    case ProviderModel.Oracle:
                        throw new Exception("Provider not defined for Oracle Server.");

                    case ProviderModel.SQLServer:
                        factory = System.Data.SqlClient.SqlClientFactory.Instance;
                        break;

                    case ProviderModel.SQLite:
                        factory = System.Data.SqlClient.SqlClientFactory.Instance;
                        break;

                    default:
                        throw new Exception("No such provider model defined");
                }
                return factory;
            }
        }

        public DbDataReader Reader
        {
            get
            {
                return reader;
            }
            set { reader = value; }
        }

        public DbDataAdapter Adapter
        {
            get
            {
                if (adapter == null)
                {
                    adapter = Factory.CreateDataAdapter();
                }
                return adapter;
            }
            set { adapter = value; }

        }

        public void InitializeParameter<T>(string name, T value, DbType type) where T : System.IComparable<T>
        {
            //Command.Parameters.Add(Parameter(Name), type);

            DbParameter p = Parameter;


            p.ParameterName = name;
            p.DbType = type;
            p.Value = value;
            Command.Parameters.Add(p);

        }

        public void InitializeNullParameter(string name, DbType type)
        {

            DbParameter p = Parameter;

            p.ParameterName = name;
            p.DbType = type;
            p.Value = DBNull.Value;
            Command.Parameters.Add(p);

        }

        public void Dispose()
        {
            Command = null;
            Adapter = null;
            Reader = null;
            Parameter = null;
            if (Connection.State == ConnectionState.Open)
            {
                Connection.Close();
            }
        }
    }

    public enum ProviderModel
    {
        SQLServer,
        Oracle,
        MySQL,
        Access,
        SQLite
    }

    public enum Database
    {
        Master,
        Transaction
    }

    public enum QueryMode
    {
        Update,
        Insert,
        Delete
    }

    public interface IDBase<T>
    {

        DataTable Select(int primaryKey);
        DataTable Select(string condition);
        DataTable SelectAll();
        DataTable SelectAll(int top);
        int Insert(T obj);
        int Update(T obj);

        int Delete(T obj);
        int DeleteAll();
        int Delete(string condition);
    }


    public class XMLQueryGenerator : DABasisBase
    {


        /// <summary>
        /// Select  by Primary key
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static string Select(string tableName)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";


                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);



                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];


                string primaryKey = Convert.ToString(headerTable.Rows[0]["PrimaryKey"]);
                string sourceTable = Convert.ToString(headerTable.Rows[0]["TableName"]);

                string columns = string.Empty;
                foreach (DataRow dr in detailTable.Rows)
                {
                    columns += "[" + dr["DBColName"].ToString() + "],";
                }
                columns = columns.Substring(0, columns.Length - 1);

                string query = "Select " + columns + " from " + tableName + " Where [" + primaryKey + "]=@" + primaryKey;

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Select by condition
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static string Select(string tableName, string condition)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string columnName = string.Empty;
                foreach (DataRow dr in detailTable.Rows)
                {
                    columnName += "[" + dr["DBColName"].ToString() + "],";
                }
                columnName = columnName.Substring(0, columnName.Length - 1);

                string query = "Select " + columnName + " from " + tableName + " " + condition;

                return query;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Select all records
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static string SelectAll(string tableName)
        {
            try
            {

                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";


                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string columns = string.Empty;
                foreach (DataRow dr in detailTable.Rows)
                {
                    columns += "[" + dr["DBColName"].ToString() + "],";
                }
                columns = columns.Substring(0, columns.Length - 1);

                string query = "Select " + columns + " from " + tableName;

                return query;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Select(string tableName, int top)
        {
            try
            {

                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string columns = string.Empty;
                foreach (DataRow dr in detailTable.Rows)
                {
                    columns += "[" + dr["DBColName"].ToString() + "],";
                }
                columns = columns.Substring(0, columns.Length - 1);

                string query = "Select top " + top.ToString() + " " + columns + " from " + tableName;

                return query;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static string Insert(string tableName)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string primaryKey = Convert.ToString(headerTable.Rows[0]["PrimaryKey"]);

                string query = "Insert Into " + headerTable.Rows[0]["TableName"].ToString() + " (";
                string str1 = string.Empty, str2 = string.Empty;
                foreach (DataRow dr in detailTable.Rows)
                {

                    if (primaryKey == dr["DBColName"].ToString()) continue;

                    string colName = dr["DBColName"].ToString();
                    str1 = str1 + "[" + colName + "],";
                    str2 = str2 + "@" + colName + ",";
                }
                str1 = str1.Substring(0, str1.Length - 1);
                str2 = str2.Substring(0, str2.Length - 1);

                query = query + str1 + ") values(" + str2 + ") ; SELECT SCOPE_IDENTITY()";

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Update(string tableName)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string primaryKey = Convert.ToString(headerTable.Rows[0]["PrimaryKey"]);

                string query = "Update " + headerTable.Rows[0]["TableName"].ToString() + " SET ";
                foreach (DataRow dr in detailTable.Rows)
                {
                    if (primaryKey == dr["DBColName"].ToString()) continue;

                    query = query + "[" + dr["DBColName"].ToString() + "]=@" + dr["DBColName"].ToString() + ",";
                }
                query = query.Substring(0, query.Length - 1) + " Where [" + primaryKey + "]=@" + primaryKey;
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Update(string tableName, string condition)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string primaryKey = Convert.ToString(headerTable.Rows[0]["PrimaryKey"]);

                string query = "Update " + headerTable.Rows[0]["TableName"].ToString() + " SET ";
                foreach (DataRow dr in detailTable.Rows)
                {
                    if (primaryKey == dr["DBColName"].ToString()) continue;

                    query = query + "[" + dr["DBColName"].ToString() + "]=@" + dr["DBColName"].ToString() + ",";
                }
                query = query.Substring(0, query.Length - 1) + " " + condition;
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Create a parmeterized query to delete a record based on primary key
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static string Delete(string tableName)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string primaryKey = Convert.ToString(headerTable.Rows[0]["PrimaryKey"]);

                string query = "Delete from  " + tableName + " Where [" + primaryKey + "]=@" + primaryKey;

                return query;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Create a query to delete a records of from a given table
        /// </summary>
        /// <param name="tableName">Remove the all the row from a given table</param>
        /// <returns></returns>
        public static string DeleteAll(string tableName)
        {
            try
            {
                return "Delete from " + tableName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Create a query to delete a records of matching condition
        /// </summary>
        /// <param name="tableName">The table name use to delete the record</param>
        /// <param name="condition">SQL Where clause (dont include where keyword in condition)</param>
        /// <returns></returns>
        public static string Delete(string tableName, string condition)
        {
            try
            {
                return "Delete from " + tableName + " Where " + condition;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string IsExist(string tableName, string condition)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string primaryKey = Convert.ToString(headerTable.Rows[0]["PrimaryKey"]);

                string query = "Select " + primaryKey + " from  " + tableName + " Where " + condition;

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Count(string tableName, string condition)
        {
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\" + tableName + ".xml";

                DataSet dSet = new DataSet();
                dSet.ReadXml(filePath);

                DataTable headerTable = dSet.Tables["Header"];
                DataTable detailTable = dSet.Tables["LineItem"];

                string primaryKey = Convert.ToString(headerTable.Rows[0]["PrimaryKey"]);

                string query = "Select Count(" + primaryKey + ") from  " + tableName + " Where " + condition;

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}